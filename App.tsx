/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useEffect} from 'react';
import {Platform} from 'react-native';
import {StatusBar, useColorScheme} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
// @ts-ignore
import {I18nLocalize} from 'react-native-i18n-localize';
import en from './src/translations/en';
import th from './src/translations/th';
I18nLocalize.initialLanguage({en, th});
import Navigation from './src/navigations';
import SplashScreen from 'react-native-splash-screen';
import {RecoilRoot} from 'recoil';
import {SafeAreaView} from 'react-native-safe-area-context';
import {MenuProvider as PopupMenuProvider} from 'react-native-popup-menu';
import {View} from 'native-base';
import {
  Provider as PaperProvider,
  DarkTheme as PaperDarkTheme,
} from 'react-native-paper';
import {NativeBaseProvider} from 'native-base';

export default function App() {
  const colorScheme = useColorScheme();
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  return (
    <SafeAreaProvider>
      {/* <SafeAreaProvider
      style={{
        paddingTop: Platform.OS === 'android' ? 25 : 0,
        backgroundColor: '#060c30',
      }}> */}
      {/* <SafeAreaView style={{flex: 1}}> */}
      <NativeBaseProvider>
        <PaperProvider theme={PaperDarkTheme}>
          <PopupMenuProvider>
            <RecoilRoot>
              <View style={{flex: 1}}>
                <Navigation colorScheme={colorScheme} />
              </View>
            </RecoilRoot>
          </PopupMenuProvider>
        </PaperProvider>
      </NativeBaseProvider>
      {/* </SafeAreaView> */}
    </SafeAreaProvider>
  );
}
