/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */

import {NavigatorScreenParams} from '@react-navigation/core';

export type RootStackParamList = {
  Index: undefined;
  Login: NavigatorScreenParams<AuthParamList>;
  NotFound: undefined;
  Hamberger: NavigatorScreenParams<HomeContentParamList>;
  PopGalaxyAll: NavigatorScreenParams<HomeContentParamList>;
  HashTagAll: NavigatorScreenParams<HomeContentParamList>;
  ContentAll: NavigatorScreenParams<FeedContentParamList>;
  PopGalaxy: NavigatorScreenParams<HomeContentParamList>;
  WalletStack: NavigatorScreenParams<WalletParamList>;
  ConfigLive: NavigatorScreenParams<LiveStreamParamList>;
  SearchResult: undefined;
  ChatStack: NavigatorScreenParams<ChatScreenParamList>;
};
export type BottomTabParamList = {
  Home: undefined; // use Name here for name in BottomTabNavigator
  Profile: undefined;
  CreatePost: undefined;
  Chat: undefined;
  Search: undefined;
};
export type FullModalScreenParamList = {
  Root: undefined;
};
export type ChatScreenParamList = {
  ChatRoom: undefined;
};
export type SearchcreenParamList = {
  SearchScreen: undefined;
};
export type ProfilesScreenParamList = {
  Profile: undefined;
  UserProfiles: undefined;
};
export type HambergerScreenParamList = {
  HambergerScreen: undefined;
  PersonalInfo: undefined;
  VeriflyOTP: undefined;
  SuccessVerifly: undefined;
  ChangeNameAndBirth: undefined;
};
export type HomeContentParamList = {
  HomeScreen: undefined;
  PopGalaxyAllScreen: undefined;
  HashTagAllScreen: undefined;
};
export type FeedContentParamList = {
  ContentAllScreen: undefined;
  ImageMore: undefined;
  VideoMore: undefined;
};
export type LiveStreamParamList = {
  ConfigLiveScreen: undefined;
  LiveMainScreen: undefined;
};
export type AuthParamList = {
  LoginScreen: undefined;
  Agreement: undefined;
  Register: undefined;
  RegisterOTP: undefined;
  CreatePassword: undefined;
  InterestContent: undefined;
  PopCreater: undefined;
};
export type ProductParamList = {
  CreateProduct: undefined;
  ProductScreen: undefined;
  AddProductDetails: undefined;
};
export type WalletParamList = {
  Wallet: undefined;
  PointFill: undefined;
  PaymentMethod: undefined;
  SummaryScreen: undefined;
  EarningAndHistory: undefined;
  Kyc: undefined;
  KycPolicy: undefined;
  VerifyIdentity: undefined;
  BankAccount: undefined;
  Approval: undefined;
};
export type PostsParamList = {
  CreatePostPhoto: undefined;
  ChoosePicture: undefined;
  SeePicturesToPost: undefined;
  PostReadyOnFeed: undefined;
  TagFriend: undefined;
  ChooseVideo: undefined;
  CameraVideo: undefined;
  CameraPicture: undefined;
};
