import React, {Component} from 'react';
// import {AsyncStorage} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import join from 'url-join';

axios.interceptors.request.use(async config => {
  const jwtToken = await AsyncStorage.getItem('token');
  if (jwtToken) {
    config.headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${jwtToken}`,
    };
    config.url = join('http://{url}:8000/', config.url);
  } else {
    config.url = join('http://{url}:8000/openapi', config.url);
  }
  // console.log(config.url);

  // config.url = 'http://localhost:4000/openapi' + config.url;
  return config;
});
export const httpClient = axios;
