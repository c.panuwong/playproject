// import AsyncStorage from '@react-native-community/async-storage';
import {AsyncStorage} from 'react-native';

export const authHeader = async () => {
  const data = await AsyncStorage.getItem('jwt');
  if (data) {
    return {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${data}`,
    };
  }
  return {};
};
