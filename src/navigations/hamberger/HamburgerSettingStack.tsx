import * as React from 'react';
import {HambergerScreenParamList} from '../../../types';

import HeaderBackButton from '../../components/HeaderBackButton';
import {Image, TouchableOpacity} from 'react-native';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';

import HamburgerSettingScreen from '../../screens/hamberger/setting/HamburgerSettingScreen';
import AboutScreen from '../../screens/hamberger/setting/AboutScreen';
import ChangePasswordScreen from '../../screens/hamberger/setting/ChangePasswordScreen';
import LanguageScreen from '../../screens/hamberger/setting/LanguageScreen';
import NotificationScreen from '../../screens/hamberger/setting/NotificationScreen';
import PrivacyScreen from '../../screens/hamberger/setting/PrivacyScreen';
import SuccessChangePasswordScreen from '../../screens/hamberger/setting/SuccessChangePasswordScreen';
import TermsOfServiceScreen from '../../screens/hamberger/setting/TermsOfServiceScreen';
import CopyrightPolicyScreen from '../../screens/hamberger/setting/CopyrightPolicyScreen';

const HambergerStack = createStackNavigator<HambergerScreenParamList>();

export default function HamburgerSettingStack({navigation}: {navigation: any}) {
  return (
    <HambergerStack.Navigator
      screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <HambergerStack.Screen
        name="HamburgerSetting"
        component={HamburgerSettingScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="About"
        component={AboutScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="ChangePassword"
        component={ChangePasswordScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="SuccessChangePassword"
        component={SuccessChangePasswordScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="Language"
        component={LanguageScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="Notification"
        component={NotificationScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="Privacy"
        component={PrivacyScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="TermsOfService"
        component={TermsOfServiceScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="CopyrightPolicy"
        component={CopyrightPolicyScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
    </HambergerStack.Navigator>
  );
}
