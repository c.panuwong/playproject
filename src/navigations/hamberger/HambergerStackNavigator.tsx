import * as React from 'react';
import {HambergerScreenParamList} from '../../../types';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import Hamberger from '../../screens/hamberger/Hamberger';
import PersonalInfo from '../../screens/hamberger/PersonalInfo';
import VeriflyOTP from '../../screens/hamberger/VeriflyOTP';
import SuccessVerifly from '../../screens/hamberger/SuccessVerifly';
import ChangeNameAndBirth from '../../screens/hamberger/ChangeNameAndBirth';
import HeaderBackButton from '../../components/HeaderBackButton';
import {Image, TouchableOpacity} from 'react-native';

import BlockedScreen from '../../screens/hamberger/BlockedScreen';
import Help_FAQScreen from '../../screens/hamberger/Help_FAQScreen';
import KYCScreen from '../../screens/hamberger/KYCScreen';
import OpenShopScreen from '../../screens/hamberger/OpenShopScreen';
import PromoteScreen from '../../screens/hamberger/PromoteScreen';
import WalletScreen from '../../screens/wallet/WalletScreen';
import SavedScreen from '../../screens/hamberger/saved/SavedScreen';
import ContentScreen from '../../screens/hamberger/saved/ContentScreen';
import WalletStack from '../wallet/WalletStack';

import HamburgerSettingStack from './HamburgerSettingStack';

const HambergerStack = createStackNavigator<HambergerScreenParamList>();

export default function HambergerStackNavigator({
  navigation,
}: {
  navigation: any;
}) {
  return (
    <HambergerStack.Navigator
      screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <HambergerStack.Screen
        name="HambergerScreen"
        component={Hamberger}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
          headerRight: () => (
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('HamburgerSettings', {
                  screen: 'HamburgerSetting',
                })
              }
              navigation={navigation}>
              <Image
                source={require('../../assets/images/setting_icon2.png')}
                style={{width: 25, height: 25, marginRight: '5%'}}
                resizeMode="contain"
              />
            </TouchableOpacity>
          ),
        }}
      />
      <HambergerStack.Screen
        name="PersonalInfo"
        component={PersonalInfo}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.navigate('HambergerScreen')}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="VeriflyOTP"
        component={VeriflyOTP}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.navigate('PersonalInfo')}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="ChangeNameAndBirth"
        component={ChangeNameAndBirth}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.navigate('PersonalInfo')}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="SuccessVerifly"
        component={SuccessVerifly}
        options={{
          headerShown: false,
        }}
      />

      <HambergerStack.Screen
        name="Blocked"
        component={BlockedScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="Help_FAQ"
        component={Help_FAQScreen}
        options={{
          headerShown: false,

          // headerTransparent: true,
          // title: '',
          // headerLeft: () => (
          //   <HeaderBackButton
          //     navigation={navigation}
          //     onPress={() => navigation.goBack()}
          //   />
          // ),
        }}
      />
      <HambergerStack.Screen
        name="KYC"
        component={KYCScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="OpenShop"
        component={OpenShopScreen}
        options={{
          // headerShown: false,
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="Promote"
        component={PromoteScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="Wallet"
        component={WalletStack}
        options={{
          // headerTransparent: true,
          // title: '',
          headerShown: false,
          // headerLeft: () => (
          //   <HeaderBackButton
          //     navigation={navigation}
          //     onPress={() => navigation.goBack()}
          //   />
          // ),
        }}
      />

      <HambergerStack.Screen
        name="Saved"
        component={SavedScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="HamburgerSettings"
        component={HamburgerSettingStack}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <HambergerStack.Screen
        name="Content"
        component={ContentScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
    </HambergerStack.Navigator>
  );
}
