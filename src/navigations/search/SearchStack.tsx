import * as React from 'react';
import {SearchcreenParamList} from '../../../types';
import SearchScreen from '../../screens/search/index';
import {createStackNavigator} from '@react-navigation/stack';
const Search = createStackNavigator<SearchcreenParamList>();

function SearchStack() {
  return (
    <Search.Navigator>
      <Search.Screen
        name="SearchScreen"
        component={SearchScreen}
        options={() => {
          return {
            headerShown: false,
            // headerRight: () => {
            //   <TouchableOpacity>
            //     <Text style={{color: '#000', fontSize: 30}}>Hiiiiiii</Text>
            //     {/* <Fontawesome icon={faStream} size={30} color="#000" /> */}
            //   </TouchableOpacity>;
            // },
          };
        }}
      />
    </Search.Navigator>
  );
}
export default SearchStack;
