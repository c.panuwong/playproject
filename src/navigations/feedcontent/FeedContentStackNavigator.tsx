import ContentAllScreen from '../../screens/feedcontent/ContentAll';
import ImageMore from '../../screens/feedcontent/ImageMore';
import VideoMore from '../../screens/feedcontent/videoscreen/VideoMore';
import * as React from 'react';
import {Text} from 'react-native';
import {FeedContentParamList} from '../../../types';
import {createStackNavigator,CardStyleInterpolators} from '@react-navigation/stack';
import HeaderBackButton from '../../components/HeaderBackButton';

const FeedContent = createStackNavigator<FeedContentParamList>();

function FeedContentNavigator({navigation}: {navigation: any}) {
  return (
    <FeedContent.Navigator screenOptions={{
      cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
    }}>
      <FeedContent.Screen
        name="ContentAllScreen"
        component={ContentAllScreen}
        options={{
          headerTitle: () => (
            <>
              <Text style={{color: '#fff', fontSize: 20, textAlign: 'center'}}>
                Galaxy Feed
              </Text>
            </>
          ),
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTransparent: true,
        }}
      />
      <FeedContent.Screen
        name="ImageMore"
        component={ImageMore}
        options={{
          headerTitle: () => (
            <>
              <Text style={{color: '#fff', fontSize: 20, textAlign: 'center'}}>
                Post More
              </Text>
            </>
          ),
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTransparent: true,
        }}
      />
      <FeedContent.Screen
        name="VideoMore"
        component={VideoMore}
        options={{
          headerShown: false,
        }}
      />
    </FeedContent.Navigator>
  );
}
export default FeedContentNavigator;
