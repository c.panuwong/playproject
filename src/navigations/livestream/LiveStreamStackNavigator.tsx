import ConfigLiveScreen from '../../screens/livestream/ConfigLive';
import LiveMainScreen from '../../screens/livestream/LiveMain';

import * as React from 'react';
import {Text} from 'react-native';
import {LiveStreamParamList} from '../../../types';
import {createStackNavigator} from '@react-navigation/stack';
import HeaderBackButton from '../../components/HeaderBackButton';
import CreatePostScreen from '../../screens/createpost/CreatePostScreen';

const LiveStream = createStackNavigator<LiveStreamParamList>();

function LiveStreamNavigator({navigation}: {navigation: any}) {
  return (
    <LiveStream.Navigator>
      <LiveStream.Screen
        name="ConfigLiveScreen"
        component={ConfigLiveScreen}
        options={{
          headerTitle: () => (
            <>
              <Text style={{color: '#fff', fontSize: 20, textAlign: 'center'}}>
                Galaxy Feed
              </Text>
            </>
          ),
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTransparent: true,
        }}
      />
      <LiveStream.Screen
        name="LiveMainScreen"
        component={LiveMainScreen}
        options={{
          headerTitle: () => (
            <>
              <Text style={{color: '#fff', fontSize: 20, textAlign: 'center'}}>
                HashTag
              </Text>
            </>
          ),
          headerRight: () => <></>,
          headerTransparent: true,
        }}
      />
      <LiveStream.Screen
        name="CreatePostPhoto"
        component={CreatePostScreen}
        options={{
          headerShown: false,
        }}
      />
    </LiveStream.Navigator>
  );
}
export default LiveStreamNavigator;
