import React from 'react';
import {View, Text} from 'react-native';
import {ProductParamList} from '../../../types';
import {createStackNavigator} from '@react-navigation/stack';
import CreateProductScreen from '../../screens/product/CreateProductScreen';
import AddProductDetailsScreen from '../../screens/product/AddProductDetailsScreen';
import ProductScreen from '../../screens/product/ProductScreen';
import BackButton from '../../components/product/BackButton';

const ProductsStack = createStackNavigator<ProductParamList>();

export default function ProductStack({navigation}: {navigation: any}) {
  return (
    <ProductsStack.Navigator>
      <ProductsStack.Screen
        name="CreateProduct"
        component={CreateProductScreen}
        options={{
          headerTitle: () => <></>,
        }}
      />
      <ProductsStack.Screen
        name="AddProductDetails"
        component={AddProductDetailsScreen}
        options={{
          // headerTransparent: true,
          headerShown: false,
          // headerTitle: () => <></>,
          // headerLeft: () => (
          //   <>
          //     <BackButton
          //       navigation={navigation}
          //       onPress={() => navigation.navigate('CreateProduct')}
          //     />
          //   </>
          // ),
        }}
      />
      <ProductsStack.Screen
        name="ProductScreen"
        component={ProductScreen}
        options={{
          // headerTransparent: true,
          headerShown: false,
          // headerTitle: () => <></>,
          // headerLeft: () => (
          //   <>
          //     <BackButton
          //       navigation={navigation}
          //       onPress={() => navigation.navigate('AddProductDetails')}
          //     />
          //   </>
          // ),
        }}
      />
    </ProductsStack.Navigator>
  );
}
