import * as React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {ProfilesScreenParamList} from '../../../types';
import UserProfiles from '../../screens/profiles/UserProfiles';
import {createStackNavigator} from '@react-navigation/stack';
import {faStream} from '@fortawesome/pro-regular-svg-icons';
import Fontawesome from '@fortawesome/fontawesome-pro';
const ProfilesStack = createStackNavigator<ProfilesScreenParamList>();

function ProfileStack() {
  return (
    <ProfilesStack.Navigator>
      <ProfilesStack.Screen
        name="UserProfiles"
        component={UserProfiles}
        options={() => {
          return {
            headerTransparent: true,
            headerTitle: '',
            // headerRight: () => {
            //   <TouchableOpacity>
            //     <Text style={{color: '#000', fontSize: 30}}>Hiiiiiii</Text>
            //     {/* <Fontawesome icon={faStream} size={30} color="#000" /> */}
            //   </TouchableOpacity>;
            // },
          };
        }}
      />
    </ProfilesStack.Navigator>
  );
}
export default ProfileStack;
