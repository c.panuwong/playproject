import * as React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {WalletParamList} from '../../../types';
import WalletScreen from '../../screens/wallet/WalletScreen';
import PointFill from '../../screens/wallet/PointFill';
import PaymentMethod from '../../screens/wallet/PaymentMethod';
import SummaryScreen from '../../screens/wallet/SummaryScreen';
import EarningAndHistory from '../../screens/wallet/EarningAndHistory';
import HeaderBackButton from '../../components/HeaderBackButton';
import {DBHText} from '../../components/StyledText';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';

import KycScreen from '../../screens/wallet/KycScreen';
import KycPolicyScreen from '../../screens/wallet/KycPolicyScreen';
import UserInfoScreen from '../../screens/wallet/UserInfoScreen';
import VerifyIdentityScreen from '../../screens/wallet/VerifyIdentityScreen';
import BankAccountScreen from '../../screens/wallet/BankAccountScreen';
import ApprovalScreen from '../../screens/wallet/ApprovalScreen';

const WalletStackNavigator = createStackNavigator<WalletParamList>();

function WalletStack({navigation}: {navigation: any}) {
  return (
    <WalletStackNavigator.Navigator
      screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <WalletStackNavigator.Screen
        name="Wallet"
        component={WalletScreen}
        options={() => {
          return {
            headerShown: false,
          };
        }}
      />
      <WalletStackNavigator.Screen
        name="PointFill"
        component={PointFill}
        options={() => {
          return {
            headerShown: false,
          };
        }}
      />
      <WalletStackNavigator.Screen
        name="PaymentMethod"
        component={PaymentMethod}
        options={() => {
          return {
            headerShown: false,
          };
        }}
      />
      <WalletStackNavigator.Screen
        name="SummaryScreen"
        component={SummaryScreen}
        options={() => {
          return {
            headerShown: false,
          };
        }}
      />
      <WalletStackNavigator.Screen
        name="EarningAndHistory"
        component={EarningAndHistory}
        options={() => {
          return {
            headerShown: false,
          };
        }}
      />
      <WalletStackNavigator.Screen
        name="Kyc"
        component={KycScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
          headerRight: () => (
            <TouchableOpacity
              onPress={() => navigation.navigate('KycPolicy')}
              navigation={navigation}>
              <DBHText size={28} style={{paddingRight: '5%'}}>
                {' '}
                SKIP{' '}
              </DBHText>
            </TouchableOpacity>
          ),
        }}
      />
      <WalletStackNavigator.Screen
        name="KycPolicy"
        component={KycPolicyScreen}
        options={{
          headerShown: false,

          // headerTransparent: true,
          // title: '',
          // headerLeft: () => (
          //   <HeaderBackButton
          //     navigation={navigation}
          //     onPress={() => navigation.goBack()}
          //   />
          // ),
        }}
      />
      <WalletStackNavigator.Screen
        name="UserInfo"
        component={UserInfoScreen}
        options={{
          headerShown: false,

          // headerTransparent: true,
          // title: '',
          // headerLeft: () => (
          //   <HeaderBackButton
          //     navigation={navigation}
          //     onPress={() => navigation.goBack()}
          //   />
          // ),
        }}
      />
      <WalletStackNavigator.Screen
        name="VerifyIdentity"
        component={VerifyIdentityScreen}
        options={{
          // headerShown: false,

          headerTransparent: true,
          headerTitle: () => (
            <>
              <DBHText size={34} color="white" style={{marginLeft: '34%'}}>
                Verify Identity
              </DBHText>
            </>
          ),
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <WalletStackNavigator.Screen
        name="BankAccount"
        component={BankAccountScreen}
        options={{
          // headerShown: false,

          headerTransparent: true,
          headerTitle: () => (
            <>
              <DBHText size={34} color="white" style={{marginLeft: '34%'}}>
                Bank Account
              </DBHText>
            </>
          ),
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <WalletStackNavigator.Screen
        name="Approval"
        component={ApprovalScreen}
        options={{
          headerShown: false,
        }}
      />
    </WalletStackNavigator.Navigator>
  );
}
export default WalletStack;
