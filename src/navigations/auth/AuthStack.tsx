import {createStackNavigator} from '@react-navigation/stack';
import * as React from 'react';
import LoginScreen from '../../screens/auth/LoginScreen';
import AgreementScreen from '../../screens/auth/AgreementScreen';
import Register from '../../screens/auth/Register';
import RegisterOTP from '../../screens/auth/RegisterOTP';
import CreatePassword from '../../screens/auth/CreatePassword';
import InterestContent from '../../screens/auth/InterestContent';
import PopCreater from '../../screens/auth/PopCreater';
import {AuthParamList} from '../../../types';
import {DBHText} from '../../components/StyledText';
const Stack = createStackNavigator<AuthParamList>();

export default function AuthStack({navigation}: {navigation: any}) {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Agreement"
        component={AgreementScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="RegisterOTP"
        component={RegisterOTP}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="CreatePassword"
        component={CreatePassword}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="PopCreater"
        component={PopCreater}
        options={{
          headerLeft: () => (
            <>
              <DBHText></DBHText>
            </>
          ),
          headerTitle: () => (
            <>
              <DBHText></DBHText>
            </>
          ),
          headerRight: () => (
            <>
              <DBHText
                size={26}
                style={{marginRight: 10}}
                onPress={() => navigation.navigate('Index')}>
                SKIP
              </DBHText>
            </>
          ),
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="InterestContent"
        component={InterestContent}
        options={{
          headerLeft: () => null,
          headerTitle: () => (
            <>
              <DBHText></DBHText>
            </>
          ),
          headerRight: () => (
            <>
              <DBHText
                size={26}
                style={{marginRight: 10}}
                onPress={() => navigation.navigate('Index')}>
                SKIP
              </DBHText>
            </>
          ),
          headerTransparent: true,
        }}
      />
    </Stack.Navigator>
  );
}
