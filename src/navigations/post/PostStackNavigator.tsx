import * as React from 'react';
import {Text, TouchableOpacity} from 'react-native';

import {createStackNavigator} from '@react-navigation/stack';
import CreatePostScreen from '../../screens/createpost/CreatePostScreen';
import ChoosePictureScreen from '../../screens/createpost/ChoosePictureScreen';
import SeePicturesToPostScreen from '../../screens/createpost/SeePicturesToPostScreen';
import PostReadyOnFeedScreen from '../../screens/createpost/PostReadyOnFeedScreen';
import TagFriendScreen from '../../screens/createpost/TagFriendScreen';
import ChooseVideoScreen from '../../screens/createpost/ChooseVideoScreen';
// import CameraVideoScreen from '../../screens/createpost/CameraVideoScreen';
// import CameraPictureScreen from '../../screens/createpost/CameraPictureScreen';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCheck, faTimes} from '@fortawesome/pro-light-svg-icons';
import {faEllipsisV} from '@fortawesome/pro-regular-svg-icons';
import HeaderBackButton from '../../components/HeaderBackButton';

const Posts = createStackNavigator<PostsParamList>();

export default function PostStackNavigator({navigation}: {navigation: any}) {
  return (
    <Posts.Navigator>
      <Posts.Screen
        name="CreatePostPhoto"
        component={CreatePostScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
          headerRight: () => (
            <TouchableOpacity style={{paddingRight: '5%'}}>
              <FontAwesomeIcon icon={faEllipsisV} size={25} color="#ffff" />
            </TouchableOpacity>
          ),

          // headerShown: false,
        }}
      />
      <Posts.Screen
        name="ChoosePicture"
        component={ChoosePictureScreen}
        options={{
          headerTitle: '',
          headerLeft: () => (
            <TouchableOpacity
              style={{paddingLeft: '5%'}}
              onPress={() => navigation.navigate('CreatePostPhoto')}>
              <FontAwesomeIcon icon={faTimes} color="white" size={32} />
            </TouchableOpacity>
          ),
          headerRight: () => (
            <TouchableOpacity
              style={{paddingRight: '5%'}}
              onPress={() =>
                navigation.navigate('CreatePostPhoto', {
                  route: 'Choose',
                  navigation: {navigation},
                })
              }>
              <FontAwesomeIcon icon={faCheck} color="white" size={32} />
            </TouchableOpacity>
          ),
          headerTransparent: true,
        }}
      />
      <Posts.Screen
        name="SeePicturesToPost"
        component={SeePicturesToPostScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
          headerRight: () => (
            <TouchableOpacity style={{paddingRight: '5%'}}>
              <FontAwesomeIcon icon={faEllipsisV} size={25} color="#ffff" />
            </TouchableOpacity>
          ),

          // headerShown: false,
        }}
      />
      <Posts.Screen
        name="PostReadyOnFeed"
        component={PostReadyOnFeedScreen}
        options={{
          headerShown: false,
        }}
      />
      <Posts.Screen
        name="TagFriend"
        component={TagFriendScreen}
        options={{
          headerTransparent: true,
          title: '',
          headerLeft: () => (
            <Text
              style={{
                fontFamily: 'Prompt',
                fontSize: 21,
                fontWeight: '300',
                marginLeft: '10%',
                color: 'white',
              }}>
              Tag Friend
            </Text>
          ),
          headerRight: () => (
            <TouchableOpacity
              style={{paddingRight: '5%'}}
              onPress={() => navigation.navigate('CreatePostPhoto')}>
              <FontAwesomeIcon icon={faTimes} color="white" size={32} />
            </TouchableOpacity>
          ),
        }}
      />
      <Posts.Screen
        name="ChooseVideo"
        component={ChooseVideoScreen}
        options={{
          headerTitle: '',
          headerLeft: () => (
            <TouchableOpacity
              style={{paddingLeft: '5%'}}
              onPress={() => navigation.navigate('CreatePostPhoto')}>
              <FontAwesomeIcon icon={faTimes} color="white" size={32} />
            </TouchableOpacity>
          ),
          headerRight: () => (
            <TouchableOpacity
              style={{paddingRight: '5%'}}
              onPress={() =>
                navigation.navigate('CreatePostPhoto', {
                  route: 'Choose',
                  navigation: {navigation},
                })
              }>
              <FontAwesomeIcon icon={faCheck} color="white" size={32} />
            </TouchableOpacity>
          ),
          headerTransparent: true,
        }}
      />
      {/* <Posts.Screen
        name="CameraPicture"
        component={CameraPictureScreen}
        options={{
          headerTitle: '',
          headerLeft: () => (
            <TouchableOpacity
              style={{paddingLeft: '5%'}}
              onPress={() => navigation.navigate('CreatePostPhoto')}>
              <FontAwesomeIcon icon={faTimes} color="white" size={32} />
            </TouchableOpacity>
          ),
          headerRight: () => (
            <TouchableOpacity
              style={{paddingRight: '5%'}}
              onPress={() =>
                navigation.navigate('CreatePostPhoto', {
                  route: 'Choose',
                  navigation: {navigation},
                })
              }>
              <FontAwesomeIcon icon={faCheck} color="white" size={32} />
            </TouchableOpacity>
          ),
          headerTransparent: true,
        }}
      /> */}
      {/* <Posts.Screen
        name="CameraVideo"
        component={CameraVideoScreen}
        options={{
          headerTitle: '',
          headerLeft: () => (
            <TouchableOpacity
              style={{paddingLeft: '5%'}}
              onPress={() => navigation.navigate('CreatePostPhoto')}>
              <FontAwesomeIcon icon={faTimes} color="white" size={32} />
            </TouchableOpacity>
          ),
          headerRight: () => (
            <TouchableOpacity
              style={{paddingRight: '5%'}}
              onPress={() =>
                navigation.navigate('CreatePostPhoto', {
                  route: 'Choose',
                  navigation: {navigation},
                })
              }>
              <FontAwesomeIcon icon={faCheck} color="white" size={32} />
            </TouchableOpacity>
          ),
          headerTransparent: true,
        }}
      /> */}
    </Posts.Navigator>
  );
}
