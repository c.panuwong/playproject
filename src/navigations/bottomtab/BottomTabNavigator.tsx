import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import * as React from 'react';
import {View, Image} from 'react-native';
import {BottomTabParamList, HomeContentParamList} from '../../../types';
import ChatStackNavigator from '../chat/ChatStackNavigator';
import HomeStackNavigator from '../home/HomeStackNavigator';
import WalletStack from '../wallet/WalletStack';
import ProfileStack from '../profile/ProfileStack';
import Chat from '../../screens/chat/Chat';
import SearchStack from '../search/SearchStack';
import LiveStreamStackNavigator from '../livestream/LiveStreamStackNavigator';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faStar, faPlay} from '@fortawesome/pro-solid-svg-icons';
const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function HomeTabNavigator({navigation}: {navigation: any}) {
  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#99FF00',
        style: {
          backgroundColor: '#111925',
          borderRadius: 25,
          // borderRadius: 25,
          position: 'absolute',
          bottom: 6,
          marginHorizontal: 10,
          paddingBottom: 3,
        },
        // keyboardHidesTabBar: true,
      }}
      lazy
      >
      <BottomTab.Screen
        name="Home"
        component={HomeStackNavigator}
        options={{
          tabBarIcon: ({color}) => (
            <Image
              source={require('../../assets/images/bottom_1.png')}
              style={{height: 25, width: 25}}
              resizeMode="contain"
              tintColor={color}
            />
          ),
          tabBarVisible: true,
        }}
      />
      <BottomTab.Screen
        name="Search"
        component={SearchStack}
        options={{
          tabBarIcon: ({color}) => (
            <Image
              source={require('../../assets/images/bottom_2.png')}
              style={{height: 25, width: 25}}
              resizeMode="contain"
              tintColor={color}
            />
          ),
          tabBarVisible: true,
        }}
      />
      <BottomTab.Screen
        name="ConfigLive"
        component={LiveStreamStackNavigator}
        options={{
          tabBarIcon: ({color}) => (
            <View
              style={{
                width: 50,
                height: 50,
                backgroundColor: color,
                borderRadius: 25,
                marginBottom: 8,
                borderWidth: 2,
                borderColor: 'rgba(0, 0, 0, 0)',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <FontAwesomeIcon
                icon={faStar}
                size={28}
                color="#000"
                // style={{bla}}
              />
              <FontAwesomeIcon
                icon={faPlay}
                size={8}
                color="#fff"
                style={{position: 'absolute', left: 20}}
              />
              {/* </FontAwesomeIcon> */}
            </View>
          ),
          tabBarLabel: () => {
            return null;
          },
          tabBarVisible: false,
          unmountOnBlur: true
        }}
      />
      <BottomTab.Screen
        name="Chat"
        component={Chat}
        options={{
          tabBarIcon: ({color}) => (
            <Image
              source={require('../../assets/images/bottom_4.png')}
              style={{height: 25, width: 25}}
              resizeMode="contain"
              tintColor={color}
            />
          ),
          tabBarVisible: true,
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={ProfileStack}
        options={{
          tabBarIcon: ({color}) => (
            <Image
              source={require('../../assets/images/bottom_5.png')}
              style={{height: 25, width: 25}}
              resizeMode="contain"
              tintColor={color}
            />
          ),
          tabBarVisible: true,
        }}
      />
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
// function TabBarIcon(props: {
//   name: React.ComponentProps<typeof Ionicons>["name"];
//   color: string;
// }) {
//   return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
// }

// function SimpleLineIcon(props: {
//   name: React.ComponentProps<typeof SimpleLineIcons>["name"];
//   color: string;
// }) {
//   return <SimpleLineIcons size={24} style={{ marginBottom: -3 }} {...props} />;
// }

// function MaterialCommunityIcon(props: {
//   name: React.ComponentProps<typeof MaterialCommunityIcons>["name"];
//   color: string;
// }) {
//   return (
//     <MaterialCommunityIcons size={24} style={{ marginBottom: -3 }} {...props} />
//   );
// }

// function FontAwesome5Icon(props: {
//   name: React.ComponentProps<typeof FontAwesome5>["name"];
//   color: string;
// }) {
//   return <FontAwesome5 size={24} style={{ marginBottom: -3 }} {...props} />;
// }

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab

// const ChatStack = createStackNavigator<ChatParamList>();

// function ChatStackNavigator() {
//   return (
//     <ChatStack.Navigator headerMode="none">
//       <ChatStack.Screen name="Chat" component={Chat} />
//     </ChatStack.Navigator>
//   );
// }

// const ProfileStack = createStackNavigator<ProfileParamList>();

// function ProfileStackNavigator() {
//   return (
//     <ProfileStack.Navigator>
//       <ProfileStack.Screen name="Profile" component={OwnerProfile} />
//     </ProfileStack.Navigator>
//   );
// }

// function TabBarIcon(props: {
//   name: React.ComponentProps<typeof Ionicons>['name'];
//   color: string;
// }) {
//   return <Ionicons size={30} style={{marginBottom: -3}} {...props} />;
// }
