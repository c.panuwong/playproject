import React, {useState} from 'react';
import {ChatScreenParamList} from '../../../types';
import ChatRoom from '../../screens/chat/ChatRoom';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import EllipsisMenuChat from '../../components/EllipsisMenuChat';
import { backgroundColor } from 'styled-system';
const ChatStackNavigator = createStackNavigator<ChatScreenParamList>();

function ChatStack() {
  const [visible, setVisible] = useState<boolean>(false);

  return (
    <ChatStackNavigator.Navigator
      screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerStyle: { backgroundColor: '#101531' },
        headerTitleStyle: { color: '#FFF' },
        headerTitleAlign: "left",
        headerTintColor:'#FFF'
      }}
     
      >
      <ChatStackNavigator.Screen
        name="ChatRoom"
        component={ChatRoom}
        options={({route}) => ({
          headerTitle: route.params.data.name,
          headerRight: () => (
            <EllipsisMenuChat
              visible={visible}
              close={() => setVisible(false)}
              onPress={() => setVisible(true)}
            />
          ),
        })}
      />
    </ChatStackNavigator.Navigator>
  );
}
export default ChatStack;
