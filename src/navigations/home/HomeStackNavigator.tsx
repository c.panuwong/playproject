import HomeScreen from '../../screens/home/HomeScreen';
import PopGalaxyAllScreen from '../../screens/home/PopGalaxyAll';
import HashTagAllScreen from '../../screens/home/HashTagAll';
import * as React from 'react';
import {Text} from 'react-native';
import {HomeContentParamList} from '../../../types';
import {createStackNavigator} from '@react-navigation/stack';
import HeaderBackButton from '../../components/HeaderBackButton';

const HomeStack = createStackNavigator<HomeContentParamList>();

function HomeStackNavigator({navigation}: {navigation: any}) {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{headerShown: false}}
      />
      <HomeStack.Screen
        name="PopGalaxyAllScreen"
        component={PopGalaxyAllScreen}
        options={{
          headerTitle: () => (
            <>
              <Text style={{color: '#fff', fontSize: 20, textAlign: 'center'}}>
                Pop Galaxys
              </Text>
            </>
          ),
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
          headerRight: () => <></>,
          headerTransparent: true,
        }}
      />
      <HomeStack.Screen
        name="HashTagAllScreen"
        component={HashTagAllScreen}
        options={{
          headerTitle: () => (
            <>
              <Text style={{color: '#fff', fontSize: 20, textAlign: 'center'}}>
                HashTag
              </Text>
            </>
          ),
          headerRight: () => <></>,
          headerTransparent: true,
        }}
      />
    </HomeStack.Navigator>
  );
}
export default HomeStackNavigator;
