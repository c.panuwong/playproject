/**
 * If you are not familiar with React Navigation, check out the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
  NavigationContainerRef,
} from '@react-navigation/native';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import * as React from 'react';
import {ColorSchemeName} from 'react-native';
import NotFoundScreen from '../screens/NotFoundScreen';
import BottomTabNavigator from './bottomtab/BottomTabNavigator';
import {RootStackParamList, FullModalScreenParamList} from '../../types';
import {Provider} from 'react-native-paper';
//   import { PromptText } from "../components/StyledText";
import LinearGradient from 'react-native-linear-gradient';
import AuthStack from './auth/AuthStack';
import HomeStackNavigator from './home/HomeStackNavigator';
import ChatStack from './chat/ChatStackNavigator';
import CreateProductScreen from '../screens/product/CreateProductScreen';
import ProductStack from './product/ProductStack';
import HambergerStackNavigator from './hamberger/HambergerStackNavigator';
import WalletStack from './wallet/WalletStack';
import FeedContentStackNavigator from './feedcontent/FeedContentStackNavigator';
import LiveStreamStackNavigator from './livestream/LiveStreamStackNavigator';
import SearchResult from '../screens/search/SearchResult';
import HeaderBackButton from '../components/HeaderBackButton';
import ContentScreen from '../screens/hamberger/saved/ContentScreen';
import PostStackNavigator from './post/PostStackNavigator';

const colors = ['#C78DFF', '#3366CC'];

const diagonalGradient = {
  start: {x: 0, y: 1},
  end: {x: 0.05, y: 0},
};

export default function Navigation({
  colorScheme,
}: {
  colorScheme: ColorSchemeName;
}) {
  const navigationRef = React.useRef<NavigationContainerRef<any>>(null);

  return (
    <Provider>
      <NavigationContainer
        ref={navigationRef}
        theme={colorScheme === 'light' ? DefaultTheme : DarkTheme}>
        <FullModalScreen />
      </NavigationContainer>
    </Provider>
  );
}

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>();

function RootNavigator({navigation}: {navigation: any}) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen name="Index" component={BottomTabNavigator} />
      <Stack.Screen name="Login" component={AuthStack} />
      <Stack.Screen name="Hamberger" component={HambergerStackNavigator} />
      <Stack.Screen name="PopGalaxyAll" component={HomeStackNavigator} />
      <Stack.Screen name="HashTagAll" component={HomeStackNavigator} />
      <Stack.Screen name="ContentAll" component={FeedContentStackNavigator} />
      <Stack.Screen name="PopGalaxy" component={HomeStackNavigator} />
      <Stack.Screen name="WalletStack" component={WalletStack} />
      <Stack.Screen name="ConfigLive" component={LiveStreamStackNavigator} />
      <Stack.Screen name="SearchResult" component={SearchResult} />
      <Stack.Screen name="ChatStack" component={ChatStack} />

      <Stack.Screen
        name="NotFound"
        component={NotFoundScreen}
        options={{title: 'Oops!'}}
      />
      <Stack.Screen name="Product" component={ProductStack} />
      <Stack.Screen
        name="ViewContent"
        component={ContentScreen}
        options={{
          headerTransparent: true,
          headerLeft: () => (
            <HeaderBackButton
              navigation={navigation}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
      <Stack.Screen name="Posts" component={PostStackNavigator} />
    </Stack.Navigator>
  );
}

const FullModalStack = createStackNavigator<FullModalScreenParamList>();

function FullModalScreen() {
  return (
    <FullModalStack.Navigator screenOptions={{presentation: 'modal'}}>
      <FullModalStack.Screen
        name="Root"
        component={RootNavigator}
        options={{
          headerShown: false,
        }}
      />
    </FullModalStack.Navigator>
  );
}
// const ProfileStackScreen = createStackNavigator<ProfileParamList>();

// function ProfileStackScreen() {
//   return (
//     <ProfileStackScreen.Navigator
//   )
// }
