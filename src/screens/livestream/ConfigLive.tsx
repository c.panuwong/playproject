import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  PermissionsAndroid,
  Dimensions,
  Image,
  Platform,
  SafeAreaView,
  Alert,
} from 'react-native';
import RtcEngine, {
  ChannelProfile,
  ClientRole,
  RtcLocalView,
  RtcRemoteView,
  VideoRemoteState,
} from 'react-native-agora';
import {useRecoilState, useResetRecoilState} from 'recoil';
import {Col, Row, Grid} from 'react-native-easy-grid';
// @ts-ignore
import styled from 'styled-components';
import uuid from 'react-native-uuid';
import {channel} from '../../recoil/auth';
import Tabslide from './components/Tabslide';

const dimensions = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

export default function ConfigLive({navigation}: {navigation: any}) {
  //   const [ChanelTest, setChanelTest] = useRecoilState<any>(channel);
  const [ChanelTest, setChanelTest] = useState<any>('dotplay2test');
  const resetList = useResetRecoilState(channel);
  const AgoraEngine = useRef<any>();

  const createLive = () => {
    navigation.navigate('LiveMainScreen', {
      type: 'create',
      channel: ChanelTest,
    });
    console.log(ChanelTest);
  };

  const popupCreateLive = () => {
    Alert.alert('รอทำ modal เลือกหมวด');
  };

  async function requestCameraAndAudioPermission() {
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      ]);
      if (
        granted['android.permission.RECORD_AUDIO'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
        granted['android.permission.CAMERA'] ===
          PermissionsAndroid.RESULTS.GRANTED
      ) {
        console.log('You can use the cameras & mic');
      } else {
        console.log('Permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }
  const init = async () => {
    AgoraEngine.current = await RtcEngine.create(
      'c7e742d5df23478285a9dc4f4ff62407',
    );
    setChanelTest(uuid.v4());
    AgoraEngine.current.enableVideo();
    AgoraEngine.current.startPreview();
  };
  useEffect(() => {
    if (Platform.OS === 'android') requestCameraAndAudioPermission();
    init();
    return () => {
      AgoraEngine.current.destroy();
      resetList();
      console.log('unmount');
    };
  }, [navigation]);

  return (
    <SafeAreaView style={{flex: 1}}>
      <Grid>
        <Row
          style={{
            width: dimensions.width,
            height: dimensions.height,
          }}>
          <Col>
            <RtcLocalView.SurfaceView
              style={{
                width: dimensions.width,
                height: dimensions.height,
              }}
              channelId={ChanelTest}
            />
          </Col>
        </Row>
        <Row
          style={{
            marginTop: -220,
          }}>
          <Col>
            <TouchableOpacity
              style={{
                alignItems: 'center',
              }}
              onPress={createLive}>
              <Image
                source={require('../../assets/images/livestart.png')}
                resizeMode="cover"
              />
            </TouchableOpacity>
          </Col>
        </Row>
        <Row
          style={{
            marginTop: -30,
          }}>
          <Col>
            <Tabslide navigation={navigation} />
          </Col>
        </Row>
      </Grid>
    </SafeAreaView>
  );
}
