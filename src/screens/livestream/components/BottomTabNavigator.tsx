// import React ,{useState}from 'react';
// import {View, Text} from 'react-native';
// import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
// import CreatePostScreen from '../../createpost/CreatePostScreen';

// const Tab = createMaterialTopTabNavigator();

// interface IContentState {
//   onFocusColorState: any;
//   outOfFocusColorState: any;
// }

// function BottomTabNavigator({navigation}: {navigation: any}) {

//   const [colorState] = useState<IContentState>({
//     onFocusColorState: '#99ff00',
//     outOfFocusColorState: '#ffffff',
//   });

//   return (
//     <Tab.Navigator initialRouteName="Post"
//      screenOptions={{
//         tabBarStyle: {backgroundColor: null},
//         tabBarShowLabel: false,
//         tabBarInactiveTintColor: colorState.outOfFocusColorState,
//         tabBarActiveTintColor: colorState.onFocusColorState,
//     >
//       <Tab.Screen
//         name="CreatePost"
//         component={CreatePostScreen}
//         options={{tabBarLabel: Post}}
//       />
//     </Tab.Navigator>
//   );
// }
