import React from 'react';
import {View, Text, TouchableOpacity, Image, Alert} from 'react-native';
import CreatePost from '../../createpost/CreatePostScreen';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faBell,
  faStream,
  faPlayCircle,
  faImagePolaroid,
  faShoppingBasket,
} from '@fortawesome/pro-regular-svg-icons';
import {
  TabView,
  SceneMap,
  TabBar,
  TabViewAnimated,
} from 'react-native-tab-view';
import live from '../../../assets/images/live.png';
import photo from '../../../assets/images/mockimage/Rectangle3749.png';
import CreatePostScreen from '../../createpost/CreatePostScreen';
// import BottomTabNavigator from './BottomTabNavigator';

export default function Tabslide({navigation}: {navigation: any}) {
  // const [index, setIndex] = React.useState(0);
  // const TabPostPhoto = () => {
  //   <>
  //     <CreatePost />
  //   </>;
  // };
  // const TabLive = () => {
  //   <>
  //     <CreatePost />
  //   </>;
  // };
  // const [routes] = React.useState([
  //   {key: 'tabPhoto', title: 'Photo', icon: photo},
  //   {key: 'tabLive', title: 'Live', icon: live},
  // ]);
  // const renderScene = SceneMap({
  //   tabPhoto: TabPostPhoto,
  //   tabLive: TabLive,
  // });
  // const ContentFromState = () => {
  //   switch (content) {
  //     case 'live':
  //       return <CreatePostScreen />;
  //     case 'photo':
  //       return <CreatePostScreen />;
  //     default:
  //       return <Text>coming soon</Text>;
  //   }
  // };
  return (
    <TouchableOpacity
      style={{
        alignItems: 'center',
      }}
      onPress={() =>
        navigation.navigate('ConfigLive', {screen: 'CreatePostPhoto'})
      }>
      <Image
        source={require('./../../../assets/images/live.png')}
        resizeMode="cover"
        style={{width: 30, height: 30, marginTop: 20}}
      />
      {/* <Image
        source={require('../../../assets/images/livestart.png')}
        resizeMode="cover"
      /> */}
    </TouchableOpacity>
  );
}
