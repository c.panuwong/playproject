import React from 'react';
import {View, Text, FlatList} from 'react-native';
import {DBHText} from '../../components/StyledText';
import styled from 'styled-components';

const data = [
  {
    id: 1,
    name: 'idol Dance',
    post: '99K',
  },
  {
    id: 2,
    name: 'idol Dance Dotplay',
    post: '99K',
  },
  {
    id: 3,
    name: 'idol Dance1111',
    post: '99K',
  },
  {
    id: 4,
    name: 'idol Dance',
    post: '99K',
  },
  {
    id: 5,
    name: 'idol Dance',
    post: '99K',
  },
];
export default function HashTagTab({
  childRef,
  handlePosition,
  navigation,
}: {
  childRef: any;
  handlePosition: any;
  navigation: any;
}) {
  const renderItem = (item: any) => {
    return (
      <View1>
        <View2>
          <DBHText color="#99FF00" size={24}>
            #{item.name}
          </DBHText>
        </View2>
        <View style={{justifyContent: 'center'}}>
          <DBHText size={20}>{item.post} Galaxy</DBHText>
        </View>
      </View1>
    );
  };
  return (
    <View
      style={{width: '100%', backgroundColor: 'transparent', marginTop: 10}}>
      <FlatList
        data={data}
        keyExtractor={(item: any) => item.id}
        renderItem={(item: any) => renderItem(item.item)}
        onScroll={e => {
          childRef.current.handleLogo(e);
          handlePosition(e);
        }}
      />
    </View>
  );
}
const View1 = styled(View)`
  width: 100%;
  justify-content: space-between;
  flex-direction: row;
  padding-horizontal: 15px;
  padding-vertical: 8px;
`;
const View2 = styled(View)`
  border-color: #7f7f7f;
  align-self: baseline;
  border-width: 1px;
  border-radius: 20px;
  padding-vertical: 2px;
  padding-horizontal: 5px;
  background-color: #252e57;
`;
