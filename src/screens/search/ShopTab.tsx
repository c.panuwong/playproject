import React from 'react';
import {View, FlatList, Image, StyleSheet} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
import {TouchableOpacity} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components';
import {DBHText} from '../../components/StyledText';
import EngagementLike from '../../components/postengagement/EngagementLike';
import EngagementShare from '../../components/postengagement/EngagementShare';
import EngagementComment from '../../components/postengagement/EngagementComment';
import EngagementBookmark from '../../components/postengagement/EngagementBookmark';
// @ts-ignore
import ReadMore from '@fawazahmed/react-native-read-more';
const data = [
  {
    id: '1',
    image: require('../../assets/images/mockimage/Rectangle3751.png'),
    galaxy_name: 'OnlyFans',
    galaxy_description: 'เรื่องเสียว เสียวยันไข่',
    galaxy_description_full:
      'หัวเรื่องหรือพาดหัว (headline) เป็นข้อความที่อยู่บนสุดของบทความแสดงถึงประเด็นหลักของบทความ หัวเรื่องมักถูกออกแบบให้โดดเด่นสะดุดตา และใช้คำที่เลือกสรรมาให้เกี่ยวข้องกับหัวเรื่อง หัวเรื่องหรือพาดหัวมักจะถูกเขียนหรือพิมพ์ด้วยรูปแบบอย่างย่อ ซึ่งอาจเรียบเรียงอย่างไม่เป็นประโยคหรือไวยากรณ์ที่ไม่สมบูรณ์',
    follow: 1,
  },
  {
    id: '2',
    image: require('../../assets/images/mockimage/Rectangle3751.png'),
    galaxy_name: 'OnlyFans',
    galaxy_description: 'เรื่องเสียว เสียวยันไข่',
    galaxy_description_full:
      'หัวเรื่องหรือพาดหัว (headline) เป็นข้อความที่อยู่บนสุดของบทความแสดงถึงประเด็นหลักของบทความ หัวเรื่องมักถูกออกแบบให้โดดเด่นสะดุดตา และใช้คำที่เลือกสรรมาให้เกี่ยวข้องกับหัวเรื่อง หัวเรื่องหรือพาดหัวมักจะถูกเขียนหรือพิมพ์ด้วยรูปแบบอย่างย่อ ซึ่งอาจเรียบเรียงอย่างไม่เป็นประโยคหรือไวยากรณ์ที่ไม่สมบูรณ์',
    follow: 0,
  },
  {
    id: '3',
    image: require('../../assets/images/mockimage/Rectangle3751.png'),
    galaxy_name: 'OnlyFans',
    galaxy_description: 'เรื่องเสียว เสียวยันไข่',
    galaxy_description_full:
      'หัวเรื่องหรือพาดหัว (headline)เป็นข้อความที่อยู่บนสุดของบทความแสดงถึงประเด็นหลักของบทความ ประเด็นหลักของบทความ ประเด็นหลักของบทความ หัวเรื่องมักถูกออกแบบให้โดดเด่นสะดุดตา และใช้คำที่เลือกสรรมาให้เกี่ยวข้องกับหัวเรื่อง หัวเรื่องหรือพาดหัวมักจะถูกเขียนหรือพิมพ์ด้วยรูปแบบอย่างย่อ ซึ่งอาจเรียบเรียงอย่างไม่เป็นประโยคหรือไวยากรณ์ที่ไม่สมบูรณ์',
    follow: 0,
  },
];

export default function VideoTab({
  childRef,
  handlePosition,
  navigation,
}: {
  childRef: any;
  handlePosition: any;
  navigation: any;
}) {
  return (
    <View style={{width: '100%', backgroundColor: 'transparent'}}>
      <FlatList
        data={data}
        keyExtractor={(item: any) => item.id}
        renderItem={(item: any) => renderItem(item.item, navigation)}
        onScroll={e => {
          childRef.current.handleLogo(e);
          handlePosition(e);
        }}
      />
    </View>
  );
}
const ClickHandle = (navigation: any) => {
  navigation.navigate('ImageMore');
};

const FollowCheck = (follow: any) => {
  if (follow == 1) {
    return (
      <DBHText color="#99FF00" size={16}>
        <ImageFollowing
          source={require('../../assets/images/followingicon.png')}
          resizeMode="cover"
        />
        &nbsp;Following
      </DBHText>
    );
  } else {
    return <DBHText color="#99FF00">+ Follow</DBHText>;
  }
};
const renderItem = (item: any, navigation: any) => {
  return (
    <ViewList>
      <ViewImage>
        <Linear
          start={{x: 0.2, y: 1.5}}
          end={{x: 1.3, y: 0}}
          colors={['#257CE2', '#33D52B', '#99FF00']}>
          <ImageProfile source={item.image} />
        </Linear>
      </ViewImage>
      <ViewRight>
        <Row>
          <ColNameWithFollowLeft>
            <TouchableOpacity onPress={() => ClickHandle(navigation)}>
              <DBHText size={21}>{item.galaxy_name}</DBHText>
            </TouchableOpacity>
          </ColNameWithFollowLeft>
          <ColNameWithFollowRight>
            <TouchableOpacity onPress={() => ClickHandle(navigation)}>
              <View style={{marginTop: 3}}>{FollowCheck(item.follow)}</View>
            </TouchableOpacity>
          </ColNameWithFollowRight>
        </Row>
        <Row>
          <TouchableOpacity onPress={() => ClickHandle(navigation)}>
            <DBHText color="#B5B5B5">{item.galaxy_description}</DBHText>
          </TouchableOpacity>
        </Row>
        <Row style={{marginTop: 10}}>
          <Image source={item.image} style={{width: '100%', height: 200}} />
          <ImageSearchIcon
            source={require('../../assets/images/icons/multi_image_search.png')}
          />
        </Row>
        <Row>
          <Engagement>
            <EngagementLike amount="99K" />
            <EngagementComment amount="99K" />
            <EngagementShare amount="99K" />
            <EngagementBookmark />
          </Engagement>
        </Row>
        <Row style={{marginTop: 8}}>
          <TextSubDescription
            numberOfLines={2}
            seeMoreStyle={styles.seeMoreStyle}
            seeLessStyle={styles.seeLessStyle}
            seeMoreText="Read More"
            seeLessText="See Less">
            {item.galaxy_description_full}
          </TextSubDescription>
        </Row>
      </ViewRight>
    </ViewList>
  );
};

const ViewList = styled(View)`
  flex-direction: row;
  padding-vertical: 1.5%;
  padding-horizontal: 10px;
`;
const ViewRight = styled(View)`
  flex-direction: column;
  padding-horizontal: 10px;
  flex: 0.8;
`;
const ViewImage = styled(View)`
  flex: 0.2;
  padding-vertical: 1.5%;
`;
const Linear = styled(LinearGradient)`
  justify-content: center;
  align-items: center;
  align-content: center;
  border-radius: 17px;
  width: 64px;
  height: 64px;
  padding-vertical: 28px;
`;
const ImageProfile = styled(Image)`
  width: 60px;
  height: 60px;
  border-radius: 17px;
  border-width: 3px;
  border-color: #fff;
`;
const ColNameWithFollowLeft = styled(Col)`
  width: 30%;
`;
const ColNameWithFollowRight = styled(Col)`
  width: 70%;
`;
const ImageFollowing = styled(Image)`
  width: 10px;
  height: 10px;
`;
const Engagement = styled(View)`
  height: 40px;
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  border-bottom-color: white;
  border-bottom-width: 1.5px;
`;
const ImageSearchIcon = styled(Image)`
  width: 57px;
  height: 23px;
  position: absolute;
  top: 3px;
  right:5px
`;
const TextSubDescription = styled(ReadMore)`
  font-family: DBHelvethaicaX-55Regular;
  font-size: 20px;
  margin-horizontal: 1%;
  color: #b3b3b3;
`;
const RowImage = styled(Row)`
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
`;
const styles = StyleSheet.create({
  seeMoreStyle: {
    color: '#FFF',
    fontSize: 18,
    alignSelf: 'center',
    fontFamily: 'DBHelvethaicaX-55Regular',
  },
  seeLessStyle: {
    color: '#FFF',
    fontSize: 18,
    alignSelf: 'center',
    fontFamily: 'DBHelvethaicaX-55Regular',
  },
});