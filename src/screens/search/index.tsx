import React, {useRef, useState, useEffect} from 'react';
import {View, Animated} from 'react-native';
import {NativeBaseProvider} from 'native-base';
import SearchBar from './components/SearchBar';
import LinearGradient from 'react-native-linear-gradient';
// @ts-ignore
import styled from 'styled-components';
import HeaderAnimated from '../../components/HeaderAnimated';
import {DBHText} from '../../components/StyledText';
import SearchListItem from './components/SearchListItem';
import {useRecoilState, useResetRecoilState} from 'recoil';
import {searchInput} from '../../recoil/search';

const listItem = [
  'โควิดป่วน',
  'โควิดหยุด',
  'โควิดวัคซีน',
  'โควิดวัคซีน',
  'โควิดวัคซีน',
  'โควิดวัคซีน',
  'โควิดวัคซีน',
  'โควิดวัคซีน',
  'โควิดวัคซีน',
];

export default function index({navigation}: {navigation: any}) {
  const childRef = useRef<any>();
 
  return (
    <NativeBaseProvider>
      <ViewBG
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}
        colors={['#21284e', '#0e1333', '#080D28']}>
        <HeaderAnimated ref={childRef} title="Search" navigation={navigation} />
        <Animated.ScrollView
          style={{width: '100%'}}
          contentContainerStyle={{flexGrow: 1}}
          showsVerticalScrollIndicator={true}
          stickyHeaderIndices={[2]}
          onScroll={e => childRef.current.handleLogo(e)}>
          {/* <DBHText
            size={34}
            color="white"
            style={{paddingHorizontal: 15, top: 10}}>
            Search
          </DBHText> */}
          <View style={{marginTop: 10, width: '100%'}}>
            <SearchBar navigation={navigation} />
          </View>
          <DBHText size={30} style={{paddingHorizontal: 15}}>
            Today Trends
          </DBHText>
          <SearchListItem listItem={listItem} navigation={navigation} />
        </Animated.ScrollView>
      </ViewBG>
    </NativeBaseProvider>
  );
}
const ViewBG = styled(LinearGradient)`
  flex: 1;
  align-items: center;
`;
