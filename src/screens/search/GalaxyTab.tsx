import React, {useState} from 'react';
import {View, Image} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components';
import {DBHText} from '../../components/StyledText';
import {data} from '../hamberger/data';
import {ButtonBorder} from '../../components/ButtonComponent';

export default function GalaxyTab({
  childRef,
  handlePosition,
}: {
  childRef: any;
  handlePosition: any;
}) {
  const [isfollow, setIsFollow] = useState<any>([]);

  const handleFollow = (id: any) => {
    if (isfollow.indexOf(id) > -1) {
      setIsFollow(isfollow.filter((x: any) => x != id));
    } else {
      setIsFollow((oldArray: any) => [...oldArray, id]);
    }
  };
  const renderItem = (item: any) => {
    return (
      <>
        <ViewList>
          <ViewImage>
            <Linear
              start={{x: 0.2, y: 1.5}}
              end={{x: 1.3, y: 0}}
              colors={['#257CE2', '#33D52B', '#99FF00']}>
              <ImageProfile source={{uri: item.user_profile}} />
            </Linear>
          </ViewImage>
          <ViewTexts>
            <DBHText numberOfLines={1}>{item.user_name}</DBHText>
            <DBHText numberOfLines={1}>{item.bio}</DBHText>
          </ViewTexts>
          <ViewButton>
            <ButtonBorder
              title={isfollow?.indexOf(item.id) > -1 ? 'Following' : 'Follow'}
              onPress={() => handleFollow(item.id)}
              height={30}
              color={isfollow?.indexOf(item.id) > -1 ? '#fff' : '#020b3c'}
              textColor={
                isfollow?.indexOf(item.id) > -1 ? '#3C7D0A' : '#99FF00'
              }
            />
          </ViewButton>
        </ViewList>
      </>
    );
  };
  return (
    <View style={{width: '100%', backgroundColor: 'transparent'}}>
      <FlatList
        data={data}
        keyExtractor={(item: any) => item.id}
        renderItem={(item: any) => renderItem(item.item)}
        onScroll={e => {
          childRef.current.handleLogo(e);
          handlePosition(e);
        }}
        onEndReachedThreshold={0.05}
        onEndReached={() => console.log('ใกล้สุด')}
      />
    </View>
  );
}
const ViewList = styled(View)`
  flex-direction: row;
  align-items: center;
  padding-vertical: 1.5%;
  padding-horizontal: 18px;
`;
const ViewTexts = styled(View)`
  flex: 0.65;
  padding-end: 10px;
`;
const ViewButton = styled(View)`
  flex: 0.27;
`;
const ViewImage = styled(View)`
  flex: 0.2;
`;
const Linear = styled(LinearGradient)`
  justify-content: center;
  align-items: center;
  align-content: center;
  border-radius: 17px;
  width: 57px;
  height: 52px;
  padding-vertical: 28px;
`;
const ImageProfile = styled(Image)`
  width: 51.51px;
  height: 51.51px;
  border-radius: 17px;
  border-width: 3px;
  border-color: #fff;
`;
