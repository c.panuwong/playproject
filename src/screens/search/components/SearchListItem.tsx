import React from 'react';
import {List, View} from 'native-base';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {DBHText} from '../../../components/StyledText';
// @ts-ignore
import styled from 'styled-components';
export default function SearchListItem({
  listItem,
  navigation,
}: {
  listItem: any;
  navigation: any;
}) {
  return (
    <>
      {listItem.map((item: any, index: number) => {
        return (
            <ViewBG key={index}>
              <DBHText size={24} color="#99FF00">
                {item}
              </DBHText>
              <DBHText size={20}>{item}</DBHText>
            </ViewBG>
        );
      })}
    </>
  );
}
const ViewBG = styled(TouchableOpacity)`
  width: 100%;
  padding-horizontal: 15px;
  height: 70px;
  background-color: #1b2242;
  flex-direction: column;
  justify-content: center;
  margin-top:5px;
`;
