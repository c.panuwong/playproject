import React, {useRef, useEffect, useState} from 'react';
import {View, useWindowDimensions, Animated} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import {NativeBaseProvider} from 'native-base';
import SearchBar from './components/SearchBar';
import LinearGradient from 'react-native-linear-gradient';
import HeaderAnimatedSearch from '../../components/HeaderAnimatedSearch';
import GalaxyTab from './GalaxyTab';
import LiveTab from './LiveTab';
import VideoTab from './VideoTab';
import PostTab from './PostTab';
import ShopTab from './ShopTab';
import HashTagTab from './HashTagTab';
import {Col, Row, Grid} from 'react-native-easy-grid';
// @ts-ignore
import styled from 'styled-components';

import {useResetRecoilState} from 'recoil';
import {searchInput} from '../../recoil/search';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faSlidersH} from '@fortawesome/pro-regular-svg-icons';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default function SearchResult({navigation}: {navigation: any}) {
  const resetList = useResetRecoilState(searchInput);
  const childRef = useRef<any>();
  let [fadeAnimationPosition] = useState<any>(new Animated.Value(0));

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'Galaxy', title: 'Galaxy'},
    {key: 'Live', title: 'Live'},
    {key: 'Video', title: 'Video'},
    {key: 'Post', title: 'Post'},
    {key: 'Shop', title: 'Shop'},
    {key: 'HashTag', title: 'HashTag'},
  ]);
  const Galaxy = () => (
    <GalaxyTab childRef={childRef} handlePosition={handlePosition} />
  );
  //------------------Want re-render -------------   //
  // const renderScene = SceneMap({
  //   Galaxy: Galaxy,
  //   Live: Live,
  //   Video: Video,
  //   Post: Post,
  //   Shop: Shop,
  //   HashTag: HashTag,
  // });

  //------------------Want not re-render -------------   //
  const renderScene = ({route}: {route: any}) => {
    switch (route.key) {
      case 'Galaxy':
        return (
          <GalaxyTab childRef={childRef} handlePosition={handlePosition} />
        );
      case 'Live':
        return (
          <LiveTab
            childRef={childRef}
            handlePosition={handlePosition}
            navigation={navigation}
          />
        );
      case 'Video':
        return (
          <VideoTab
            childRef={childRef}
            handlePosition={handlePosition}
            navigation={navigation}
          />
        );
      case 'Post':
        return (
          <PostTab
            childRef={childRef}
            handlePosition={handlePosition}
            navigation={navigation}
          />
        );
      case 'Shop':
        return (
          <ShopTab
            childRef={childRef}
            handlePosition={handlePosition}
            navigation={navigation}
          />
        );
      case 'HashTag':
        return (
          <HashTagTab
            childRef={childRef}
            handlePosition={handlePosition}
            navigation={navigation}
          />
        );
      default:
        return null;
    }
  };
  useEffect(() => {
    return () => {
      console.log('unmount');
      resetList();
    };
  }, [navigation]);
  const fadeDown = () => {
    Animated.timing(fadeAnimationPosition, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };
  const fadeUp = () => {
    Animated.timing(fadeAnimationPosition, {
      toValue: -48,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };
  const handlePosition = (event: any) => {
    if (event.nativeEvent.contentOffset.y == 0) {
      fadeDown();
    } else if (
      event.nativeEvent.contentOffset.y > 40 &&
      event.nativeEvent.contentOffset.y <= 300
    ) {
      fadeUp();
    } else {
    }
  };
  return (
    <NativeBaseProvider>
      <ViewBG
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}
        colors={['#21284e', '#0e1333', '#080D28']}>
        <Animated.View
          style={{
            width: '100%',
            height: '100%',
            transform: [{translateY: fadeAnimationPosition}],
          }}>
          <HeaderAnimatedSearch
            ref={childRef}
            title="Search"
            navigation={navigation}
          />

          <View style={{width: '100%', flexDirection: 'row'}}>
            <Col style={{flex: 0.9}}>
              <SearchBar navigation={navigation} />
            </Col>
            <Col style={{flex: 0.1, justifyContent: 'center'}}>
              <TouchableOpacity
                onPress={() => navigation.navigate('Hamberger')}>
                <FontAwesomeIcon icon={faSlidersH} size={28} color="#fff" />
              </TouchableOpacity>
            </Col>
          </View>

          <View style={{height: '100%'}}>
            <TabView
              lazy
              navigationState={{index, routes}}
              renderScene={renderScene}
              onIndexChange={setIndex}
              initialLayout={{width: 200}}
              renderTabBar={props => (
                <TabBar
                  {...props}
                  style={{
                    backgroundColor: 'transparent',
                    borderBottomColor: '#B5B5B5',
                    borderBottomWidth: 1,
                  }}
                  activeColor="#99FF00"
                  indicatorStyle={{backgroundColor: 'transparent'}}
                  tabStyle={{width: 120}}
                  scrollEnabled={true}
                />
              )}
            />
          </View>
        </Animated.View>
      </ViewBG>
    </NativeBaseProvider>
  );
}
const ViewBG = styled(LinearGradient)`
  flex: 1;
  align-items: center;
`;
