import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  KeyboardAvoidingView,
  Platform,
  TextInput,
  Image,
  TouchableOpacity,
  Keyboard,
  Animated,
  NativeModules,
  NativeEventEmitter,
  FlatList,
} from 'react-native';

import {socket} from './socket';

import {ScrollView} from 'react-native-gesture-handler';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faBell} from '@fortawesome/pro-regular-svg-icons';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import {DBHText} from '../../components/StyledText';
import IconGift from 'react-native-vector-icons/Ionicons';
import IconEmoji from 'react-native-vector-icons/Entypo';
import {Modalize} from 'react-native-modalize';
import {chat_modalize_height} from '../../constants/Layout';
import StickerModal from './components/StickerModal';
import ChatFriend from './components/ChatFriend';
import ChatMe from './components/ChatMe';
interface IUser {
  _id: number;
  avatar: string;
  message: string;
  name: string;
  time: string;
  image_message: any;
  type: string;
  sticker: string;
}

interface RefObject<T> {
  readonly current: T | null | undefined;
}

const {StatusBarManager} = NativeModules;

const ChatRoom = ({route, navigation}: {route: any; navigation: any}) => {
  const {name, image_url, lastest_message, accept} = route.params.data;
  const scrollRef = React.createRef<ScrollView>();
  const menuRef = useRef(null);
  const [stickerModalVisible, setStickerModalVisible] = useState(false);
  const [visible, setVisible] = useState<boolean>(false);
  const [toggleAnimation, setToggleAnimation] = useState(false);
  const [typing, setTyping] = useState('');
  const [user, setUser] = useState<Array<IUser>>([]);
  const stickerHeight = useRef<any>(new Animated.Value(0)).current;
  const [statusbarHeight, setStatusbarHeight] = useState(0);
  const [stickerState, setStickerState] = useState(-1);
  const [heightOfInput, setHeightOfInput] = useState(0);

  const [stickerList, setStickerList] = useState([
    {
      _id: '1D',
      name: 'dog',
      sticker_assets: require('../../assets/images/sticker/dog.png'),
      sticker_list_assets: [
        {
          sticker_action_id: 'dAc1',
          sticker_action_name: 'dance',
          sticker_action_image: require('../../assets/images/sticker/full_dog.png'),
        },
        {
          sticker_action_id: 'dAc2',
          sticker_action_name: 'dance',
          sticker_action_image: require('../../assets/images/sticker/full_dog.png'),
        },
        {
          sticker_action_id: 'dAc3',
          sticker_action_name: 'dance',
          sticker_action_image: require('../../assets/images/sticker/full_dog.png'),
        },
        {
          sticker_action_id: 'dAc4',
          sticker_action_name: 'dance',
          sticker_action_image: require('../../assets/images/sticker/full_dog.png'),
        },
        {
          sticker_action_id: 'dAc5',
          sticker_action_name: 'dance',
          sticker_action_image: require('../../assets/images/sticker/full_dog.png'),
        },
      ],
    },
    {
      _id: '2P',
      name: 'pig',
      sticker_assets: require('../../assets/images/sticker/pig.png'),
      sticker_list_assets: [
        {
          sticker_action_id: 'pAc1',
          sticker_action_name: 'dance',
          sticker_action_image: require('../../assets/images/sticker/full_pig.png'),
        },
        {
          sticker_action_id: 'pAc2',
          sticker_action_name: 'dance',
          sticker_action_image: require('../../assets/images/sticker/full_pig.png'),
        },
        {
          sticker_action_id: 'pAc3',
          sticker_action_name: 'dance',
          sticker_action_image: require('../../assets/images/sticker/full_pig.png'),
        },
        {
          sticker_action_id: 'pAc4',
          sticker_action_name: 'dance',
          sticker_action_image: require('../../assets/images/sticker/full_pig.png'),
        },
        {
          sticker_action_id: 'pAc5',
          sticker_action_name: 'dance',
          sticker_action_image: require('../../assets/images/sticker/full_pig.png'),
        },
      ],
    },
  ]);
  const [image, setImage] = useState<any | null>(null);
  const [stickerOrGift, setStickerOrGift] = useState('');
  const [gift, setGift] = useState([
    {
      name: 'rose',
      image: require('../../assets/images/gift/rose.png'),
      price: 2,
      quantity: 6,
    },
    {
      name: 'bigrose',
      image: require('../../assets/images/gift/bigrose.png'),
      price: 2,
      quantity: 2,
    },
    {
      name: 'hugerose',
      image: require('../../assets/images/gift/hugerose.png'),
      price: 2,
      quantity: 0,
    },
    {
      name: 'love',
      image: require('../../assets/images/gift/love.png'),
      price: 2,
      quantity: 0,
    },
    {
      name: 'duck',
      image: require('../../assets/images/gift/duck.png'),
      price: 2,
      quantity: 0,
    },
    {
      name: 'robot',
      image: require('../../assets/images/gift/robot.png'),
      price: 2,
      quantity: 0,
    },
    {
      name: 'rose4u',
      image: require('../../assets/images/gift/rose4u.png'),
      price: 2,
      quantity: 0,
    },
    {
      name: 'monkey',
      image: require('../../assets/images/gift/monkey.png'),
      price: 2,
      quantity: 0,
    },
  ]);
  const modalizeRef = useRef<Modalize>(null);
  const [giftState, setGiftState] = useState(0);
  const [isAccept, setIsAccept] = useState(accept);

  useEffect(() => {
    const emitter = new NativeEventEmitter(StatusBarManager);
    if (Platform.OS === 'android') {
      setStatusbarHeight(StatusBarManager.HEIGHT);
    } else if (Platform.OS === 'ios') {
      StatusBarManager.getHeight((status: any) => {
        setStatusbarHeight(status.height);
      });
      const listener = emitter.addListener('statusBarFrameWillChange', data => {
        setStatusbarHeight(data.frame.height);
      });

      return () => listener.remove();
    }
  }, []);
  useEffect(() => {
    socket.connect();
    socket.emit('joinroom', {userId: '1', roomName: '1'});
  }, []);
  useEffect(() => {
    ResponseNewMsg(1, '1');
  }, []);
  const ResponseNewMsg = async (roomId, userId) => {
    socket.on(`new-message-${roomId}`, async messageNew => {
      const date = new Date(Date.now());
      var obj: any = {
        _id: 1,
        avatar:
          'https://www.prachachat.net/wp-content/uploads/2019/01/%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%A2%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B9%8C.png',
        message: messageNew.msg,
        name: 'Jacky',
        time: `${date.getHours()}:${date.getMinutes()}`,
        image_message: '',
        type: messageNew?.type ? 'sticker' : 'typing',
        sticker: messageNew?.type == 'sticker' ? messageNew.msg : '',
      };
      setUser(oldArray => [...oldArray, obj]);
      console.log(user);
    });
    return () => socket.disconnect();
  };
  // useEffect(() => {
  //   (async () => {
  //     if (Platform.OS !== 'web') {
  //       const {status} =
  //         await ImagePicker.requestMediaLibraryPermissionsAsync();
  //       if (status !== 'granted') {
  //         alert('Please granted before ');
  //       }
  //     }
  //   })();
  // }, []);

  // useEffect(() => {
  //   setUser([
  //     {
  //       _id: 1,
  //       avatar: image_url,
  //       message: lastest_message.length !== 0 ? lastest_message : '',
  //       name: name,
  //       time: '14:20',
  //       image_message: image_url,
  //       // type: 'image',
  //       type: 'typing',
  //       sticker: '',
  //     },
  //   ]);
  // }, []);

  // useEffect(() => {
  //   if (image) {
  //     const date = new Date(Date.now());
  //     const obj: any = {
  //       _id: 2,
  //       avatar:
  //         'https://www.prachachat.net/wp-content/uploads/2019/01/%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%A2%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B9%8C.png',
  //       message: '',
  //       name: 'Jacky',
  //       time: `${date.getHours()}:${date.getMinutes()}`,
  //       image_message: image,
  //       type: 'image',
  //       sticker: '',
  //     };
  //     setUser([...user, obj]);
  //     setImage(null);
  //   }
  // }, [image]);

  // const pickImage = async () => {
  //   let result = await ImagePicker.launchImageLibraryAsync({
  //     mediaTypes: ImagePicker.MediaTypeOptions.All,
  //     allowsEditing: true,
  //     aspect: [4, 3],
  //     quality: 1,
  //   });

  //   if (!result.cancelled) {
  //     setImage(result.uri);
  //   }
  // };

  const onSendingSticker = (sticker_img: string) => {
    const date = new Date(Date.now());
    const obj: any = {
      _id: 2,
      avatar:
        'https://www.prachachat.net/wp-content/uploads/2019/01/%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%A2%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B9%8C.png',
      message: '',
      name: 'Jacky',
      time: `${date.getHours()}:${date.getMinutes()}`,
      image_message: '',
      type: 'sticker',
      sticker: sticker_img,
    };
    socket.emit('sent-message', {
      chat_room_id: '1',
      msg: sticker_img,
      user_id: 1,
    });

    setUser([...user, obj]);
  };

  const toggleSticker = (value: string) => {
    const prev = stickerOrGift;
    setStickerOrGift(value);
    if (!stickerModalVisible) {
      setStickerModalVisible(!stickerModalVisible);
      setToggleAnimation(!toggleAnimation);
    } else if (prev === value) {
      setStickerModalVisible(!stickerModalVisible);
      setToggleAnimation(!toggleAnimation);
    }
    Keyboard.dismiss();
  };

  const closeSticker = () => {
    setStickerModalVisible(false);
    setToggleAnimation(false);
    Keyboard.dismiss();
  };

  const onSending = () => {
    const date = new Date(Date.now());
    const obj: any = {
      _id: 2,
      avatar:
        'https://www.prachachat.net/wp-content/uploads/2019/01/%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%A2%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B9%8C.png',
      message: typing,
      name: 'Jacky',
      time: `${date.getHours()}:${date.getMinutes()}`,
      image_message: '',
      type: 'typing',
      sticker: '',
    };
    setUser([...user, obj]);
    setTyping('');
  };

  useEffect(() => {
    if (stickerModalVisible && toggleAnimation) {
      slideUp();
    } else {
      slideDown();
    }
  }, [stickerModalVisible, toggleAnimation]);

  const slideUp = () => {
    Animated.timing(stickerHeight, {
      toValue: 250,
      duration: 300,
      useNativeDriver: false,
    }).start();
  };

  const slideDown = () => {
    Animated.timing(stickerHeight, {
      toValue: 0,
      duration: 200,
      useNativeDriver: false,
    }).start();
  };

  const sendingType = (item: any) => {
    switch (item.type) {
      case 'typing':
        return <DBHText size={18}>{item.message}</DBHText>;
      case 'image':
        return (
          <Image
            source={{uri: item.image_message}}
            style={{width: 100, height: 100}}
          />
        );
      case 'sticker':
        return <Image source={item.sticker} />;
    }
  };

  const UserPosition: Function = () => {
    return user.map((item, index) => {
      if (item._id === 1) {
        return (
          <ChatFriend
            menuRef={menuRef}
            index={index}
            item={item}
            sendingType={sendingType}
            key={index}
          />
        );
      } else if (item._id === 2) {
        return (
          <ChatMe
            menuRef={menuRef}
            index={index}
            item={item}
            sendingType={sendingType}
            key={index}
          />
        );
      }
      return <></>;
    });
  };

  const onOpenModalize = async (index: number) => {
    await setGiftState(index);
    modalizeRef.current?.open();
  };

  const ModalizeContent = () => {
    const currentGift = gift[giftState];
    return (
      <Modalize
        ref={modalizeRef}
        modalHeight={chat_modalize_height}
        modalStyle={{backgroundColor: '#111111'}}
        handlePosition="outside">
        <TouchableOpacity
          onPress={() => modalizeRef.current?.close()}
          style={{
            alignItems: 'flex-end',
            marginRight: '4%',
            marginTop: '2%',
          }}>
          <DBHText style={{fontSize: 20}}>X</DBHText>
        </TouchableOpacity>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <DBHText style={{fontSize: 18, textAlign: 'center'}}>
            {currentGift.quantity !== 0
              ? `Confirm send Gift Item`
              : `Coin is not enough.\n Do you want to purchase more Coin?`}
          </DBHText>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: '4%',
          }}>
          <Image source={currentGift.image} />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginLeft: '3%',
            }}>
            <Image
              source={require('../../assets/images/icons/coin_icon.png')}
              style={{
                width: 20,
                height: 20,
              }}
            />
            <DBHText style={{fontSize: 20}}>{'  ' + currentGift.price}</DBHText>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: '5%',
          }}>
          <TouchableOpacity
            onPress={() => modalizeRef.current?.close()}
            style={{
              borderWidth: 1,
              borderColor: '#3975EC',
              paddingHorizontal: 20,
              paddingVertical: 5,
              borderRadius: 10,
              marginRight: '1%',
            }}>
            <DBHText>Cancel</DBHText>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              borderWidth: 1,
              borderColor: '#3975EC',
              paddingHorizontal: 20,
              paddingVertical: 5,
              borderRadius: 10,
              marginLeft: '1%',
            }}>
            <DBHText>Confirm</DBHText>
          </TouchableOpacity>
        </View>
      </Modalize>
    );
  };

  const GiftForYou = () => {
    return (
      <>
        {gift.map((gift, index) => {
          return (
            <View
              key={index}
              style={{marginHorizontal: '2%', marginTop: '1.5%'}}>
              <TouchableOpacity
                onPress={() => onOpenModalize(index)}
                style={{
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  backgroundColor: 'rgba(72, 83, 131, 1)',
                  borderRadius: 8,
                }}>
                <Image
                  source={gift.image}
                  style={{width: 80, height: 80, marginBottom: '2%'}}
                />

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingHorizontal: 8,
                    borderRadius: 20,
                  }}>
                  <Image
                    source={require('../../assets/images/icons/coin_icon.png')}
                    style={{width: 15, height: 15, marginRight: '5%'}}
                  />
                  <DBHText style={{color: 'black'}}>{gift.price}</DBHText>
                </View>
              </TouchableOpacity>
            </View>
          );
        })}
      </>
    );
  };

  const StickerModalize = () => {
    if (stickerOrGift === 'sticker') {
      return toggleAnimation === true ? (
        <StickerModal
          onSendingSticker={onSendingSticker}
          stickerHeight={stickerHeight}
          stickerState={stickerState}
          setStickerState={setStickerState}
          stickerList={stickerList}
        />
      ) : (
        <Animated.View
          style={{
            width: '100%',
            height: stickerHeight,
          }}></Animated.View>
      );
    } else if (stickerOrGift === 'gift') {
      return toggleAnimation === true ? (
        <Animated.View
          style={{
            width: '100%',
            height: stickerHeight,
            backgroundColor: '#24305E',
          }}>
          <ScrollView scrollEnabled>
            <View
              style={{
                flexWrap: 'wrap',
                flexDirection: 'row',
                alignItems: 'center',
                marginHorizontal: '3%',
                marginTop: '2%',
                paddingBottom: '4%',
              }}>
              <GiftForYou />
            </View>
          </ScrollView>
        </Animated.View>
      ) : (
        <Animated.View
          style={{
            width: '100%',
            height: stickerHeight,
          }}></Animated.View>
      );
    }
    return <></>;
  };

  const IsTyping = () => {
    if (typing.length === 0) {
      return (
        <View
          style={{
            flex: 0.3,
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
          }}>
          <IconGift
            name="gift-outline"
            size={28}
            color="white"
            onPress={() => toggleSticker('gift')}
          />
          <IconEmoji
            name="emoji-flirt"
            size={28}
            color="white"
            onPress={() => toggleSticker('sticker')}
          />
        </View>
      );
    }
    return <></>;
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: '#111011',
      }}
      edges={['bottom', 'left', 'right']}>
      <View
        style={{
          width: '100%',
          height: '100%',
          flex: 1,
          backgroundColor: '#060C30',
        }}>
        <KeyboardAvoidingView
          enabled
          behavior={Platform.OS === 'ios' ? 'padding' : undefined}
          // behavior='padding'
          keyboardVerticalOffset={44 + statusbarHeight}
          style={{flex: 1}}>
          <ScrollView
            ref={scrollRef}
            onContentSizeChange={() =>
              scrollRef.current?.scrollToEnd({animated: true})
            }
            onTouchStart={closeSticker}
            keyboardShouldPersistTaps="handled"
            style={{
              flex: 1,
              flexDirection: 'column',
              marginHorizontal: '2.5%',
            }}>
            <UserPosition />
          </ScrollView>
          {/* <FlatList onScrollAnimationEnd={()=>scrollRef.current?.scrollToEnd({animated: true})}/> */}
          {isAccept ? (
            <View
              style={{
                height: Math.max(35, heightOfInput + 5),
                backgroundColor: '#101531',
                borderRadius: 8,
                width: '100%',
                paddingLeft: '2%',
                flexDirection: 'row',
                alignItems: 'center',
                // borderWidth: 1,
                // borderColor: 'white',
                maxHeight: 60,
                justifyContent: 'center',
                paddingBottom: 5,
              }}>
              <TextInput
                multiline
                value={typing}
                onChangeText={typing => setTyping(typing)}
                blurOnSubmit={true}
                onFocus={() => {
                  setToggleAnimation(false);
                  setStickerModalVisible(false);
                }}
                onContentSizeChange={e => {
                  setHeightOfInput(e.nativeEvent.contentSize.height);
                }}
                style={{
                  color: 'white',
                  width: '100%',
                  alignItems: 'center',
                  justifyContent: 'center',
                  flex: typing ? 1 : 0.7,
                  height: Math.max(30, heightOfInput + 5),
                  maxHeight: 41,
                  borderRadius: 20,
                  borderColor: '#1ADF45',
                  borderWidth: 1,
                  paddingLeft: 15,
                }}
                onSubmitEditing={() =>
                  socket.emit('sent-message', {
                    chat_room_id: '1',
                    msg: typing,
                    user_id: '1',
                  })
                }
                // onSubmitEditing={() => onSending()}
              />
              {typing.length !== 0 ? (
                <View
                  style={{
                    // marginLeft: '3%',
                    flex: typing.length !== 0 ? 0.2 : 0.3,
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  {/* <TouchableOpacity onPress={onSending}> */}
                  <TouchableOpacity
                    onPress={() =>
                      socket.emit('sent-message', {
                        chat_room_id: '1',
                        msg: typing,
                        user_id: '1',
                      })
                    }>
                    <DBHText style={{color: '#A2E5F4'}}>SEND</DBHText>
                  </TouchableOpacity>
                </View>
              ) : (
                <IsTyping />
              )}
            </View>
          ) : (
            <></>
          )}
          <StickerModalize />
          <ModalizeContent />
        </KeyboardAvoidingView>
        {isAccept ? (
          <></>
        ) : (
          <View
            style={{
              height: '35%',
              borderTopStartRadius: 15,
              borderTopEndRadius: 15,
              backgroundColor: '#222222',
              alignItems: 'center',
            }}>
            <View
              style={{
                borderWidth: 3,
                borderColor: '#444444',
                alignSelf: 'center',
                width: '20%',
                marginTop: '3%',
              }}
            />
            <View style={{flexDirection: 'row', marginTop: '3%'}}>
              <FontAwesomeIcon icon={faBell} size={24} color={'white'} />
              <DBHText style={{marginLeft: 10}}>Accept</DBHText>
            </View>
            <View style={{marginHorizontal: '10%', marginTop: '2%'}}>
              <DBHText>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque
                euismod erat purus, in egestas nulla rutrum non. Vestibulum
                pulvinar id diam vel egestas. Maecenas quis neque quis purus
                efficitur vehicula. Duis sit amet dictum velit.
              </DBHText>
            </View>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                style={{
                  borderWidth: 1.5,
                  borderColor: '#2B7EFB',
                  paddingHorizontal: 30,
                  paddingVertical: 10,
                  borderRadius: 10,
                  marginRight: '2.5%',
                }}>
                <DBHText onPress={() => navigation.goBack()}>Cancel</DBHText>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setIsAccept(true)}
                style={{
                  backgroundColor: '#3366CC',
                  paddingHorizontal: 30,
                  paddingVertical: 10,
                  borderRadius: 10,
                  marginLeft: '2.5%',
                }}>
                <DBHText>Accept</DBHText>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

export default ChatRoom;
