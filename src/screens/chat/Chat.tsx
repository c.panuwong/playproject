import React, {useState, useRef} from 'react';
import {ImageBackground} from 'react-native';
import {NativeBaseProvider} from 'native-base';
import {SafeAreaView} from 'react-native-safe-area-context';
import HeaderAnimated from '../../components/HeaderAnimated';
import styled from 'styled-components';
import ChatList from './components/ChatList';

interface ISearchingInput {
  search: string;
}
const Chat = ({navigation}: {navigation: any}) => {
  const [profileData, setProfileData] = useState(null);
  const [headerState, setHeaderState] = useState<string>('0');
  const [searchInput, setSearchInput] = useState<string>('');
  const childRef = useRef<any>();
  const [page, setpage] = useState(0);

  // const handleTextChange = (name: any) => (text: any) => {
  //   setSearchInput(prevState => ({...prevState, [name]: text}));
  // };

  // const ChatContentFromState = () => {
  //   switch (headerState) {
  //     case '0':
  //       return <ChatFriend page={headerState} navigation={navigation} />;
  //     case '1':
  //       return <ChatFriend page={headerState} navigation={navigation} />;
  //     case '2':
  //       return <ChatFriend page={headerState} navigation={navigation} />;
  //     default:
  //       return <Typography>Something Err here</Typography>;
  //   }
  // };
  return (
    <NativeBaseProvider>
      <Background
        source={require('../../assets/images/bg/BG_wallet_p3.jpg')}
        resizeMode="stretch">
        <SafeAreaView style={{flex: 1}}>
          <HeaderAnimated
            ref={childRef}
            title={page == 0 ? 'Massage' : 'Request'}
            navigation={navigation}
          />
          <ChatList childRef={childRef} navigation={navigation} setpage={setpage} page={page}/>
        </SafeAreaView>
      </Background>
    </NativeBaseProvider>
  );
};
const Background = styled(ImageBackground)`
  flex: 1;
`;

export default Chat;
