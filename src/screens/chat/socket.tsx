import socketIOClient from 'socket.io-client';

const endpoint = 'http://{url}:8000';
// const endpoint = 'http://localhost:8000/'
export const socket = socketIOClient(endpoint);