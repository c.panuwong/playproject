import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {Input} from 'native-base';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faSearch} from '@fortawesome/pro-regular-svg-icons';
import IconRight from 'react-native-vector-icons/Octicons';

// import {useRecoilState, useResetRecoilState} from 'recoil';
// import {searchInput} from '../../../recoil/search';
export default function SearchBar({
  navigation,
  setSearchInput,
  searchInput,
}: {
  navigation: any;
  setSearchInput: any;
  searchInput: string;
}) {
  // const [searchinput, setSearchInput] = useRecoilState<string>(searchInput);
  // const resetList = useResetRecoilState(searchInput);
  return (
    <View style={{paddingHorizontal: 15, paddingVertical: 10}}>
      <Input
        placeholder="Search"
        variant="filled"
        width="100%"
        height={45}
        bg="#2B304A"
        borderRadius={38}
        paddingLeft={3}
        py={1}
        px={2}
        _web={{
          _focus: {borderColor: 'muted.300', style: {boxShadow: 'none'}},
        }}
        color="#FFF"
        InputLeftElement={
          <FontAwesomeIcon
            icon={faSearch}
            size={28}
            color="#fff"
            style={{marginLeft: 10}}
          />
        }
        InputRightElement={
          searchInput.length ? (
            <IconRight
              name="x"
              size={20}
              color="#fff"
              style={{marginRight: 15}}
              onPress={() => setSearchInput('')}
            />
          ) : (
            <></>
          )
        }
        returnKeyType="search"
        onChangeText={(text: string) => setSearchInput(text)}
        value={searchInput}
        onSubmitEditing={() => navigation.navigate('SearchResult')}
      />
    </View>
  );
}
