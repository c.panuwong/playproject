import React from 'react';
import {View, TouchableOpacity, Image} from 'react-native';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
import styled from 'styled-components';
import {DBHText} from '../../../components/StyledText';
export default function ChatFriend({
  menuRef,
  index,
  item,
  sendingType,
}: {
  menuRef: any;
  index: number;
  item: any;
  sendingType: any;
}) {
  const ModalText = () => {
    return (
      <Menu
        // renderer={Popover}
        rendererProps={{preferredPlacement: 'bottom'}}
        ref={menuRef}>
        <MenuTrigger>{/* <DBHText></DBHText> */}</MenuTrigger>
        <MenuOptions
          optionsContainerStyle={{width: 80, backgroundColor: 'null'}}>
          <ViewMenu>
            <MenuOption style={{borderColor: '#B5B5B5', borderRightWidth: 0.5}}>
              <DBHText style={{textAlign: 'center'}}>Reply</DBHText>
            </MenuOption>
            <MenuOption>
              <DBHText style={{textAlign: 'center'}}>Share</DBHText>
            </MenuOption>
          </ViewMenu>
        </MenuOptions>
      </Menu>
    );
  };
  return (
    <ViewComponent key={index} index={index}>
      <View style={{flexDirection: 'row'}}>
        <View style={{flex: 0.15, marginTop: 5}}>
          <Image
            source={{uri: item.avatar}}
            style={{width: 50, height: 50, borderRadius: 15}}
          />
        </View>
        <View style={{flex: 0.6, flexDirection: 'column', marginTop: 2}}>
          <View style={{flexDirection: 'row'}}>
            <DBHText style={{marginRight: '2.5%'}} color="rgba(47, 204, 75, 1)">
              {item.name}
            </DBHText>
            <DBHText style={{color: '#BDBDBD'}}>{item.time}</DBHText>
          </View>
          <View style={{flexDirection: 'row'}}>
            <BoxMassage
              onLongPress={() => menuRef.current.open()}
              type={item.type}>
              {sendingType(item)}
              <ModalText />
            </BoxMassage>
          </View>
        </View>
      </View>
    </ViewComponent>
  );
}
const ViewMenu = styled(View)`
  flex-direction: row;
  background-color: rgba(0, 0, 0, 1);
  border-width: 0.5px;
  border-color: #b5b5b5;
  border-radius: 8px;
`;
const ViewComponent = styled<any>(View)`
  flex-direction: column;
  margin-bottom: 1%;
  margin-top: ${props => (props.index == 0 ? '5' : 0)};
`;
const ViewMassage = styled(View)`
  flex: 0.8;
  align-items: flex-start;
`;
const BoxMassage = styled<any>(TouchableOpacity)`
  background-color: ${props =>
    props.type == 'typing' ? 'rgba(82, 82, 85, 1)' : 'null'};
  padding-vertical: 2%;
  padding-horizontal: 2%;
  border-radius: 10px;
  flex-direction: column;
`;
