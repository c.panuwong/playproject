import React, {useState} from 'react';
import {View, Image, TouchableHighlight} from 'react-native';
import {FlatList, TouchableOpacity} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components';
import {DBHText} from '../../../components/StyledText';
import {data} from '../../hamberger/data';
import SearchBar from './SearchBar';
interface IFriend {
  friend_list: Array<object>;
}
export default function ChatList({
  childRef,
  navigation,
  setpage,
  page,
}: {
  childRef: any;
  navigation: any;
  setpage: any;
  page: any;
}) {
  const [profileData, setProfileData] = useState(null);
  const [headerState, setHeaderState] = useState<string>('0');
  const [searchInput, setSearchInput] = useState<string>('');
  const [friend] = useState<IFriend>({
    friend_list: [
      {
        id: 'sdfrvxd@!IamThorsssssssss',
        name: 'Chris Hemsworth',
        description: 'I am Chris Hemsworth',
        lastest_message: 'วันนี้จะลง Content อะไรดี',
        timestamp: '02.30 pm',
        none_reading_message: 1,
        image_url:
          'https://edgar.ae/media/images/002_BOSS_ChrisHemsworth_SR21_e8yJBqm.original.jpg',
        accept: true,
        following: '10202',
        followers: '28928888',
        addFriend: true,
        request: false,
      },
      {
        id: 'dasdfmE!BiteBiteMan',
        name: 'Luis Suarez',
        description: 'I am Luis Suarez',
        lastest_message: 'วันนี้จะลง Content อะไรดี',
        timestamp: '01.30 am',
        none_reading_message: 42,
        image_url:
          'https://imgresizer.eurosport.com/unsafe/1200x0/filters:format(jpeg):focal(1448x313:1450x311)/origin-imgresizer.eurosport.com/2020/09/17/2888422-59476448-2560-1440.jpg',
        accept: true,
        following: '89355',
        followers: '122222',
        addFriend: false,
        request: true,
      },
      {
        id: 'W_T_F_WithDOGE?',
        name: 'Elon Musk',
        description: 'I am Elon Musk',
        lastest_message: 'วันนี้จะลง Content อะไรดี',
        timestamp: '08.30 am',
        none_reading_message: 99,
        image_url:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYEwYDOzZIGqRI3C-_-u1DzD700ABImSbkMg&usqp=CAU',
        accept: true,
        following: '99099',
        followers: '561155',
        addFriend: false,
        request: false,
      },
      {
        id: 'JordiTheStartBro',
        name: 'Jordi El Nino Polla',
        description: 'I am Jordi El Nino Polla',
        lastest_message:
          'วันนี้จะลง Content อะไรดี ffwdqwdqwdmwq;mdwq;kfmw;kqfmq;wkfmwqk;fmnwqklfnkwfffwdqwdqwdmwq;mdwq;kfmw;kqfmq;wkfmwqk;fmnwqklfnkwfffwdqwdqwdmwq;mdwq;kfmw;kqfmq;wkfmwqk;fmnwqklfnkwf',
        timestamp: '00.00 pm',
        none_reading_message: 4,
        image_url:
          'http://thumbsup.in.th/wp-content/uploads/2015/08/stevejobs1.jpg',
        accept: true,
        following: '1111111',
        followers: '28928888',
        addFriend: false,
        request: true,
      },
      {
        id: 'LeoNaldoWhosDidntGetAwardAndSad',
        name: 'Leonardo Dicaprio',
        description: 'I am Leonardo Dicaprio',
        lastest_message: 'วันนี้จะลง Content อะไรดี',
        timestamp: '00.00 pm',
        none_reading_message: 555,
        image_url:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTD03GmX_9KVrbxNvWtgjwVL9x44bYTVizhVg&usqp=CAU',
        accept: true,
        following: '190000',
        followers: '2898888',
        addFriend: true,
        request: false,
      },
      {
        id: 'Cutest_Angel_TayLoRSwift',
        name: 'Taylor Swift',
        description: 'I am Taylor Swift',
        lastest_message: 'วันนี้จะลง Content อะไรดี',
        timestamp: '00.00 pm',
        none_reading_message: 14,
        image_url: 'https://files.vogue.co.th/uploads/COVVERNN90.jpg',
        accept: false,
        following: '10222202',
        followers: '228888',
        addFriend: false,
        request: true,
      },
    ],
  });
  const renderItem = (item: any) => {
    if (page == 0 && item.request == false) {
      null;
    } else if (page == 1 && item.request == true) {
      null;
    } else {
      return <></>;
    }
    return (
      <>
        <ViewList
          onPress={() =>
            navigation.navigate('ChatStack', {
              screen: 'ChatRoom',
              params: {data: item},
            })
          }>
          <ViewImage>
            <Linear
              start={{x: 0.2, y: 1.5}}
              end={{x: 1.3, y: 0}}
              colors={['#257CE2', '#33D52B', '#99FF00']}>
              <ImageProfile source={{uri: item.image_url}} />
            </Linear>
          </ViewImage>
          <ViewTexts>
            <DBHText numberOfLines={1} size={24}>
              {item.name}
            </DBHText>
            <DBHText numberOfLines={1} size={18}>
              {item.lastest_message}
            </DBHText>
          </ViewTexts>
          <ViewCountMassage>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: 34,
                height: 18,
                backgroundColor: 'rgba(47, 204, 75, 1)',
                borderRadius: 8,
              }}>
              <DBHText>test</DBHText>
            </View>
            <View>
              <DBHText color="rgba(179, 179, 179, 1)" style={{bottom: -5}}>
                05:48
              </DBHText>
            </View>
          </ViewCountMassage>
        </ViewList>
      </>
    );
  };
  return (
    <View style={{width: '100%', backgroundColor: 'transparent'}}>
      <FlatList
        data={friend.friend_list}
        keyExtractor={(item: any) => item.id}
        renderItem={(item: any) => renderItem(item.item)}
        onScroll={e => {
          childRef.current.handleLogo(e);
        }}
        onEndReachedThreshold={0.05}
        onEndReached={() => console.log('ใกล้สุด')}
        ListHeaderComponent={() => (
          <Hexder
            page={page}
            setpage={setpage}
            navigation={navigation}
            setSearchInput={setSearchInput}
            searchInput={searchInput}
          />
        )}
      />
    </View>
  );
}

const Hexder = ({
  page,
  setpage,
  navigation,
  setSearchInput,
  searchInput,
}: {
  page: number;
  setpage: any;
  navigation: any;
  setSearchInput: any;
  searchInput: any;
}) => {
  return (
    <>
      {/* <DBHText
      size={34}
      color="white"
      style={{paddingHorizontal: 30, top: 15}}>
      {page == 0 ? 'Earnings' : 'Historys'}
    </DBHText> */}
      <ContainerButton>
        <ViewGradient
          start={{x: 0.3, y: 1}}
          end={{x: 1.0, y: 1}}
          colors={
            page == 0
              ? [
                  'rgba(37, 124, 226, 1)',
                  'rgba(51, 213, 43, 1)',
                  'rgba(153, 255, 0, 1)',
                ]
              : ['#000', '#000', '#000']
          }>
          <ViewButtonBG
            onPress={() => setpage(0)}
            activeOpacity={0.2}
            underlayColor="#283567"
            select={page == 0 ? true : false}>
            <DBHText size={24}>Message</DBHText>
          </ViewButtonBG>
        </ViewGradient>
        <ViewGradient2
          start={{x: 0.3, y: 1}}
          end={{x: 1.0, y: 1}}
          colors={
            page == 1
              ? [
                  'rgba(37, 124, 226, 1)',
                  'rgba(51, 213, 43, 1)',
                  'rgba(153, 255, 0, 1)',
                ]
              : ['#000', '#000', '#000']
          }>
          <ViewButtonBG2
            onPress={() => setpage(1)}
            activeOpacity={0.2}
            underlayColor="#283567"
            select={page == 1 ? true : false}>
            <DBHText size={24} color={page == 1 ? '#99FF00' : '#FFF'}>
              Request
            </DBHText>
          </ViewButtonBG2>
        </ViewGradient2>
      </ContainerButton>
      <View>
        <SearchBar
          navigation={navigation}
          setSearchInput={setSearchInput}
          searchInput={searchInput}
        />
      </View>
    </>
  );
};
const ViewList = styled(TouchableOpacity)`
  flex-direction: row;
  align-items: center;
  padding-vertical: 1.5%;
  padding-horizontal: 18px;
`;
const ViewTexts = styled(View)`
  flex: 0.68;
  padding-end: 10px;
`;
const ViewCountMassage = styled(View)`
  flex: 0.1;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
`;
const ViewImage = styled(View)`
  flex: 0.2;
`;
const Linear = styled(LinearGradient)`
  justify-content: center;
  align-items: center;
  align-content: center;
  border-radius: 17px;
  width: 57px;
  height: 52px;
  padding-vertical: 28px;
`;
const ImageProfile = styled(Image)`
  width: 51.51px;
  height: 51.51px;
  border-radius: 17px;
  border-width: 3px;
  border-color: #fff;
`;
const ContainerButton = styled(View)`
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-horizontal: 30px;
  margin-top: 30px;
  flex-direction: row;
`;
const ViewGradient = styled(LinearGradient)`
  width: 50%;
  height: 36px;
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
  align-items: center;
  justify-content: center;
  padding-horizontal: 2px;
`;
const ViewGradient2 = styled(LinearGradient)`
  width: 50%;
  height: 36px;
  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
  align-items: center;
  justify-content: center;
  padding-horizontal: 2px;
`;
const ViewButtonBG = styled<any>(TouchableHighlight)`
  width: 100%;
  height: 34px;
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
  align-items: center;
  justify-content: center;
  background-color: ${props =>
    props.select ? 'null' : 'rgba(79, 84, 113, 1)'};
`;
const ViewButtonBG2 = styled<any>(TouchableHighlight)`
  width: 100%;
  height: 34px;
  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
  align-items: center;
  justify-content: center;
  background-color: ${props =>
    props.select ? 'null' : 'rgba(79, 84, 113, 1)'};
`;
