import React from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { Typography } from "../StyledText";

const ChatBlockList = ({ list, navigation }) => {
  console.log(navigation);
  return (
    <View style={{ marginHorizontal: "2.5%" }}>
      <TouchableOpacity
        style={{
          flexDirection: "row",
          borderBottomWidth: 2,
          borderColor: "#444444",
          paddingVertical: "3%",
        }}
      >
        <View style={{ flex: 0.2 }}>
          <Image
            source={{ uri: list.image_url }}
            style={{ width: 70, height: 70, borderRadius: 50 }}
          />
        </View>
        <View style={{ flex: 0.6, marginLeft: "5%" }}>
          <Typography style={{ fontSize: 16 }}>{list.name}</Typography>
          <Typography>{list.description}</Typography>
        </View>
        <View style={{ flex: 0.2, alignItems: "center" }}>
          <Typography>{list.date}</Typography>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default ChatBlockList;
