import React, {useRef} from 'react';
import {View, TouchableOpacity, Image} from 'react-native';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
  renderers,
} from 'react-native-popup-menu';
import styled from 'styled-components';
import {DBHText} from '../../../components/StyledText';
const {Popover} = renderers;
export default function ChatMe({
  menuRef,
  index,
  item,
  sendingType,
}: {
  menuRef: any;
  index: number;
  item: any;
  sendingType: any;
}) {
  const elRefs = useRef<any>([]);

  const saveRef = (index: number) => (e: any) => {
    elRefs.current[index] = e;
  };
  const ModalText = ({index}: {index: number}) => {
    return (
      <Menu
        // renderer={Popover}
        rendererProps={{preferredPlacement: 'bottom'}}
        ref={saveRef(index)}>
        <MenuTrigger>{/* <DBHText></DBHText> */}</MenuTrigger>
        <MenuOptions
          optionsContainerStyle={{width: 80, backgroundColor: 'null'}}>
          <ViewMenu>
            <MenuOption style={{borderColor: '#B5B5B5', borderRightWidth: 0.5}}>
              <DBHText style={{textAlign: 'center'}}>Reply</DBHText>
            </MenuOption>
            <MenuOption>
              <DBHText style={{textAlign: 'center'}}>Share</DBHText>
            </MenuOption>
          </ViewMenu>
        </MenuOptions>
      </Menu>
    );
  };
  return (
    <ViewComponent key={index} index={index}>
      <View style={{flexDirection: 'row-reverse', alignItems: 'center'}}>
        <ViewMassage>
          <BoxMassage
            onLongPress={() => elRefs.current[index].open()}
            type={item.type}>
            {sendingType(item)}
          </BoxMassage>
          <ModalText index={index} />
        </ViewMassage>
      </View>
    </ViewComponent>
  );
}
const ViewMenu = styled(View)`
  flex-direction: row;
  background-color: rgba(0, 0, 0, 1);
  border-width: 0.5px;
  border-color: #b5b5b5;
  border-radius: 8px;
`;
const ViewComponent = styled<any>(View)`
  flex-direction: column;
  margin-bottom: 1%;
  justify-content: center;
  align-items: flex-end;
  margin-top: ${props => (props.index == 0 ? '5' : 0)};
`;
const BoxMassage = styled<any>(TouchableOpacity)`
  background-color: ${props =>
    props.type == 'typing' ? 'rgba(72, 83, 131, 1)' : 'null'};
  padding-vertical: 2%;
  padding-horizontal: 2%;
  border-radius: 10px;
  flex-direction: column;
  margin-right: 2.5%;
`;
const ViewMassage = styled(View)`
  flex: 0.8;
  align-items: flex-end;
  margin-left: 5%;
`;
