import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Animated,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faSlidersH, faBell} from '@fortawesome/pro-regular-svg-icons';
import {DBHText} from '../../../components/StyledText';
import {Modalize} from 'react-native-modalize';
import styled from 'styled-components';
export default function StickerModal({
  stickerHeight,
  stickerState,
  setStickerState,
  stickerList,
  onSendingSticker,
}: {
  stickerHeight: any;
  stickerState: number;
  setStickerState: any;
  stickerList: any;
  onSendingSticker: any;
}) {
  const StickerWithList = () => {
    return (
      <>
        {stickerList.map((sticker: any, index: number) => {
          return (
            <StickerList
              key={sticker._id}
              onPress={() => setStickerState(index)}
              stickerState={stickerState}
              index={index}>
              <Image source={sticker.sticker_assets} />
            </StickerList>
          );
        })}
      </>
    );
  };
  const StickerMapWithState = () => {
    if (stickerState === -1) {
      return (
        <View>
          <DBHText>Recent Sticker</DBHText>
        </View>
      );
    }
    return (
      <>
        {stickerList[stickerState].sticker_list_assets.map(
          (action: any, index: number) => {
            return (
              <TouchableOpacity
                onPress={() => onSendingSticker(action.sticker_action_image)}
                key={action.sticker_action_id}>
                <StickerBox source={action.sticker_action_image} />
              </TouchableOpacity>
            );
          },
        )}
      </>
    );
  };
  return (
    <Animated.View
      style={{
        width: '100%',
        height: stickerHeight,
        backgroundColor: 'black',
      }}>
      <ScrollView scrollEnabled>
        <ScrollView
          horizontal
          contentContainerStyle={{
            alignItems: 'center',
            marginLeft: '2%',
            marginTop: '2%',
          }}>
          <StateSticker onPress={() => setStickerState(-1)}>
            {/* <MaterialCommunityIcons
                name="clock-time-eight-outline"
                size={24}
                color="white"
              /> */}
          </StateSticker>
          <StickerWithList />
        </ScrollView>
        <ViewSticker>
          <StickerMapWithState />
        </ViewSticker>
      </ScrollView>
    </Animated.View>
  );
}
const StickerList = styled<any>(TouchableOpacity)`
  margin-right: 10px;
  border-radius: 50px;
  padding: 3.5px;
  background-color: ${props =>
    props.stickerState === props.index ? 'rgba(255,255,255, .3)' : 'null'};
`;
const StickerBox = styled(Image)`
  max-width: 150px;
  max-height: 150px;
  width: 80px;
  height: 100px;
  border-width: 1px;
  margin-horizontal: 2%;
  margin-vertical: 20%;
`;
const ViewSticker = styled(View)`
  flex-wrap: wrap;
  flex-direction: row;
  align-items: center;
  margin-horizontal: 3%;
`;
const StateSticker = styled<any>(TouchableOpacity)`
  margin-right: 10px;
  background-color: ${props =>
    props.stickerState === -1 ? 'rgba(255,255,255, .3)' : 'null'};
  border-radius: 50px;
  padding: 3.5px;
`;
