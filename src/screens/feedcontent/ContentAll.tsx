import React, {useEffect, useState, useRef} from 'react';
import {
  Text,
  SafeAreaView,
  Image,
  FlatList,
  Alert,
  TouchableOpacity,
  View,
  ImageBackground,
  Button,
  RefreshControl,
} from 'react-native';
// import FeedFlat from '../../components/home/FeedFlat';
// @ts-ignore
import ReadMore from '@fawazahmed/react-native-read-more';
import LinearGradient from 'react-native-linear-gradient';

// @ts-ignore
import {Col, Row} from 'react-native-easy-grid';
// @ts-ignore
import styled from 'styled-components';
import Modallize from './components/Modallize';
import {Modalize} from 'react-native-modalize';

import PostOnePhoto from './components/PostOnePhoto';
import PostTwoPhoto from './components/PostTwoPhoto';
import PostsThreePhoto from './components/PostsThreePhoto';
import PostFourPhoto from './components/PostFourPhoto';

import {getdatapagiante} from './crudaxios/getdata';
import jwt_decode from 'jwt-decode';
import moment from 'moment';
import 'moment/locale/th';
import {DBHText} from '../../components/StyledText';
moment.locale('th');

const ContentAll = ({navigation}: {navigation: any}) => {
  const [DATA, setDATA] = useState<any>([]);
  const modalizeRef = useRef<Modalize>(null);
  const [contents, setContents] = useState<any>([]);
  const [refreshing, setRefreshing] = useState(false);
  const [page, setpage] = useState<number>(1);
  const onOpenChangeImage = () => {
    modalizeRef.current?.open();
  };

  useEffect(() => {
    getcontent();
  }, []);
  const getcontent = async () => {
    setRefreshing(true);
    await getdatapagiante(1)
      .then(res => {
        setpage(2);
        // jwt_decode(res.data.data_encode).map((item: any) => {
        //   setContents((oldArray: any) => [...oldArray, item]);
        // });
        setContents(jwt_decode(res.data.data_encode));
        setRefreshing(false);
      })
      .catch(e => {
        console.log(e);
        setRefreshing(false);
      });
  };
  const getcontentandadd = async () => {
    await getdatapagiante(page)
      .then(res => {
        setpage(page + 1);
        // console.log('====================================');
        // console.log(jwt_decode(res.data.data_encode));
        // console.log('====================================');
        jwt_decode(res.data.data_encode).map((item: any) => {
          setContents((oldArray: any) => [...oldArray, item]);
        });
        // setContents((oldArray:any) => [...oldArray, jwt_decode(res.data.data_encode)]);
        // setContents(jwt_decode(res.data.data_encode));
        setRefreshing(false);
      })
      .catch(e => {
        console.log(e);
      });
  };
  const renderContent = () => (
    <View
      style={{
        backgroundColor: 'white',
        padding: 16,
        height: 450,
      }}>
      <Row>
        <Col>
          <TouchableOpacity onPress={() => onOpenChangeImage()}>
            <Text style={{alignSelf: 'flex-end'}}>close</Text>
          </TouchableOpacity>
        </Col>
      </Row>
    </View>
  );

  const Item = ({
    image,
    galaxy_name,
    create,
    galaxy_description_full,
    follow,
  }: {
    image: any;
    galaxy_name: any;
    create: any;
    galaxy_description_full: any;
    follow: any;
  }) => (
    <ViewWithBorder>
      <RowResult>
        <Col style={{width: '15%'}}>
          <LinearGradientImageGalaxy
            colors={[
              'rgba(15, 103, 206, 1)',
              'rgba(51, 213, 43, 1)',
              'rgba(153, 255, 0, 1)',
            ]}>
            <TouchableOpacity onPress={() => ClickHandle(navigation)}>
              <ImageGalaxy
                source={require('../../assets/images/mockimage/girlgalaxy.png')}
                resizeMode="cover"
              />
            </TouchableOpacity>
          </LinearGradientImageGalaxy>
        </Col>
        <Col style={{width: '70%'}}>
          <ColNameWithFollowLeft>
            <Row>
              <TouchableOpacity onPress={() => ClickHandle(navigation)}>
                <TextTitle numberOfLines={1}>
                  {galaxy_name[0]?.user_display_name || 'ไม่ระบุ'}
                </TextTitle>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => ClickHandle(navigation)}
                style={{marginLeft: 10}}>
                <View>{FollowCheck(follow)}</View>
              </TouchableOpacity>
            </Row>
          </ColNameWithFollowLeft>
          <Row>
            <TouchableOpacity onPress={() => ClickHandle(navigation)}>
              <TextDesc>
                {moment(create).subtract(1, 'days').calendar()}
              </TextDesc>
            </TouchableOpacity>
          </Row>
        </Col>
        <Col>
          <TouchableOpacity onPress={() => ClickHandle(navigation)}>
            <Imagegalaxymorebtn
              source={require('../../assets/images/galaxymorebtn.png')}
              resizeMode="cover"
            />
          </TouchableOpacity>
        </Col>
      </RowResult>
      <Row>
        {/* <ColWithBorderRightOnly style={{width: '60%'}}>
          <TouchableOpacity onPress={() => ClickHandle(navigation)}>
            <ImageFourOne
              source={require('../../assets/images/mockimage/girlgalaxy.png')}
              resizeMode="cover"
            />
          </TouchableOpacity>
        </ColWithBorderRightOnly>
        <Col style={{width: '40%'}}>
          <ColWithBorderLeftNBottom>
            <TouchableOpacity onPress={() => ClickHandle(navigation)}>
              <ImageFourTwo
                source={require('../../assets/images/mockimage/girlgalaxy.png')}
                resizeMode="cover"
              />
            </TouchableOpacity>
          </ColWithBorderLeftNBottom>
          <ColWithBorderLeftNBottom>
            <TouchableOpacity onPress={() => ClickHandle(navigation)}>
              <ImageFourTwo
                source={require('../../assets/images/mockimage/girlgalaxy.png')}
                resizeMode="cover"
              />
            </TouchableOpacity>
          </ColWithBorderLeftNBottom>
          <ColWithBorderLeftWithoutBottom>
            <TouchableOpacity onPress={() => ClickHandle(navigation)}>
              <ImageFourLast
                source={require('../../assets/images/mockimage/girlgalaxy.png')}
                resizeMode="cover">
                <ImageCenterFourLast
                  source={require('../../assets/images/mockimage/play.png')}
                  resizeMode="cover"
                />
              </ImageFourLast>
            </TouchableOpacity>
          </ColWithBorderLeftWithoutBottom>
        </Col> */}
        {image?.length == 1 ? (
          <PostOnePhoto navigation={navigation} url={image} />
        ) : image?.length == 2 ? (
          <PostTwoPhoto navigation={navigation} url={image} />
        ) : image?.length == 3 ? (
          <PostsThreePhoto navigation={navigation} url={image} />
        ) : image?.length >= 4 ? (
          <PostFourPhoto navigation={navigation} url={image} />
        ) : null}
      </Row>
      {/* <Row>
        <ColViewCount>
          <TouchableOpacity onPress={() => ClickHandle(navigation)}>
            <TextViewCount>View 99k</TextViewCount>
          </TouchableOpacity>
        </ColViewCount>
      </Row> */}
      <RowReadDesc>
        <ReadMore
          numberOfLines={3}
          seeMoreText="More"
          seeMoreStyle={{color: '#fff'}}
          seeLessText="Less"
          seeLessStyle={{color: '#fff'}}
          style={{width: '95%'}}>
          <TextRead>{galaxy_description_full}</TextRead>
        </ReadMore>
      </RowReadDesc>
      <Engagement>
        <EngagementBoxView>
          <ImagegalaxyEngagement
            source={require('../../assets/images/galaxyicon/likepostingalaxy.png')}
            resizeMode="cover"
          />
          <DBHText size={20}>99K</DBHText>
        </EngagementBoxView>
        <EngagementBoxView>
          <ImageGalaxyComment
            source={require('../../assets/images/galaxyicon/commenticon.png')}
            resizeMode="cover"
          />
          <DBHText size={20}>99K</DBHText>
        </EngagementBoxView>
        <EngagementBoxView>
          <ImageGalaxyShare
            source={require('../../assets/images/galaxyicon/shareicongalaxy.png')}
            resizeMode="cover"
          />
          <DBHText size={20}>99K</DBHText>
        </EngagementBoxView>
        <EngagementBoxView>
          <ImageBookmark
            source={require('../../assets/images/galaxyicon/bookmarkgalaxy.png')}
            resizeMode="cover"
          />
        </EngagementBoxView>
      </Engagement>
    </ViewWithBorder>
  );
  const renderItem = ({item}: {item: any}) => (
    <Item
      image={item.upload_files}
      galaxy_name={item?.user_info || 'ไม่ระบุ'}
      create={item.datetime_create}
      galaxy_description_full={item.description}
      follow={item.follow}
    />
  );

  const ClickHandle = (navigation: any) => {
    navigation.navigate('ImageMore');
  };

  const FollowCheck = (follow: any) => {
    if (follow == 1) {
      return (
        <TextFollow>
          <ImageFollowing
            source={require('../../assets/images/followingicon.png')}
            resizeMode="cover"
          />
          &nbsp;Following
        </TextFollow>
      );
    } else {
      return <TextFollow>+ Follow</TextFollow>;
    }
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#060c30'}}>
      <RowFeedGalaxy>
        <FlatList
          data={contents}
          renderItem={renderItem}
          keyExtractor={(item, index) => item._id}
          onEndReached={() => getcontentandadd()}
          onEndReachedThreshold={0.7}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={getcontent} />
          }
        />
      </RowFeedGalaxy>
      <Modallize modalizeRef={modalizeRef} navigation={navigation} />
    </SafeAreaView>
  );
};

const ViewWithBorder = styled(View)`
  border-bottom-width: 6px;
  border-color: #21263e;
`;
const RowFeedGalaxy = styled(Row)`
  width: 100%;
  margin-top: 50px;
`;
const RowEngagement = styled(Row)`
  width: 85%;
  align-self: center;
  border-bottom-width: 1px;
  border-color: #fff;
  padding-bottom: 15px;
  margin-top: 20px;
`;
const RowReadDesc = styled(Row)`
  width: 85%;
  align-self: center;
  margin-top: 7px;
`;
const ColViewCount = styled(Col)`
  width: 95%;
  margin-top: 10px;
`;
const ColWithBorderLeftNBottom = styled(Col)`
  border-left-width: 2px;
  border-bottom-width: 4px;
  border-color: #000;
`;
const ColWithBorderLeftWithoutBottom = styled(Col)`
  border-left-width: 2px;
  border-color: #000;
`;
const ColWithBorderRightOnly = styled(Col)`
  border-right-width: 4px;
  border-color: #000;
`;
const ColEngagement = styled(Col)`
  width: 31.66%;
`;
const ColTwoColumnLeft = styled(Col)`
  width: 30%;
`;
const ColTwoColumnRight = styled(Col)`
  width: 70%;
`;
const ColNameWithFollowLeft = styled(Col)`
  width: 65%;
`;
const ColNameWithFollowRight = styled(Col)`
  width: 70%;
`;
const ColEngagementBookmark = styled(Col)`
  width: 5%;
`;
const RowResult = styled(Row)`
  width: 100%;
  margin: 20px;
`;
const ImageGalaxy = styled(Image)`
  width: 50px;
  height: 50px;
  border-radius: 15px;
`;
const ImageFollowing = styled(Image)`
  width: 10px;
  height: 10px;
`;
const ImageFourOne = styled(Image)`
  width: 100%;
  height: 394px;
`;
const ImageFourTwo = styled(Image)`
  width: 100%;
  height: 128px;
`;
const ImageFourLast = styled(ImageBackground)`
  width: 100%;
  height: 128px;
  opacity: 0.6;
`;

const Imagegalaxymorebtn = styled(Image)`
  width: 3px;
  height: 15px;
  margin-top: 10px;
  align-self: center;
`;

const ImageCenterFourLast = styled(Image)`
  width: 36px;
  height: 36px;
  left: 40%;
  top: 40%;
`;

const TextViewCount = styled(Text)`
  color: #99ff00;
  font-size: 14px;
  align-self: flex-end;
`;
const TextTitle = styled(Text)`
  color: #fff;
  font-size: 18px;
`;
const TextRead = styled(Text)`
  color: #b3b3b3;
  font-size: 16px;
`;
const TextReadmore = styled(Text)`
  color: #fff;
  font-size: 16px;
`;
const TextEngagement = styled(Text)`
  color: #b3b3b3;
  font-size: 18px;
`;
const TextFollow = styled(Text)`
  color: #99ff00;
  font-size: 12px;
  margin-top: 5px;
`;
const TextDesc = styled(Text)`
  color: #aaa;
`;
const LinearGradientImageGalaxy = styled(LinearGradient)`
  padding: 2px;
  border-radius: 20px;
  width: 54px;
`;
const Engagement = styled(View)`
  height: 50px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-horizontal: 15px;
  margin-top: 10px;
  border-top-color: white;
  border-top-width: 1.5px;
  padding-vertical: 10px;
`;
const EngagementBoxView = styled(View)`
  flex: 0.3px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const ImagegalaxyEngagement = styled(Image)`
  width: 28.5px;
  height: 28px;
  margin-top: -5px;
  margin-right: 5px;
  margin-left: 7px;
`;
const ImageGalaxyComment = styled(Image)`
  width: 25px;
  height: 23.33px;
  margin-right: 5px;
  margin-left: 7px;
`;
const ImageGalaxyShare = styled(Image)`
  width: 30px;
  height: 22.26px;
  margin-right: 5px;
  margin-left: 7px;
`;
const ImageBookmark = styled(Image)`
  width: 16px;
  height: 19px;
  margin-top: 5px;
  align-self: flex-end;
`;
export default ContentAll;
