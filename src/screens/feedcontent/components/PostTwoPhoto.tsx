import React from 'react';
import {Image, Dimensions} from 'react-native';
import styled from 'styled-components';
import {Col, Row, Grid} from 'react-native-easy-grid';

const {width, height} = Dimensions.get('window');

export default function PostTwoPhoto({
  navigation,
  url,
}: {
  navigation: any;
  url: any;
}) {
  return (
    <Grid>
      <Col>
        <Image
          source={{uri: url[0].oss_file_url}}
          style={{width: '100%', height: height * 0.5}}
          resizeMode={'cover'}
        />
      </Col>
      <Col>
        <Image
          source={{uri: url[1].oss_file_url}}
          style={{width: '100%', height: height * 0.5}}
          resizeMode={'cover'}
        />
      </Col>
    </Grid>
  );
}
