import React from 'react';
import {View, Text, TouchableOpacity, Image, Dimensions} from 'react-native';
import styled from 'styled-components';

const {width, height} = Dimensions.get('window');

export default function PostOnePhoto({
  navigation,
  url,
}: {
  navigation: any;
  url: any;
}) {
  return (
    <ViewPost>
      <Image
        source={{uri:url[0].oss_file_url}}
        style={{width: width, height: height * 0.5}}
        resizeMode={'cover'}
      />
    </ViewPost>
  );
}
const ViewPost = styled(View)`
  flex: 1;
`;
const CloseIcon = styled(TouchableOpacity)`
  position: absolute;
  end: 3%;
  top: 3%;
`;
