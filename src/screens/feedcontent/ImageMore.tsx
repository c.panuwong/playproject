import React from 'react';
import {View, Text, FlatList, Image} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import styled from 'styled-components';
import {DBHText} from '../../components/StyledText';

export default function ImageMore() {
  const data = [
    {
      id: '1',
      image: require('../../assets/images/mockimage/Rectangle3751.png'),
      galaxy_name: 'OnlyFans',
      galaxy_description: 'เรื่องเสียว เสียวยันไข่',
      galaxy_description_full:
        'หัวเรื่องหรือพาดหัว (headline) เป็นข้อความที่อยู่บนสุดของบทความแสดงถึงประเด็นหลักของบทความ หัวเรื่องมักถูกออกแบบให้โดดเด่นสะดุดตา และใช้คำที่เลือกสรรมาให้เกี่ยวข้องกับหัวเรื่อง หัวเรื่องหรือพาดหัวมักจะถูกเขียนหรือพิมพ์ด้วยรูปแบบอย่างย่อ ซึ่งอาจเรียบเรียงอย่างไม่เป็นประโยคหรือไวยากรณ์ที่ไม่สมบูรณ์',
      follow: 1,
    },
    {
      id: '2',
      image: require('../../assets/images/mockimage/Rectangle3751.png'),
      galaxy_name: 'OnlyFans',
      galaxy_description: 'เรื่องเสียว เสียวยันไข่',
      galaxy_description_full:
        'หัวเรื่องหรือพาดหัว (headline) เป็นข้อความที่อยู่บนสุดของบทความแสดงถึงประเด็นหลักของบทความ หัวเรื่องมักถูกออกแบบให้โดดเด่นสะดุดตา และใช้คำที่เลือกสรรมาให้เกี่ยวข้องกับหัวเรื่อง หัวเรื่องหรือพาดหัวมักจะถูกเขียนหรือพิมพ์ด้วยรูปแบบอย่างย่อ ซึ่งอาจเรียบเรียงอย่างไม่เป็นประโยคหรือไวยากรณ์ที่ไม่สมบูรณ์',
      follow: 0,
    },
    {
      id: '3',
      image: require('../../assets/images/mockimage/Rectangle3751.png'),
      galaxy_name: 'OnlyFans',
      galaxy_description: 'เรื่องเสียว เสียวยันไข่',
      galaxy_description_full:
        'หัวเรื่องหรือพาดหัว (headline)เป็นข้อความที่อยู่บนสุดของบทความแสดงถึงประเด็นหลักของบทความ ประเด็นหลักของบทความ ประเด็นหลักของบทความ หัวเรื่องมักถูกออกแบบให้โดดเด่นสะดุดตา และใช้คำที่เลือกสรรมาให้เกี่ยวข้องกับหัวเรื่อง หัวเรื่องหรือพาดหัวมักจะถูกเขียนหรือพิมพ์ด้วยรูปแบบอย่างย่อ ซึ่งอาจเรียบเรียงอย่างไม่เป็นประโยคหรือไวยากรณ์ที่ไม่สมบูรณ์',
      follow: 0,
    },
  ];

  const renderItem = (item: any, index: any) => {
    return (
      <ImageContainer>
        <ImageView source={item.image} />
        <Engagement>
          <EngagementBoxView>
            <ImagegalaxyEngagement
              source={require('../../assets/images/galaxyicon/likepostingalaxy.png')}
              resizeMode="cover"
            />
            <DBHText size={20}>99K</DBHText>
          </EngagementBoxView>
          <EngagementBoxView>
            <ImageGalaxyComment
              source={require('../../assets/images/galaxyicon/commenticon.png')}
              resizeMode="cover"
            />
            <DBHText size={20}>99K</DBHText>
          </EngagementBoxView>
          <EngagementBoxView>
            <ImageGalaxyShare
              source={require('../../assets/images/galaxyicon/shareicongalaxy.png')}
              resizeMode="cover"
            />
            <DBHText size={20}>99K</DBHText>
          </EngagementBoxView>
        </Engagement>
      </ImageContainer>
    );
  };
  return (
    <Background>
      <FlatList
        data={data}
        keyExtractor={(item: any) => item.id}
        renderItem={(item: any) => renderItem(item.item, item.index)}
      />
    </Background>
  );
}
const Background = styled(SafeAreaView)`
  flex: 1;
  border-bottom-width: 6px;
  border-color: #21263e;
  background-color: #101531;
  padding-top: 50px;
`;
const ImageView = styled(Image)`
  width: 100%;
  height: 384px;
`;
const ImageContainer = styled(View)`
  width: 100%;
  margin-top: 20px;
`;
const Engagement = styled(View)`
  height: 50px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-horizontal: 15px;
  border-bottom-color: white;
  border-bottom-width: 1.5px;
`;
const EngagementBoxView = styled(View)`
  flex: 0.33px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const ImagegalaxyEngagement = styled(Image)`
  width: 28.5px;
  height: 28px;
  margin-top: -5px;
  margin-right: 5px;
`;
const ImageGalaxyComment = styled(Image)`
  width: 25px;
  height: 23.33px;
  margin-right: 5px;
`;
const ImageGalaxyShare = styled(Image)`
  width: 30px;
  height: 22.26px;
  margin-right: 5px;
`;
