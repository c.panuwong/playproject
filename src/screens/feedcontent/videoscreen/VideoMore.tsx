import React, {useRef} from 'react';
import {View, StyleSheet, FlatList, Image} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import styled from 'styled-components';
import {DBHText} from '../../../components/StyledText';
// @ts-ignore
import ReadMore from '@fawazahmed/react-native-read-more';
import HeaderResultPost from '../../../components/feeddata/HeaderResultPost';
import HeaderAnimated from '../../../components/HeaderAnimated';
const data = [
  {
    id: '1',
    image: require('../../../assets/images/mockimage/Rectangle3751.png'),
    galaxy_name: 'OnlyFans',
    galaxy_description: 'เรื่องเสียว เสียวยันไข่',
    galaxy_description_full:
      'หัวเรื่องหรือพาดหัว (headline) เป็นข้อความที่อยู่บนสุดของบทความแสดงถึงประเด็นหลักของบทความ หัวเรื่องมักถูกออกแบบให้โดดเด่นสะดุดตา และใช้คำที่เลือกสรรมาให้เกี่ยวข้องกับหัวเรื่อง หัวเรื่องหรือพาดหัวมักจะถูกเขียนหรือพิมพ์ด้วยรูปแบบอย่างย่อ ซึ่งอาจเรียบเรียงอย่างไม่เป็นประโยคหรือไวยากรณ์ที่ไม่สมบูรณ์',
    follow: 1,
  },
  {
    id: '2',
    image: require('../../../assets/images/mockimage/Rectangle3751.png'),
    galaxy_name: 'OnlyFans',
    galaxy_description: 'เรื่องเสียว เสียวยันไข่',
    galaxy_description_full:
      'หัวเรื่องหรือพาดหัว (headline) เป็นข้อความที่อยู่บนสุดของบทความแสดงถึงประเด็นหลักของบทความ หัวเรื่องมักถูกออกแบบให้โดดเด่นสะดุดตา และใช้คำที่เลือกสรรมาให้เกี่ยวข้องกับหัวเรื่อง หัวเรื่องหรือพาดหัวมักจะถูกเขียนหรือพิมพ์ด้วยรูปแบบอย่างย่อ ซึ่งอาจเรียบเรียงอย่างไม่เป็นประโยคหรือไวยากรณ์ที่ไม่สมบูรณ์',
    follow: 0,
  },
  {
    id: '3',
    image: require('../../../assets/images/mockimage/Rectangle3751.png'),
    galaxy_name: 'OnlyFans',
    galaxy_description: 'เรื่องเสียว เสียวยันไข่',
    galaxy_description_full:
      'หัวเรื่องหรือพาดหัว (headline)เป็นข้อความที่อยู่บนสุดของบทความแสดงถึงประเด็นหลักของบทความ ประเด็นหลักของบทความ ประเด็นหลักของบทความ หัวเรื่องมักถูกออกแบบให้โดดเด่นสะดุดตา และใช้คำที่เลือกสรรมาให้เกี่ยวข้องกับหัวเรื่อง หัวเรื่องหรือพาดหัวมักจะถูกเขียนหรือพิมพ์ด้วยรูปแบบอย่างย่อ ซึ่งอาจเรียบเรียงอย่างไม่เป็นประโยคหรือไวยากรณ์ที่ไม่สมบูรณ์',
    follow: 0,
  },
];

export default function VideoMore({navigation}: {navigation: any}) {
  const childRef = useRef<any>();
  const renderItem = (item: any, index: any) => {
    return (
      <>
        <VideoContainer>
          <HeaderResultPost
            navigation={navigation}
            galaxy_description="เรื่องของใคร??"
            galaxy_name="Songded"
            follow={1}
          />
          <VideoView source={item.image} />
          <DBHText
            size={18}
            color="#99FF00"
            style={{textAlign: 'right', paddingRight: 30, paddingTop: 10}}>
            View 99K
          </DBHText>
          <Engagement>
            <EngagementBoxView>
              <ImagegalaxyEngagement
                source={require('../../../assets/images/galaxyicon/likepostingalaxy.png')}
                resizeMode="cover"
              />
              <DBHText size={20}>99K</DBHText>
            </EngagementBoxView>
            <EngagementBoxView>
              <ImageGalaxyComment
                source={require('../../../assets/images/galaxyicon/commenticon.png')}
                resizeMode="cover"
              />
              <DBHText size={20}>99K</DBHText>
            </EngagementBoxView>
            <EngagementBoxView>
              <ImageGalaxyShare
                source={require('../../../assets/images/galaxyicon/shareicongalaxy.png')}
                resizeMode="cover"
              />
              <DBHText size={20}>99K</DBHText>
            </EngagementBoxView>
            <EngagementBoxView>
              <ImageBookmark
                source={require('../../../assets/images/galaxyicon/bookmarkgalaxy.png')}
                resizeMode="cover"
              />
            </EngagementBoxView>
          </Engagement>
        </VideoContainer>
        <ViewSubHeader>
          <TextSubDescription
            numberOfLines={3}
            seeMoreStyle={styles.seeMoreStyle}
            seeLessStyle={styles.seeLessStyle}
            seeMoreText="Read More"
            seeLessText="See Less">
            {
              ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Craspellentesque posuere augue at laoreet. Quisque ut justo aliquam, vestibulum nibh a, mattis dolor. Vestibulum at molestie lectus.Suspendisse blandit accumsan cursus. Quisque ullamcorper quis libero vel blandit. Sed et mauris sed magna viverra aliquet quis eget est. Pellentesque vitae ornare augue. Maecenas vel nisl arcu. '
            }
          </TextSubDescription>
        </ViewSubHeader>
      </>
    );
  };
  return (
    <Background>
      <HeaderAnimated
        ref={childRef}
        title="Content"
        navigation={navigation}
      />
      <FlatList
        data={data}
        keyExtractor={(item: any) => item.id}
        renderItem={(item: any) => renderItem(item.item, item.index)}
        onScroll={e => childRef.current.handleLogo(e)}
      />
    </Background>
  );
}
const Background = styled(SafeAreaView)`
  flex: 1;
  border-bottom-width: 6px;
  border-color: #21263e;
  background-color: #101531;
`;
const VideoView = styled(Image)`
  width: 100%;
  height: 384px;
`;
const VideoContainer = styled(View)`
  width: 100%;
`;
const Engagement = styled(View)`
  height: 40px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-horizontal: 15px;
  border-bottom-color: white;
  border-bottom-width: 1.5px;
`;
const EngagementBoxView = styled(View)`
  flex: 0.3px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const ImagegalaxyEngagement = styled(Image)`
  width: 28.5px;
  height: 28px;
  margin-top: -5px;
  margin-right: 5px;
  margin-left: 7px;
`;
const ImageGalaxyComment = styled(Image)`
  width: 25px;
  height: 23.33px;
  margin-right: 5px;
  margin-left: 7px;
`;
const ImageGalaxyShare = styled(Image)`
  width: 30px;
  height: 22.26px;
  margin-right: 5px;
  margin-left: 7px;
`;
const ImageBookmark = styled(Image)`
  width: 16px;
  height: 19px;
  margin-top: 5px;
  align-self: flex-end;
`;
const ViewSubHeader = styled(View)`
  background-color: #171c39;
  padding: 3% 3% 3% 5%;
`;
const TextSubDescription = styled(ReadMore)`
  font-family: DBHelvethaicaX-55Regular;
  font-size: 20px;
  margin-horizontal: 1%;
  color: #b3b3b3;
`;
const styles = StyleSheet.create({
  seeMoreStyle: {
    color: '#e7e7e7e7',
    fontSize: 18,
    alignSelf: 'center',
    fontFamily: 'DBHelvethaicaX-55Regular',
  },
  seeLessStyle: {
    color: '#e7e7e7e7',
    fontSize: 18,
    alignSelf: 'center',
    fontFamily: 'DBHelvethaicaX-55Regular',
  },
});
