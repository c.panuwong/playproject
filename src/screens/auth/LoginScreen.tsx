import React from 'react';
import {ImageBackground, Image, View, ScrollView} from 'react-native';
// @ts-ignore
import styled from 'styled-components';
import LoginForm from './component/LoginForm';
import LoginFacebook from './component/LoginFacebook';
import LoginGoogle from './component/LoginGoogle';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useRecoilState, useRecoilValue} from 'recoil';
import {username} from '../../recoil/auth';

const LoginScreen = ({navigation}: {navigation: any}) => {
  const [text, setText] = useRecoilState(username);

  return (
    <Background
      source={require('../../assets/images/BG_Login.jpg')}
      resizeMode="cover">
      <SafeAreaView style={{flex: 1, alignItems: 'center'}} edges={['top']}>
        <ScrollView>
          <ViewLoginIcom>
            <Icon
              source={require('../../assets/images/logo_login.png')}
              resizeMode="cover"></Icon>
          </ViewLoginIcom>
          <ViewLoginForm>
            <LoginForm navigation={navigation} />
          </ViewLoginForm>
          <ViewLoginFacebook>
            <LoginFacebook />
          </ViewLoginFacebook>
          <ViewLoginGoogle>
            <LoginGoogle />
          </ViewLoginGoogle>
        </ScrollView>
      </SafeAreaView>
    </Background>
  );
};

const Container = styled(View)`
  flex: 1;
`;
const ViewLoginIcom = styled(View)`
  width: 100%;
  height: 110px;
  align-items: center;
  justify-content: flex-end;
`;
const ViewLoginForm = styled(View)`
  width: 100%;
  height: 280px;
`;
const ViewLoginFacebook = styled(View)`
  width: 100%;
  height: 70px;
  margin-top: 30px;
`;
const ViewLoginGoogle = styled(View)`
  width: 100%;
  height: 15%;
`;
const Background = styled(ImageBackground)`
  flex: 1;
  padding: 30px;
  align-items: center;
`;
const Icon = styled(Image)`
  width: 75px;
  height: 75px;
`;
export default LoginScreen;
