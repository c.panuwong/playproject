import React, {useEffect, Suspense, useState} from 'react';
import {View, ImageBackground} from 'react-native';
// @ts-ignore
import styled from 'styled-components';
import {SafeAreaView} from 'react-native-safe-area-context';
import axios from 'axios';
import {DBHText} from '../../components/StyledText';
import {ScrollView} from 'react-native-gesture-handler';
import {ButtonDisble} from '../../components/ButtonComponent';
import { httpClient } from '../../utils/setupAxios';
import jwt_decode from "jwt-decode";
const Interrest = React.lazy(() => import('./component/Interrest'));
export default function InterestContent({navigation}: {navigation: any}) {
  const [dataitem, setdataitem] = useState<any>([]);
  const [itemselect, setItemSelect] = useState<any>([]);

  const getdata = async () => {
    console.log('ll');
    
    await httpClient
      .get(`/interests`)
      .then(res => {
        setdataitem(jwt_decode(res.data.data_encode));
      })
      .catch(e => {
        console.log(e);
      });
  };

  useEffect(() => {
    getdata();
  }, []);

  return (
    <Background
      source={require('../../assets/images/BG_Login.jpg')}
      resizeMode="cover">
      <SafeAreaView
        style={{
          flex: 1,
          alignItems: 'center',
          paddingHorizontal: 30,
          marginTop: 50,
        }}>
        <DBHText size={28} style={{textAlign: 'left', width: '100%'}}>
          What's your interest ?
        </DBHText>
        <ScrollView style={{width: '100%'}}>
          <Suspense fallback={<DBHText>....Loading</DBHText>}>
            {dataitem?.length ? (
              <Interrest
                dataitem={dataitem}
                itemselect={itemselect}
                setItemSelect={setItemSelect}
              />
            ) : null}
          </Suspense>
        </ScrollView>
      </SafeAreaView>
      <ViewButton>
        <ButtonDisble
          color={
            itemselect?.length
              ? ['#257CE2', '#33D52B', '#99FF00']
              : ['#7F8181', '#E7E7E7']
          }
          title="Next"
          padding={5}
          marginTop={0}
          onPress={() => {
            itemselect?.length ? navigation.navigate('PopCreater') : null;
          }}
        />
      </ViewButton>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
`;
const ViewButton = styled(View)`
  width: 100%;
  height: 12%;
  background-color: 'rgba(6,12,48, 0.8)';
  justify-content: center;
  padding-horizontal: 30px;
`;
