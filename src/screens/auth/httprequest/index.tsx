import {httpClient} from '../../../utils/setupAxios';

export const register = async (phone_number: string, password: string) => {
  return httpClient.post('/user/register', {
    telephone_number: phone_number,
    password: password,
  });
};
export const login = async (phone_number: string, password: string) => {
  return httpClient.post('/login', {
    username: phone_number,
    password: password,
  });
};
