import React, {useState, useEffect} from 'react';
import {View, ImageBackground, TextInput, Text} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
// @ts-ignore
import styled from 'styled-components';
import {DBHText} from '../../components/StyledText';
// @ts-ignore
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faPhoneAlt,
  faExclamationTriangle,
} from '@fortawesome/pro-regular-svg-icons';
import {ButtonDisble} from '../../components/ButtonComponent';
// @ts-ignore
import axios from 'axios';

export default function Register({navigation}: {navigation: any}) {
  const [input, setinput] = useState<string>('');
  const [error, setError] = useState<string>('');
  const [enablebutton, setEnableButton] = useState<boolean>(false);
  // const [OTP, setOTP] = useState<string>(
  //   Math.floor(100000 + Math.random() * 900000).toString(),
  // );
  const checklength = async () => {
    if (input.length >= 9) {
      if (input?.length == 10 && input?.split('')[0] == '0') {
        await setinput('66' + input.substring(1, input.length));
        setEnableButton(true);
      } else if (input?.length == 9 && input?.split('')[0] != '0') {
        await setinput('66' + input);
        setEnableButton(true);
      } else if (input?.length == 9 && input?.split('')[0] == '0') {
        setEnableButton(false);
      } else if (input?.length == 10 && input?.split('')[0] != '0') {
        setError('กรุณากรอกหมายเลขให้ถูกต้อง');
      }
    } else {
      setEnableButton(false);
    }
  };
  useEffect(() => {
    checklength();
  }, [input]);

  const sendOTP = async () => {
    const OTP = Math.floor(100000 + Math.random() * 900000).toString();
    setEnableButton(false);
    await axios
      .get('http://www.thsms.com/api/rest', {
        params: {
          method: 'send',
          username: 'supasand',
          password: 'RXWJxtam',
          from: 'DotPlay',
          to: input,
          message: `OTP ของคุณคือ ${OTP}`,
        },
      })
      .then((res:any) => {
        setEnableButton(true);
        navigation.navigate('RegisterOTP', {number: input, otp: OTP});
      })
      .catch((e:any) => {
        console.log(e);
        setEnableButton(true);
      });
  };
  return (
    <Background
      source={require('../../assets/images/BG_Login.jpg')}
      resizeMode="cover">
      <SafeAreaView style={{flex: 1, alignItems: 'center', padding: 30}}>
        <DBHText size={36}>Register</DBHText>
        <ViewInput>
          <FontAwesomeIcon
            icon={faPhoneAlt}
            size={18}
            color="#FFF"
            style={{position: 'absolute', paddingBottom: 44, marginLeft: 10}}
          />
          <NumberInput
            keyboardType="number-pad"
            placeholder="Phone Number"
            placeholderTextColor="white"
            maxLength={10}
            onChangeText={(text: any) => {
              setinput(text);
              setError('');
            }}
          />
          {error != '' ? (
            <FontAwesomeIcon
              icon={faExclamationTriangle}
              size={18}
              color="red"
              style={{position: 'absolute', paddingBottom: 44, right: 10}}
            />
          ) : null}
        </ViewInput>
        {error != '' ? <Error textValid={error} /> : null}
        <ViewButton>
          <ButtonDisble
            color={
              enablebutton && error == ''
                ? ['#257CE2', '#33D52B', '#99FF00']
                : ['#7F8181', '#E7E7E7']
            }
            title="Next"
            padding={5}
            marginTop={0}
            onPress={() => {
              enablebutton && error == '' ? sendOTP() : null;
            }}
          />
        </ViewButton>
      </SafeAreaView>
    </Background>
  );
}
const Error = (props: any) => {
  return <LabelError>{props.textValid}</LabelError>;
};
const Background = styled(ImageBackground)`
  flex: 1;
  padding-top: 30px;
`;
const LabelError = styled(Text)`
  margin-top: 4px;
  width: 100%;
  color: #ff0000;
  font-size: 15px;
  font-family: DBHelvethaicaX-55Regular;
`;
const ViewInput = styled(View)`
  flex-direction: row;
  align-items: flex-end;
  margin-top: 15px;
`;
const NumberInput = styled(TextInput)`
  border-bottom-color: white;
  border-bottom-width: 1px;
  padding-bottom: 5px;
  padding-left: 40px;
  margin-top: 20px;
  width: 100%
  color: white;
  font-family: DBHelvethaicaX-55Regular;
  font-size: 28px;
`;
const ViewButton = styled(View)`
  width: 100%;
  justify-content: center;
  margin-top: 120px;
`;
