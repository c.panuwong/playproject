import React from 'react';
// @ts-ignore
import styled from 'styled-components';
import {View} from 'react-native';
import {DBHText} from '../../../components/StyledText';
// @ts-ignore
import IconFB from 'react-native-vector-icons/MaterialIcons';
import {Alert} from 'react-native';
export default function LoginFacebook() {
  return (
    <Container>
      <ButtonFB onPress={() => Alert.alert('facebook')}>
        <IconFB
          name="facebook"
          size={30}
          color="white"
          style={{marginRight: 5}}
        />
        <DBHText size={20}>Log In with Facebook</DBHText>
      </ButtonFB>
    </Container>
  );
}
const Container = styled(View)`
  flex: 1;
`;
const ButtonFB = styled.TouchableOpacity`
  width: 100%;
  height: 52px;
  justify-content: center;
  align-items: center;
  background-color: #1877f2;
  border-radius: 25px;
  flex-direction: row;
`;
