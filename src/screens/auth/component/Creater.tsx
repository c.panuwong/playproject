import React, {useState} from 'react';
import {View, ImageBackground, TouchableOpacity, FlatList} from 'react-native';
// @ts-ignore
import styled from 'styled-components';
import {DBHText} from '../../../components/StyledText';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCheckCircle} from '@fortawesome/pro-regular-svg-icons';
import LinearGradient from 'react-native-linear-gradient';

export default function Creater({
  dataitem,
  itemselect,
  setItemSelect,
}: {
  dataitem: any;
  itemselect: any;
  setItemSelect: any;
}) {
  const checkitemselect = (id: any) => {
    if (itemselect.indexOf(id) > -1) {
      setItemSelect(itemselect.filter((x: any) => x != id));
    } else {
      setItemSelect((oldArray: any) => [...oldArray, id]);
    }
  };
  const indexdata = [0, 6, 10, 16, 20, 26, 30, 36, 40, 46, 50, 56, 60, 66];
  const renderItem = (item: any, index: any) => {
    return (
      <BoxImage
        key={index}
        itemselect={itemselect.indexOf(item._id) > -1 ? true : false}
        itemindex={indexdata.indexOf(index) > -1 ? true : false}>
        <TouchableOpacity onPress={() => checkitemselect(item._id)}>
          <ItemImage
            source={{
              uri: `http://{url}:4000${item.image_path}`,
            }}>
            {itemselect.indexOf(item._id) > -1 ? (
              <FontAwesomeIcon
                icon={faCheckCircle}
                size={18}
                color="#99FF00"
                style={{
                  position: 'absolute',
                  top: 10,
                  right: 10,
                }}
              />
            ) : null}

            <LinearGradient
              colors={['transparent', 'rgba(0,0,0,1)']}
              start={{x: 0, y: 0.5}}
              end={{x: 0, y: 1}}
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
              }}
            />
            <DBHText style={{left: 10, bottom: 7}} size={18}>
              {item.name}
            </DBHText>
          </ItemImage>
        </TouchableOpacity>
      </BoxImage>
    );
  };
  return (
    <>
      <View
        style={{width: '100%'}}>
        <FlatList
          data={dataitem}
          columnWrapperStyle={{ flexWrap: 'wrap', flex: 1}}
          renderItem={(item: any) => renderItem(item.item, item.index)}
          keyExtractor={(item: any) => item._id}
          numColumns={10}
        />
      </View>
    </>
  );
}
const BoxImage = styled(View)<any>`
  width: ${(props: any) => (props.itemindex ? '64%' : '31%')};
  height: 135px;
  border-radius: 10px;
  margin-top: 8px;
  margin-horizontal:3px;
  overflow: hidden;
  border-color: ${(props: any) => (props.itemselect ? '#99FF00' : '#FFF')};
  border-width: ${(props: any) => (props.itemselect ? '2px' : '0px')};
`;
const ItemImage = styled(ImageBackground)`
  width: 100%;
  height: 100%;
  border-radius: 10px;
  justify-content: flex-end;
  overflow: hidden;
`;
