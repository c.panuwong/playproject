import React, {useState} from 'react';
import {View, ImageBackground, TouchableOpacity} from 'react-native';
// @ts-ignore
import styled from 'styled-components';
import {Col, Row, Grid} from 'react-native-easy-grid';
import {DBHText} from '../../../components/StyledText';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCheckCircle} from '@fortawesome/pro-regular-svg-icons';
import LinearGradient from 'react-native-linear-gradient';
export default function Interrest({
  dataitem,
  itemselect,
  setItemSelect,
}: {
  dataitem: any;
  itemselect: any;
  setItemSelect: any;
}) {
  const checkitemselect = (id: any) => {
    if (itemselect.indexOf(id) > -1) {
      setItemSelect(itemselect.filter((x: any) => x != id));
    } else {
      setItemSelect((oldArray: any) => [...oldArray, id]);
    }
  };
  return (
    <>
      <Grid>
        <Col style={{marginRight: 3, marginTop: 55}}>
          {dataitem?.map((item: any, index: number) => {
            if (+index % 2 != 0) {
              return (
                <BoxImage
                  key={index}
                  itemselect={itemselect.indexOf(item._id) > -1 ? true : false}>
                  <TouchableOpacity onPress={() => checkitemselect(item._id)}>
                    <ItemImage
                      source={{
                        uri: `http://{url}:4000${item.image_path}`,
                      }}>
                      {itemselect.indexOf(item._id) > -1 ? (
                        <FontAwesomeIcon
                          icon={faCheckCircle}
                          size={18}
                          color="#99FF00"
                          style={{
                            position: 'absolute',
                            top: 10,
                            right: 10,
                          }}
                        />
                      ) : null}

                      <LinearGradient
                        colors={['transparent', 'rgba(0,0,0,1)']}
                        start={{x: 0, y: 0.5}}
                        end={{x: 0, y: 1}}
                        style={{
                          position: 'absolute',
                          top: 0,
                          left: 0,
                          right: 0,
                          bottom: 0,
                        }}
                      />
                      <DBHText style={{left: 10, bottom: 7}} size={18}>
                        {item.name}
                      </DBHText>
                    </ItemImage>
                  </TouchableOpacity>
                </BoxImage>
              );
            }
          })}
        </Col>
        <Col style={{marginLeft: 3}}>
          {dataitem?.map((item: any, index: number) => {
            if (+index % 2 == 0) {
              return (
                <BoxImage
                  key={index}
                  itemselect={itemselect.indexOf(item._id) > -1 ? true : false}>
                  <TouchableOpacity onPress={() => checkitemselect(item._id)}>
                    <ItemImage
                      source={{
                        uri: `http://{url}:4000${item.image_path}`,
                      }}>
                      {itemselect.indexOf(item._id) > -1 ? (
                        <FontAwesomeIcon
                          icon={faCheckCircle}
                          size={18}
                          color="#99FF00"
                          style={{
                            position: 'absolute',
                            top: 10,
                            right: 10,
                          }}
                        />
                      ) : null}
                      <LinearGradient
                        colors={['transparent', 'rgba(0,0,0,1)']}
                        start={{x: 0, y: 0.5}}
                        end={{x: 0, y: 1}}
                        style={{
                          position: 'absolute',
                          top: 0,
                          left: 0,
                          right: 0,
                          bottom: 0,
                        }}
                      />
                      <DBHText style={{left: 10, bottom: 7}} size={18}>
                        {item.name}
                      </DBHText>
                    </ItemImage>
                  </TouchableOpacity>
                </BoxImage>
              );
            }
          })}
        </Col>
      </Grid>
    </>
  );
}

const BoxImage = styled(View)<any>`
  width: 100%;
  height: 186px;
  border-radius: 10px;
  margin-top: 8px;
  overflow: hidden;
  border-color: ${(props: any) => (props.itemselect ? '#99FF00' : '#FFF')};
  border-width: ${(props: any) => (props.itemselect ? '2px' : '0px')};
`;
const ItemImage = styled(ImageBackground)`
  width: 100%;
  height: 100%;
  border-radius: 10px;
  justify-content: flex-end;
  overflow: hidden;
`;
