import React, {useState} from 'react';
import {View, TextInput, Text} from 'react-native';
// @ts-ignore
import styled from 'styled-components';
// @ts-ignore
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faUser, faLockAlt} from '@fortawesome/pro-regular-svg-icons';
// @ts-ignore
import Eye from 'react-native-vector-icons/Entypo';
import {ButtonGreen1} from '../../../components/ButtonComponent';
import {DBHText} from '../../../components/StyledText';
import {login} from '../httprequest';

import AsyncStorage from '@react-native-async-storage/async-storage';

export default function LoginForm({navigation}: {navigation: any}) {
  const [showpassword, setShowPassword] = useState<boolean>(false);
  const [form, setForm] = useState<any>({});
  const [errors, setErrors] = useState<any>({});
  const [datavalue, setDataValue] = useState<any>([
    {
      phone_number: '',
      password: '',
    },
  ]);
  const handleChange = (row: number, column: string, value: string) => {
    let copy: any = [...datavalue];
    copy[row][column] = value;
    setDataValue(copy);
  };
  const findFormErrors = () => {
    const {password, phone_number} = form;
    const newErrors: any = {};
    if (!phone_number || phone_number === '') {
      newErrors.errors = 'กรุณากรอกข้อมูลให้ครบถ้วน';
    } else if (!password || password === '') {
      newErrors.errors = 'กรุณากรอกข้อมูลให้ครบถ้วน';
    }
    return newErrors;
  };
  const setField = (field: string, value: string) => {
    setForm({
      ...form,
      [field]: value,
    });
    if (!!errors[field])
      setErrors({
        ...errors,
        [field]: null,
      });
  };
  const confirm = async (e: any) => {
    const newErrors = findFormErrors();
    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors);
    } else {
      await login(datavalue[0].phone_number, datavalue[0].password)
        .then(async res => {
          // console.log(res.data.data_encode);
          await AsyncStorage.setItem('token', res.data.data_encode);
          navigation.navigate('Index');
        })
        .catch(e => {
          console.log(e.status);
          setErrors({errors: 'ไม่พบข้อมูลผู้ใช้'});
        });
    }
  };
  return (
    <Container>
      <ViewUsername>
        <FontAwesomeIcon
          icon={faUser}
          size={18}
          color={errors.errors && errors.errors != '' ? 'red' : '#FFF'}
          style={{position: 'absolute', paddingBottom: 44, marginLeft: 10}}
        />
        <UsernameInput
          placeholder="Phone Number"
          placeholderTextColor="white"
          onChangeText={(text: any) => {
            handleChange(0, 'phone_number', text);
            setField('phone_number', text);
            setErrors('');
          }}
        />
      </ViewUsername>
      <ViewPassword>
        <FontAwesomeIcon
          icon={faLockAlt}
          size={18}
          color={errors.errors && errors.errors != '' ? 'red' : '#FFF'}
          style={{position: 'absolute', paddingBottom: 44, marginLeft: 10}}
        />

        <PasswordInput
          placeholder="Password"
          placeholderTextColor="white"
          secureTextEntry={showpassword}
          onChangeText={(text: any) => {
            handleChange(0, 'password', text);
            setField('password', text);
            setErrors('');
          }}
        />
        <Eye
          name={!showpassword ? 'eye-with-line' : 'eye'}
          color="white"
          size={18}
          style={{position: 'absolute', paddingBottom: 8, right: 17}}
          onPress={() => setShowPassword(!showpassword)}
        />
      </ViewPassword>
      {errors.errors && errors.errors != '' ? (
        <Error textValid={errors.errors} />
      ) : null}
      <ButtonGreen1
        onPress={(e: any) => confirm(e)}
        title="Login"
        padding={5}
        marginTop={20}
      />
      <ViewRegister>
        <DBHText size={20} onPress={() => navigation.navigate('Register')}>
          Register
        </DBHText>
        <DBHText size={20}>Forgot Pass</DBHText>
      </ViewRegister>
      <ViewOR>
        <DBHText size={22}>OR</DBHText>
      </ViewOR>
    </Container>
  );
}
const Error = (props: any) => {
  return <LabelError>{props.textValid}</LabelError>;
};
const LabelError = styled(Text)`
  margin-top: 4px;
  margin-left: 20px;
  width: 100%;
  color: #ff0000;
  font-size: 15px;
  font-family: DBHelvethaicaX-55Regular;
`;
const Container = styled(View)`
  flex: 1;
  margin-top: 15px;
  justify-content: center;
`;
const ViewUsername = styled(View)`
  flex-direction: row;
  align-items: flex-end;
`;
const ViewPassword = styled(View)`
  flex-direction: row;
  align-items: flex-end;
  margin-top: 15px;
`;
const ViewRegister = styled(View)`
  justify-content: space-between;
  flex-direction: row;
  align-items: flex-end;
  margin-top: 5px;
`;
const ViewOR = styled(View)`
  align-items: center;
  margin-top: 10px;
`;
const UsernameInput = styled(TextInput)`
  width: 100%
  border-bottom-color: white;
  border-bottom-width: 1px;
  padding-bottom: 3.5px;
  padding-left: 40px;
  color: white;
  font-family: DBHelvethaicaX-55Regular;
  font-size: 20px;
`;
const PasswordInput = styled(TextInput)`
  border-bottom-color: white;
  border-bottom-width: 1px;
  padding-bottom: 3.5px;
  padding-left: 40px;
  margin-top: 20px;
  width: 100%
  color: white;
  font-family: DBHelvethaicaX-55Regular;
  font-size: 20px;
`;
