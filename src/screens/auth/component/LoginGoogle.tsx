import React from 'react';
// @ts-ignore
import styled from 'styled-components';
import {TouchableOpacity, Image, View} from 'react-native';
import {DBHText} from '../../../components/StyledText';
// @ts-ignore
import IconFB from 'react-native-vector-icons/MaterialIcons';
import {Alert} from 'react-native';
export default function LoginGoogle() {
  return (
    <Container>
      <ButtonGoogle onPress={() => Alert.alert('facebook')}>
        <ImageGoogle
          source={require('../../../assets/images/Google_Logo.png')}
        />
        <DBHText size={20} color="#000" style={{left: -18}}>
          Log In with Google
        </DBHText>
      </ButtonGoogle>
    </Container>
  );
}
const Container = styled(View)`
  flex: 1;
`;
const ButtonGoogle = styled(TouchableOpacity)`
  width: 100%;
  height: 52px;
  justify-content: center;
  align-items: center;
  background-color: #fff;
  border-radius: 25px;
  flex-direction: row;
`;
const ImageGoogle = styled(Image)`
  width: 25px;
  height: 25px;
  justify-content: center;
  align-items: center;
  margin-right: 25px;
`;
