import React, {useState} from 'react';
import {View, Alert,ImageBackground} from 'react-native';
import {Checkbox} from 'react-native-paper';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView} from 'react-native-gesture-handler';
// @ts-ignore
import styled from 'styled-components';
import {DBHText} from '../../components/StyledText';
import agreement from './component/Agreement';
import {ButtonDisble} from '../../components/ButtonComponent';

interface ScrollProps {
  layoutMeasurement: any;
  contentOffset: any;
  contentSize: any;
}
const isCloseToBottom = ({
  layoutMeasurement,
  contentOffset,
  contentSize,
}: ScrollProps) => {
  const paddingToBottom = 20;
  return (
    layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom
  );
};

export default function AgreementScreen({navigation}: {navigation: any}) {
  const [checkboxdisvle, setcheckboxDisble] = useState<boolean>(true);
  const [checkbox, setcheckbox] = useState<boolean>(false);
  return (
    <Background
      source={require('../../assets/images/BG_Login.jpg')}
      resizeMode="cover">
      <SafeAreaView style={{flex: 1, alignItems: 'center'}} edges={['top']}>
        <DBHText size={36}>Agreement</DBHText>
        <ViewAgreement>
          <ScrollView
            scrollEventThrottle={16}
            onScroll={({nativeEvent}) => {
              if (isCloseToBottom(nativeEvent)) {
                setcheckboxDisble(false);
              }
            }}>
            <DBHText size={22}>{agreement.agreement}</DBHText>
          </ScrollView>
          <ViewCheckbox>
            <Checkbox.Android
              status={checkbox ? 'checked' : 'unchecked'}
              disabled={checkboxdisvle}
              onPress={() => {
                setcheckbox(!checkbox);
              }}
              color="#1ADF45"
              uncheckedColor="white"
            />
            <DBHText size={22}>Accept Policy</DBHText>
          </ViewCheckbox>
        </ViewAgreement>
        <ViewButton>
          <ButtonDisble
            color={
              checkbox
                ? ['#257CE2', '#33D52B', '#99FF00']
                : ['#7F8181', '#E7E7E7']
            }
            title="Next"
            padding={5}
            marginTop={0}
            onPress={() => {
              checkbox
                ? navigation.navigate('Register')
                : Alert.alert('คำเตือน', 'กรุณายอมรับเงื่อนไข');
            }}
          />
        </ViewButton>
      </SafeAreaView>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
  padding-top: 30px;
`;
const ViewAgreement = styled(View)`
  width: 100%;
  height: 80%;
  padding-horizontal: 30px;
  background-color: 'rgba(255,255,255,0.1)';
`;
const ViewButton = styled(View)`
  width: 100%;
  height: 15%;
  background-color: 'rgba(6,12,48, 0.8)';
  justify-content: center;
`;
const ViewCheckbox = styled(View)`
  width: 100%;
  flex-direction: row;
  align-items: center;
  padding-top: 10px;
`;
