import React, {useState} from 'react';
import {ImageBackground, TextInput, Text, View, Image} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faLockAlt} from '@fortawesome/pro-regular-svg-icons';
import {ButtonGreen1} from '../../components/ButtonComponent';
// @ts-ignore
import styled from 'styled-components';
import {DBHText} from '../../components/StyledText';
import {register} from './httprequest';
export default function CreatePassword({
  navigation,
  route,
}: {
  navigation: any;
  route: any;
}) {
  const {number} = route.params;
  const [form, setForm] = useState<any>({});
  const [errors, setErrors] = useState<any>({});
  const [datavalue, setDataValue] = useState<any>([
    {
      password: '',
      password_confirmation: '',
    },
  ]);
  const handleChange = (row: number, column: string, value: string) => {
    let copy: any = [...datavalue];
    copy[row][column] = value;
    setDataValue(copy);
  };
  const findFormErrors = () => {
    const {password, confirm_password} = form;
    const newErrors: any = {};
    if (!password || password === '') {
      newErrors.password = 'กรุณากรอกข้อมูลให้ครบถ้วน';
    } else if (!confirm_password || confirm_password === '') {
      newErrors.password = 'กรุณากรอกข้อมูลให้ครบถ้วน';
    } else if (confirm_password != password) {
      newErrors.password = 'Password not Match';
    }
    return newErrors;
  };
  const setField = (field: string, value: string) => {
    setForm({
      ...form,
      [field]: value,
    });
    if (!!errors[field])
      setErrors({
        ...errors,
        [field]: null,
      });
  };
  const confirm = async (e: any) => {
    const newErrors = findFormErrors();
    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors);
    } else {
      await register(number, datavalue[0].password)
        .then(res => {
          console.log(res);
          navigation.navigate('InterestContent');
        })
        .catch(e => console.log(e));
    }
  };
  return (
    <Background
      source={require('../../assets/images/BG_Login.jpg')}
      resizeMode="cover">
      <SafeAreaView style={{flex: 1, alignItems: 'center', padding: 30}}>
        <ImageIcon source={require('../../assets/images/create_pass.png')} />
        <DBHText size={28}>Create your password </DBHText>
        <ViewPassword>
          <FontAwesomeIcon
            icon={faLockAlt}
            size={18}
            color={errors.password && errors.password != '' ? 'red' : '#FFF'}
            style={{position: 'absolute', paddingBottom: 44, marginLeft: 10}}
          />

          <PasswordInput
            placeholder="Password"
            placeholderTextColor="white"
            secureTextEntry
            onChangeText={(text: any) => {
              handleChange(0, 'password', text);
              setField('password', text);
              setErrors('');
            }}
          />
        </ViewPassword>
        <ViewConfirmPassword>
          <FontAwesomeIcon
            icon={faLockAlt}
            size={18}
            color={errors.password && errors.password != '' ? 'red' : '#FFF'}
            style={{position: 'absolute', paddingBottom: 44, marginLeft: 10}}
          />
          <ConfirmPasswordInput
            placeholder="Password"
            placeholderTextColor="white"
            secureTextEntry
            onChangeText={(text: any) => {
              handleChange(0, 'password_confirmation', text);
              setField('confirm_password', text);
              setErrors('');
            }}
          />
        </ViewConfirmPassword>

        {errors.password && errors.password != '' ? (
          <Error textValid={errors.password} />
        ) : null}
        <ViewButton>
          <ButtonGreen1
            onPress={(e: any) => confirm(e)}
            title="Create an Account"
            padding={5}
            marginTop={20}
          />
        </ViewButton>
      </SafeAreaView>
    </Background>
  );
}
const Error = (props: any) => {
  return <LabelError>{props.textValid}</LabelError>;
};
const Background = styled(ImageBackground)`
  flex: 1;
  padding-top: 30px;
`;
const LabelError = styled(Text)`
  margin-top: 4px;
  margin-left: 20px;
  width: 100%;
  color: #ff0000;
  font-size: 15px;
  font-family: DBHelvethaicaX-55Regular;
`;
const ImageIcon = styled(Image)`
  width: 120px;
  height: 120px;
  margin-bottom: 33px;
`;
const PasswordInput = styled(TextInput)`
  border-bottom-color: white;
  border-bottom-width: 1px;
  padding-bottom: 6px;
  padding-left: 40px;
  margin-top: 20px;
  width: 100%
  color: white;
  font-family: DBHelvethaicaX-55Regular;
  font-size: 22px;
`;
const ConfirmPasswordInput = styled(TextInput)`
  border-bottom-color: white;
  border-bottom-width: 1px;
  padding-bottom: 6px;
  padding-left: 40px;
  margin-top: 20px;
  width: 100%
  color: white;
  font-family: DBHelvethaicaX-55Regular;
  font-size: 22px;
`;
const ViewButton = styled(View)`
  width: 100%;
  padding-top: 25px;
`;
const ViewPassword = styled(View)`
  flex-direction: row;
  align-items: flex-end;
  margin-top: 20px;
`;
const ViewConfirmPassword = styled(View)`
  flex-direction: row;
  align-items: flex-end;
  margin-top: 10px;
`;
