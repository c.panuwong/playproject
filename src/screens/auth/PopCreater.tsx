import React, {Suspense, useState, useEffect} from 'react';
import {View, ImageBackground} from 'react-native';
// @ts-ignore
import styled from 'styled-components';
import {SafeAreaView} from 'react-native-safe-area-context';
import axios from 'axios';
import {DBHText} from '../../components/StyledText';
import {ButtonDisble} from '../../components/ButtonComponent';

const Creater = React.lazy(() => import('./component/Creater'));

export default function PopCreater({navigation}: {navigation: any}) {
  const [dataitem, setdataitem] = useState<any>([]);
  const [itemselect, setItemSelect] = useState<any>([]);

  const getdata = async () => {
    await axios
      .get(`http://{url}:4000/interests`)
      .then(res => setdataitem(res.data))
      .catch(e => {
        console.log(e);
      });
  };

  useEffect(() => {
    getdata();
  }, []);
  return (
    <Background
      source={require('../../assets/images/BG_Login.jpg')}
      resizeMode="cover">
      <SafeAreaView
        style={{
          flex: 1,
          alignItems: 'center',
          paddingHorizontal: 30,
          marginTop: 50,
          marginBottom:45
        }}>
        <DBHText size={28} style={{textAlign: 'left', width: '100%'}}>
          Pop Creater
        </DBHText>
        {/* <ScrollView style={{width: '100%'}}> */}
        <Suspense fallback={<DBHText>....Loading</DBHText>}>
          <Creater
            dataitem={dataitem}
            itemselect={itemselect}
            setItemSelect={setItemSelect}
          />
        </Suspense>
        {/* </ScrollView> */}
      </SafeAreaView>
      <ViewButton>
        <ButtonDisble
          color={
            itemselect?.length
              ? ['#257CE2', '#33D52B', '#99FF00']
              : ['#7F8181', '#E7E7E7']
          }
          title="Next"
          padding={30}
          marginTop={0}
          onPress={() => {
            itemselect?.length ? navigation.navigate('Index') : null;
          }}
        />
      </ViewButton>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
`;
const ViewButton = styled(View)`
  width: 100%;
  height: 12%;
  background-color: 'rgba(6,12,48, 0.8)';
  justify-content: center;
  padding-horizontal: 30px;
`;
