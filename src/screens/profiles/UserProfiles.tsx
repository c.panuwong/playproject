/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  Button,
  TouchableOpacity,
  Alert,
  ImageBackground,
  ScrollView,
  SafeAreaView,
  FlatList,
  useWindowDimensions,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
import styled from 'styled-components';
// import { data } from './data';
import ProfileUser from './components/profiles/ProfileUser';
import {
  faBell,
  faStream,
  faPlayCircle,
  faImagePolaroid,
  faShoppingBasket,
} from '@fortawesome/pro-regular-svg-icons';
import {faPlusCircle} from '@fortawesome/pro-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import ToptabVideoScreen from '../profiles/components/toptapScreen/ToptabVideoScreen';
import ToptabImageScreen from '../profiles/components/toptapScreen/ToptabImageScreen';
import ToptabShopScreen from '../profiles/components/toptapScreen/ToptabShopScreen';
import select_interest from '../../assets/images/select_interest&creator.png';

import {TabView, SceneMap, TabBar} from 'react-native-tab-view';

const initialLayout = {width: Dimensions.get('window').width};

export default function UserProfiles({navigation}: {navigation: any}) {
  const FirstRoute = () => (
    <View>
      <ToptabVideoScreen navigation={navigation} />
    </View>
  );

  const SecondRoute = () => (
    <View>
      <ToptabImageScreen navigation={navigation} />
    </View>
  );
  const ThirdRoute = () => (
    <View>
      <ToptabShopScreen navigation={navigation} />
    </View>
  );

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: ThirdRoute,
  });

  const TabViewExample = () => {
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
      {key: 'first', title: '', icons: faPlayCircle},
      {key: 'second', title: '', icons: faImagePolaroid},
      {key: 'third', title: '', icons: faShoppingBasket},
    ]);

    const renderIcon = ({route, focused}) => {
      let iconColor = focused ? '#99ff00' : '#ffffff';
      return <FontAwesomeIcon icon={route.icons} size={20} color={iconColor} />;
    };

    return (
      <TabViewStyle
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={initialLayout}
        style={styles.container}
        renderTabBar={props => (
          <TabBar
            {...props}
            renderIcon={renderIcon}
            indicatorStyle={{backgroundColor: null}}
            style={{backgroundColor: null}}
          />
        )}
      />
    );
  };
  const DATA = [
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28bsdsa',
      title: '#First Item',
      url: 'https://www.pikpng.com/pngl/m/514-5147505_green-square-png-illustration-clipart.png',
    },
  ];
  const renderItem = ({item}: {item: any}) => (
    <Row>
      <TabViewExample />
    </Row>
  );
  const FloatinButton = () => {
    return (
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('Product', {screen: 'CreateProduct'})
        }
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          width: 70,
          position: 'absolute',
          bottom: '10%',
          right: 10,
          height: 70,
        }}>
        <FontAwesomeIcon
          icon={faPlusCircle}
          size={58}
          color="#99FF00"
          style={{
            shadowColor: '#000',
            shadowOffset: {width: 0, height: 2},
            shadowOpacity: 0.8,
            shadowRadius: 2,
          }}
        />
      </TouchableOpacity>
    );
  };
  return (
    <Grid>
      <Background
        source={require('../../assets/images/bg_otherUser.png')}
        resizeMode="cover">
        <FlatList
          data={DATA}
          ListHeaderComponent={() => (
            <>
              <ProfileUser navigation={navigation} />
            </>
          )}
          keyExtractor={(item, index) => item.id + index}
          listKey="99999"
          renderItem={renderItem}
        />
        <FloatinButton />
      </Background>
    </Grid>
  );
}
const styles = StyleSheet.create({
  container: {
    height: 781,
  },
  scene: {
    flex: 1,
  },
  bubble: {
    color: '#99ff00',
  },
  noLabel: {
    color: '#fff',
  },
});

const TabViewStyle = styled(TabView)`
  background-color: #171c39;
back
`;

const Container = styled(View)`
  flex: 1;
`;
const RowProfile = styled(Row)`
  width: 100%;
  align-content: center;
  justify-content: center;
`;
const Background = styled(ImageBackground)`
  flex: 1;
`;
const RowHeader = styled(Row)`
  padding: 0 20px 0 20px;
  width: 100%;
  height: 60px;
`;
const ColHeader = styled(Col)`
  align-content: center;
  justify-content: center;
`;
const ImageHeader = styled(Image)`
  width: 30px;
  height: 30px;
`;

const ColHeaderRightIcon = styled(Row)`
  align-items: center;
  justify-content: flex-end;
`;
const ViewStyled = styled(View)`
  margin-right: 15px;
`;
const ViewCircle = styled(View)`
  position: absolute;
  width: 8px;
  height: 8px;
  border-radius: 4px;
  background-color: #99ff00;
  margin: 2px 18px;
`;
const OnTouchableOpacity = styled(TouchableOpacity)``;
const RowContent = styled(Row)`
  height: 200%;
  border-color: #f666;
  border-width: 1px;
`;
