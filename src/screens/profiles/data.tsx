export const data = {
  type: 'other',
  user_name: 'ยูสเซอร์เนม',
  description:
    'แดนซ์ตาปรือ เวสต์ แฟรนไชส์ครูเสด อุด้งอพาร์ตเมนต์ ก่อนหน้าคาเฟ่ฟลุตสต็อกเซ็กส์ ม้านั่งนาฏย ',
  followers: '999099',
  following: '999999',
  user_profile:
    'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
  video: [
    {
      view: '8k',
      keys: '1',
      url: 'https://images.pexels.com/photos/2499628/pexels-photo-2499628.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      view: '3k',
      keys: '2',
      url: 'https://images.pexels.com/photos/3866555/pexels-photo-3866555.png?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      view: '99k',
      keys: '3',
      url: 'https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
    },
    {
      view: '98k',
      keys: '4',
      url: 'https://images.pexels.com/photos/3275029/pexels-photo-3275029.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      view: '99k',
      keys: '5',
      url: 'https://images.pexels.com/photos/7038116/pexels-photo-7038116.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      view: '9k',
      keys: '6',
      url: 'https://images.pexels.com/photos/775358/pexels-photo-775358.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      view: '9k',
      keys: '7',
      url: 'https://images.pexels.com/photos/8447299/pexels-photo-8447299.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      view: '99k',
      keys: '8',
      url: 'https://images.pexels.com/photos/8647550/pexels-photo-8647550.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      view: '99k',
      keys: '9',
      url: 'https://images.pexels.com/photos/594610/pexels-photo-594610.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      view: '99k',
      keys: '10',
      url: 'https://images.pexels.com/photos/8447299/pexels-photo-8447299.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      view: '99k',
      keys: '11',
      url: 'https://images.pexels.com/photos/8647550/pexels-photo-8647550.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      view: '99k',
      keys: '12',
      url: 'https://images.pexels.com/photos/594610/pexels-photo-594610.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    // {
    //   view: '8k',
    //   keys: '13',
    //   url: 'https://images.pexels.com/photos/2499628/pexels-photo-2499628.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    // },
    // {
    //   view: '14k',
    //   keys: '14',
    //   url: 'https://images.pexels.com/photos/3866555/pexels-photo-3866555.png?auto=compress&cs=tinysrgb&dpr=2&w=500',
    // },
    // {
    //   view: '99k',
    //   keys: '15',
    //   url: 'https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
    // },
    // {
    //   view: '98k',
    //   keys: '16',
    //   url: 'https://images.pexels.com/photos/3275029/pexels-photo-3275029.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    // },
    // {
    //   view: '99k',
    //   keys: '17',
    //   url: 'https://images.pexels.com/photos/7038116/pexels-photo-7038116.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    // },
    // {
    //   view: '9k',
    //   keys: '18',
    //   url: 'https://images.pexels.com/photos/775358/pexels-photo-775358.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    // },
    // {
    //   view: '9k',
    //   keys: '18',
    //   url: 'https://images.pexels.com/photos/8447299/pexels-photo-8447299.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    // },
    // {
    //   view: '99k',
    //   keys: '20',
    //   url: 'https://images.pexels.com/photos/8647550/pexels-photo-8647550.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    // },
    // {
    //   view: '99k',
    //   keys: '21',
    //   url: 'https://images.pexels.com/photos/594610/pexels-photo-594610.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    // },
    // {
    //   view: '99k',
    //   keys: '22',
    //   url: 'https://images.pexels.com/photos/8447299/pexels-photo-8447299.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    // },
    // {
    //   view: '99k',
    //   keys: '23',
    //   url: 'https://images.pexels.com/photos/8647550/pexels-photo-8647550.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    // },
    // {
    //   view: '99k',
    //   keys: '24',
    //   url: 'https://images.pexels.com/photos/594610/pexels-photo-594610.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    // },
  ],
  shop: [
    {
      name_product: 'Fabrizioriva',
      price: '2000',
      keys: '1',
      url: 'https://images.pexels.com/photos/1050244/pexels-photo-1050244.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'FRUIT',
      price: '100',
      keys: '2',
      url: 'https://images.pexels.com/photos/4856590/pexels-photo-4856590.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Cosmatics set',
      price: '55000',
      keys: '3',
      url: 'https://images.pexels.com/photos/2536965/pexels-photo-2536965.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },

    {
      name_product: 'Stone Stone',
      price: '289',
      keys: '4',
      url: 'https://images.pexels.com/photos/4040599/pexels-photo-4040599.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Bag',
      price: '30590',
      keys: '5',
      url: 'https://images.pexels.com/photos/904350/pexels-photo-904350.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'ShoeS',
      price: '6592',
      keys: '6',
      url: 'https://images.pexels.com/photos/336372/pexels-photo-336372.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Bunch OF Flowers',
      price: '300',
      keys: '7',
      url: 'https://images.pexels.com/photos/4467139/pexels-photo-4467139.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'coffee & tea',
      price: '78',
      keys: '8',
      url: 'https://images.pexels.com/photos/1854652/pexels-photo-1854652.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'ShoeS',
      price: '6592',
      keys: '9',
      url: 'https://images.pexels.com/photos/336372/pexels-photo-336372.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Bunch OF Flowers',
      price: '300',
      keys: '10',
      url: 'https://images.pexels.com/photos/4467139/pexels-photo-4467139.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'coffee & tea',
      price: '78',
      keys: '11',
      url: 'https://images.pexels.com/photos/1854652/pexels-photo-1854652.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Stone Stone',
      price: '289',
      keys: '12',
      url: 'https://images.pexels.com/photos/4040599/pexels-photo-4040599.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Fabrizioriva',
      price: '2000',
      keys: '21',
      url: 'https://images.pexels.com/photos/2499628/pexels-photo-2499628.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Fabrizioriva',
      price: '2000',
      keys: '22',
      url: 'https://images.pexels.com/photos/3866555/pexels-photo-3866555.png?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Fabrizioriva',
      price: '2000',
      keys: '23',
      url: 'https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
    },
    {
      name_product: 'Fabrizioriva',
      price: '2000',
      keys: '24',
      url: 'https://images.pexels.com/photos/3275029/pexels-photo-3275029.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Fabrizioriva',
      price: '2000',
      keys: '25',
      url: 'https://images.pexels.com/photos/7038116/pexels-photo-7038116.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Fabrizioriva',
      price: '2000',
      keys: '26',
      url: 'https://images.pexels.com/photos/775358/pexels-photo-775358.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Fabrizioriva',
      price: '2000',
      keys: '27',
      url: 'https://images.pexels.com/photos/8447299/pexels-photo-8447299.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Fabrizioriva',
      price: '2000',
      keys: '28',
      url: 'https://images.pexels.com/photos/8647550/pexels-photo-8647550.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Fabrizioriva',
      price: '2000',
      keys: '29',
      url: 'https://images.pexels.com/photos/594610/pexels-photo-594610.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Fabrizioriva',
      price: '2000',
      keys: '30',
      url: 'https://images.pexels.com/photos/8447299/pexels-photo-8447299.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Fabrizioriva',
      price: '2000',
      keys: '31',
      url: 'https://images.pexels.com/photos/8647550/pexels-photo-8647550.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      name_product: 'Fabrizioriva',
      price: '2000',
      keys: '32',
      url: 'https://images.pexels.com/photos/594610/pexels-photo-594610.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
  ],
  image: [
    {
      keys: '1',
      url: 'https://images.pexels.com/photos/3659862/pexels-photo-3659862.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '2',
      url: 'https://images.pexels.com/photos/322207/pexels-photo-322207.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '3',
      url: 'https://images.pexels.com/photos/7319321/pexels-photo-7319321.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '4',
      url: 'https://images.pexels.com/photos/7245597/pexels-photo-7245597.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '5',
      url: 'https://images.pexels.com/photos/4587991/pexels-photo-4587991.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '6',
      url: 'https://images.pexels.com/photos/5358161/pexels-photo-5358161.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '7',
      url: 'https://images.pexels.com/photos/8778065/pexels-photo-8778065.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '8',
      url: 'https://images.pexels.com/photos/701724/pexels-photo-701724.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '9',
      url: 'https://images.pexels.com/photos/6976104/pexels-photo-6976104.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    },
    {
      keys: '10',
      url: 'https://images.pexels.com/photos/8778065/pexels-photo-8778065.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '11',
      url: 'https://images.pexels.com/photos/701724/pexels-photo-701724.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '12',
      url: 'https://images.pexels.com/photos/6976104/pexels-photo-6976104.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    },
    {
      keys: '21',
      url: 'https://images.pexels.com/photos/2499628/pexels-photo-2499628.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '22',
      url: 'https://images.pexels.com/photos/3866555/pexels-photo-3866555.png?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '23',
      url: 'https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
    },
    {
      keys: '24',
      url: 'https://images.pexels.com/photos/3275029/pexels-photo-3275029.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '25',
      url: 'https://images.pexels.com/photos/7038116/pexels-photo-7038116.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '26',
      url: 'https://images.pexels.com/photos/775358/pexels-photo-775358.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '27',
      url: 'https://images.pexels.com/photos/8447299/pexels-photo-8447299.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '28',
      url: 'https://images.pexels.com/photos/8647550/pexels-photo-8647550.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '29',
      url: 'https://images.pexels.com/photos/594610/pexels-photo-594610.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '30',
      url: 'https://images.pexels.com/photos/8447299/pexels-photo-8447299.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '31',
      url: 'https://images.pexels.com/photos/8647550/pexels-photo-8647550.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
    {
      keys: '32',
      url: 'https://images.pexels.com/photos/594610/pexels-photo-594610.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    },
  ],
};
