import React, {useState} from 'react';
import {
  View,
  Image,
  Platform,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
import styled from 'styled-components';
import {data} from '../../data';
import {faPlay} from '@fortawesome/pro-regular-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';

export default function ToptabImageScreen({navigation}: {navigation: any}) {
  const [numColumns, setNumColumn] = useState(3);
  return (
    <>
      <FlatList
        data={data.image}
        numColumns={numColumns}
        key={numColumns}
        listKey="1.64756"
        scrollEnabled={false}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item, index) => item.keys + index}
        renderItem={data => {
          return (
            <Row
              key={data.item.keys}
              style={{alignContent: 'center', justifyContent: 'center'}}>
              <Col>
                <ImageView>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('ViewContent')}>
                    <ImageStyled
                      source={{uri: data.item.url}}
                      resizeMode={'cover'}
                    />
                  </TouchableOpacity>
                </ImageView>
              </Col>
            </Row>
          );
        }}
      />
    </>
  );
}

const Container = styled(View)`
  flex: 1;

  /* height: 100%; */
  /* padding-bottom: 100px; */

  background-color: #171c39;
`;
const ImageStyled = styled(Image)`
  width: 100%;
  height: 130px;
  border-radius: 5px;
`;
const ImageView = styled(View)`
  margin: 1%;
`;

const TextStyled = styled(Text)`
  font-size: 12px;
  font-weight: 300;
  color: white;
  /* font-family: Prompt; */
`;
const ColText = styled(Col)`
  position: absolute;
  bottom: 1%;
  left: 3%;
`;
