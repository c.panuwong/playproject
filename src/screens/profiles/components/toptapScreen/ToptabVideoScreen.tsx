import React, {useState} from 'react';
import {
  Animated,
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  Platfrom,
  TouchableOpacity,
} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
import styled from 'styled-components';
import {faPlay} from '@fortawesome/pro-regular-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {data} from '../../data';

const ToptabVideoScreen = ({navigation}: {navigation: any}) => {
  const [numColumns, setNumColumn] = useState(3);

  return (
    <FlatList
      data={data.video}
      numColumns={numColumns}
      key={numColumns}
      listKey="1.357"
      scrollEnabled={false}
      showsVerticalScrollIndicator={false}
      keyExtractor={(item, index) => item.keys + index}
      renderItem={data => {
        return (
          <Row
            key={data.item.keys}
            style={{alignContent: 'center', justifyContent: 'center'}}>
            <Col>
              <TouchableOpacity
                onPress={() => navigation.navigate('ViewContent')}>
                <ImageView>
                  <ImageStyled
                    source={{uri: data.item.url}}
                    resizeMode={'cover'}
                  />
                  <ColText>
                    <Row>
                      <FontAwesomeIcon icon={faPlay} size={15} color="#ffff" />
                      <TextStyled> {data.item.view} </TextStyled>
                    </Row>
                  </ColText>
                </ImageView>
              </TouchableOpacity>
            </Col>
          </Row>
        );
      }}
    />
  );
};

const Container = styled(View)`
  flex: 1;
  background-color: #171c39;
`;

const TextStyled = styled(Text)`
  font-size: 14px;
  font-weight: 300;
  color: white;
  text-shadow: 0px 0px 2px rgba(0, 0, 0, 0.75);
  font-family: DBHelvethaicaX-55Regular;
`;
const ColText = styled(Col)`
  position: absolute;
  bottom: 1%;
  left: 3%;
`;
const ImageStyled = styled(Image)`
  width: 100%;
  height: 180px;
  border-radius: 5px;
`;
const ImageView = styled(View)`
  margin: 1%;
`;

export default ToptabVideoScreen;
