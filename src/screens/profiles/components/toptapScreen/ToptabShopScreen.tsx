import React, {useState} from 'react';
import {
  Animated,
  View,
  Text,
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  FlatList,
  Platfrom,
  TouchableOpacity,
} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
import styled from 'styled-components';
import {faPlay} from '@fortawesome/pro-regular-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {data} from '../../data';

const ToptabShopScreen = ({navigation}: {navigation: any}) => {
  const [numColumns, setNumColumn] = useState(3);
  return (
    <FlatList
      data={data.shop}
      numColumns={numColumns}
      key={numColumns}
      listKey="1.6475dsd6"
      scrollEnabled={false}
      showsVerticalScrollIndicator={false}
      keyExtractor={(item, index) => item.keys + index}
      renderItem={data => {
        return (
          <Row
            key={data.item.keys}
            style={{alignContent: 'center', justifyContent: 'center'}}>
            <Col>
              <ImageView>
                <TouchableOpacity
                  onPress={() => navigation.navigate('ViewContent')}>
                  <ImageStyled
                    source={{uri: data.item.url}}
                    resizeMode={'cover'}
                  />
                </TouchableOpacity>

                <ColText>
                  <TextStyled> ฿ {data.item.price}</TextStyled>
                  <TextStyled>{data.item.name_product}</TextStyled>
                </ColText>
              </ImageView>
            </Col>
          </Row>
        );
      }}
    />
  );
};

const Container = styled(View)`
  flex: 1;
  background-color: #171c39;
`;
const TextStyled = styled(Text)`
  font-size: 14px;
  font-weight: 300;
  color: white;
  text-shadow: 0px 0px 2px rgba(0, 0, 0, 0.75);
  font-family: DBHelvethaicaX-55Regular;
`;
const ColText = styled(Col)`
  position: absolute;
  bottom: 1%;
  left: 3%;
`;
const ImageStyled = styled(Image)`
  width: 100%;
  height: 187px;
  border-radius: 5px;
`;
const ImageView = styled(View)`
  margin: 1%;
`;

export default ToptabShopScreen;
