import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Alert,
  FlatList,
  SafeAreaView,
} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
import styled from 'styled-components';
import {data} from '../../data';
import {
  ButtonGreen1,
  ButtonDisble,
  ButtonFollow,
} from '../../../../../src/components/ButtonComponent';
import {faBell, faStream} from '@fortawesome/pro-regular-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';

interface IData {
  user_name: string;
  description: string;
  followers: number;
  following: number;
  user_profile: any;
  video: Array<object>;
  shop: Array<object>;
  image: Array<object>;
}

const TopHeader = ({navigation}: {navigation: any}) => {
  return (
    <Row
      style={{
        paddingHorizontal: 20,
      }}>
      <Col style={{}}>
        <ImageHeader
          source={require('../../../../assets/images/logo.png')}
          resizeMode={'cover'}
        />
      </Col>

      <Col>
        <ColHeaderRightIcon>
          <ViewStyled>
            <OnTouchableOpacity onPress={() => Alert.alert('Bell')}>
              <FontAwesomeIcon icon={faBell} size={30} color="#fff" />
            </OnTouchableOpacity>
            <ViewCircle />
          </ViewStyled>
          <OnTouchableOpacity onPress={() => navigation.navigate('Hamberger')}>
            <FontAwesomeIcon icon={faStream} size={30} color="#fff" />
          </OnTouchableOpacity>
        </ColHeaderRightIcon>
      </Col>
    </Row>
  );
};

export default function ProfileUser({navigation}: {navigation: any}) {
  return (
    <SafeAreaView>
      <TopHeader navigation={navigation} />
      <RowHeader>
        <ColHeader>
          <RowImage size={1}>
            <Profile source={{uri: data.user_profile}} />
          </RowImage>
          <RowText size={1}>
            <TextStyle style={{fontSize: 26, color: '#ffff'}}>
              {data.user_name}
            </TextStyle>
            <TextStyle
              numberOfLines={2}
              style={{
                fontSize: 18,
                color: '#ffff',
                marginVertical: '3%',
                marginHorizontal: '5%',
              }}>
              {data.description}
            </TextStyle>
          </RowText>

          <Col>
            <RowButton
              size={0.4}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Col>
                <StyledTouchableOpacity>
                  <Row>
                    <Col>
                      <Row
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          alignContent: 'center',
                        }}>
                        <Image
                          source={require('../../../../assets/images/follow.png')}
                          style={{width: 20, height: 20}}
                          resizeMode={'cover'}
                        />

                        <TextStyle
                          style={{
                            fontSize: 22,
                            color: '#33d52b',
                            paddingHorizontal: 5,
                          }}>
                          Follow
                        </TextStyle>
                      </Row>
                    </Col>
                  </Row>
                </StyledTouchableOpacity>
              </Col>

              <Col>
                <ButtonFollow
                  onPress={() => Alert.alert('Message')}
                  title="Message"
                  image={() => (
                    <Image
                      source={require('../../../../assets/images/bottom_4.png')}
                      style={{width: 20, height: 20}}
                      resizeMode="cover"
                    />
                  )}
                />
              </Col>
            </RowButton>

            <RowText
              size={0.5}
              directionRow
              style={{
                width: '100%',
              }}>
              <ColText directionRow>
                <TextStyle
                  style={{fontSize: 26, color: '#ffff', paddingEnd: 8}}>
                  {/* {data.followers} */}
                  99K
                </TextStyle>
                <TextStyle style={{fontSize: 18, color: '#ffff'}}>
                  Followers
                </TextStyle>
              </ColText>

              <ColText directionRow>
                <TextStyle
                  style={{fontSize: 26, color: '#ffff', paddingEnd: 8}}>
                  {/* {data.followers} */}
                  99K
                </TextStyle>
                <TextStyle
                  style={{
                    fontSize: 18,
                    color: '#ffff',
                    // marginHorizontal: 20,
                    width: 100,
                  }}>
                  {/* {data.following} */}
                  Following
                </TextStyle>
              </ColText>
            </RowText>
          </Col>
        </ColHeader>
      </RowHeader>
    </SafeAreaView>
  );
}

const RowHeader = styled(Row)`
  padding: 0 20px 0 20px;
  height: 300px;
  width: 100%;
`;
const ColHeader = styled(Col)`
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;
const RowImage = styled(Row)`
  align-items: center;
`;
const Profile = styled(Image)`
  width: 86px;
  height: 86px;
  border-radius: 28px;
  border-width: 5px;
  border-color: #99ff00;
  /* color: #0bf0ff ; */
  /* border: 5px solid #0bf0ff; */
`;
const TextStyle = styled(Text)`
  font-family: DBHelvethaicaX-55Regular;
  ${props => props.numberOfLines}
`;
const ColText = styled(Col)`
  flex-direction: column;
  align-items: center;
  justify-content: center;
  align-content: center;
  /* border-color: #ff8f;
  border-width: 1; */
  ${props =>
    props.directionRow &&
    `
  flex-direction: row;
  `}
`;
const RowText = styled(Row)`
  flex-direction: column;
  align-items: center;
  justify-content: center;
  align-content: center;
  /* border-color: #ff8f;
  border-width: 1; */
  ${props =>
    props.directionRow &&
    `
  flex-direction: row;
  `}
`;
const RowButton = styled(Row)`
  flex-direction: row;
  align-content: center;
  justify-content: center;
  padding-horizontal: 40px;
`;
const StyledTouchableOpacity = styled(TouchableOpacity)`
  align-content: center;
  justify-content: center;
  align-items: center;
  border-radius: 25px;
  color: #257ce2;
  border: 2px solid #33d52b;
  margin: 3px 15px;
  padding: 0 5px;
  width: 120px;
  height: 34px;
  ${props =>
    props.primary &&
    `
    background-color: #33d52b;
    color:#000
  `}
`;
const FollowText = styled(Row)`
  flex-direction: row;
`;
const ImageHeader = styled(Image)`
  width: 30px;
  height: 30px;
`;

const ColHeaderRightIcon = styled(Row)`
  align-items: center;
  justify-content: flex-end;
`;
const ViewStyled = styled(View)`
  margin-right: 15px;
`;
const OnTouchableOpacity = styled(TouchableOpacity)``;

const RowContent = styled(Row)`
  height: 200%;
  border-color: #f666;
  border-width: 1px;
`;
const ViewCircle = styled(View)`
  position: absolute;
  width: 8px;
  height: 8px;
  border-radius: 4px;
  background-color: #99ff00;
  margin: 2px 18px;
`;
