import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
// @ts-ignore
import styled from 'styled-components';
// @ts-ignore
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faUserCheck} from '@fortawesome/pro-regular-svg-icons';
import {DBHText} from '../../components/StyledText';
export default function SuccessVerifly({navigation}: {navigation: any}) {
  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 0, y: 1}}
      colors={['#21284e', '#0e1333', '#080D28']}
      style={{
        flex: 1,
        alignItems: 'center',
      }}>
      <Linear
        start={{x: 0.0, y: 1.0}}
        end={{x: 1.0, y: 1.0}}
        colors={['#257CE2', '#33D52B', '#99FF00']}>
        <ViweInLinear>
          <FontAwesomeIcon
            icon={faUserCheck}
            size={70}
            color="#FFF"
            style={{padding: 15, marginLeft: 1, marginRight: 1, width: 198}}
            // style={{position: 'absolute'}}
          />
        </ViweInLinear>
      </Linear>
      <DBHText size={28} style={{textAlign: 'center', marginTop: 50}}>
        We already saved your information
      </DBHText>
      <DBHText
        size={28}
        color="#99FF00"
        style={{textAlign: 'center', marginTop: 15}}
        onPress={() => navigation.navigate('Index')}>
        Back to home
      </DBHText>
    </LinearGradient>
  );
}
const ViweInLinear = styled(TouchableOpacity)`
  width: 134px;
  height: 134px;
  align-items: center;
  justify-content: center;
  padding-left: 15px;
  background-color: #121947;
  border-radius: 77px;
`;
const Linear = styled(LinearGradient)`
  justify-content: center;
  border-radius: 70px;
  width: 140px;
  height: 140px
  align-items: center;
  margin-top: 104px;
`;
