import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  SafeAreaView,
  FlatList,
  Dimensions,
  ScrollView,
} from 'react-native';
import {DBHText} from '../../components/StyledText';
import styled from 'styled-components';
import Search from './components/Search';
import {data} from './data';
import {ButtonBorder} from '../../components/ButtonComponent';
import LinearGradient from 'react-native-linear-gradient';

const {width, height} = Dimensions.get('window');

export default function BlockedScreen({navigation}: {navigation: any}) {
  // console.log(data);
  const [isBlocked, setIsBlocked] = useState([]);

  const handleBlocked = (id: any) => {
    if (isBlocked.indexOf(id) > -1) {
      setIsBlocked(isBlocked.filter(x => x != id));
    } else {
      setIsBlocked(oldArray => [...oldArray, id]);
    }

    // if (isBlocked(id) > -1) {
    //   setIsBlocked(isBlocked.filter(x => x != id));
    // } else {
    //   setIsBlocked(oldArray => [...oldArray, id]);
    // }
  };

  return (
    <Background
      source={require('../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <SafeAreaView>
        <ScrollView>
          <DBHText size={28}>Blocked</DBHText>
          <Search />
          <ViewAll>
            {data.map((res: any) => {
              return (
                <ViewList>
                  <ViewImage>
                    <Linear
                      start={{x: 0.2, y: 1.5}}
                      end={{x: 1.3, y: 0}}
                      colors={['#257CE2', '#33D52B', '#99FF00']}>
                      <ImageProfile
                        source={{uri: res.user_profile}}
                        resizeMode="cover"
                      />
                    </Linear>
                  </ViewImage>
                  <ViewTexts>
                    <DBHText numberOfLines={1}>{res.user_name}</DBHText>
                    <DBHText numberOfLines={1}>{res.bio}</DBHText>
                  </ViewTexts>
                  <ViewButton>
                    {isBlocked?.indexOf(res.id) > -1 ? (
                      <ButtonBorder
                        title="Unblocked"
                        onPress={() => handleBlocked(res.id)}
                        height={35}
                        color="#fff"
                        textColor="#3C7D0A"
                      />
                    ) : (
                      <ButtonBorder
                        title="Blocked"
                        onPress={() => handleBlocked(res.id)}
                        height={35}
                        textColor="#99FF00"
                      />
                    )}
                  </ViewButton>
                </ViewList>
              );
            })}
          </ViewAll>
        </ScrollView>
      </SafeAreaView>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
  padding: 25px;
  padding-top: 40px;
`;
const ViewList = styled(View)`
  flex-direction: row;
  align-items: center;
  padding-vertical: 1.5%;
`;
const ViewTexts = styled(View)`
  flex: 0.45;
  padding-end: 10px;
`;
const ViewButton = styled(View)`
  padding-vertical: 5px;

  flex: 0.35;
`;
const ViewImage = styled(View)`
  flex: 0.2;
`;
const ImageProfile = styled(Image)`
  width: 51.51px;
  height: 51.51px;
  border-radius: 17px;
  border-width: 3px;
  border-color: #fff;
`;
const Linear = styled(LinearGradient)`
  justify-content: center;
  align-items: center;
  align-content: center;
  border-radius: 17px;
  width: 57px;
  height: 52px;
  padding-vertical: 28px;
`;
const ViewAll = styled(View)`
  margin-top: 5%;
`;
