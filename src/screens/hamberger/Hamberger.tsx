import React from 'react';
import {ImageBackground, Image, View, SafeAreaView} from 'react-native';
// @ts-ignore
import styled from 'styled-components';
import {DBHText} from '../../components/StyledText';
import {faWallet} from '@fortawesome/pro-regular-svg-icons';
import AccountButton from './components/AccountButton';

const Hamberger = ({navigation}: {navigation: any}) => {
  return (
    <Background
      source={require('../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <SafeAreaView style={{marginTop: '10%', flex: 1}}>
        <DBHText size={34}>Account</DBHText>
        <ViewAll>
          <ViewButton>
            <AccountButton
              // nameIcon={faUser}
              icon={
                <Image
                  source={require('../../assets/images/icons/hamburger_personal.png')}
                  style={{width: 23, height: 23}}
                  resizeMode="contain"
                />
              }
              title="Personal Info"
              onPress={() => navigation.navigate('PersonalInfo')}
            />
          </ViewButton>
          <ViewButton>
            <AccountButton
              // nameIcon={faHeartSquare}
              icon={
                <Image
                  source={require('../../assets/images/icons/hamburger_kycstatus.png')}
                  style={{width: 23, height: 23}}
                  resizeMode="contain"
                />
              }
              title="KYC Status"
              onPress={() => navigation.navigate('KYC')}
            />
          </ViewButton>
          <ViewButton>
            <AccountButton
              nameIcon={faWallet}
              title="Wallet"
              onPress={() => navigation.navigate('Wallet')}
            />
          </ViewButton>
          <ViewButton>
            <AccountButton
              icon={
                <Image
                  source={require('../../assets/images/icons/hamburger_saved.png')}
                  style={{width: 23, height: 23}}
                  resizeMode="contain"
                />
              }
              title="Saved"
              onPress={() => navigation.navigate('Saved')}
            />
          </ViewButton>
          <ViewButton>
            <AccountButton
              icon={
                <Image
                  source={require('../../assets/images/icons/hamburger_blocked.png')}
                  style={{width: 23, height: 23}}
                  resizeMode="contain"
                />
              }
              title="Blocked"
              onPress={() => navigation.navigate('Blocked')}
            />
          </ViewButton>
          <ViewButton>
            <AccountButton
              icon={
                <Image
                  source={require('../../assets/images/icons/hamburger_promote.png')}
                  style={{width: 23, height: 23}}
                  resizeMode="contain"
                />
              }
              title="Promote"
              onPress={() => navigation.navigate('Promote')}
            />
          </ViewButton>
          <ViewButton>
            <AccountButton
              icon={
                <Image
                  source={require('../../assets/images/icons/hamburger_openshop.png')}
                  style={{width: 23, height: 23}}
                  resizeMode="contain"
                />
              }
              title="Open Shop"
              onPress={() => navigation.navigate('OpenShop')}
            />
          </ViewButton>
          <ViewButton>
            <AccountButton
              icon={
                <Image
                  source={require('../../assets/images/icons/hamburger_help.png')}
                  style={{width: 23, height: 23}}
                  resizeMode="contain"
                />
              }
              title="Help (FAQ)"
              onPress={() => navigation.navigate('Help_FAQ')}
            />
          </ViewButton>
        </ViewAll>
        <ViewLineColor>
          <DBHText size={14}>V 0.00001</DBHText>
        </ViewLineColor>
      </SafeAreaView>
    </Background>
  );
};

const Background = styled(ImageBackground)`
  flex: 1;
  padding: 25px;
  padding-top: 40px;
`;
const ViewAll = styled(View)`
  margin-top: 5%;
`;
const ViewButton = styled(View)`
  width: 100%;
  height: 40px;
  margin-bottom: 10px;
`;
const ViewLineColor = styled(View)`
  position: absolute;
  width: 100%;
  bottom: 0px;
  align-items: center;
  border-top-width: 6px;
  border-color: #21263e;
  padding-vertical: 5px;
`;
export default Hamberger;
