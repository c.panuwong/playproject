import React, {useState, useEffect} from 'react';
import {View, ImageBackground, StyleSheet, Text} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
// @ts-ignore
import styled from 'styled-components';
import {DBHText} from '../../components/StyledText';
// @ts-ignore
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {ButtonDisble} from '../../components/ButtonComponent';

export default function VeriflyOTP({
  route,
  navigation,
}: {
  route: any;
  navigation: any;
}) {
  const {number, otp} = route.params;
  const [error, setError] = useState<string>('');
  const [otpmatch, setOTPMatch] = useState<string>('');
  const [enablebutton, setEnableButton] = useState<boolean>(false);
 
  const checkotp = (code: any) => {
    if (code == otp) {
      setEnableButton(true);
      setError('');
      setOTPMatch('หมายเลข OTP ถูกต้อง');
    } else {
      setError('หมาย OTP ไม่ถูกต้อง');
      setOTPMatch('');
    }
  };
  return (
    <Background
      source={require('../../assets/images/BG_Login.jpg')}
      resizeMode="cover">
      <SafeAreaView style={{flex: 1, alignItems: 'center', padding: 30}}>
        <DBHText size={28} style={{textAlign:'center'}}>Code is sent to your email.Type code to verify.</DBHText>
        <DBHText size={28}>+{number}</DBHText>

        <OTPInputView
          style={styles.OTP}
          pinCount={6}
          // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
          //   onCodeChanged={code => {
          //     setOTPInput(code);
          //   }}
          autoFocusOnLoad
          codeInputFieldStyle={styles.underlineStyleBase}
          codeInputHighlightStyle={styles.underlineStyleHighLighted}
          onCodeFilled={(code: any) => {
            checkotp(code);
          }}
        />
        {error != '' ? <Error textValid={error} /> : null}
        {otpmatch != '' ? <Success textValid={otpmatch} /> : null}
        <DBHText size={28} color="#99FF00" style={{marginTop: 25}}>
          Send OTP Again
        </DBHText>
        <ViewButton>
          <ButtonDisble
            color={
              enablebutton && error == ''
                ? ['#257CE2', '#33D52B', '#99FF00']
                : ['#7F8181', '#E7E7E7']
            }
            title="Next"
            padding={5}
            marginTop={0}
            onPress={() => {
              enablebutton && error == ''
                ? navigation.navigate('SuccessVerifly')
                : null;
            }}
          />
        </ViewButton>
      </SafeAreaView>
    </Background>
  );
}
const Error = (props: any) => {
  return <LabelError>{props.textValid}</LabelError>;
};
const Success = (props: any) => {
  return <LabelSuccess>{props.textValid}</LabelSuccess>;
};
const Background = styled(ImageBackground)`
  flex: 1;
  padding-top: 30px;
`;
const ViewButton = styled(View)`
  width: 100%;
  justify-content: center;
  margin-top: 80px;
`;
const LabelError = styled(Text)`
  margin-top: 4px;
  width: 100%;
  color: #ff0000;
  text-align: center;
  font-size: 15px;
  font-family: DBHelvethaicaX-55Regular;
`;
const LabelSuccess = styled(Text)`
  margin-top: 4px;
  width: 100%;
  color: #99ff00;
  text-align: center;
  font-size: 15px;
  font-family: DBHelvethaicaX-55Regular;
`;

const styles = StyleSheet.create({
  borderStyleBase: {
    width: 30,
    height: 45,
  },

  borderStyleHighLighted: {
    borderColor: '#03DAC6',
  },

  underlineStyleBase: {
    width: 41,
    height: 57,
    borderRadius: 15,
    borderWidth: 1,
    backgroundColor: 'white',
    marginHorizontal: '2%',
    color: '#000',
  },

  underlineStyleHighLighted: {
    borderColor: '#03DAC6',
  },
  OTP: {
    width: '100%',
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 60,
  },
});
