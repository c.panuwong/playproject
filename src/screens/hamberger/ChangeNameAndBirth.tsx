import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {DBHText} from '../../components/StyledText';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components';
import {TextInput} from 'react-native-paper';
import {FloatingLabelInput} from 'react-native-floating-label-input';
import {ButtonGreen1} from '../../components/ButtonComponent';

export default function ChangeNameAndBirth({
  route,
  navigation,
}: {
  route: any;
  navigation: any;
}) {
  // const {number, otp} = route.params;
  const [value, setValue] = useState('');
  return (
    <ViewBG
      start={{x: 0, y: 0}}
      end={{x: 0, y: 1}}
      colors={['#21284e', '#0e1333', '#080D28']}>
      <DBHText size={34}>Display Name</DBHText>
      <FloatingLabelInput
        label={'Name'}
        containerStyles={{
          borderBottomWidth: 1,
          paddingHorizontal: 10,
          borderBottomColor: 'white',
          borderRadius: 8,
          marginTop: 60,
        }}
        inputStyles={{
          color: 'white',
          paddingBottom: 7,
          fontSize: 22,
          fontFamily: 'DBHelvethaicaX-55Regular',
          paddingLeft: 5,
        }}
        labelStyles={{
          fontFamily: 'DBHelvethaicaX-55Regular',
          fontSize: 22,
          bottom:0
        }}
        customLabelStyles={{
          colorFocused: '#B3B3B3',
          fontSizeFocused: 18,
          fontSizeBlurred: 22,
          colorBlurred: '#FFF',
        }}
        showCountdownStyles={{
          color: 'gray',
          fontSize: 18,
          fontFamily: 'DBHelvethaicaX-55Regular',
          top: 0,
        }}
        showCountdown
        maxLength={110}
        countdownLabel="/110"
        value={value}
        onChangeText={value => setValue(value)}
      />
      <ButtonGreen1
        onPress={() => navigation.navigate('PersonalInfo')}
        title="Save"
        padding={5}
        marginTop={50}
      />
    </ViewBG>
  );
}
const ViewBG = styled(LinearGradient)`
  flex: 1;
  padding: 20px;
  padding-top: 40px;
`;
