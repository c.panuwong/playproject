import React from 'react';
import {View, ImageBackground, Image, TouchableOpacity} from 'react-native';
import {Modalize} from 'react-native-modalize';
import {DBHText} from '../../../components/StyledText';
// @ts-ignore
import styled from 'styled-components';
import {faPencilAlt} from '@fortawesome/pro-regular-svg-icons';
import {faTrashAlt} from '@fortawesome/free-regular-svg-icons';
import LinearGradient from 'react-native-linear-gradient';
import AccountButton from './AccountButton';
export default function ModallizeChangeImage({
  modalizeRef,
  navigation,
}: {
  modalizeRef: any;
  navigation: any;
}) {
  return (
    <Modalize
      ref={modalizeRef}
      modalStyle={{
        flex: 1,
        backgroundColor: '#091340',
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        width: '100%',
      }}
      withHandle={false}
      modalHeight={340}>
      <ViewBG
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}
        colors={['#21284e', '#0e1333', '#080D28']}>
        <ViewImage
          start={{x: 0.0, y: 1.0}}
          end={{x: 1.0, y: 1.0}}
          colors={['#257CE2', '#33D52B', '#99FF00']}>
          <Imageslide
            source={require('../../../assets/images/mockimage/Rectangle4100.png')}
          />
        </ViewImage>
        <DBHText size={18} style={{marginTop: 10}}>
          User: 1234123
        </DBHText>
        <ViewLine></ViewLine>
        <ViewButton>
          <AccountButton
            nameIcon={faPencilAlt}
            title="Change Photo"
            onPress={() => navigation.navigate('')}
          />
        </ViewButton>
        <ViewButton2>
          <AccountButton
            nameIcon={faTrashAlt}
            title="Remove Photo"
            onPress={() => navigation.navigate('')}
          />
        </ViewButton2>
      </ViewBG>
    </Modalize>
  );
}
const ViewBG = styled(LinearGradient)`
  align-items: center;
  border-top-right-radius: 30px;
  border-top-left-radius: 30px;
  width: 100%;
  height: 340px;
  padding-top: 15px;
`;
const Imageslide = styled(Image)`
  width: 80px;
  height: 80px;
  border-radius: 30px;
  border-width: 2px;
  border-color: #c4c4c4;
`;
const ViewImage = styled(LinearGradient)`
  width: 82px;
  height: 82px;
  border-radius: 30px;
  align-items: center;
  justify-content: center;
`;
const ViewLine = styled(View)`
  width: 100%;
  height: 0px;
  border-width: 0.5px;
  border-color: gray;
  margin-top: 20px;
`;
const ViewButton = styled(View)`
  width: 100%;
  height: 40px;
  margin-top: 20px;
  margin-left: 40px;
`;
const ViewButton2 = styled(View)`
  width: 100%;
  height: 40px;
  margin-top: 5px;
  margin-left: 40px;
`;
