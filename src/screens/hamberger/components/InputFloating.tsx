import React, {useState} from 'react';
import {View, Dimensions, StyleSheet} from 'react-native';
import styled from 'styled-components';
import {DBHText} from '../../../components/StyledText';
import {FloatingLabelInput} from 'react-native-floating-label-input';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCheckCircle, faTimesCircle} from '@fortawesome/pro-solid-svg-icons';

export default function InputFloating({
  label,
  value,
  onChangeText,
  rightComponent,
  keyboardType,
  isFocused,
}: {
  label: string;
  value: any;
  onChangeText: any;
  rightComponent?: any;
  keyboardType?: string;
  isFocused?: string;
}) {
  //   const [value, setValue] = useState('');
  //   const [isOldPassword, setIsOldPassword] = useState('');

  return (
    <FloatingLabelInput
      label={label}
      containerStyles={styles.containerStyles}
      inputStyles={styles.inputStyles}
      labelStyles={styles.labelStyles}
      //   showCountdownStyles={styles.showCountdownStyles}
      customLabelStyles={{
        colorFocused: '#B3B3B3',
        fontSizeFocused: 18,
        fontSizeBlurred: 22,
        colorBlurred: '#b3b3b3',
      }}
      isFocused={isFocused || undefined}
      //   showCountdown
      //   isPassword={isPassword}
      //   maxLength={maxLength}
      //   countdownLabel={countdownLabel}
      value={value}
      onChangeText={onChangeText}
      keyboardType={keyboardType}
      //   togglePassword={togglePassword}
      //   customShowPasswordComponent={<></>}
      //   customHidePasswordComponent={<></>}
      rightComponent={rightComponent}
    />
  );
}
const styles = StyleSheet.create({
  containerStyles: {
    borderBottomWidth: 0.5,
    paddingHorizontal: 10,
    borderBottomColor: '#B3b3b3',
    borderRadius: 8,
    marginTop: '7%',
  },
  inputStyles: {
    color: 'white',
    paddingBottom: 1,
    fontSize: 22,
    fontFamily: 'DBHelvethaicaX-55Regular',
    paddingLeft: 5,
  },
  labelStyles: {
    fontFamily: 'DBHelvethaicaX-55Regular',
    marginTop: '3%',
    fontSize: 22,
    bottom: 0,
  },
  showCountdownStyles: {
    color: 'gray',
    fontSize: 18,
    fontFamily: 'DBHelvethaicaX-55Regular',
    bottom: -23,
    end: 25,
  },
  iconStyle: {
    marginTop: 20,
    backgroundColor: '#fff',
    borderRadius: 15,
  },
});
