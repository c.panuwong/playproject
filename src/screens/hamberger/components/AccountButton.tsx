import React from 'react';
import {
  ImageBackground,
  Image,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
// @ts-ignore
import styled from 'styled-components';
// @ts-ignore
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {DBHText} from '../../../components/StyledText';
export default function AccountButton({
  nameIcon,
  title,
  onPress,
  icon,
}: {
  nameIcon?: any;
  title: string;
  onPress: any;
  icon?: any;
}) {
  return (
    <ViewAll>
      <TouchableOpacity
        onPress={onPress}
        style={{width: '100%', flexDirection: 'row', alignItems: 'center'}}>
        {icon}
        <FontAwesomeIcon
          icon={nameIcon}
          size={25}
          color="#FFF"
          // style={{position: 'absolute'}}
        />
        <DBHText size={26} style={{marginLeft: 20}}>
          {title}
        </DBHText>
      </TouchableOpacity>
    </ViewAll>
  );
}

const ViewAll = styled(View)`
  flex: 1;
`;
