import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Searchbar, Menu} from 'react-native-paper';
import styled from 'styled-components';
import {DBHText} from '../../../components/StyledText';
// import {SearchBar} from 'react-native-elements';

export default function Search({autoFocus}: {autoFocus: boolean}) {
  const [textInput, setTextInput] = useState('');

  return (
    <Content>
      <Searchbar
        placeholder="Search"
        autoFocus={autoFocus}
        value={textInput}
        onChangeText={setTextInput}
        returnKeyType="search"
        placeholderTextColor="#fff"
        iconColor="#fff"
        inputStyle={styles.input}
        style={styles.searchBar}
      />
    </Content>
  );
}

const styles = StyleSheet.create({
  searchBar: {
    backgroundColor: '#21263E',
    borderWidth: 1,
    borderColor: '#B3B3B3',
    borderRadius: 50,
  },
  input: {
    fontFamily: 'DBHelvethaicaX-55Regular',
    fontSize: 24,
    marginLeft: -15,
    color: '#fff',
  },
});

const Content = styled(View)`
  width: 100%;
  margin-top: 3%;
`;
