import React from 'react';
import {View, Text} from 'react-native';
import {DBHText} from '../../../components/StyledText';
import styled from 'styled-components';

export default function CopyrightPolicyScreen() {
  return (
    <View>
      <DBHText>CopyrightPolicyScreen</DBHText>
    </View>
  );
}
