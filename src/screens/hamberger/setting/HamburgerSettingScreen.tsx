import React from 'react';
import {
  ImageBackground,
  Image,
  View,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import AccountButton from '../components/AccountButton';
import styled from 'styled-components';
import {DBHText} from '../../../components/StyledText';
import {faGlobe, faPowerOff} from '@fortawesome/pro-light-svg-icons';
// import {faPowerOff} from '@fortawesome/pro-solid-svg-icons';
import SwitchButton from '../../../components/SwitchButton';

export default function HamburgerSettingScreen({
  navigation,
}: {
  navigation: any;
}) {
  // const handleToggleIsPublic = (isPublic) => setFormData({ ...formData, is_publish: isPublic });

  return (
    <Background
      source={require('../../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <SafeArea>
        <DBHText size={34}>Settings</DBHText>
        <ViewButton>
          <AccountButton
            icon={
              <Image
                source={require('../../../assets/images/icons/changepassword.png')}
                style={{width: 23, height: 23}}
                resizeMode="contain"
              />
            }
            title="Change Password "
            onPress={() => navigation.navigate('ChangePassword')}
          />
        </ViewButton>
        <ViewButton>
          <AccountButton
            icon={
              <Image
                source={require('../../../assets/images/icons/hamburger_notifications.png')}
                style={{width: 23, height: 23}}
                resizeMode="contain"
              />
            }
            title="Notifications "
            onPress={() => navigation.navigate('Notification')}
          />
        </ViewButton>
        <ViewButton>
          <AccountButton
            icon={
              <Image
                source={require('../../../assets/images/icons/hamburger_privacy.png')}
                style={{width: 23, height: 23}}
                resizeMode="contain"
              />
            }
            title="Privacy"
            onPress={() => navigation.navigate('Privacy')}
          />
        </ViewButton>
        <ViewButton>
          <AccountButton
            nameIcon={faGlobe}
            title="Language"
            onPress={() => navigation.navigate('Language')}
          />
        </ViewButton>
        <ViewButton>
          <AccountButton
            icon={
              <Image
                source={require('../../../assets/images/icons/hamburger_bout.png')}
                style={{width: 23, height: 23}}
                resizeMode="contain"
              />
            }
            title="About"
            onPress={() => navigation.navigate('About')}
          />
        </ViewButton>
        <ViewButton>
          <AccountButton
            nameIcon={faPowerOff}
            title="Logout"
            onPress={() => Alert.alert('Logout')}
          />
        </ViewButton>
      </SafeArea>
      <ViewLineColor />
    </Background>
  );
}
const ViewAll = styled(View)`
  flex: 1;
  padding: 20px;
`;
const ViewButton = styled(View)`
  width: 100%;
  height: 40px;
  margin-bottom: 10px;
`;
const Background = styled(ImageBackground)`
  flex: 1;
`;
const ViewLineColor = styled(View)`
  width: 100%;
  height: 0.5%;
  background-color: #21263e;
`;
const SafeArea = styled(SafeAreaView)`
  padding: 25px;
  padding-top: 40px;
`;
