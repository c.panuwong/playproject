import React from 'react';
import {
  ImageBackground,
  View,
  ScrollView,
  SafeAreaView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {DBHText} from '../../../components/StyledText';
import styled from 'styled-components';
import {ButtonGreen1} from '../../../components/ButtonComponent';

export default function AboutScreen({navigation}: {navigation: any}) {
  return (
    <Background
      source={require('../../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <ViewAll>
        <ImageLogo
          source={require('../../../assets/images/logo_text.png')}
          resizeMode="contain"
        />
        <DBHText size={24}>Version 0.1xxx</DBHText>
        <ViewButton>
          <ButtonGreen1
            title="Update Now"
            onPress={() => navigation.navigate('')}
            padding={5}
            marginTop={20}
          />
        </ViewButton>
        {/* <ViewTouchableText>
          <TouchableText>
            <DBHText size={28}>Copyright Policy </DBHText>
          </TouchableText>
          <TouchableText>
            <DBHText size={28}>Terms of Service </DBHText>
          </TouchableText>
        </ViewTouchableText> */}
      </ViewAll>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
  padding-top: 40px;
`;
const ViewAll = styled(View)`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const ImageLogo = styled(Image)`
  width: 30%;
  height: 30%;
`;

const ViewButton = styled(View)`
  width: 30%;
  height: 10%;
  padding-top: 25px;
`;

const ViewTouchableText = styled(View)`
  padding-vertical: 15%;
`;
const TouchableText = styled(TouchableOpacity)`
  padding-vertical: 2%;
`;
