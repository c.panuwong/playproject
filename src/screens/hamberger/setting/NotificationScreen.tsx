import React, {useState} from 'react';
import {ImageBackground, View, ScrollView, SafeAreaView} from 'react-native';
import {DBHText} from '../../../components/StyledText';
import styled from 'styled-components';
import SwitchButton from '../../../components/SwitchButton';

export default function NotificationScreen({navigation}: {navigation: any}) {
  const [isPush, setIsPush] = useState(true);
  const [isRequest, setIsRequest] = useState(true);
  const [isChat, setIsChat] = useState(true);

  const handleToggleIsPush = () => setIsPush(!isPush);
  const handleToggleIsRequest = () => setIsRequest(!isRequest);
  const handleToggleIsChat = () => setIsChat(!isChat);

  return (
    <Background
      source={require('../../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <ViewHeader>
        <DBHText size={34}>Notifications</DBHText>
      </ViewHeader>
      <ViewAll>
        <ViewButton>
          <SwitchButton
            title="Push Notification "
            value={isPush}
            onValueChange={handleToggleIsPush}
          />
        </ViewButton>
        <ViewButton>
          <SwitchButton
            title="Request Massage "
            value={isRequest}
            onValueChange={handleToggleIsRequest}
          />
        </ViewButton>
        <ViewButton>
          <SwitchButton
            title="Chat Message "
            value={isChat}
            onValueChange={handleToggleIsChat}
          />
        </ViewButton>
      </ViewAll>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
  padding-top: 40px;
`;
const ViewAll = styled(View)`
  padding: 20px;
`;
const ViewButton = styled(View)`
  width: 100%;
  height: 40px;
  margin-bottom: 10px;
`;
const ViewHeader = styled(View)`
  padding-horizontal: 25px;
`;
