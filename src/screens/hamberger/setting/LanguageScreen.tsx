import React, {useState} from 'react';
import {ImageBackground, View, ScrollView, SafeAreaView} from 'react-native';
import {DBHText} from '../../../components/StyledText';
import styled from 'styled-components';
import SwitchButton from '../../../components/SwitchButton';

export default function LanguageScreen({navigation}: {navigation: any}) {
  const [isLanguage, setIsLanguage] = useState(true);
  const handleToggleIsLanguage = () => setIsLanguage(!isLanguage);

  return (
    <Background
      source={require('../../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <ViewHeader>
        <DBHText size={34}>Notifications</DBHText>
      </ViewHeader>
      <ViewAll>
        <ViewButton>
          <SwitchButton
            title={isLanguage ? 'English' : 'ไทย'}
            value={isLanguage}
            onValueChange={handleToggleIsLanguage}
          />
        </ViewButton>
      </ViewAll>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
  padding-top: 40px;
`;
const ViewAll = styled(View)`
  padding: 20px;
`;
const ViewButton = styled(View)`
  width: 100%;
  height: 40px;
  margin-bottom: 10px;
`;
const ViewHeader = styled(View)`
  padding-horizontal: 25px;
`;
