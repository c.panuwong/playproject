import React, {useState} from 'react';
import {ImageBackground, View, ScrollView, SafeAreaView} from 'react-native';
import {DBHText} from '../../../components/StyledText';
import styled from 'styled-components';
import SwitchButton from '../../../components/SwitchButton';

export default function PrivacyScreen({navigation}: {navigation: any}) {
  const [isPrivate, setIsPrivate] = useState(true);
  const handleToggleIsPrivate = isPublic => setIsPrivate(!isPrivate);

  return (
    <Background
      source={require('../../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <ViewHeader>
        <DBHText size={34}>PrivacyScreen</DBHText>
      </ViewHeader>
      <ViewAll>
        <ViewButton>
          <SwitchButton
            title="Private "
            texTitle="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pellentesque"
            value={isPrivate}
            onValueChange={handleToggleIsPrivate}
          />
        </ViewButton>
      </ViewAll>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
  padding-top: 40px;
`;
const ViewAll = styled(View)`
  padding: 20px;
`;
const ViewButton = styled(View)`
  width: 100%;
  height: 100;
  margin-bottom: 10px;
`;
const ViewHeader = styled(View)`
  padding-horizontal: 25px;
`;
// background-color: #3e3e3e;
