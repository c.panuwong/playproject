import React, {useState, useEffect} from 'react';
import {
  View,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import {DBHText} from '../../../components/StyledText';
import {FloatingLabelInput} from 'react-native-floating-label-input';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCheckCircle, faTimesCircle} from '@fortawesome/pro-solid-svg-icons';
import {ButtonGreen1, ButtonDisble} from '../../../components/ButtonComponent';
import ChangeNameAndBirth from '../ChangeNameAndBirth';
import styled from 'styled-components';

const {width, height} = Dimensions.get('window');

export default function ChangePasswordScreen({navigation}: {navigation: any}) {
  const [value, setValue] = useState('');
  const [isOldPassword, setIsOldPassword] = useState('');
  const [isNewPassword, setIsNewPassword] = useState('');
  const [isConfirmPassword, setIsConfirmPassword] = useState('');
  const [isMatchPassword, setIsMatchPassword] = useState(false);

  const ChangePassword = () => {
    if (isOldPassword && isNewPassword && isConfirmPassword != '') {
      if (isNewPassword !== isConfirmPassword) {
        console.log('Password is not match');
      } else {
        navigation.navigate('SuccessChangePassword');
        console.log('Password is match');
        setIsMatchPassword(!isMatchPassword);
      }
    } else {
      console.log('กรอกข้อมูลไม่สมบูรณ์');
    }
  };
  console.log(isMatchPassword);

  return (
    <Background
      source={require('../../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <SafeAreaView style={styles.safeArea}>
        <ScrollView>
          <ViewAll>
            <ViewImage>
              <Image
                source={require('../../../assets/images/create_pass.png')}
                style={styles.imageLogo}
                resizeMode="contain"
              />
            </ViewImage>
            <ViewInput>
              <FloatingLabelInput
                label="Old Password"
                containerStyles={styles.containerStyles}
                inputStyles={styles.inputStyles}
                labelStyles={styles.labelStyles}
                showCountdownStyles={styles.showCountdownStyles}
                customLabelStyles={{
                  colorFocused: '#B3B3B3',
                  fontSizeFocused: 18,
                  fontSizeBlurred: 22,
                  colorBlurred: '#FFF',
                }}
                showCountdown
                isPassword={true}
                maxLength={110}
                countdownLabel="/110"
                value={isOldPassword}
                onChangeText={value => setIsOldPassword(value)}
                togglePassword={false}
                customShowPasswordComponent={<></>}
                customHidePasswordComponent={<></>}
                rightComponent={
                  <FontAwesomeIcon
                    icon={faTimesCircle}
                    size={15}
                    color="#cacaca"
                    style={styles.iconStyle}
                  />
                }
              />
            </ViewInput>
            <ViewInput>
              <FloatingLabelInput
                label="New Password"
                containerStyles={styles.containerStyles}
                inputStyles={styles.inputStyles}
                labelStyles={styles.labelStyles}
                showCountdownStyles={styles.showCountdownStyles}
                customLabelStyles={{
                  colorFocused: '#B3B3B3',
                  fontSizeFocused: 18,
                  fontSizeBlurred: 22,
                  colorBlurred: '#FFF',
                }}
                showCountdown
                isPassword={true}
                maxLength={110}
                countdownLabel="/110"
                value={isNewPassword}
                onChangeText={value => setIsNewPassword(value)}
                togglePassword={false}
                customShowPasswordComponent={<></>}
                customHidePasswordComponent={<></>}
                rightComponent={
                  <FontAwesomeIcon
                    icon={faTimesCircle}
                    size={15}
                    color="#cacaca"
                    style={styles.iconStyle}
                  />
                }
              />
            </ViewInput>
            <ViewInput>
              <FloatingLabelInput
                label="Confirm Password"
                containerStyles={styles.containerStyles}
                inputStyles={styles.inputStyles}
                labelStyles={styles.labelStyles}
                showCountdownStyles={styles.showCountdownStyles}
                customLabelStyles={{
                  colorFocused: '#B3B3B3',
                  fontSizeFocused: 18,
                  fontSizeBlurred: 22,
                  colorBlurred: '#FFF',
                }}
                showCountdown
                isPassword={true}
                maxLength={110}
                countdownLabel="/110"
                value={isConfirmPassword}
                onChangeText={value => setIsConfirmPassword(value)}
                togglePassword={false}
                customShowPasswordComponent={<></>}
                customHidePasswordComponent={<></>}
                rightComponent={
                  <FontAwesomeIcon
                    icon={faTimesCircle}
                    size={15}
                    color="#cacaca"
                    style={styles.iconStyle}
                  />
                }
              />
            </ViewInput>
            <ViewButton>
              {/* <ButtonDisble
                color={
                  isMatchPassword === true
                    ? ['#257CE2', '#33D52B', '#99FF00']
                    : ['#7F8181', '#E7E7E7']
                }
                // onPress={() => navigation.navigate('SuccessChangePassword')}
                onPress={ChangePassword}
                title="Confirm"
                padding={5}
                marginTop={50}
              /> */}
              <ButtonGreen1
                // onPress={() => navigation.navigate('SuccessChangePassword')}
                onPress={ChangePassword}
                title="Confirm"
                padding={5}
                marginTop={50}
              />
            </ViewButton>
          </ViewAll>
        </ScrollView>
      </SafeAreaView>
    </Background>
  );
}
const styles = StyleSheet.create({
  safeArea: {
    marginTop: '10%',
    flex: 1,
  },
  imageLogo: {
    width: width * 0.3,
    height: height * 0.14,
  },
  containerStyles: {
    borderBottomWidth: 1,
    paddingHorizontal: 10,
    borderBottomColor: 'white',
    borderRadius: 8,
    marginTop: '7%',
  },
  inputStyles: {
    color: 'white',
    paddingBottom: 1,
    fontSize: 22,
    fontFamily: 'DBHelvethaicaX-55Regular',
    paddingLeft: 5,
  },
  labelStyles: {
    fontFamily: 'DBHelvethaicaX-55Regular',
    marginTop: '3%',
    fontSize: 22,
    bottom: 0,
  },
  showCountdownStyles: {
    color: 'gray',
    fontSize: 18,
    fontFamily: 'DBHelvethaicaX-55Regular',
    bottom: -23,
    end: 25,
  },
  iconStyle: {
    marginTop: 20,
    backgroundColor: '#fff',
    borderRadius: 15,
  },
});
const Background = styled(ImageBackground)`
  flex: 1;
  padding-top: 40px;
`;
const ViewAll = styled(View)`
  padding-horizontal: 20px;
`;
const ViewImage = styled(View)`
  align-items: center;
  justify-content: center;
`;
const ViewInput = styled(View)`
  width: 100%;
`;
const ViewButton = styled(View)`
  margin-top: 5%;
`;
