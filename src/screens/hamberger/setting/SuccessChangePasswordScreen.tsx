import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {DBHText} from '../../../components/StyledText';
import styled from 'styled-components';

const {width, height} = Dimensions.get('window');

export default function SuccessChangePasswordScreen({
  navigation,
}: {
  navigation: any;
}) {
  return (
    <Background
      source={require('../../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <SafeAreaView style={styles.safeArea}>
        <ViewAll>
          <ViewImage>
            <Image
              source={require('../../../assets/images/create_pass_key.png')}
              style={styles.imageLogo}
              resizeMode="contain"
            />
          </ViewImage>
          <ViewText>
            <DBHText size={36} style={styles.textStyle}>
              We already {'\n'} changed your password
            </DBHText>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
              <DBHText
                size={24}
                color={'#99FF00'}
                style={[styles.textStyle, {marginTop: '8%'}]}>
                Back To Home
              </DBHText>
            </TouchableOpacity>
          </ViewText>
        </ViewAll>
      </SafeAreaView>
    </Background>
  );
}
const styles = StyleSheet.create({
  safeArea: {
    marginTop: '10%',
    flex: 1,
  },
  imageLogo: {
    width: width * 0.3,
    height: height * 0.14,
  },
  textStyle: {
    textAlign: 'center',
  },
});
const Background = styled(ImageBackground)`
  flex: 1;
  padding-top: 40px;
`;
const ViewAll = styled(View)`
  padding-horizontal: 20px;
  align-content: center;
  justify-content: center;
  align-items: center;
`;
const ViewImage = styled(View)`
  align-items: center;
  justify-content: center;
`;
const ViewText = styled(View)`
  padding-top: 3%;
  align-items: center;
  width: 80%;
`;
// justify-content: center;
// align-items: center;
