import React from 'react';
import {View, ImageBackground} from 'react-native';
import {DBHText} from '../../components/StyledText';
import styled from 'styled-components';

export default function KYCScreen() {
  return (
    <Background
      source={require('../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <DBHText size={26}>KYCScreen</DBHText>

      <ViewAll>
        <DBHText size={26} color="#e7e7e7">
          Private
        </DBHText>
        <ViewTexts>
          <DBHText size={18} color="#e7e7e7">
            No KYC
          </DBHText>
          <DBHText size={18} color="#e7e7e7">
            {'\t'}/{'\t'}
          </DBHText>
          <DBHText size={18} color="#e7e7e7">
            Waiting
          </DBHText>
          <DBHText size={18} color="#e7e7e7">
            {'\t'}/{'\t'}
          </DBHText>
          <DBHText size={18} color={'rgb(0,203,36)'}>
            Success
          </DBHText>
        </ViewTexts>
      </ViewAll>
    </Background>
  );
}

const Background = styled(ImageBackground)`
  flex: 1;
  padding: 25px;
  padding-top: 40px;
`;
const ViewAll = styled(View)`
  padding-top: 5%;
  flex-direction: row;
  text-align: center;
  justify-content: space-between;
`;
const ViewTexts = styled(View)`
  flex-direction: row;
  align-items: center;
`;
