export const data = {
  type: 'save',
  video: [
    {
      id: '1',
      username: 'ยูสเซอร์เนม',
      title: 'Today . Movie - Gamer -TV Show',
      profile:
        'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      image:
        'https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
      star: '99k',
      comment: '99l',
      share: '99k',
      view: '99k',
      bio: 'หัวเรื่องหรือพาดหัว (headline) เป็นข้อความที่อยู่บนสุดของบทความแสดงถึงประเด็นหลักของบทความ หัวเรื่องมักถูกออกแบบให้โดดเด่นสะดุดตา และใช้คำที่เลือกสรรมาให้เกี่ยวข้องกับหัวเรื่องหัวเรื่องหรือพาดหัวมักจะถูกเขียนหรือพิมพ์ด้วยรูปแบบอย่างย่อ ซึ่งอาจเรียบเรียงอย่างไม่เป็นประโยคหรือไวยากรณ์ที่ไม่สมบูรณ์',
    },
    {
      id: '2',
      username: 'ยูสเซอร์เนม',
      title: 'Today . Movie - Gamer -TV Show',
      profile:
        'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      image:
        'https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
      star: '99k',
      comment: '99l',
      share: '99k',
      view: '99k',
      bio: 'หัวเรื่องหรือพาดหัว (headline) เป็นข้อความที่อยู่บนสุดของบทความแสดงถึงประเด็นหลักของบทความ หัวเรื่องมักถูกออกแบบให้โดดเด่นสะดุดตา และใช้คำที่เลือกสรรมาให้เกี่ยวข้องกับหัวเรื่องหัวเรื่องหรือพาดหัวมักจะถูกเขียนหรือพิมพ์ด้วยรูปแบบอย่างย่อ ซึ่งอาจเรียบเรียงอย่างไม่เป็นประโยคหรือไวยากรณ์ที่ไม่สมบูรณ์',
    },
    {
      id: '3',
      username: 'ยูสเซอร์เนม',
      title: 'Today . Movie - Gamer -TV Show',
      profile:
        'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      image:
        'https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
      star: '99k',
      comment: '99l',
      share: '99k',
      view: '99k',
      bio: 'หัวเรื่องหรือพาดหัว (headline) เป็นข้อความที่อยู่บนสุดของบทความแสดงถึงประเด็นหลักของบทความ หัวเรื่องมักถูกออกแบบให้โดดเด่นสะดุดตา และใช้คำที่เลือกสรรมาให้เกี่ยวข้องกับหัวเรื่องหัวเรื่องหรือพาดหัวมักจะถูกเขียนหรือพิมพ์ด้วยรูปแบบอย่างย่อ ซึ่งอาจเรียบเรียงอย่างไม่เป็นประโยคหรือไวยากรณ์ที่ไม่สมบูรณ์',
    },
    {
      id: '4',
      username: 'ยูสเซอร์เนม',
      title: 'Today . Movie - Gamer -TV Show',
      profile:
        'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      image:
        'https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
      star: '99k',
      comment: '99l',
      share: '99k',
      view: '99k',
      bio: 'หัวเรื่องหรือพาดหัว (headline) เป็นข้อความที่อยู่บนสุดของบทความแสดงถึงประเด็นหลักของบทความ หัวเรื่องมักถูกออกแบบให้โดดเด่นสะดุดตา และใช้คำที่เลือกสรรมาให้เกี่ยวข้องกับหัวเรื่องหัวเรื่องหรือพาดหัวมักจะถูกเขียนหรือพิมพ์ด้วยรูปแบบอย่างย่อ ซึ่งอาจเรียบเรียงอย่างไม่เป็นประโยคหรือไวยากรณ์ที่ไม่สมบูรณ์',
    },
    {
      id: '5',
      username: 'ยูสเซอร์เนม',
      title: 'Today . Movie - Gamer -TV Show',
      profile:
        'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      image:
        'https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
      star: '99k',
      comment: '99l',
      share: '99k',
      view: '99k',
      bio: 'หัวเรื่องหรือพาดหัว (headline) เป็นข้อความที่อยู่บนสุดของบทความแสดงถึงประเด็นหลักของบทความ หัวเรื่องมักถูกออกแบบให้โดดเด่นสะดุดตา และใช้คำที่เลือกสรรมาให้เกี่ยวข้องกับหัวเรื่องหัวเรื่องหรือพาดหัวมักจะถูกเขียนหรือพิมพ์ด้วยรูปแบบอย่างย่อ ซึ่งอาจเรียบเรียงอย่างไม่เป็นประโยคหรือไวยากรณ์ที่ไม่สมบูรณ์',
    },
  ],
};
