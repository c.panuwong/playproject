import React from 'react';
import {
  View,
  ImageBackground,
  SafeAreaView,
  Image,
  StyleSheet,
  useWindowDimensions,
} from 'react-native';
import {DBHText} from '../../../components/StyledText';
import styled from 'styled-components';
import ToptabImageScreen from '../../profiles/components/toptapScreen/ToptabImageScreen';
import ToptabShopScreen from '../../profiles/components/toptapScreen/ToptabShopScreen';
import ToptabVideoScreen from '../../profiles/components/toptapScreen/ToptabVideoScreen';
import {
  TabView,
  SceneMap,
  TabBar,
  TabViewAnimated,
} from 'react-native-tab-view';
import vdoicon from '../../../assets/images/icons/vdo.png';
import shopicon from '../../../assets/images/icons/product.png';
import imageicon from '../../../assets/images/icons/image.png';
import Arrow from 'react-native-vector-icons/AntDesign';

export default function SavedScreen({navigation}: {navigation: any}) {
  const TabVideo1 = () => (
    <>
      <ToptabVideoScreen navigation={navigation} />
    </>
  );
  const TabImage1 = () => (
    <>
      <ToptabImageScreen navigation={navigation} />
    </>
  );
  const TabShop1 = () => (
    <>
      <ToptabShopScreen navigation={navigation} />
    </>
  );
  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'tabvideo', title: 'Video', icon: vdoicon},
    {key: 'tabimage', title: 'Image', icon: imageicon},
    {key: 'tabshop', title: 'Profile', icon: shopicon},
  ]);
  const renderScene = SceneMap({
    tabvideo: TabVideo1,
    tabimage: TabImage1,
    tabshop: TabShop1,
  });

  return (
    <Background
      source={require('../../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <>
        <TabView
          navigationState={{index, routes}}
          renderScene={renderScene}
          onIndexChange={setIndex}
          initialLayout={{width: layout.width}}
          tabBarPosition={'bottom'}
          renderTabBar={props => (
            <TabBar
              {...props}
              renderLabel={({route, focused}) => {
                if (focused) {
                  return (
                    <Image
                      source={route.icon}
                      tintColor={'#99ff00'}
                      style={{width: 26, height: 26}}
                    />
                    // <DBHText style={{color: '#99FF00'}}>{route.icons}</DBHText>
                  );
                } else {
                  return (
                    <Image
                      source={route.icon}
                      tintColor={'#fff'}
                      style={{width: 26, height: 26}}
                    />
                  );
                }
              }}
              style={{backgroundColor: ''}}
              indicatorStyle={{backgroundColor: '#99FF00'}}
              active={'#99FF00'}
            />
          )}
        />
      </>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
  padding: 25px;
  padding-top: 40px;
`;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
