import React from 'react';
import {
  View,
  ImageBackground,
  SafeAreaView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import styled from 'styled-components';
import {DBHText} from '../../../components/StyledText';
import LinearGradient from 'react-native-linear-gradient';
import {faEllipsisV} from '@fortawesome/pro-regular-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import ReadMore from '@fawazahmed/react-native-read-more';
import {data} from './data';

const {width, height} = Dimensions.get('window');

export default function ContentScreen({navigation}: {navigation: any}) {
  return (
    <Background
      source={require('../../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <SafeAreaView>
        <View>
          <FlatList
            data={data.video}
            listKey="save"
            numColumns={1}
            renderItem={data => {
              return (
                <View style={{flex: 1, marginBottom: '60%'}}>
                  <ViewProfile>
                    <Viewheader>
                      <Linear
                        start={{x: 0.2, y: 1.5}}
                        end={{x: 1.3, y: 0}}
                        colors={['#257CE2', '#33D52B', '#99FF00']}>
                        <Image
                          source={{uri: data.item.profile}}
                          style={styles.styleImage}
                          resizeMode="cover"
                        />
                      </Linear>
                      <ViewTextHeader>
                        <DBHText size={22}>{data.item.username}</DBHText>
                        <DBHText size={16} color="#b3b3b3">
                          {data.item.title}
                        </DBHText>
                      </ViewTextHeader>
                    </Viewheader>
                    <TouchableOpacity>
                      <FontAwesomeIcon
                        icon={faEllipsisV}
                        size={25}
                        color="#ffff"
                      />
                    </TouchableOpacity>
                  </ViewProfile>

                  <ImageContent
                    source={{uri: data.item.image}}
                    resizeMode="cover"
                  />
                  <NumberView>
                    <DBHText size={16} color="#99ff00">
                      View {data.item.view}
                    </DBHText>
                  </NumberView>

                  <ViewBottomAll>
                    <Touchable>
                      <ImagegalaxyEngagement
                        source={require('../../../assets/images/galaxyicon/likepostingalaxy.png')}
                        resizeMode="contain"
                        color="white"
                        // tintColor={'#99ff00'}
                        tintColor={'#ffff'}
                      />
                      <TextEngagement>{data.item.star}</TextEngagement>
                    </Touchable>
                    <Touchable>
                      <ImageGalaxyComment
                        source={require('../../../assets/images/galaxyicon/commenticon.png')}
                        resizeMode="contain"
                        color="white"
                        // tintColor={'#99ff00'}
                        tintColor={'#ffff'}
                      />
                      <TextEngagement>{data.item.comment}</TextEngagement>
                    </Touchable>
                    <Touchable>
                      <ImageGalaxyShare
                        source={require('../../../assets/images/galaxyicon/shareicongalaxy.png')}
                        resizeMode="contain"
                        color="white"
                        // tintColor={'#99ff00'}
                        tintColor={'#ffff'}
                      />
                      <TextEngagement>{data.item.share}</TextEngagement>
                    </Touchable>
                  </ViewBottomAll>
                  <ViewDescription>
                    <TextSubDescription
                      numberOfLines={3}
                      seeMoreText="Read More >>"
                      seeMoreStyle={styles.seeMoreStyle}
                      seeLessText="<< Less"
                      seeLessStyle={styles.seeLessStyle}
                      ellipsis="">
                      {data.item.bio}
                    </TextSubDescription>
                  </ViewDescription>
                  <ViewLine />
                </View>
              );
            }}
            keyExtractor={item => item.id}
          />
        </View>
      </SafeAreaView>
    </Background>
  );
}
const styles = StyleSheet.create({
  styleImage: {
    width: width * 0.1,
    height: width * 0.1,
    borderRadius: width * 0.035,
    borderWidth: 2,
    borderColor: '#FFFFFF',
  },
  seeMoreStyle: {
    color: '#e7e7e7e7',
    fontSize: 20,
    left: 1,
    alignSelf: 'center',
    fontFamily: 'DBHelvethaicaX-55Regular',
  },
  seeLessStyle: {
    color: '#e7e7e7e7',
    fontSize: 20,
    alignSelf: 'center',
    fontFamily: 'DBHelvethaicaX-55Regular',
  },
});
//   padding: 25px;
//
const Background = styled(ImageBackground)`
  flex: 1;
  padding-top: 40px;
`;

const ViewProfile = styled(View)`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 2%;
`;
const Linear = styled(LinearGradient)`
  justify-content: center;
  align-items: center;
  align-content: center;
  border-radius: 17px;
  width: 23px;
  height: 23px;
  padding: 23px;
`;
const ViewTextHeader = styled(View)`
  padding-horizontal: 2%;
`;
const Viewheader = styled(View)`
  flex-direction: row;
  align-items: center;
  align-content: center;
  justify-content: center;
`;

const ImageContent = styled(Image)`
  width: 100%;
  height: 100%;
`;
const NumberView = styled(View)`
  align-items: flex-end;
  padding: 1% 5%;
`;
const ViewBottomAll = styled(View)`
  flex-direction: row;
  justify-content: space-between;
  padding: 1% 0px 3% 0px;
  margin-horizontal:5%
  border-bottom-width: 1px;
  border-color: #fff;

`;
const ImageIcon = styled(Image)`
  width: 20px;
  height: 20px;
`;
//   justify-content: space-between;
const ImageGalaxyShare = styled(Image)`
  width: 30px;
  height: 22.26px;
`;
const ImageGalaxyComment = styled(Image)`
  width: 25px;
  height: 23.33px;
`;
const ImagegalaxyEngagement = styled(Image)`
  width: 28.5px;
  height: 28px;
  margin-top: -5px;
`;
const TextEngagement = styled(DBHText)`
  color: #b3b3b3;
  font-size: 20px;
  padding-horizontal: 2%;
`;
const Touchable = styled(TouchableOpacity)`
  flex-direction: row;
`;
const TextSubDescription = styled(ReadMore)`
  font-family: DBHelvethaicaX-55Regular;
  font-size: 20px;
  color: #b3b3b3;
`;
const ViewDescription = styled(View)`
  padding-horizontal: 5%;
  padding-vertical: 2%;
`;
const RowReadDesc = styled(View)`
  width: 85%;
  align-self: center;
  padding-bottom: 15px;
  margin-top: 20px;
  margin-bottom: 20px;
`;
const ViewLine = styled(View)`
  padding-vertical: 1%;
  border-bottom-width: 5px;
  border-color: #21263e;
`;
