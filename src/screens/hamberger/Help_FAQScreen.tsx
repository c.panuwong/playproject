import React, {useState, useRef} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  FlatList,
  Animated,
  ScrollView,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {DBHText} from '../../components/StyledText';
import styled from 'styled-components';
import {faAngleDown, faAngleUp} from '@fortawesome/pro-regular-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import dataQA from './components/dataQA';
import HeaderAnimated from '../../components/HeaderAnimated';

const {width, height} = Dimensions.get('window');

export default function Help_FAQScreen({navigation}: {navigation: any}) {
  const [isOpenAnswer, setIsOpenAnswer] = useState([]);
  const [isStatus, setIsStatus] = useState(false);
  const childRef = useRef<any>();

  console.log(isOpenAnswer);

  const handleQuestion = () => {
    setIsOpenAnswer(!isOpenAnswer);
  };

  return (
    <Background
      source={require('../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <HeaderAnimated
        ref={childRef}
        title="Help (FAQ)"
        navigation={navigation}
      />
      {/* <Collapse>
        <View>
          <View>
            <Text>FORWARD</Text>
          </View>
        </View>
        <View>
          <View>
            <Text>Aaron Bennet</Text>
          </View>
          <View>
            <Text>Claire Barclay</Text>
          </View>
          <View>
            <Text>Kelso Brittany</Text>
          </View>
        </View>
      </Collapse> */}
      {/* <SafeAreaView> */}
      <Animated.ScrollView
        style={{flex: 1}}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={true}
        stickyHeaderIndices={[2]}
        onScroll={e => childRef.current.handleLogo(e)}>
        <View style={{padding: 25}}>
          <DBHText size={34}>Help (FAQ)</DBHText>
          <View style={{flex: 1, marginTop: '5%'}}>
            <FlatList
              data={dataQA}
              listKey="QA"
              number={1}
              keyExtractor={item => item.id}
              renderItem={data => {
                return (
                  <TouchableOpacityContent
                    onPress={() => setIsStatus(!isStatus)}>
                    <ViewAll>
                      <ViewQ>
                        <DBHText>Q : {data.item.question}</DBHText>
                      </ViewQ>
                      <ViewIcon>
                        <FontAwesomeIcon
                          icon={faAngleDown}
                          size={25}
                          color="#ffffff"
                          style={{paddingHorizontal: '7%'}}
                        />
                      </ViewIcon>
                    </ViewAll>
                    {isStatus === true ? (
                      <ViewAnswer>
                        <DBHText style={{color: '#b3b3b3'}}>
                          {' '}
                          {data.item.answer}
                        </DBHText>
                      </ViewAnswer>
                    ) : (
                      <></>
                    )}
                  </TouchableOpacityContent>
                );
              }}
            />
          </View>
        </View>
      </Animated.ScrollView>
      {/* </SafeAreaView> */}
    </Background>
  );
}
{
  /* <DBHText>Q : {dataQA[0].answer}</DBHText> */
}

const Background = styled(ImageBackground)`
  flex: 1;
`;
const TouchableOpacityContent = styled(TouchableOpacity)`
  flex: 1;
  padding-vertical: 2%;
  border-bottom-width: 1px;
  border-color: #7f7f7f;
`;

const ViewQ = styled(View)`
  flex: 0.9;
`;
const ViewIcon = styled(View)`
  align-items: center;
  justify-content: center;
  flex: 0.1;
`;

const ViewAnswer = styled(View)`
  padding-vertical: 2%;
`;
const ViewAll = styled(View)`
  flex-direction: row;
`;
