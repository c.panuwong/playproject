import React, {useRef} from 'react';
import {
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
// @ts-ignore
import styled from 'styled-components';
import {DBHText} from '../../components/StyledText';
import ModallizeChangeImage from './components/ModallizeChangeImage';
import {Modalize} from 'react-native-modalize';
import {ScrollView} from 'react-native-gesture-handler';
import RadioButton from '../../components/RadioButton';
import {ButtonGreen1} from '../../components/ButtonComponent';
// import { TextInput } from 'react-native-paper';
const datasex = [
  {
    key: '0',
    text: 'Not specified',
  },
  {
    key: '1',
    text: 'Male',
  },
  {
    key: '2',
    text: 'Famale',
  },
];
export default function PersonalInfo({navigation}: {navigation: any}) {
  const [value, setValue] = React.useState('0');
  const modalizeRef = useRef<Modalize>();
  const onOpenChangeImage = () => {
    modalizeRef.current?.open();
  };
  return (
    <Background
      source={require('../../assets/images/BG_5.jpg')}
      resizeMode="cover">
      <DBHText size={34}>Personal Info</DBHText>
      <ScrollView>
        <View
          style={{
            alignItems: 'center',
            width: '100%',
            height: '100%',
            padding: 10,
          }}>
          <ViewImage onPress={() => onOpenChangeImage()} activeOpacity={0}>
            <Imageslide
              source={require('../../assets/images/mockimage/Rectangle4100.png')}
            />
          </ViewImage>
          <DBHText size={18} style={{marginTop: 10}}>
            User: 1234123
          </DBHText>
          <ShowDataPerson
            label="Name"
            value="Songded taweeporn"
            onPress={() => navigation.navigate('ChangeNameAndBirth')}
          />
          <ShowDataPerson label="Phone Number" value="0970422222" />
          <ShowDataPerson label="Email" value="songded.ta@ekkolab.ai" />
          <View style={{width: '100%'}}>
            <DBHText color="#999999" style={{marginLeft: 10}}>
              For user’s password recovery
            </DBHText>
          </View>
          <ViewSex>
            <DBHText size={20}>Sex</DBHText>
            <View style={{flexDirection: 'row', width: '100%', marginTop: 10}}>
              <RadioButton value={value} setvalue={setValue} data={datasex} />
            </View>
          </ViewSex>
          <ShowDataPerson
            label="Birth Date"
            value="31 มิถุนายน 2800"
            onPress={() => navigation.navigate('ChangeNameAndBirth')}
          />
          <ViewButton>
            <ButtonGreen1
              onPress={() =>
                navigation.navigate('VeriflyOTP', {
                  number: '1234567',
                  otp: '111111',
                })
              }
              title="Save"
              padding={5}
              marginTop={20}
            />
          </ViewButton>
        </View>
      </ScrollView>

      <ModallizeChangeImage modalizeRef={modalizeRef} navigation={navigation} />
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
  padding: 20px;
  padding-top: 40px;
`;
const Imageslide = styled(Image)`
  width: 80px;
  height: 80px;
  border-radius: 30px;
  border-width: 3px;
  border-color: #fff;
`;
const ViewImage = styled(TouchableOpacity)`
  width: 84px;
  height: 84px;
  border-radius: 31px;
  border-width: 2px;
  border-color: #99ff00;
  background-color: transparent;
`;
const ViewSex = styled(View)`
  width: 100%;
  flex-direction: column;
  margin-left: 20px;
  margin-top: 20px;
`;
const ViewButton = styled(View)`
  width: 100%;
`;

const Input = styled(TextInput)`
  width: 100%;
  border-bottom-width: 1px;
  border-color: #7f7f7f;
  padding-left: 10px;
  color: white;
  font-size: 20px;
  margin-top: 20px;
  font-family: DBHelvethaicaX-55Regular;
  background-color: transparent;
`;
const InputBirthDate = styled(TextInput)`
  width: 100%;
  border-bottom-width: 1px;
  border-color: #7f7f7f;
  padding-bottom: 5px;
  padding-left: 10px;
  padding-top: 20px;
  color: white;
  font-size: 20px;
  font-family: DBHelvethaicaX-55Regular;
`;
const ShowDataPerson = ({
  label,
  value,
  onPress,
}: {
  label: any;
  value: any;
  onPress: any;
}) => {
  return (
    <>
      <ViewDataPerson onPress={onPress}>
        <DBHText size={18} color="gray">
          {label}
        </DBHText>
        <DBHText size={20}>{value}</DBHText>
      </ViewDataPerson>
    </>
  );
};
const ViewDataPerson = styled(TouchableOpacity)`
  width: 100%;
  border-bottom-width: 1px;
  border-color: #7f7f7f;
  padding-left: 10px;
  padding-bottom: 7px;
  margin-top: 15px;
  background-color: transparent;
`;
