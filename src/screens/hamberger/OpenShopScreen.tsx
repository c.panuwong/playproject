import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Alert,
  Image,
  TextStyle,
  Style,
  Dimensions,
} from 'react-native';
import {DBHText} from '../../components/StyledText';
import openShop from './components/TextOpenShop';
import {Checkbox} from 'react-native-paper';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView} from 'react-native-gesture-handler';
import {ButtonDisble} from '../../components/ButtonComponent';
import styled from 'styled-components';

const {width, height} = Dimensions.get('window');

interface ScrollProps {
  layoutMeasurement: any;
  contentOffset: any;
  contentSize: any;
}
const isCloseToBottom = ({
  layoutMeasurement,
  contentOffset,
  contentSize,
}: ScrollProps) => {
  const paddingToBottom = 20;
  return (
    layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom
  );
};

export default function OpenShopScreen() {
  const [checkboxDisable, setCheckboxDisable] = useState<boolean>(true);
  const [checkbox, setCheckbox] = useState<boolean>(false);

  return (
    <Background
      source={require('../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <SafeAreaView style={{flex: 1}} edges={['top']}>
        <ViewHeader>
          <DBHText size={36}>Open Shop</DBHText>
        </ViewHeader>

        <ViewOpenShop>
          <ScrollView
            scrollEventThrottle={16}
            onScroll={({nativeEvent}) => {
              if (isCloseToBottom(nativeEvent)) {
                setCheckboxDisable(false);
              }
            }}>
            <ViewImage>
              <Image
                source={require('../../assets/images/icons/shop.png')}
                resizeMode="contain"
                style={{width: width * 0.3, height: height * 0.14}}
              />
            </ViewImage>
            <DBHText size={26}>{openShop.open_shop}</DBHText>
          </ScrollView>
          <ViewCheckbox>
            <Checkbox.Android
              status={checkbox ? 'checked' : 'unchecked'}
              disabled={checkboxDisable}
              onPress={() => {
                setCheckbox(!checkbox);
              }}
              color="#1ADF45"
              uncheckedColor="white"
            />
            <DBHText size={22}>Accept Policy</DBHText>
          </ViewCheckbox>
        </ViewOpenShop>
        <ViewButton>
          <ButtonDisble
            color={
              checkbox
                ? ['#257CE2', '#33D52B', '#99FF00']
                : ['#7F8181', '#E7E7E7']
            }
            title="Next"
            padding={5}
            marginTop={0}
            onPress={() => {
              checkbox
                ? // ? navigation.navigate('')
                  Alert.alert('Next')
                : Alert.alert('คำเตือน', 'กรุณายอมรับเงื่อนไข');
            }}
          />
        </ViewButton>
      </SafeAreaView>
    </Background>
  );
}

const Background = styled(ImageBackground)`
  flex: 1;
  padding-top: 30px;
`;
const ViewOpenShop = styled(View)`
  width: 100%;
  height: 80%;
  padding-horizontal: 30px;
`;
const ViewCheckbox = styled(View)`
  width: 100%;
  flex-direction: row;
  align-items: center;
  padding-top: 10px;
`;
const ViewButton = styled(View)`
  width: 100%;
  height: 15%;
  background-color: #060c30;
  justify-content: center;
  padding-horizontal: 8%;
`;
const ViewImage = styled(View)`
  width: 100%;
  align-items: center;
  margin-vertical: 4%;
`;

const ViewHeader = styled(View)`
  padding-horizontal: 30px;
`;
