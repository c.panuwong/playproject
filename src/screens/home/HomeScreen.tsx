import React, {useState, useEffect} from 'react';
import {
  Animated,
  View,
  Text,
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  FlatList,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import SlideBox from './SlideBox';
import HeaderHome from '../../components/home/HeaderHome';
import HashTag from '../../components/home/Hashtag';
import Galaxy from '../../components/home/Galaxy';
// import FeedFlat from '../../components/home/FeedFlat';
import FeedNoFlat from '../../components/home/FeedNoFlat';

// @ts-ignore
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faChevronSquareRight, faBell} from '@fortawesome/pro-regular-svg-icons';

import {Col, Row, Grid} from 'react-native-easy-grid';
// @ts-ignore
import styled from 'styled-components';
import {getdatapagiante, gethashtag, getpopcreater} from './httprequest';
import jwt_decode from 'jwt-decode';
const HomeScreen = ({navigation}: {navigation: any}) => {
  let [fadeAnimation] = useState<any>(new Animated.Value(1));
  const [page, setpage] = useState(1);
  const [contents, setContents] = useState<any>([]);
  const [hashtag, setHashtag] = useState<any>([]);
  const [popcreater, setPopCreater] = useState<any>([]);
  let [fadeAnimationswap] = useState<any>(new Animated.Value(0));
  let [fadeAnimationGalaxy] = useState<any>(new Animated.Value(1));

  const getdatastart = async () => {
    await getdatapagiante(page).then(res => {
      setContents(jwt_decode(res.data.data_encode));
    });
  };
  const getdatahashtag = async () => {
    await gethashtag(page).then(res => {
      setHashtag(jwt_decode(res.data.data_encode));
    });
  };
  const getdatapopcreater = async () => {
    await getpopcreater(page).then(res => {
      setPopCreater(jwt_decode(res.data.data_encode));
    });
  };
  useEffect(() => {
    Promise.all([getdatastart(), getdatahashtag(), getdatapopcreater()]);
  }, []);
  const fadeIn = () => {
    Animated.timing(fadeAnimation, {
      toValue: 1,
      duration: 200,
      useNativeDriver: true,
    }).start();
  };

  const fadeOut = () => {
    Animated.timing(fadeAnimation, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };

  const fadeInswap = () => {
    Animated.timing(fadeAnimationswap, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  };

  const fadeOutswap = () => {
    Animated.timing(fadeAnimationswap, {
      toValue: 0,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  };

  const fadeInGalaxy = () => {
    Animated.timing(fadeAnimationGalaxy, {
      toValue: 1,
      duration: 0,
      useNativeDriver: true,
    }).start();
  };

  const fadeOutGalaxy = () => {
    Animated.timing(fadeAnimationGalaxy, {
      toValue: 0,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  };
  const handleLogo = (event: any) => {
    if (event.nativeEvent.contentOffset.y == 0) {
      fadeIn();
      fadeOutswap();
      fadeInGalaxy();
    } else if (
      event.nativeEvent.contentOffset.y > 0 &&
      event.nativeEvent.contentOffset.y <= 600
    ) {
      fadeOut();
      fadeInswap();
      fadeInGalaxy();
    } else {
      fadeOutGalaxy();
    }
  };

  const GoToPopAll = () => {
    navigation.navigate('PopGalaxyAll', {screen: 'PopGalaxyAllScreen'});
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#000000'}}>
      <Grid>
        <HeaderHome
          fadeAnimationswap={fadeAnimationswap}
          fadeAnimation={fadeAnimation}
          navigation={navigation}
        />
        <Row>
          <Animated.ScrollView
            style={{flex: 1, backgroundColor: '#000000'}}
            contentContainerStyle={{flexGrow: 1, backgroundColor: '#000000'}}
            showsVerticalScrollIndicator={true}
            stickyHeaderIndices={[2]}
            onScroll={handleLogo}>
            <View style={{height: 200, width: '100%'}}>
              <SlideBox navigation={navigation} />
            </View>
            <View style={{backgroundColor: '#060C30'}}>
              <Row>
                <ColTextBelowSlide>
                  <TextBelowSlide>POP GALLERY</TextBelowSlide>
                </ColTextBelowSlide>
                <Col>
                  <TouchableOpacity onPress={GoToPopAll}>
                    <FontAwesomeIcon
                      icon={faChevronSquareRight}
                      size={31}
                      color="#fff"
                      style={{marginTop: 10, right: 3}}
                    />
                  </TouchableOpacity>
                </Col>
              </Row>
            </View>
            <View style={{opacity: fadeAnimationGalaxy}}>
              <Galaxy popcreater={popcreater}/>
            </View>
            <HashTag
              navigation={navigation}
              ImageMore={ImageMore}
              ColTextBelowSlide={ColTextBelowSlide}
              hashtag={hashtag}
            />
            <FeedNoFlat navigation={navigation} data={contents} />
          </Animated.ScrollView>
        </Row>
      </Grid>
    </SafeAreaView>
  );
};

const ImageMore = styled(Image)`
  width: 25px;
  height: 25px;
  margin: 10px 0 0 0;
`;
const ColTextBelowSlide = styled(Col)`
  width: 90%;
`;
const TextBelowSlide = styled(Text)`
  font-size: 20px;
  color: #fff;
  padding: 10px;
`;
export default HomeScreen;
