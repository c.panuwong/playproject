import React, {useState} from 'react';
import {View, Text} from 'react-native';
// @ts-ignore
import {SliderBox} from 'react-native-image-slider-box';
import Navigation from '../../navigations';

interface ISlider {
  picture_url: string[];
}
export default function SlideBox({navigation}: {navigation: any}) {
  const [homeData] = useState<ISlider>({
    picture_url: [
      require('../../assets/images/mockimage/Rectangle3753-2.png'),
      'https://cdn.pixabay.com/photo/2015/03/26/09/41/chain-690088_960_720.jpg',
      'https://cdn.pixabay.com/photo/2012/12/27/19/40/chain-link-72864_960_720.jpg',
    ],
  });
  return (
    <View style={{backgroundColor: '#333333'}}>
      <SliderBox
        images={homeData.picture_url}
        key={homeData.picture_url}
        onCurrentImagePressed={(index: any) => 
          navigation.navigate('Login')
        }
        // disableOnPress={true}
        autoplay
        circleLoop
        activeOpacity={5}
        resizeMethod={'resize'}
        resizeMode={'cover'} // warning ! ที่รูปมันไม่เต็มเพราะบังคับให้มันเป็น contain ดังนั้นรูปที่ได้มาตวรมีสเกลทีสูง 180 ยาว 90% ของจอ
        dotStyle={{
          width: 15,
          height: 12,
          borderRadius: 50,
        }}
        ImageComponentStyle={{
          // borderRadius: 20,
          width: '100%',
          height: '100%',
          borderWidth: 1,
          // borderColor: 'grey',
        }}
        paginationBoxStyle={{
          alignSelf: 'flex-end',
        }}
      />
    </View>
  );
}
