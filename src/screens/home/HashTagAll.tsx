import React, { useState } from 'react';
import {
    Animated,
    View,
    Text,
    SafeAreaView,
    ScrollView,
    Image,
    StyleSheet,
    FlatList,
    ImageBackground,
    Alert,
    TouchableOpacity
} from 'react-native';
import SlideBox from './SlideBox';
import HeaderHome from '../../components/home/HeaderHome';
import HashTag from '../../components/home/Hashtag';
import Galaxy from '../../components/home/Galaxy';
// import FeedFlat from '../../components/home/FeedFlat';
import FeedNoFlat from '../../components/home/FeedNoFlat';

// @ts-ignore
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faChevronSquareRight, faBell } from '@fortawesome/pro-regular-svg-icons';

import { Col, Row, Grid } from 'react-native-easy-grid';
// @ts-ignore
import styled from 'styled-components';
import { SearchBar } from 'react-native-elements';

const HashtagAll = ({ navigation }: { navigation: any }) => {

    const DATA = [
        {
            'id': '1',
            'hashtag_name': 'OnlyFans',
            'hashtag_count_galaxy': '99k Galaxys',
        },
        {
            'id': '2',
            'hashtag_name': 'ทอยทอย',
            'hashtag_count_galaxy': '99k Galaxys',
        },
        {
            'id': '3',
            'hashtag_name': 'ชายต๊องหญิงเพี้ยน',
            'hashtag_count_galaxy': '99k Galaxys',
        },
        {
            'id': '4',
            'hashtag_name': 'เด็กรังสิตหลุด',
            'hashtag_count_galaxy': '99k Galaxys',
        },
        {
            'id': '5',
            'hashtag_name': 'หลุดทางบ้าน',
            'hashtag_count_galaxy': '99k Galaxys',
        },
        {
            'id': '6',
            'hashtag_name': 'หลุดพี่เป้แทงปาก',
            'hashtag_count_galaxy': '99k Galaxys',
        }
    ];

    const ClickNameHashtagHandle = () => {
        Alert.alert('คลิกแล้วไปไหนไม่มีใครทราบ')
    }

    const Item = ({ hashtag_name, hashtag_count_galaxy }: { hashtag_name: any, hashtag_count_galaxy: any }) => (
        <>
            <RowResult>
                <Col style={{ width: '75%' }}>
                    <Row>
                        <TouchableOpacity onPress={ClickNameHashtagHandle}>
                            <TextHashtag>#{hashtag_name}</TextHashtag>
                        </TouchableOpacity>
                    </Row>
                </Col>
                <Col>
                    <TextCountGalaxy>{hashtag_count_galaxy}</TextCountGalaxy>
                </Col>
            </RowResult>
        </>
    );

    const renderItem = ({ item }: { item: any }) => (
        <Item hashtag_name={item.hashtag_name} hashtag_count_galaxy={item.hashtag_count_galaxy} />
    );

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#060c30' }}>
            <FlatList
                data={DATA}
                ListHeaderComponent={() => (
                    <RowSearchBar>
                        <SearchbarView />
                    </RowSearchBar>
                )}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
            />
        </SafeAreaView>
    );
};

const SearchbarView = () => (
    <ViewSearchBar>
        <SearchBar
            placeholder='Search'
            round
            inputContainerStyle={{ borderRadius: 25, height: 50, borderColor: '#aaa', borderBottomWidth: 1, borderTopWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, color: '#7F7F7F' }}
            containerStyle={{ backgroundColor: 'transparent', borderTopColor: 'transparent', borderBottomColor: 'transparent', }}
            // onChangeText={this.updateSearch}
            // value={search}
            placeholderTextColor={'#fff'}
            searchIcon={{ size: 35, color: '#fff' }}
        />
    </ViewSearchBar>
)

const TextHashtag = styled(Text)`
  
  font-size:15px;
  color:#99FF00;
  padding:5px 8px 0 8px;
  border-radius:20px;
  border-width:2px;
  border-color:gray;
`

const RowSearchBar = styled(Row)`
    width: 100%;
    margin-top:50px;
`;
const RowResult = styled(Row)`
    width: 100%;
    margin:10px;
`;
const ViewSearchBar = styled(View)`
    width: 100%;
    margin:0;
`;
const TextCountGalaxy = styled(Text)`
    color:#fff;
    font-size:12px;
    margin-top:8px;
`;

export default HashtagAll;
