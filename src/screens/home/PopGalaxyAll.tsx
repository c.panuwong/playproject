import React, {useState} from 'react';
import {
  Animated,
  View,
  Text,
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  FlatList,
  ImageBackground,
  Alert,
  TouchableOpacity,
} from 'react-native';
import SlideBox from './SlideBox';
import HeaderHome from '../../components/home/HeaderHome';
import HashTag from '../../components/home/Hashtag';
import Galaxy from '../../components/home/Galaxy';
// import FeedFlat from '../../components/home/FeedFlat';
import FeedNoFlat from '../../components/home/FeedNoFlat';

// @ts-ignore
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faChevronSquareRight, faBell} from '@fortawesome/pro-regular-svg-icons';

import {Col, Row, Grid} from 'react-native-easy-grid';
// @ts-ignore
import styled from 'styled-components';
import {SearchBar} from 'react-native-elements';

const PopGalaxyAll = ({navigation}: {navigation: any}) => {
  const DATA = [
    {
      id: '1',
      image: require('../../assets/images/mockimage/Rectangle3752.png'),
      galaxy_name: 'OnlyFans',
      galaxy_description: 'เรื่องเสียว เสียวยันไข่',
    },
    {
      id: '2',
      image: require('../../assets/images/mockimage/Rectangle37492.png'),
      galaxy_name: 'OnlyFans',
      galaxy_description: 'เรื่องเสียว เสียวยันไข่',
    },
    {
      id: '3',
      image: require('../../assets/images/mockimage/Rectangle3753-2.png'),
      galaxy_name: 'OnlyFans',
      galaxy_description: 'เรื่องเสียว เสียวยันไข่',
    },
    {
      id: '4',
      image: require('../../assets/images/mockimage/Rectangle3750.png'),
      galaxy_name: 'OnlyFans',
      galaxy_description: 'เรื่องเสียว เสียวยันไข่',
    },
  ];

  const Item = ({
    image,
    galaxy_name,
    galaxy_description,
  }: {
    image: any;
    galaxy_name: any;
    galaxy_description: any;
  }) => (
    <>
      <RowResult>
        <TouchableOpacity
          onPress={ClickGalaxyHandle}
          style={{width: '100%', flexDirection: 'row'}}>
          <Col style={{width: '15%'}}>
            <ImageGalaxy source={image} resizeMode="cover" />
          </Col>
          <Col style={{width: '50%'}}>
            <Row>
              <TextTitle>{galaxy_name}</TextTitle>
            </Row>
            <Row>
              <TextDesc>{galaxy_description}</TextDesc>
            </Row>
          </Col>
          <Col>
            <TouchableOpacity onPress={ClickFollowHandle}>
              <Imagefollowbtn
                source={require('../../assets/images/follow1.png')}
                resizeMode="cover"
              />
            </TouchableOpacity>
          </Col>
        </TouchableOpacity>
      </RowResult>
    </>
  );

  const renderItem = ({item}: {item: any}) => (
    <Item
      image={item.image}
      galaxy_name={item.galaxy_name}
      galaxy_description={item.galaxy_description}
    />
  );

  const ClickFollowHandle = () => {
    Alert.alert('Event Swap Btn');
  };

  const ClickGalaxyHandle = () => {
    Alert.alert('Go to Single Galaxy');
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#060c30'}}>
      <FlatList
        data={DATA}
        ListHeaderComponent={() => (
          <RowSearchBar>
            <SearchbarView />
          </RowSearchBar>
        )}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
};

const SearchbarView = () => (
  <ViewSearchBar>
    <SearchBar
      placeholder="Search"
      round
      inputContainerStyle={{
        borderRadius: 25,
        height: 50,
        borderColor: '#aaa',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        color: '#7F7F7F',
      }}
      containerStyle={{
        backgroundColor: 'transparent',
        borderTopColor: 'transparent',
        borderBottomColor: 'transparent',
      }}
      // onChangeText={this.updateSearch}
      // value={search}
      placeholderTextColor={'#fff'}
      searchIcon={{size: 35, color: '#fff'}}
    />
  </ViewSearchBar>
);

const RowSearchBar = styled(Row)`
  width: 100%;
  margin-top: 50px;
`;
const RowResult = styled(Row)`
  width: 100%;
  margin: 20px;
`;
const ViewSearchBar = styled(View)`
  width: 100%;
  margin: 0;
`;
const ImageGalaxy = styled(Image)`
  width: 50px;
  height: 50px;
  border-radius: 15px;
`;
const Imagefollowbtn = styled(Image)`
  width: 80px;
  height: 30px;
  margin-top: 10px;
  align-self: center;
`;
const TextTitle = styled(Text)`
  color: #fff;
  font-size: 18px;
`;
const TextDesc = styled(Text)`
  color: #aaa;
`;
export default PopGalaxyAll;
