import {httpClient} from '../../../utils/setupAxios';

export const getdatapagiante = async (
  page: number,
) => {
  return httpClient.get(`/dotplayapi/getcontent/home?page=1&limit=14`);
};
export const gethashtag = async (
  page: number,
) => {
  return httpClient.get(`/hashtag`);
};
export const getpopcreater = async (
  page: number,
) => {
  return httpClient.get(`/popcreater`);
};
