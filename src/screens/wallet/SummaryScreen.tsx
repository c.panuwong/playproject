import React, {useRef, useState} from 'react';
import {
  View,
  ImageBackground,
  Animated,
  Image,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
// @ts-ignore
import styled from 'styled-components';
import HeaderAnimated from '../../components/HeaderAnimated';
import {DBHText} from '../../components/StyledText';
import LinearGradient from 'react-native-linear-gradient';

export default function SummaryScreen({navigation}: {navigation: any}) {
  const childRef = useRef<any>();
  const [type, settype] = useState(0);
  const replacestring = (data: string) => {
    return '******' + data.substring(data.length - 4, data.length);
  };
  return (
    <Background
      source={require('../../assets/images/bg/BG_wallet_p3.jpg')}
      resizeMode="stretch">
      <HeaderAnimated ref={childRef} title="Summary" navigation={navigation} />
      <Animated.ScrollView
        style={{flex: 1}}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={true}
        stickyHeaderIndices={[2]}
        onScroll={e => childRef.current.handleLogo(e)}>
        <DBHText
          size={34}
          color="white"
          style={{paddingHorizontal: 30, top: 15}}>
          Summary
        </DBHText>
        <BoxSummary source={require('../../assets/images/bg/summary_box.png')}>
          <View style={{flexDirection: 'row'}}>{HeaderSummary(type)}</View>
          <DBHText>06 May 2022 - 13:31</DBHText>
          <ViewText1>
            <DBHText size={24} color="#B3B3B3">
              Transaction ID
            </DBHText>
            <DBHText size={24}>#17162629</DBHText>
          </ViewText1>
          <ViewText2>
            <DBHText size={24} color="#B3B3B3">
              Fill by
            </DBHText>
            <DBHText size={24}>Songded Taweeporn</DBHText>
          </ViewText2>
          <Line />
          <ViewText1>
            <DBHText size={24}>Payment by</DBHText>
            <View style={{flexDirection: 'row'}}>
              <GBPay
                source={require('../../assets/images/icons/gbpay_icon.png')}
              />
              <DBHText size={24}>{replacestring('0123456789')}</DBHText>
            </View>
          </ViewText1>
          <ViewText2>
            <DBHText size={24}>Payment by</DBHText>
            <View style={{flexDirection: 'row'}}>
              <DBHText size={24}>200</DBHText>
              <Coin
                source={require('../../assets/images/icons/coin_icon.png')}
              />
            </View>
          </ViewText2>
        </BoxSummary>
        <ContainerButton>
          <ViewGradient
            start={{x: 0.3, y: 1}}
            end={{x: 1.0, y: 1}}
            colors={['#257CE2', '#33D52B', '#99FF00']}>
            <ViewButtonBG
                onPress={()=> navigation.navigate('WalletStack',{screen: 'Wallet'})}
              activeOpacity={0.2}
              underlayColor="#283567">
              <DBHText size={24} color="#99FF00">
                Back to Wallet
              </DBHText>
            </ViewButtonBG>
          </ViewGradient>
        </ContainerButton>
        {type == 0 ? (
          <View style={{marginTop: 20}}>
            <DBHText size={26} color="#99FF00" style={{textAlign: 'center'}}>
              Get PDF Receipt
            </DBHText>
          </View>
        ) : null}
      </Animated.ScrollView>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
`;
const BoxSummary = styled(ImageBackground)`
  width:100%
  top: 25px;
  align-items: center;
  border-bottom-width:3px
  border-bottom-color:#283567;
  border-top-width:1.5px
  border-top-color:#99FF00;
  padding-top: 20px;
  padding-horizontal: 25px;
`;
const SummarySuccess = styled(Image)`
  width: 45px;
  height: 45px;
  margin-right: 10px;
  top: -3px;
`;
const GBPay = styled(Image)`
  width: 30px;
  height: 25px;
  margin-right: 10px;
  top: -3px;
`;
const Coin = styled(Image)`
  width: 21px;
  height: 21px;
  top: 3px;
  margin-left: 10px;
`;
const ViewText1 = styled(View)`
  justify-content: space-between;
  flex-direction: row;
  width: 100%;
  padding-vertical: 32px;
`;
const ViewText2 = styled(View)`
  justify-content: space-between;
  flex-direction: row;
  width: 100%;
  padding-bottom: 32px;
`;
const Line = styled(View)`
  border-color: #7f7f7f;
  border-width: 1px;
  width: 100%;
`;
const ContainerButton = styled(View)`
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-horizontal: 30px;
  margin-top: 50px;
`;
const ViewGradient = styled(LinearGradient)`
  width: 100%;
  height: 44px;
  border-radius: 32px;
  align-items: center;
  justify-content: center;
  margin-left: 3px;
  padding-horizontal: 2px;
`;
const ViewButtonBG = styled(TouchableHighlight)`
  width: 100%;
  height: 42px;
  border-radius: 30px;
  align-items: center;
  justify-content: center;
  background-color: #101531;
`;
const HeaderSummary = (type: any) => {
  return (
    <>
      {type == 0 ? (
        <>
          <SummarySuccess
            source={require('../../assets/images/icons/summary_success.png')}
          />
          <DBHText size={34}>Success point fill</DBHText>
        </>
      ) : (
        <DBHText size={34} color="#FF0F0F">
          Fail point fill
        </DBHText>
      )}
    </>
  );
};
