import React, {useState} from 'react';
import {View, ImageBackground, TouchableOpacity} from 'react-native';
import {ButtonDisble, ButtonGreen1} from '../../components/ButtonComponent';
import {DBHText} from '../../components/StyledText';
import styled from 'styled-components';
import IconStep from './components/IconStep';
import CameraIdCard from './components/CameraIdCard';
import InputFloating from '../hamberger/components/InputFloating';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faAngleDown, faAngleUp} from '@fortawesome/pro-regular-svg-icons';
import {faTimesCircle} from '@fortawesome/pro-solid-svg-icons';

export default function BankAccountScreen({
  navigation,
  status,
}: {
  navigation: any;

  status: string;
}) {
  const [isBankAccunt, setIsBankAccunt] = useState('');
  const [isNameAccount, setIsNameAccount] = useState('');

  const [isBankName, setIsBankName] = useState(false);

  const IconClear = () => {
    return (
      <Touchable>
        {/* <Icon name="close-circle" color="#4e4e4e" size={15} /> */}
        <FontAwesome
          icon={faTimesCircle}
          size={15}
          color="rgba(78,78,78, 0.5)"
        />
      </Touchable>
    );
  };
  return (
    <Background>
      <IconStep status={status} />

      <ViewContent>
        <HeaderText size={30}>Bank Info</HeaderText>
        <Name onPress={() => setIsBankName(!isBankName)}>
          <DBHText size={20}>Bank Name</DBHText>
          {isBankName === true ? (
            <IconClose>
              <IconClear />
            </IconClose>
          ) : (
            <ViewOpacity icon={faAngleDown} size={25} color="#ffffff" />
          )}
        </Name>
        <InputFloating
          label="Bank Account"
          value={isBankAccunt}
          onChangeText={text => setIsBankAccunt(text)}
          rightComponent={<IconClear />}
        />
        <InputFloating
          label="Account Name"
          value={isNameAccount}
          onChangeText={text => setIsNameAccount(text)}
          rightComponent={<IconClear />}
        />
        <IDCard>
          <Title size={26}>Bookbank Photo</Title>
          <SubTitle size={24}>Descript Detail Bookbank Photo</SubTitle>
          <Camera>
            <CameraIdCard />
          </Camera>
        </IDCard>
      </ViewContent>

      <DBHText> BankAccountScreen </DBHText>

      <ViewButtom>
        <ButtonGreen1
          title="Next"
          padding={10}
          onPress={() => navigation.navigate('Approval')}
        />
      </ViewButtom>
    </Background>
  );
}
const Background = styled(View)`
  flex: 1;
  background-color: #101531;
  align-items: center;
  padding-top: 45px;
`;
const ViewButtom = styled(View)`
  position: absolute;
  bottom: 1%;
  width: 90%;
`;
const ViewContent = styled(View)`
  background-color: #40198f11;
  width: 100%;
  height: 100%;
  padding: 0% 0% 5% 3%;
`;
const HeaderText = styled(DBHText)`
  color: #fff;
  padding-top: 3%;
  padding-horizontal: 3%;
`;
const Title = styled(DBHText)`
  color: #fff;
`;
const SubTitle = styled(DBHText)`
  color: #999;
`;
const IDCard = styled(View)`
  padding-top: 4%;
  padding-horizontal: 3%;
  height: 45%;
`;
const Name = styled(TouchableOpacity)`
  padding: 4% 2% 3% 2%;
  margin-horizontal: 1.5%;
  border-bottom-width: 1px;
  border-color: #ffff;
  flex-direction: row;
`;
const ViewOpacity = styled(FontAwesomeIcon)`
  position: absolute;
  end: 12px;
  bottom: 10px;
`;
const Camera = styled(View)`
  padding-top: 2%;
`;
const FontAwesome = styled(FontAwesomeIcon)`
  background-color: #fff;
  border-radius: 15px;
`;
const Touchable = styled(TouchableOpacity)`
  margin-top: 20px;
  margin-end: 10px;
`;
const IconClose = styled(View)`
  position: absolute;
  end: 5px;
  bottom: 10px;
`;
