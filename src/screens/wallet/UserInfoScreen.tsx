import React, {useState, useRef} from 'react';
import {
  View,
  ImageBackground,
  ScrollView,
  FlatList,
  Animated,
  TouchableOpacity,
} from 'react-native';
import {ButtonDisble, ButtonGreen1} from '../../components/ButtonComponent';
import {DBHText} from '../../components/StyledText';
import styled from 'styled-components';
import {faCheckCircle, faTimesCircle} from '@fortawesome/pro-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import IconStep from './components/IconStep';
import InputFloating from '../hamberger/components/InputFloating';
import {Checkbox} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import HeaderAnimated from '../../components/HeaderAnimated';
import {SafeAreaView} from 'react-native-safe-area-context';

export default function UserInfoScreen({
  navigation,
  status,
}: {
  navigation: any;
  status: string;
}) {
  const [isName, setIsName] = useState<any>('');
  const [isLastName, setIsLastName] = useState('');
  const [isCitizenID, setIsCitizenID] = useState('');
  const [isPhoneNumber, setIsPhoneNumber] = useState('');
  const [isEmail, setIsEmail] = useState(false);

  const [isPlaceholder, setIsPlaceholder] = useState('');
  const [isProvince, setIsProvince] = useState('');
  const [isCity, setIsCity] = useState('');
  const [isPostal, setIsPostal] = useState('');
  const [isTown, setIsTown] = useState('');

  // const [checkboxDiferance, setCheckboxDiferance] = useState<boolean>(true);
  const [checkbox, setCheckbox] = useState<boolean>(false);

  const [isNewAddress, setIsNewAddress] = useState('');
  const [isNewProvince, setIsNewProvince] = useState('');
  const [isNewCity, setIsNewCity] = useState('');
  const [isNewTown, setIsNewTown] = useState('');
  const [isNewPostalCode, setIsNewPostalCode] = useState('');

  const childRef = useRef<any>();

  const IconClear = ({onPress}: {onPress?: any}) => {
    return (
      <Touchable onPress={onPress}>
        <FontAwesome
          icon={faTimesCircle}
          size={15}
          color="rgba(78,78,78, 0.5)"
        />
      </Touchable>
    );
  };

  // const checkInput = () => {
  //   if (
  //     isName &&
  //     isLastName &&
  //     isCitizenID &&
  //     isPhoneNumber &&
  //     isEmail &&
  //     isPlaceholder &&
  //     isProvince &&
  //     isCity &&
  //     isPostal &&
  //     isTown !== ''
  //   ) {
  //     navigation.navigate('VerifyIdentity');
  //     console.log('====================================');
  //     console.log('OK');
  //     console.log('====================================');
  //   } else {
  //     console.log('NO');
  //   }
  //   navigation.navigate('VerifyIdentity');
  // };
  return (
    <Background>
      <Scroll>
        {/* <HeaderAnimated
          ref={childRef}
          title="User Info"
          navigation={navigation}
        /> */}
        <SaveArea edges={['top']}>
          {/* <Animated.ScrollView
          style={{flex: 1}}
          contentContainerStyle={{flexGrow: 1}}
          showsVerticalScrollIndicator={true}
          stickyHeaderIndices={[2]}
          onScroll={e => childRef.current.handleLogo(e)}>
          <IconStep status={status} /> */}

          <ViewContentPersonal>
            <DBHText size={30}> Personal info </DBHText>
            <InputFloating
              label="Name"
              value={isName}
              onChangeText={text => setIsName(text)}
              rightComponent={
                isName !== '' ? (
                  <IconClear onPress={() => setIsName('')} />
                ) : null
              }
            />
            <InputFloating
              label="Last name"
              value={isLastName}
              onChangeText={text => setIsLastName(text)}
              rightComponent={
                isLastName !== '' ? (
                  <IconClear onPress={() => setIsLastName('')} />
                ) : null
              }
            />
            <InputFloating
              label="Citizen ID"
              value={isCitizenID}
              onChangeText={text => setIsCitizenID(text)}
              keyboardType="numeric"
              rightComponent={
                isCitizenID !== '' ? (
                  <IconClear onPress={() => setIsCitizenID('')} />
                ) : null
              }
            />
            <InputFloating
              label="Mobile Phone"
              value={isPhoneNumber}
              onChangeText={text => setIsPhoneNumber(text)}
              keyboardType="numeric"
              rightComponent={
                isPhoneNumber !== '' ? (
                  <IconClear onPress={() => setIsPhoneNumber('')} />
                ) : null
              }
            />
            <InputFloating
              label="Email"
              value={isEmail}
              onChangeText={text => setIsEmail(text)}
              rightComponent={
                isEmail !== '' ? (
                  <IconClear onPress={() => setIsEmail('')} />
                ) : null
              }
            />
          </ViewContentPersonal>

          <ViewContentAddress>
            <DBHText size={30}> Address info </DBHText>

            <InputFloating
              label="Placeholder"
              value={isPlaceholder}
              onChangeText={text => setIsPlaceholder(text)}
              rightComponent={
                isPlaceholder !== '' ? (
                  <IconClear onPress={() => setIsPlaceholder('')} />
                ) : null
              }
            />

            <InputFloating
              label="Province"
              value={isProvince}
              onChangeText={text => setIsProvince(text)}
              rightComponent={
                isProvince !== '' ? (
                  <IconClear onPress={() => setIsProvince('')} />
                ) : null
              }
            />
            <InputFloating
              label="City"
              value={isCity}
              onChangeText={text => setIsCity(text)}
              rightComponent={
                isCity !== '' ? (
                  <IconClear onPress={() => setIsCity('')} />
                ) : null
              }
            />
            <InputFloating
              label="Town"
              value={isTown}
              onChangeText={text => setIsTown(text)}
              rightComponent={
                isTown !== '' ? (
                  <IconClear onPress={() => setIsTown('')} />
                ) : null
              }
            />
            <InputFloating
              label="Postal"
              value={isPostal}
              onChangeText={text => setIsPostal(text)}
              keyboardType="numeric"
              rightComponent={
                isPostal !== '' ? (
                  <IconClear onPress={() => setIsPostal('')} />
                ) : null
              }
            />
          </ViewContentAddress>

          <ViewCheckbox>
            <Checkbox.Android
              status={checkbox ? 'checked' : 'unchecked'}
              onPress={() => {
                setCheckbox(!checkbox);
              }}
              color="#1ADF45"
              uncheckedColor="white"
            />
            <DBHText size={30}>Diferance Address send document ?</DBHText>
          </ViewCheckbox>

          {checkbox === true ? (
            <ViewContentNewAddress>
              <DBHText size={30}> Address document </DBHText>

              <InputFloating
                label="Placeholder"
                value={isNewAddress}
                onChangeText={text => setIsNewAddress(text)}
                rightComponent={
                  isNewAddress !== '' ? (
                    <IconClear onPress={() => setIsNewAddress('')} />
                  ) : null
                }
              />
              <InputFloating
                label="Province"
                value={isNewProvince}
                onChangeText={text => setIsNewProvince(text)}
                rightComponent={
                  isNewProvince !== '' ? (
                    <IconClear onPress={() => setIsNewProvince('')} />
                  ) : null
                }
              />
              <InputFloating
                label="City"
                value={isNewCity}
                onChangeText={text => setIsNewCity(text)}
                rightComponent={
                  isNewCity !== '' ? (
                    <IconClear onPress={() => setIsNewCity('')} />
                  ) : null
                }
              />
              <InputFloating
                label="Postal"
                value={isNewTown}
                onChangeText={text => setIsNewTown(text)}
                rightComponent={
                  isNewTown !== '' ? (
                    <IconClear onPress={() => setIsNewTown('')} />
                  ) : null
                }
              />
              <InputFloating
                label="Postal Code"
                value={isNewPostalCode}
                keyboardType="numeric"
                onChangeText={text => setIsNewPostalCode(text)}
                rightComponent={
                  isNewPostalCode !== '' ? (
                    <IconClear onPress={() => setIsNewPostalCode('')} />
                  ) : null
                }
              />
            </ViewContentNewAddress>
          ) : null}

          {/* </Animated.ScrollView> */}
        </SaveArea>
      </Scroll>
      <ViewButtom>
        <ButtonGreen1
          title="Next"
          padding={10}
          onPress={() => navigation.navigate('VerifyIdentity', {status: '2'})}
        />
      </ViewButtom>
    </Background>
  );
}
const Background = styled(View)`
  flex: 1;
  align-items: center;
  /* background-color: #101531; */
  background-color: #060c30;
`;
const ViewButtom = styled(View)`
  /* padding: 5% 3%; */
  /* position: absolute; */
  /* bottom: 1%; */
  /* background-color: #6fff30; */

  width: 90%;
  padding-vertical: 3%;
`;
const ViewContentPersonal = styled(View)`
  background-color: #131b47;
  width: 100%;
  padding: 0% 3% 5% 3%;
  margin-bottom: 2%;
`;
const ViewContentAddress = styled(View)`
  background-color: #131b47;
  width: 100%;
  padding: 0% 3% 5% 3%;
  margin-bottom: 2%;
`;
const ViewContentNewAddress = styled(View)`
  background-color: #131b47;
  width: 100%;
  padding: 0% 3% 5% 3%;
`;
// background-color: #131b47;
// background-color: #ff89;
//
const Scroll = styled(ScrollView)`
  flex: 1;
`;
const ViewCheckbox = styled(View)`
  width: 100%;
  flex-direction: row;
  align-items: center;
  padding: 0% 3% 1% 3%;
`;
const Touchable = styled(TouchableOpacity)`
  margin-top: 20px;
`;
const FontAwesome = styled(FontAwesomeIcon)`
  background-color: #fff;
  border-radius: 15px;
`;
// const TouchableClose = styled(TouchableOpacity)`
//   background-color: #fff;
//   justify-content: flex-end;
//   align-items: center;
//   height: 15px;
//   width: 15px;
//   border-radius: 20px;
//   margin-top: 20px;
// `;
const SaveArea = styled(SafeAreaView)`
  flex: 1;
`;
