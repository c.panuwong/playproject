import React, {useRef, useState} from 'react';
import {
  View,
  ImageBackground,
  FlatList,
  Dimensions,
  Image,
  Animated,
} from 'react-native';
import styled from 'styled-components';
import {DBHText} from '../../components/StyledText';
import {ButtonGreen1} from '../../components/ButtonComponent';
// import {SliderBox} from 'react-native-image-slider-text-box';
import kyc1 from '../../assets/images/icons/kyc1.png';
import kyc2 from '../../assets/images/icons/kyc2.png';
import kyc3 from '../../assets/images/icons/kyc3.png';

const {width, height} = Dimensions.get('window');

export default function KycScreen({navigation}: {navigation: any}) {
  const images = [
    {
      id: '1',
      img: require('../../assets/images/icons/kyc1.png'),
      buttonText: 'buttonText ',
      header: 'What KYC process ? ',
      title:
        '1 Drescription Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ',
    },
    {
      id: '3',

      img: require('../../assets/images/icons/kyc2.png'),
      header: 'What KYC process ? ',
      buttonText: 'buttonText ',
      title:
        '2 Drescription Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ',
    },
    {
      id: '2',
      img: require('../../assets/images/icons/kyc3.png'),
      header: 'What KYC process ? ',
      buttonText: 'buttonText ',
      title:
        '3 Drescription Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ',
    },
  ];
  const scrollX = useRef(new Animated.Value(0)).current;
  const [currentIndex, setCurrentIndex] = useState(0);

  const Paginator = ({data, scrollX}: {data: any; scrollX: any}) => {
    return (
      <ContentDot>
        {data.map((_, i) => {
          const inputRange = [(i - 1) * width, i * width, (i + 1) * width];

          const ditWidth = scrollX.interpolate({
            inputRange,
            outputRange: [20, 20, 20],
            exterpolate: 'clamp',
          });

          return (
            <Animated.View
              style={{
                height: 8,
                width: ditWidth,
                borderRadius: 4,
                backgroundColor: '#fff',
                marginHorizontal: 8,
              }}
              key={i.toString()}
            />
          );
        })}
      </ContentDot>
    );
  };
  return (
    <Background
      source={require('../../assets/images/bg/BG_kyc.jpg')}
      resizeMode="cover">
      <Content>
        <FlatList
          data={images}
          keyExtractor={(item, index) => item.id + index}
          horizontal
          showsHorizontalScrollIndicator
          pagingEnabled
          bounces={false}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {x: scrollX}}}],
            {
              useNativeDiver: false,
            },
          )}
          renderItem={data => {
            return (
              <ViewAll style={{width}}>
                <ImageStyle source={data.item.img} resizeMode="contain" />
                <TitleText size={36}>{data.item.header} </TitleText>
                <Description size={22}> {data.item.title}</Description>
              </ViewAll>
            );
          }}
        />
      </Content>

      <Paginator data={images} scrollX={scrollX} />

      <ViewButtom>
        <ButtonGreen1
          title="Next"
          padding={10}
          onPress={() => navigation.navigate('KycPolicy')}
        />
      </ViewButtom>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
  align-items: center;
`;
const ViewButtom = styled(View)`
  position: absolute;
  bottom: 1%;
  width: 90%;
`;
const Content = styled(View)`
  justify-content: center;
  flex: 1;
  align-items: center;
  margin-top: -20%;
`;
const ImageStyle = styled(Image)`
  width: 100%;
  height: 40%;
`;
const TitleText = styled(DBHText)`
  text-align: center;
`;

const Description = styled(DBHText)`
  width: 90%;
  text-align: center;
  justify-content: center;
  color: #b3b3b3;
`;
const ViewAll = styled(View)`
  flex: 1;
  justify-content: center;
  align-items: center;
`;
const ViewDot = styled(View)`
  height: 8px;
  /* width:${props => props.width} */
  border-radius: 4px;
  background-color: #fff;
  margin-horizontal: 8px;
`;

const ContentDot = styled(View)`
  position: absolute;
  flex-direction: row;
  height: 64px;
  width: 20%;
  z-index: 1;
  bottom: 8%;
`;
