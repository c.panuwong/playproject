import React, {useState, useRef} from 'react';
import {View, ImageBackground, Dimensions, Animated, Alert} from 'react-native';
import {ButtonDisble, ButtonGreen1} from '../../components/ButtonComponent';
import {DBHText} from '../../components/StyledText';
import styled from 'styled-components';
import openShop from '../hamberger/components/TextOpenShop';
import {ScrollView} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Checkbox} from 'react-native-paper';
// import HeaderAnimated from '../../components/HeaderAnimated';
const {width, height} = Dimensions.get('window');

interface ScrollProps {
  layoutMeasurement: any;
  contentOffset: any;
  contentSize: any;
}
const isCloseToBottom = ({
  layoutMeasurement,
  contentOffset,
  contentSize,
}: ScrollProps) => {
  const paddingToBottom = 20;
  return (
    layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom
  );
};

export default function KycPolicyScreen({navigation}: {navigation: any}) {
  const [checkboxDisable, setCheckboxDisable] = useState<boolean>(true);
  const [checkbox, setCheckbox] = useState<boolean>(false);
  const childRef = useRef<any>();

  return (
    <Background
      source={require('../../assets/images/bg/BG_kyc.jpg')}
      resizeMode="cover">
      {/* <HeaderAnimated
        ref={childRef}
        title="KYC policy"
        navigation={navigation}
      /> */}
      <SaveArea edges={['top']}>
        <ViewKyc>
          {/* <Animated.ScrollView
            style={{flex: 1}}
            contentContainerStyle={{flexGrow: 1}}
            showsVerticalScrollIndicator={true}
            stickyHeaderIndices={[2]}
            onScroll={e => childRef.current.handleLogo(e)}> */}
          <ScrollView
            scrollEventThrottle={16}
            onScroll={({nativeEvent}) => {
              if (isCloseToBottom(nativeEvent)) {
                setCheckboxDisable(false);
              }
            }}>
            <DBHText size={32}>KYC policy</DBHText>

            <DBHText size={20}>{openShop.open_shop}</DBHText>
            {/* </Animated.ScrollView> */}
          </ScrollView>
          <ViewCheckbox>
            <Checkbox.Android
              status={checkbox ? 'checked' : 'unchecked'}
              disabled={checkboxDisable}
              onPress={() => {
                setCheckbox(!checkbox);
              }}
              color="#1ADF45"
              uncheckedColor="white"
            />
            <DBHText size={22}>Accept Policy</DBHText>
          </ViewCheckbox>
        </ViewKyc>

        <ViewButtom>
          <ButtonDisble
            color={
              checkbox
                ? ['#257CE2', '#33D52B', '#99FF00']
                : ['#7F8181', '#E7E7E7']
            }
            title="Next"
            padding={5}
            marginTop={0}
            onPress={() => {
              checkbox
                ? // ? navigation.navigate('')
                  navigation.navigate('UserInfo', {status: '1'})
                : Alert.alert('คำเตือน', 'กรุณายอมรับเงื่อนไข');
            }}
          />
        </ViewButtom>
      </SaveArea>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
  padding-top: 45px;
`;
const ViewButtom = styled(View)`
  position: absolute;
  bottom: 0%;
  width: 100%;
  height: 10%;
  background-color: #060c30;
  justify-content: center;
  padding-horizontal: 8%;
`;
const SaveArea = styled(SafeAreaView)`
  flex: 1;
`;
const ViewKyc = styled(View)`
  width: 100%;
  height: 89%;
  padding-horizontal: 30px;
`;
const ViewCheckbox = styled(View)`
  width: 100%;
  flex-direction: row;
  align-items: center;
  padding-top: 10px;
`;
const BackgroundText = styled(View)`
  z-index: 1;
  width: 100%;
  height: 80%;
  padding-horizontal: 30px;
  background-color: #c4c4c4;
  opacity: 0.1;
`;
