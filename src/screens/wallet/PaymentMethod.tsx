import React, {useRef, useEffect} from 'react';
import {
  View,
  ImageBackground,
  Animated,
  Image,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
// @ts-ignore
import styled from 'styled-components';
import HeaderAnimated from '../../components/HeaderAnimated';
import {DBHText} from '../../components/StyledText';
// @ts-ignore
import Loading from 'react-native-whc-loading';

export default function PaymentMethod({navigation}: {navigation: any}) {
  const childRef = useRef<any>();
  const refs = useRef<any>();

  useEffect(() => {
    refs.current.show();
    setTimeout(() => {
      refs.current.close();
      navigation.navigate('SummaryScreen');
    }, 2000);
  }, []);
  return (
    <Background
      source={require('../../assets/images/bg/BG_wallet_p3.jpg')}
      resizeMode="stretch">
      <HeaderAnimated
        ref={childRef}
        title="PaymentMethod"
        navigation={navigation}
      />
      <Loading
        ref={refs}
        backgroundColor={'#00000096'}
        indicatorColor={'#99FF00'}
        size={80}
      />
      <Animated.ScrollView
        style={{flex: 1}}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={true}
        stickyHeaderIndices={[2]}
        onScroll={e => childRef.current.handleLogo(e)}>
        <DBHText
          size={34}
          color="white"
          style={{paddingHorizontal: 30, top: 15}}>
          Payment Method
        </DBHText>
        <View
          style={{
            width: '100%',
            padding: 20,
            marginTop: 30,
          }}>
          <BoxView>
            <DBHText size={24}>ผ่านบัตรเดบิต ATM</DBHText>
            <View style={{flexDirection: 'row'}}>
              <ImageVisa
                source={require('../../assets/images/icons/visa_icon.png')}
              />
              <ImageCard
                source={require('../../assets/images/icons/card_icon.png')}
                resizeMode="stretch"
              />
            </View>
          </BoxView>
          <BoxView>
            <DBHText size={24}>QR CODE ชำระเงิน</DBHText>
            <View style={{flexDirection: 'row'}}>
              <ImageQR
                source={require('../../assets/images/icons/qr_icon.png')}
                resizeMode="stretch"
              />
            </View>
          </BoxView>
          <BoxView>
            <DBHText size={24}>หักบัญชีธนาคาร</DBHText>
          </BoxView>
        </View>
      </Animated.ScrollView>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
`;
const ImageVisa = styled(Image)`
  width: 44px;
  height: 29px;
`;
const ImageCard = styled(Image)`
  width: 44px;
  height: 29px;
  margin-left: 5px;
`;
const ImageQR = styled(Image)`
  width: 37px;
  height: 37px;
  right: 20px;
`;
const BoxView = styled(TouchableOpacity)`
  height: 60px;
  padding: 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-bottom-color: #7f7f7f;
  border-bottom-width: 1px;
`;
