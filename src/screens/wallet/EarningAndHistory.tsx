import React, {useRef, useState} from 'react';
import {
  View,
  ImageBackground,
  Animated,
  Image,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
// @ts-ignore
import styled from 'styled-components';
import HeaderAnimated from '../../components/HeaderAnimated';
import {DBHText} from '../../components/StyledText';
import LinearGradient from 'react-native-linear-gradient';

export default function EarningAndHistory({navigation}: {navigation: any}) {
  const childRef = useRef<any>();
  const [page, setpage] = useState(0);
  return (
    <Background
      source={require('../../assets/images/bg/BG_wallet_p3.jpg')}
      resizeMode="stretch">
      <HeaderAnimated
        ref={childRef}
        title={page == 0 ? 'Earning' : 'History'}
        navigation={navigation}
      />
      <Animated.ScrollView
        style={{flex: 1}}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={true}
        stickyHeaderIndices={[2]}
        onScroll={e => childRef.current.handleLogo(e)}>
        <DBHText
          size={34}
          color="white"
          style={{paddingHorizontal: 30, top: 15}}>
          {page == 0 ? 'Earnings' : 'Historys'}
        </DBHText>
        <ContainerButton>
          <ViewGradient
            start={{x: 0.3, y: 1}}
            end={{x: 1.0, y: 1}}
            colors={
              page == 0
                ? ['#257CE2', '#33D52B', '#99FF00']
                : ['#000', '#000', '#000']
            }>
            <ViewButtonBG
              onPress={() => setpage(0)}
              activeOpacity={0.2}
              underlayColor="#283567"
              select={page == 0 ? true : false}>
              <DBHText size={24} color={page == 0 ? '#99FF00' : '#FFF'}>
                Earnings
              </DBHText>
            </ViewButtonBG>
          </ViewGradient>
          <ViewGradient2
            start={{x: 0.3, y: 1}}
            end={{x: 1.0, y: 1}}
            colors={
              page == 1
                ? ['#257CE2', '#33D52B', '#99FF00']
                : ['#000', '#000', '#000']
            }>
            <ViewButtonBG2
              onPress={() => setpage(1)}
              activeOpacity={0.2}
              underlayColor="#283567"
              select={page == 1 ? true : false}>
              <DBHText size={24} color={page == 1 ? '#99FF00' : '#FFF'}>
                Historys
              </DBHText>
            </ViewButtonBG2>
          </ViewGradient2>
        </ContainerButton>
      </Animated.ScrollView>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
`;
const ContainerButton = styled(View)`
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-horizontal: 30px;
  margin-top: 30px;
  flex-direction: row;
`;
const ViewGradient = styled(LinearGradient)`
  width: 50%;
  height: 36px;
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
  align-items: center;
  justify-content: center;
  padding-horizontal: 2px;
`;
const ViewGradient2 = styled(LinearGradient)`
  width: 50%;
  height: 36px;
  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
  align-items: center;
  justify-content: center;
  padding-horizontal: 2px;
`;
const ViewButtonBG = styled<any>(TouchableHighlight)`
  width: 100%;
  height: 34px;
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
  align-items: center;
  justify-content: center;
  background-color: ${props => (props.select ? '#202C66' : '#222B55')};
`;
const ViewButtonBG2 = styled<any>(TouchableHighlight)`
  width: 100%;
  height: 34px;
  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
  align-items: center;
  justify-content: center;
  background-color: ${props => (props.select ? '#202C66' : '#222B55')};
`;
