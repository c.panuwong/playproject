import React from 'react';
import {View, ImageBackground} from 'react-native';
import {ButtonDisble, ButtonGreen1} from '../../components/ButtonComponent';
import {DBHText} from '../../components/StyledText';
import styled from 'styled-components';
import IconStep from './components/IconStep';
import CameraIdCard from './components/CameraIdCard';
import HeaderAnimated from '../../components/HeaderAnimated';

export default function VerifyIdentityScreen({
  navigation,
  status,
}: {
  navigation: any;
  status: string;
}) {
  return (
    <Background>
      {/* <HeaderAnimated title="User Info" navigation={navigation} /> */}
      <IconStep status={status} />
      <ViewContent>
        <HeaderText size={30}>Document Info</HeaderText>
        <SubTitle size={24}>Take photo your document for our recheck </SubTitle>
        <IDCard>
          <Title size={26}>ID Card Photo </Title>
          <SubTitle size={24}>Descript Detail ID Card Photo</SubTitle>
          <Camera>
            <CameraIdCard />
          </Camera>
        </IDCard>
        <Selfie>
          <Title size={26}>Descript Detail ID Card Photo</Title>
          <SubTitle size={24}>
            Descript Detail Selfie with ID Card Photo
          </SubTitle>
          <Camera>
            <CameraIdCard />
          </Camera>
        </Selfie>
      </ViewContent>

      <ViewButtom>
        <ButtonGreen1
          title="Next"
          padding={10}
          onPress={() => navigation.navigate('BankAccount', {status: '3'})}
        />
      </ViewButtom>
    </Background>
  );
}
// const Background = styled(ImageBackground)`
//   flex: 1;
//   align-items: center;
// `;

const Background = styled(View)`
  flex: 1;
  background-color: #101531;
  align-items: center;
  padding-top: 45px;
`;
const ViewButtom = styled(View)`
  position: absolute;
  bottom: 1%;
  width: 90%;
`;
const ViewContent = styled(View)`
  background-color: #40198f11;
  width: 100%;
  height: 100%;
  padding: 0% 3% 5% 3%;
`;
const HeaderText = styled(DBHText)`
  color: #fff;
`;
const SubTitle = styled(DBHText)`
  color: #999;
`;
const Title = styled(DBHText)`
  color: #fff;
`;
const IDCard = styled(View)`
  padding-top: 3%;
  background-color: #111945;
  height: 44%;
`;
const Selfie = styled(View)`
  background-color: #111945;
  height: 44%;
  margin-top: -5%;
`;
const Camera = styled(View)`
  padding-top: 2%;
`;
