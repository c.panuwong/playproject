import React from 'react';
import {
  View,
  ImageBackground,
  Image,
  Dimensions,
  TouchableOpacity,
  Text,
} from 'react-native';
import {
  ButtonDisble,
  ButtonGreen1,
  ButtonBorder,
} from '../../components/ButtonComponent';
import {DBHText} from '../../components/StyledText';
import styled from 'styled-components';

const {width, height} = Dimensions.get('window');

export default function ApprovalScreen({navigation}: {navigation: any}) {
  return (
    <Background
      source={require('../../assets/images/bg/BG_kyc.jpg')}
      resizeMode="cover">
      <Image
        source={require('../../assets/images/icons/hourglass.png')}
        style={{width: width * 0.37, height: width * 0.37}}
        resizeMode="contain"
      />
      <TextStyle>Waiting for Approval</TextStyle>
      <ViewButtom onPress={() => navigation.navigate('Home')}>
        <ButtonBorder title="Back to Wallet" textColor="#99FF00" padding={10} />
      </ViewButtom>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
  align-items: center;
  padding-top: 35%;
`;
const ViewButtom = styled(TouchableOpacity)`
  position: absolute;
  bottom: 1%;
  width: 90%;
`;

const TextStyle = styled(Text)`
  padding-top: 5%;
  font-family: Entypo;
  color: #fff;
  font-size: 24px;
  line-height: 30px;
`;
