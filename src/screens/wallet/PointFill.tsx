import React, {useRef} from 'react';
import {
  View,
  ImageBackground,
  Animated,
  Image,
  TouchableHighlight,
} from 'react-native';
// @ts-ignore
import styled from 'styled-components';
import HeaderAnimated from '../../components/HeaderAnimated';
import {DBHText} from '../../components/StyledText';
import LinearGradient from 'react-native-linear-gradient';

const Data = [
  {id: 1, point: 50, price: 50},
  {id: 2, point: 100, price: 100},
  {id: 3, point: 150, price: 150},
];
export default function PointFill({navigation}: {navigation: any}) {
  const childRef = useRef<any>();
  return (
    <Background
      source={require('../../assets/images/BG_5.jpg')}
      resizeMode="stretch">
      <HeaderAnimated
        ref={childRef}
        title="PointFill"
        navigation={navigation}
      />
      <Animated.ScrollView
        style={{flex: 1}}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={true}
        stickyHeaderIndices={[2]}
        onScroll={e => childRef.current.handleLogo(e)}>
        <DBHText
          size={34}
          color="white"
          style={{paddingHorizontal: 30, top: 15}}>
          Point Fill
        </DBHText>
        <ImageView>
          <ImageIcon
            source={require('../../assets/images/icons/pointfill_icon.png')}
          />
          <DBHText size={24} style={{textAlign: 'center', marginTop: 10}}>
            Select point to fill
          </DBHText>
        </ImageView>
        <ViewContainer>
          {Data?.map((item, index) => {
            return (
              <ViewButtonPrice key={index}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <ImageCoin
                    source={require('../../assets/images/icons/coin_icon.png')}
                  />
                  <DBHText size={32} style={{top: -2.5, marginLeft: 4}}>
                    {item.point}
                  </DBHText>
                </View>
                <ViewGradient
                  start={{x: 0.3, y: 1}}
                  end={{x: 1.0, y: 1}}
                  colors={['#257CE2', '#33D52B', '#99FF00']}>
                  <ViewButtonBG
                    onPress={() => navigation.navigate('PaymentMethod')}
                    activeOpacity={0.6}
                    underlayColor="#DDDDDD">
                    <DBHText size={32} color="#51A70F" style={{top: -1}}>
                      ฿ {item.price}
                    </DBHText>
                  </ViewButtonBG>
                </ViewGradient>
              </ViewButtonPrice>
            );
          })}
        </ViewContainer>
      </Animated.ScrollView>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
`;
const ImageView = styled(View)`
  align-items: center;
  margin-top: 20px;
`;
const ImageIcon = styled(Image)`
  width: 90px;
  height: 90px;
`;
const ViewButtonPrice = styled(View)`
    width: 100%;
    height: 30px
    padding-horizontal: 30px;
    margin-vertical: 15px
    justify-content: space-between;
    flex-direction: row;
`;
const ViewContainer = styled(View)`
  width: 100%;
  background-color: rgba(229, 229, 229, 0.15);
  top: 25px;
  align-items: center;
  border-bottom-width:3px
  border-bottom-color:#283567;
  border-top-width:1.5px
  border-top-color:#99FF00;
  padding-top: 10px;
`;
const ImageCoin = styled(Image)`
  width: 24px;
  height: 24px;
`;

const ViewGradient = styled(LinearGradient)`
  width: 100px;
  height: 36px;
  border-radius: 32px;
  top: -5px;
  align-items: center;
  justify-content: center;
`;
const ViewButtonBG = styled(TouchableHighlight)`
  width: 95px;
  height: 32px;
  border-radius: 30px;
  align-items: center;
  justify-content: center;
  background-color: #fff;
`;
