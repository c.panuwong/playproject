import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCamera} from '@fortawesome/pro-light-svg-icons';
import CameraPicture from '../../createpost/components/CameraPicture';

export default function CameraIdCard() {
  return (
    <TouchableOpacity>
      <Content>
        {/* <FontAwesomeIcon icon={faCamera} size={30} color="#fff" /> */}
        <CameraPicture />
      </Content>
    </TouchableOpacity>
  );
}
const Content = styled(View)`
  background-color: #e7e7e7;
  width: 100%;
  height: 85%;
  border-width: 1px;
  border-color: #fff;
  opacity: 0.1;
  align-items: center;
  justify-content: center;
  border-radius: 15px;
`;
