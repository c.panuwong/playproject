import React from 'react';
import {View, Text, TouchableHighlight, Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
// @ts-ignore
import styled from 'styled-components';
import {DBHText} from '../../../components/StyledText';
export default function ActionWallet({
  navigation,
  url,
  title,
  onPress,
}: {
  navigation: any;
  url: any;
  title: string;
  onPress: any;
}) {
  return (
    <>
      <ContainerButton>
        <ViewGradient
          start={{x: 0.3, y: 1}}
          end={{x: 1.0, y: 1}}
          colors={['#257CE2', '#33D52B', '#99FF00']}>
          <ViewButtonBG
            onPress={onPress}
            activeOpacity={0.2}
            underlayColor="#283567">
            <Icon source={url} />
          </ViewButtonBG>
        </ViewGradient>
        <DBHText size={24} style={{marginTop: 10}}>
          {title}
        </DBHText>
      </ContainerButton>
    </>
  );
}

const ContainerButton = styled(View)`
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const ViewGradient = styled(LinearGradient)`
  width: 64px;
  height: 64px;
  border-radius: 32px;
  align-items: center;
  justify-content: center;
  margin-left: 3px;
`;
const ViewButtonBG = styled(TouchableHighlight)`
  width: 60px;
  height: 60px;
  border-radius: 30px;
  align-items: center;
  justify-content: center;
  background-color: #283567;
`;
const Icon = styled(Image)`
  width: 25px;
  height: 25px;
`;
