import React from 'react';
import {View, Text, TouchableOpacity, FlatList} from 'react-native';
import {DBHText} from '../../../components/StyledText';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faChevronCircleRight} from '@fortawesome/pro-regular-svg-icons';
// @ts-ignore
import styled from 'styled-components';
const Data = [
  {id: 1, type: 'deposit', amount: 500, date: '01/01/2020 09:00 น.'},
  {id: 2, type: 'withdraw', amount: 2000, date: '01/01/2020 09:00 น.'},
  {id: 3, type: 'deposit', amount: 1000, date: '01/01/2020 09:00 น.'},
  {id: 4, type: 'withdraw', amount: 500, date: '01/01/2020 09:00 น.'},
  {id: 4, type: 'withdraw', amount: 500, date: '01/01/2020 09:00 น.'},
  {id: 4, type: 'withdraw', amount: 500, date: '01/01/2020 09:00 น.'},
  {id: 4, type: 'withdraw', amount: 500, date: '01/01/2020 09:00 น.'},
  {id: 4, type: 'withdraw', amount: 500, date: '01/01/2020 09:00 น.'},
  {id: 4, type: 'withdraw', amount: 500, date: '01/01/2020 09:00 น.'},
  {id: 4, type: 'withdraw', amount: 500, date: '01/01/2020 09:00 น.'},
];
export default function LastestActivity() {
  return (
    <ViewContainer>
      <ViewTitle>
        <DBHText size={26}>Lastest Activity</DBHText>
        <TouchableOpacity>
          <FontAwesomeIcon icon={faChevronCircleRight} size={25} color="#fff" />
        </TouchableOpacity>
      </ViewTitle>
      {Data?.map((item, index) => {
        return (
          <ViewBox key={index}>
            <ViewBox2>
              <DBHText size={24} color="#B5B5B5">
                {item?.date}
              </DBHText>
            </ViewBox2>
            <ViewBox3>
              <DBHText size={24}>
                {item?.type == 'deposit' ? 'ฝาก Point' : 'ถอน Point'}
              </DBHText>
            </ViewBox3>
            <ViewBox3>
              <DBHText size={24} color={item?.type == 'deposit' ? `#99FF00` : `red`}>
                {item?.type == 'deposit' ? `+${item.amount}` : `-${item.amount}`}
              </DBHText>
            </ViewBox3>
          </ViewBox>
        );
      })}
    </ViewContainer>
  );
}
const ViewContainer = styled(View)`
  flex: 1;
  background-color: rgba(36, 48, 94, 0.5);
  margin-top: 20px;
  border-top-right-radius: 40px;
  padding: 20px;
`;
const ViewTitle = styled(View)`
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;
const ViewBox = styled(View)`
  width: 100%;
  height: 75px;
  background-color: rgba(181, 181, 181, 0.3);
  border-radius: 10px;
  margin-top: 10px;
  flex-direction: row;
  padding: 12px;
`;
const ViewBox2 = styled(View)`
  width: 33%;
`;
const ViewBox3 = styled(View)`
  width: 40%;
  align-items: center;
`;
