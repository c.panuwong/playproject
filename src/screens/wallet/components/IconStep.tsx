import React, {useState} from 'react';
import {View} from 'react-native';
import {faCheckCircle} from '@fortawesome/pro-duotone-svg-icons';
import {faHorizontalRule} from '@fortawesome/pro-light-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import styled from 'styled-components';
// horizontal-rule status={status}
export default function IconStep({status}: {status: `string`}) {
  console.log(status);

  return (
    <ViewContent>
      <IconBackground icon={faCheckCircle} size={15} />
      <IconStyle icon={faHorizontalRule} size={20} />
      <IconBackground icon={faCheckCircle} size={15} />
      <IconStyle icon={faHorizontalRule} size={20} />
      <IconBackground icon={faCheckCircle} size={15} />
    </ViewContent>
  );
}

const ViewContent = styled(View)`
  width: 100%;
  height: 5%;
  align-items: center;
  justify-content: center;
  flex-direction: row;
`;
const IconStyle = styled(FontAwesomeIcon)`
  padding-horizontal: 5%;
  color: #fff;
`;

const IconBackground = styled(FontAwesomeIcon)`
  color: #fff;
`;
