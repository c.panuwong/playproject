import React, {useState, useRef} from 'react';
import {View, Text, ImageBackground, Animated, Image} from 'react-native';
// @ts-ignore
import styled from 'styled-components';
import HeaderAnimated from '../../components/HeaderAnimated';
import {DBHText} from '../../components/StyledText';
import ActionWallet from './components/ActionWallet';
import LastestActivity from './components/LastestActivity';
export default function WalletScreen({navigation}: {navigation: any}) {
  const childRef = useRef<any>();
  return (
    <Background
      source={require('../../assets/images/BG_5.jpg')}
      resizeMode="cover">
      <HeaderAnimated ref={childRef} title="Wallet" navigation={navigation} />
      <Animated.ScrollView
        style={{flex: 1}}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={true}
        stickyHeaderIndices={[2]}
        onScroll={e => childRef.current.handleLogo(e)}>
        <DBHText
          size={34}
          color="white"
          style={{paddingHorizontal: 30, top: 15}}>
          Wallet
        </DBHText>
        <View>
          <ViewContainer1>
            <View
              style={{flexDirection: 'row', alignItems: 'center', bottom: -10}}>
              <DBHText size={68}>100</DBHText>
              <ImageCoin
                source={require('../../assets/images/icons/coin_icon.png')}
              />
            </View>
            <DBHText size={30}>Your Point Balance</DBHText>
          </ViewContainer1>
          <ViewContainerAction>
            <ActionWallet
              url={require('../../assets/images/icons/wallet_withdraw.png')}
              title="Withdraw"
              onPress={() => navigation.navigate('Kyc')}
            />
            <ActionWallet
              url={require('../../assets/images/icons/wallet_earning.png')}
              title=" Earning"
              navigation={navigation}
              onPress={() => navigation.navigate('EarningAndHistory')}
            />
            <ActionWallet
              url={require('../../assets/images/icons/wallet_redeem.png')}
              title="Point Fill"
              navigation={navigation}
              onPress={() => navigation.navigate('PointFill')}
            />
          </ViewContainerAction>
          <LastestActivity />
        </View>
      </Animated.ScrollView>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
`;
const ViewContainer1 = styled(View)`
  width: 100%;
  height: 135px;
  background-color: rgba(229, 229, 229, 0.15);
  top: 25px;
  align-items: center;
  border-bottom-width:3px
  border-bottom-color:#283567;
  border-top-width:1.5px
  border-top-color:#99FF00;
`;
const ImageCoin = styled(Image)`
  width: 32px;
  height: 32px;
  margin-left: 10px;
`;
const ViewContainerAction = styled(View)`
  width: 100%;
  top: 25px;
  align-items: center;
  flex-direction: row;
  justify-content:space-between;
  padding-vertical: 30px
  padding-horizontal: 40px;
`;
