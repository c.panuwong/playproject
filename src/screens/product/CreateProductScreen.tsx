import React, {useState} from 'react';
import {
  View,
  Text,
  FlatList,
  ImageBackground,
  Image,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
import styled from 'styled-components';
import {data} from '../profiles/data';
import select_interest from '../../assets/images/select_interest&creator.png';
import {ButtonGreen1} from '../../components/ButtonComponent';
import CameraRoll from '@react-native-community/cameraroll';
import ChoosePictureScreen from '../createpost/ChoosePictureScreen';
import {productAuth} from './productAuth';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default function CreateProductScreen({navigation}: {navigation: any}) {
  const [numColumns, setNumColumn] = useState(3);
  const [imagePath, setimagePath] = useState([]);

  const upload = () => {
    productAuth;
  };

  const renderItems = ({item}) => {
    return (
      <Row>
        <ColImage>
          <TouchableOpacity>
            <Image
              source={{uri: item.url}}
              style={styles.imageStyle}
              resizeMode={'cover'}
            />
          </TouchableOpacity>
        </ColImage>
      </Row>
    );
  };

  return (
    // <Container>
    <Grid>
      <Row size={2}>
        <ChoosePictureScreen
          imagePath={imagePath}
          setImagePast={setimagePath}
        />
        {/* <FlatList
          data={data.shop}
          // scrollEnabled={false}
          numColumns={numColumns}
          key={numColumns}
          keyExtractor={(item, index) => `${item.keys}`}
          renderItem={renderItems}
          removeClippedSubviews={true}
        /> */}
      </Row>

      <Row
        size={0.2}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#020B3C',
          height: '100%',
        }}>
        <ViewButton>
          <ButtonGreen1
            title="Next"
            padding={5}
            onPress={() => navigation.navigate('AddProductDetails')}
          />
        </ViewButton>
      </Row>
    </Grid>
    // </Container>
  );
}

const Container = styled(SafeAreaView)`
  flex: 1;
  background-color: #171c39;
  padding: 2%;
`;
// border: 1.03326px solid #777777;
const ColImage = styled(Col)`
  margin: 2.5%;
`;

const ViewButton = styled(View)`
  margin: 2%;
  flex: 0.9;
`;

const styles = StyleSheet.create({
  imageStyle: {
    width: '100%',
    height: windowWidth * 0.293,
    borderRadius: 10.3,
  },
});
