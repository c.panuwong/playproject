import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';

interface ISlider {
  picture_url: string[];
}

export default function SlideBoxProduct({navigation}: {navigation: any}) {
  const [product] = useState<ISlider>({
    image_url: [
      'https://images.pexels.com/photos/6776613/pexels-photo-6776613.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      'https://images.pexels.com/photos/6081495/pexels-photo-6081495.png?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      'https://images.pexels.com/photos/3987613/pexels-photo-3987613.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
      'https://images.pexels.com/photos/7038222/pexels-photo-7038222.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    ],
  });
  return (
    <SliderBox
      images={product.image_url}
      // autoplay
      // circleLoop
      // activeOpacity={200}
      inactiveDotColor="#fefefe"
      resizeMethod={'resize'}
      resizeMode={'cover'}
      onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
      dotStyle={{
        width: 8,
        height: 8,
        borderRadius: 40,
        backgroundColor: '#fefefe',
      }}
      ImageComponentStyle={{
        width: '99%',
        height: '99%',
      }}
    />
  );
}
