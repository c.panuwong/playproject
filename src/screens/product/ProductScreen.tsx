import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import SlideBox from '../home/SlideBox';
import SlideBoxProduct from './SlideBoxProduck';
import styled from 'styled-components';
import {Col, Row, Grid} from 'react-native-easy-grid';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faShare, faBookmark} from '@fortawesome/pro-regular-svg-icons';
import ListProduct from '../../components/product/ListProduct';
import HeaderProduct from '../../components/product/HeaderProduct';
import SeeMoreButton from '../../components/product/SeeMoreButton';
import BackButton from '../../components/product/BackButton';

interface ISlider {
  image: string[];
}
const {width, height} = Dimensions.get('window');

export default function ProductScreen({navigation}: {navigation: any}) {
  const [product] = useState<ISlider>({
    image: [
      {
        name: 'Lorem ipsum dolor sit amet, consect opppppppppppppp',
        keys: '1',
        price: '300',
        url: 'https://images.pexels.com/photos/6776613/pexels-photo-6776613.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      },
      {
        name: ' Lorem ipsum dolor sit amet, consect  tijigjvisjge',
        keys: '2',
        price: '3400',

        url: 'https://images.pexels.com/photos/6081495/pexels-photo-6081495.png?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      },
      {
        name: ' Lorem ipsum dolor sit amet, consect  rrrrrrr',
        keys: '3',
        price: '3000',

        url: 'https://images.pexels.com/photos/3987613/pexels-photo-3987613.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
      },
      {
        name: 'Lorem ipsum dolor sit amet, consectmopdjqwpx,eqo',
        keys: '4',
        price: '200',
        url: 'https://images.pexels.com/photos/7038222/pexels-photo-7038222.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      },
      // {
      //   name: 'Lorem ipsum dolor sit amet, consect opppppppppppppp',
      //   keys: '5',
      //   price: '300',
      //   url: 'https://images.pexels.com/photos/6776613/pexels-photo-6776613.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      // },
      // {
      //   name: ' Lorem ipsum dolor sit amet, consect  tijigjvisjge',
      //   keys: '6',
      //   price: '3400',

      //   url: 'https://images.pexels.com/photos/6081495/pexels-photo-6081495.png?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      // },
      // {
      //   name: ' Lorem ipsum dolor sit amet, consect  rrrrrrr',
      //   keys: '7',
      //   price: '3000',

      //   url: 'https://images.pexels.com/photos/3987613/pexels-photo-3987613.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
      // },
      // {
      //   name: 'Lorem ipsum dolor sit amet, consectmopdjqwpx,eqo',
      //   keys: '8',
      //   price: '200',
      //   url: 'https://images.pexels.com/photos/7038222/pexels-photo-7038222.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      // },
    ],
  });
  // console.log(`length`, product.image.length);

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#222b55'}}>
      <Grid>
        <Row>
          <ScrollView>
            <View style={{width: width, height: height * 0.45}}>
              <ViewBackButton
                style={{
                  zIndex: 1,
                }}>
                <BackButton
                  navigation={navigation}
                  onPress={() => navigation.navigate('Home')}
                />
              </ViewBackButton>
              <ViewNumberOfImage
                style={{
                  opacity: 0.5,
                  zIndex: 1,
                  width: width * 0.1,
                  height: height * 0.03,
                }}>
                <TextOfNumber>1/{product.image.length}</TextOfNumber>
              </ViewNumberOfImage>
              <SlideBoxProduct />
            </View>
            <HeaderProduct />
            <Content>
              <ViewPadding>
                <ListProduct product={product} title="Product Same Shop" />
                {/* <ButtonSeeMore>
                  <SeeMoreButton />
                </ButtonSeeMore> */}
              </ViewPadding>

              <ViewPadding>
                <ListProduct product={product} title=" Product For You" />
                {/* <ButtonSeeMore>
                  <SeeMoreButton />
                </ButtonSeeMore> */}
              </ViewPadding>
            </Content>
          </ScrollView>
        </Row>
      </Grid>
    </SafeAreaView>
  );
}

const Content = styled(View)`
  margin-top: 1.5%;
  padding: 0px 1%;
  background-color: #171c39;
`;
const ViewPadding = styled(View)`
  padding-top: 5%;
`;

const ViewNumberOfImage = styled(View)`
  background-color: #777777;
  position: absolute;
  end: 5%;
  top: 4%;
  border-radius: 10px;
`;

const TextOfNumber = styled(Text)`
  font-size: 20px;
  color: #fff;
  font-family: DBHelvethaicaX-55Regular;
  justify-content: center;
  align-self: center;
`;
const ButtonSeeMore = styled(View)`
  margin-vertical: 2%;
`;
const ViewBackButton = styled(View)`
  position: absolute;
  margin: 2% 3%;
`;
