import authHeader from '../../utils/authHeader';
import {Platform} from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';

export const uploadImage = async image => {
  let data2 = await authHeader();
  let data = [];
  image.map(item => {
    if (Platform.OS === 'android') {
      RNFetchBlob.wrap(item.files);
    } else {
      RNFetchBlob.wrap(item.files.replace('file://', ''));
    }
    data = [
      ...data,
      {
        fileupload: RNFetchBlob.wrap(item.files),
      },
    ];
  });

  try {
    const response = await RNFetchBlob.fetch(
      'POST',
      UPLOAD_IMAGE_URL,
      data2,
      data,
    );
    // console.log(response);
    const result = JSON.parse(response.data);
    var dataid = [];
    result.map(item => {
      dataid = [...dataid, item.id];
    });
    return dataid;
  } catch (error) {
    return error;
  }
};
