import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Alert,
  FlatList,
  ScrollView,
  Image,
  SafeAreaView,
  Dimensions,
  StyleSheet,
} from 'react-native';
import styled from 'styled-components';
import {Col, Row, Grid} from 'react-native-easy-grid';
// import SlideBox from '../home/SlideBox';
import SlideBoxProduct from './SlideBoxProduck';
// import DropDownPicker from 'react-native-dropdown-picker';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faAngleDown, faAngleUp} from '@fortawesome/pro-regular-svg-icons';
import {ButtonGreen1, ButtonBorder} from '../../components/ButtonComponent';
import ListCategory from '../../components/product/ListCategory';
import {TextInput} from 'react-native-paper';
import BackButton from '../../components/product/BackButton';

const {width, height} = Dimensions.get('window');
interface ISlider {
  image: string[];
}
export default function AddProductDetailsScreen({
  navigation,
}: {
  navigation: any;
}) {
  const [onPressCategory, setOnPressCategory] = useState(true);
  const [valueCategory, setValueCategory] = useState(null);

  // Input
  const [isTitle, setIsTitle] = useState('');
  const [isPrice, setIsPrice] = useState('');
  const [isDetail, setIsDetail] = useState('');

  const handleCategory = () => {
    setOnPressCategory(!onPressCategory);
  };
  const categoryLists = {
    category: [
      {label: 'Music', value: 'Music'},
      {label: 'Sport', value: 'Sport'},
      {label: 'Movie', value: 'Movie'},
      {label: 'TV Show', value: 'TVShow'},
      {label: 'Gamer', value: 'Gamer'},
      {label: 'Education', value: 'Education'},
      {label: 'News', value: 'News'},
      {label: 'Series', value: 'Series'},
      {label: 'Mystery', value: 'Mystery'},
      {label: 'Travel', value: 'Travel'},
      {label: 'Shopping', value: 'Shopping'},
      {label: 'Technology', value: 'Technology'},
      {label: 'Food', value: 'Food'},
    ],
  };
  const [product] = useState<ISlider>({
    image: [
      {
        name: 'Lorem ipsum dolor sit amet, consect opppppppppppppp',
        keys: '1',
        price: '300',
        url: 'https://images.pexels.com/photos/6776613/pexels-photo-6776613.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      },
      {
        name: ' Lorem ipsum dolor sit amet, consect  tijigjvisjge',
        keys: '2',
        price: '3400',

        url: 'https://images.pexels.com/photos/6081495/pexels-photo-6081495.png?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      },
      {
        name: ' Lorem ipsum dolor sit amet, consect  rrrrrrr',
        keys: '3',
        price: '3000',

        url: 'https://images.pexels.com/photos/3987613/pexels-photo-3987613.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
      },
      {
        name: 'Lorem ipsum dolor sit amet, consectmopdjqwpx,eqo',
        keys: '4',
        price: '200',
        url: 'https://images.pexels.com/photos/7038222/pexels-photo-7038222.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      },
    ],
  });

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#171c39'}}>
      <Grid>
        <Row>
          <ScrollView>
            <View style={{width: width, height: height * 0.45}}>
              <ViewBackButton
                style={{
                  zIndex: 1,
                }}>
                <BackButton
                  navigation={navigation}
                  onPress={() => navigation.navigate('CreateProduct')}
                />
              </ViewBackButton>

              <ViewNumberOfImage
                style={{
                  zIndex: 1,
                  opacity: 0.5,
                  width: width * 0.1,
                  height: height * 0.03,
                }}>
                <TextOfNumber>1/{product.image.length}</TextOfNumber>
              </ViewNumberOfImage>
              <SlideBoxProduct />
              <View
                style={{
                  position: 'absolute',
                  zIndex: 1,
                  end: '3%',
                  bottom: '5%',
                  shadowColor: '#000',
                  shadowOffset: {width: 30, height: 30},
                  shadowOpacity: 0.8,
                  shadowRadius: 2,
                }}>
                <TouchableOpacity>
                  <Image
                    source={require('../../assets/images/edit_image.png')}
                    style={{
                      width: width * 0.1,
                      height: height * 0.05,
                    }}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </View>
            </View>
            <ViewTexts>
              <ViewInput>
                <TextInput
                  label="Title"
                  value={isTitle}
                  onChangeText={text => setIsTitle(text)}
                  labelStyle={styles.labelStyle}
                  style={[
                    styles.textInput,
                    {
                      background: 'transparent',
                    },
                  ]}
                  theme={{
                    colors: {
                      text: '#fff',
                      accent: '#fff',
                      primary: '#7f7f7f',
                      placeholder: '#fff',
                      background: 'transparent',
                    },
                  }}
                  underlineColorAndroid="transparent"
                />
              </ViewInput>
              <ViewInput>
                <TextInput
                  label="Price"
                  value={isPrice}
                  onChangeText={text => setIsPrice(text)}
                  labelStyle={{
                    fontSize: 22,
                    color: '#fff',
                    fontFamily: 'DBHelvethaicaX-55Regular',
                  }}
                  style={[
                    styles.textInput,
                    {
                      background: 'transparent',
                    },
                  ]}
                  theme={{
                    colors: {
                      text: '#fff',
                      accent: '#fff',
                      primary: '#7f7f7f',
                      placeholder: '#fff',
                      background: 'transparent',
                    },
                  }}
                  keyboardType="numeric"
                  underlineColorAndroid="transparent"
                  selectionColor="#fff"
                />
                <ViewPrice>
                  <TextPrice> ฿ </TextPrice>
                </ViewPrice>
              </ViewInput>
              <ViewInput>
                <TextInput
                  label="Detail"
                  value={isDetail}
                  onChangeText={text => setIsDetail(text)}
                  labelStyle={styles.labelStyle}
                  style={[
                    styles.textInput,
                    {
                      background: 'transparent',
                    },
                  ]}
                  left={() => (
                    <Text style={{color: '#f67', fontSize: 22}}>฿</Text>
                  )}
                  theme={{
                    colors: {
                      text: '#fff',
                      accent: '#fff',
                      primary: '#7f7f7f',
                      placeholder: '#fff',
                      background: 'transparent',
                    },
                  }}
                  underlineColorAndroid="transparent"
                />
                <TouchableOpacity onPress={handleCategory}>
                  <ViewCategory>
                    <TextCategory>Category</TextCategory>
                    <ViewTouchableOpacityCategory>
                      {onPressCategory === false ? (
                        <FontAwesomeIcon
                          icon={faAngleDown}
                          size={25}
                          color="#ffffff"
                        />
                      ) : (
                        <FontAwesomeIcon
                          icon={faAngleUp}
                          size={25}
                          color="#ffffff"
                        />
                      )}
                    </ViewTouchableOpacityCategory>
                  </ViewCategory>
                </TouchableOpacity>

                {onPressCategory === true ? (
                  <ViewListCategory>
                    <ListCategory categoryLists={categoryLists} />
                  </ViewListCategory>
                ) : null}
              </ViewInput>
            </ViewTexts>
            <ViewButton>
              <ViewSubButton>
                <ButtonBorder
                  title="Cancle"
                  onPress={() => Alert.alert('hoooo')}
                />
              </ViewSubButton>
              <ViewSubButton>
                <ButtonGreen1
                  title="Post"
                  onPress={() => navigation.navigate('ProductScreen')}
                />
              </ViewSubButton>
            </ViewButton>
          </ScrollView>
        </Row>
      </Grid>
    </SafeAreaView>
  );
}

const ViewSlideBox = styled(View)`
  width: 100%;
  height: 50%;
`;

const TextCategory = styled(Text)`
  font-family: DBHelvethaicaX-55Regular;
  color: #ffffff;
  font-size: 22px;
`;

const ViewCategory = styled(View)`
  margin: 10px 0px 0px 3%;
  flex-direction: row;
`;

const ViewTouchableOpacityCategory = styled(View)`
  position: absolute;
  end: 12px;
`;

const ViewInput = styled(View)``;

const ViewTexts = styled(View)`
  margin-horizontal: 5%;
`;

const ViewButton = styled(View)`
  flex-direction: row;
  flex: 1;
  justify-content: center;
  margin-top: 10%;
`;
const ViewSubButton = styled(View)`
  flex: 0.4;
  margin-horizontal: 1%;
`;
const TextOfNumber = styled(Text)`
  font-size: 20px;
  color: #fff;
  font-family: DBHelvethaicaX-55Regular;
  justify-content: center;
  align-self: center;
`;
const ViewNumberOfImage = styled(View)`
  background-color: #777777;
  position: absolute;
  end: 5%;
  top: 4%;
  border-radius: 10px;
`;
const ViewBackButton = styled(View)`
  position: absolute;
  margin: 2% 3%;
`;
const ViewListCategory = styled(View)`
  margin-top: 5%;
`;
const TextPrice = styled(Text)`
  font-size: 20px;
  color: #fff;
  font-family: DBHelvethaicaX-55Regular;
`;

const ViewPrice = styled(View)`
  position: absolute;
  end: 5%;
  bottom: 7%;
`;

const styles = StyleSheet.create({
  textInput: {
    color: '#fff',
    fontSize: 16,
    fontFamily: 'DBHelvethaicaX-55Regular',
  },
  labelStyle: {
    fontSize: 22,
    color: '#fff',
    fontFamily: 'DBHelvethaicaX-55Regular',
  },
});
