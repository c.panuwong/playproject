import React, {useState} from 'react';
import {
  View,
  Text,
  Modal,
  Alert,
  FlatList,
  TouchableOpacity,
  Pressable,
  StyleSheet,
  Image,
} from 'react-native';
import styled from 'styled-components';
import {Col, Row, Grid} from 'react-native-easy-grid';
import {DBHText} from '../../../components/StyledText';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCircle} from '@fortawesome/pro-solid-svg-icons';
import {faTimes} from '@fortawesome/pro-light-svg-icons';

export default function ModalCategory({
  navigation,
  categoryLists,
  visible,
  close,
}: {
  navigation: any;
  categoryLists: any;
  visible: any;
  close: any;
}) {
  const [isChecking, setIsChecking] = useState([]);
  const handleChecking = (id: any) => {
    if (isChecking.indexOf(id) > -1) {
      setIsChecking(isChecking.filter(x => x !== id));
    } else {
      setIsChecking(oldArray => [...oldArray, id]);
    }
  };
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={visible}
      // onRequestClose={() => {
      //   Alert.alert('Modal has been closed.');
      //   setModalVisible(!modalVisible);
      // }}
    >
      <ViewContainer>
        <ViewModal>
          <Header>
            <TextHeader size={30}> Category</TextHeader>
            <IconClose onPress={close}>
              <FontAwesomeIcon icon={faTimes} color="#fff" size={30} />
            </IconClose>
          </Header>
          <ListContents>
            <FlatList
              data={categoryLists.category}
              numColumns={2}
              keyExtractor={(item: any) => item.value}
              renderItem={data => {
                return (
                  <Touchable onPress={() => handleChecking(data.item.value)}>
                    <ViewList>
                      {isChecking?.indexOf(data.item.value) > -1 ? (
                        <ImageStyle
                          source={require('../../../assets/images/select_interest&creator.png')}
                          resizeMode={'cover'}
                        />
                      ) : (
                        <Icon icon={faCircle} size={20} color="#fff" />
                      )}

                      <TextList size={22}>{data.item.value} </TextList>
                    </ViewList>
                  </Touchable>
                );
              }}
            />
          </ListContents>
        </ViewModal>
      </ViewContainer>
    </Modal>
  );
}

const ViewContainer = styled(View)`
  flex: 1;
  justify-content: flex-start;
  align-items: center;
`;
const ViewModal = styled(View)`
  background-color: #171c39;
  width: 100%;
  padding: 5%;
  justify-content: center;
`;
const TextHeader = styled(DBHText)`
  align-items: flex-start;
`;
const ListContents = styled(View)`
  padding-horizontal: 5%;
`;
const TextList = styled(DBHText)``;
const Touchable = styled(TouchableOpacity)`
  width: 50%;
  padding: 3% 5%;
`;
const ViewList = styled(View)`
  flex-direction: row;
  align-items: center;
`;
const ImageStyle = styled(Image)`
  width: 20;
  height: 20;
  margin-horizontal: 10%;
`;
const Icon = styled(FontAwesomeIcon)`
  margin-horizontal: 10%;
`;
const Header = styled(View)`
  flex-direction: row;
  align-content: space-between;
  align-items: center;
`;
const IconClose = styled(TouchableOpacity)`
  end: 0;
  position: absolute;
`;
