import React from 'react';
import {TouchableOpacity, Dimensions} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCamera} from '@fortawesome/pro-light-svg-icons';

const {width, height} = Dimensions.get('window');

export default function CameraPicture({
  width,
  height,
  size,
  setimagePath,
  imagePath,
  setData,
}: {
  width?: number;
  height?: number;
  size?: number;
  imagePath: any;
  setimagePath: any;
  setData: any;
}) {
  const takePhotoFromCamera = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      setData(oldArray => [
        ...oldArray,
        {
          node: {
            group_name: 'Download',
            image: {url: image.path},
            location: null,
            modified: image.modificationDate,
            timestamp: image.modificationDate,
            type: image.mime,
          },
        },
      ]);
      console.log(image);
    });
  };

  return (
    <Touchable onPress={() => takePhotoFromCamera()}>
      <FontAwesomeIcon icon={faCamera} color="#fff" size={55} />
    </Touchable>
  );
}
const Touchable = styled<any>(TouchableOpacity)`
  width: 120px;
  height: 120px;
  margin: 1%;
  background-color: #ef6;
  border-radius: 30px;
  align-items: center;
  align-content: center;
  justify-content: center;
  background-color: #24305e;
  border-radius: 100px;
`;
