import React from 'react';
import {
  View,
  Image,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  Alert,
} from 'react-native';
import {DBHText} from '../../../components/StyledText';
import styled from 'styled-components';
import LinearGradient from 'react-native-linear-gradient';
import {faChevronRight} from '@fortawesome/pro-regular-svg-icons';
import {faLink} from '@fortawesome/pro-light-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import IconClones from 'react-native-vector-icons/FontAwesome';
import {ButtonBorder} from '../../../components/ButtonComponent';

// chevron-right
const {width, height} = Dimensions.get('window');

const data = [
  {
    id: '1',
    url: require('../../../assets/images/mockimage/Rectangle4100-1.png'),
  },
  {
    id: '2',
    url: require('../../../assets/images/mockimage/Rectangle4100-2.png'),
  },
  {
    id: '3',
    url: require('../../../assets/images/mockimage/Rectangle4100-4.png'),
  },
];

const dataSocialMedia = [
  {
    id: '1',
    url: require('../../../assets/images/icons/facebook.png'),
  },
  {
    id: '2',
    url: require('../../../assets/images/icons/ig.png'),
  },
  {
    id: '3',
    url: require('../../../assets/images/icons/call.png'),
  },
];

export default function SharePost() {
  return (
    <Background source={require('../../../assets/images/bg/BG_post_share.jpg')}>
      <ViewAll>
        <FontDBHText>Share</FontDBHText>
        <ViewFlatList>
          <View>
            <FlatList
              data={dataSocialMedia}
              keyExtractor={(item: any) => item.id}
              numColumns={3}
              renderItem={data => {
                return (
                  <TouchableIcon
                    style={{
                      marginHorizontal: '8.5%',
                    }}>
                    <Image
                      source={data.item.url}
                      resizeMode="contain"
                      style={{
                        width: width * 0.12,
                        height: width * 0.12,
                        borderRadius: width * 0.035,
                      }}
                    />
                  </TouchableIcon>
                );
              }}
            />
            <FlatList
              data={data}
              keyExtractor={(item: any) => item.id}
              numColumns={3}
              renderItem={data => {
                return (
                  // flexWrap: 'wrap'
                  <TouchableIcon
                    style={{
                      marginHorizontal: '9%',
                    }}>
                    <Linear
                      start={{x: 0.2, y: 1.5}}
                      end={{x: 1.3, y: 0}}
                      colors={['#257CE2', '#33D52B', '#99FF00']}>
                      <ImageView
                        source={data.item.url}
                        resizeMode="cover"
                        style={{
                          width: width * 0.09,
                          height: width * 0.09,
                          borderRadius: width * 0.035,
                        }}
                      />
                    </Linear>
                  </TouchableIcon>
                );
              }}
            />
          </View>

          {/* <TouchableOpacity style={{backgroundColor: '#f22'}}> */}
          <MoreText>
            <DBHText size={24}>More</DBHText>
            <FontAwesomeIcon icon={faChevronRight} color="#fff" size={18} />
          </MoreText>
          {/* </TouchableOpacity> */}
        </ViewFlatList>
        <TouchableOpacity>
          <ViewLink>
            <IconLink>
              <FontAwesomeIcon icon={faLink} color="#fff" size={13} />
            </IconLink>
            <LinkText>
              <DBHText size={18} numberOfLines={1}>
                https://www.figma.com/file/42s
              </DBHText>
            </LinkText>
            <IconClone>
              <IconClones name="clone" color="#fff" size={18} />
            </IconClone>
          </ViewLink>
        </TouchableOpacity>

        <ViewButtom>
          <View style={{width: '70%'}}>
            <ButtonBorder
              title="Promote"
              onPress={() => Alert.alert('Promo')}
              height={40}
              color="#080d28"
              textColor="#99FF00"
              margin={'1%'}
              // changeImage
              changeImage={
                <Image
                  source={require('../../../assets/images/icons/promote.png')}
                  style={{width: 18, height: 18, marginHorizontal: '5%'}}
                  resizeMode="contain"
                />
              }
            />
          </View>
          <TouchableOpacity style={{marginVertical: '5%'}}>
            <DBHText color="#99ff00" size={20}>
              Go Your Feed
            </DBHText>
          </TouchableOpacity>
        </ViewButtom>
      </ViewAll>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
  width: 100%;
  height: 100%;
  border-top-left-radius: 50px;
  border-top-right-radius: 50px;
`;

const FontDBHText = styled(DBHText)`
  font-size: 28;
  color: #fffefe;
  margin-top: 5%;
  padding-bottom: 5%;
`;
const ViewAll = styled(View)`
  padding-horizontal: 20px;
  padding-top: 10px;
  flex: 1;
`;
const ViewIcon = styled(View)`
  flex-direction: row;
  background-color: #f77f;
`;

const Linear = styled(LinearGradient)`
  justify-content: center;
  align-items: center;
  align-content: center;
  border-radius: 15px;
  width: 20px;
  height: 20px;
  padding: 20px;
  margin-horizontal: 3px;
`;
const ImageView = styled(Image)`
  border-width: 2px;
  border-color: #ffffff;
  align-self: center;
`;
const ViewLink = styled(View)`
  background-color: rgba(116, 116, 128, 0.18);
  flex-direction: row;
  align-content: space-between;
  border-radius: 10px;
  padding-vertical: 3%;
  margin-vertical: 5%;
  align-items: center;
  padding-horizontal: 3%;
  margin-horizontal: 5%;
`;
const ImageStyled = styled(Image)`
  width: 20px;
  height: 20px;
  border-radius: 5px;
`;
const TextTouchable = styled(TouchableOpacity)`
  flex-direction: row;
`;
const MoreText = styled(TouchableOpacity)`
  flex-direction: row;
  align-items: center;
  position: absolute;
  bottom: 10;
  end: -23%;
`;
const ViewFlatList = styled(View)`
  flex-direction: row;
  width: 75%;
`;
const TouchableIcon = styled(TouchableOpacity)`
  padding-vertical: 3%;
  align-content: space-between;
`;
const IconLink = styled(View)`
  padding: 2%;
  border-width: 1px;
  border-color: #fff;
  border-radius: 100;
  margin-horizontal: 3%;
  align-items: center;
`;
const IconClone = styled(View)`
  position: absolute;
  end: 5%;
`;
const LinkText = styled(View)`
  padding-left: 2%;
`;
const ViewButtom = styled(View)`
  align-items: center;
  width: 100%;
`;
