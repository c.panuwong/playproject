import React from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import {useRecoilState, useResetRecoilState} from 'recoil';
// import { RNCamera, FaceDetector } from 'react-native-camera';
import ImagePicker from 'react-native-image-crop-picker';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCamera} from '@fortawesome/pro-light-svg-icons';

export default function CameraVideo() {
  const takePhotoFromCamera = () => {
    ImagePicker.openCamera({
      mediaType: 'video',
    }).then(image => {
      console.log(image);
    });
  };

  return (
    <Touchable
      onPress={() => takePhotoFromCamera()}
      style={{
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: '#24305e',
        borderRadius: 100,
      }}>
      <FontAwesomeIcon icon={faCamera} color="#fff" size={55} />
    </Touchable>
  );
}
const Touchable = styled(TouchableOpacity)`
  width: 31%;
  height: 120px;
  margin: 1%;
  background-color: #ef6;
  border-radius: 30px;
`;
