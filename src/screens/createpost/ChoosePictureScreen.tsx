import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  FlatList,
  PermissionsAndroid,
  Platform,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import CameraRoll from '@react-native-community/cameraroll';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCircle} from '@fortawesome/pro-solid-svg-icons';
import {faCamera} from '@fortawesome/pro-light-svg-icons';
import CameraPicture from './components/CameraPicture';
import ImagePicker from 'react-native-image-crop-picker';

export default function ChoosePictureScreen({
  navigation,
  setimagePath,
  imagePath,
}: {
  navigation: any;
  imagePath: any;
  setimagePath: any;
}) {
  const [data, setData] = useState('');
  const [isChoose, setIsChoose] = useState([]);
  // const [state, setstate] = useState(initialState);

  const openCamera = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      setData(oldArray => [...oldArray, {}]);
      console.log(image);
    });
  };
  const getPhotos = () => {
    CameraRoll.getPhotos({
      first: 50,
      assetType: 'Photos',
    })
      .then(res => {
        console.log('====================================');
        console.log(res.edges);
        console.log('====================================');
        setData(res.edges);
      })
      .catch(error => {
        console.log(error);
      });
  };
  const askPermission = async () => {
    if (Platform.OS === 'android') {
      const result = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: 'Permission Explanation',
          message: 'ReactNativeForYou would like to access your photos!',
        },
      );
      if (result !== 'granted') {
        console.log('Access to pictures was denied');
        return;
      } else {
        getPhotos();
      }
    } else {
      getPhotos();
    }
  };
  useEffect(() => {
    askPermission();
  }, []);

  const ChoosePhoto = (id: any) => {
    if (isChoose.indexOf(id) > -1) {
      setIsChoose(isChoose.filter(x => x != id));
    } else {
      setIsChoose(oldArray => [...oldArray, id]);
    }
  };

  // console.log(JSON.stringify(data, null, 2));

  return (
    <Container>
      <FlatList
        data={data}
        numColumns={3}
        keyExtractor={(item, index) => index.toString()}
        columnWrapperStyle={{flex: 1}}
        extraData={data}
        renderItem={({item, index}) => {
          return (
            <>
              {index == 0 ? (
                <CameraPicture
                  imagePath={imagePath}
                  setImagePast={setimagePath}
                  setData={setData}
                />
              ) : null}
              <Touchable onPress={() => ChoosePhoto(item.node.timestamp)}>
                {isChoose?.indexOf(item.node.timestamp) > -1 ? (
                  <Image
                    source={require('../../assets/images/select_interest&creator.png')}
                    resizeMode={'contain'}
                    style={{
                      width: '23%',
                      height: '23%',
                      position: 'absolute',
                      zIndex: 1,
                      end: '5%',
                      top: '7%',
                    }}
                  />
                ) : (
                  <FontAwesomeIcon
                    icon={faCircle}
                    color="#fff"
                    size={30}
                    style={{
                      opacity: 0.8,
                      borderWidth: 2,
                      borderColor: '#99ff00',
                      borderRadius: 100,
                      position: 'absolute',
                      zIndex: 1,
                      end: '5%',
                      top: '7%',
                    }}
                  />
                )}

                <View style={{borderRadius: 30}}>
                  <ImageStyle
                    source={{uri: item.node.image.uri}}
                    style={{width: '100%', height: '100%'}}
                  />
                </View>
              </Touchable>
            </>
          );
        }}
      />
    </Container>
  );
}
const Container = styled(View)`
  flex: 1;
  background-color: #020b3c;
  padding-top: 15%;
`;
const Touchable = styled(TouchableOpacity)`
  width: 31%;
  height: 120px;
  margin: 1%;
  border-radius: 30px;
`;
const ImageStyle = styled(Image)`
  width: 100%;
  height: 13%;
  border-radius: 30px;
`;
const TouchableView = styled(View)`
  width: 31%;
  height: 120px;
  margin: 1%;
  border-radius: 30px;
`;
const TouchableCheck = styled(TouchableOpacity)`
  z-index: 1;
  end: 5%;
  top: 5%;
  position: absolute;
`;

const ViewMargin = styled(View)`
  padding-top: 15%;
`;
