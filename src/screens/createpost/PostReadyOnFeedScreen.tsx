import React from 'react';
import {View, ImageBackground, Dimensions, Image} from 'react-native';
import styled from 'styled-components';
import {DBHText} from '../../components/StyledText';
import SharePost from './components/SharePost';
const {width, height} = Dimensions.get('window');

export default function PostReadyOnFeedScreen({navigation}: {navigation: any}) {
  return (
    <Background
      source={require('../../assets/images/bg/BG_post.jpg')}
      resizeMode="cover">
      <ViewAll>
        <Image
          source={require('../../assets/images/icons/group.png')}
          style={{width: width * 0.3, height: height * 0.14}}
          resizeMode="contain"
        />
        <FontDBHText>Your post ready on feed</FontDBHText>
      </ViewAll>
      <View style={{height: '60%'}}>
        <SharePost />
      </View>
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
`;
const ViewAll = styled(View)`
  flex: 1;
  padding-horizontal: 20px;
  align-content: center;
  justify-content: center;
  align-items: center;
`;
const ViewImage = styled(View)`
  align-items: center;
  justify-content: center;
`;
const ViewText = styled(View)`
  padding-top: 3%;
  align-items: center;
  width: 80%;
`;
const FontDBHText = styled(DBHText)`
  font-size: 30;
  color: #fffefe;
  margin-top: 5%;
`;
