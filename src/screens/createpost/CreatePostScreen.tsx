import React, {useState} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  ScrollView,
  Modal,
  Alert,
} from 'react-native';
import styled from 'styled-components';
import {DBHText} from '../../components/StyledText';
import ProfileHeader from '../../components/ProfileHeader';
import {TextArea, Stack} from 'native-base';
// import {faTimesCircle, faTimes} from '@fortawesome/pro-light-svg-icons';
// import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import PostOnePhoto from '../../components/PostOnePhoto';
import PostTwoPhoto from '../../components/PostTwoPhoto';
import PostsThreePhoto from '../../components/PostsThreePhoto';
import PostFourPhoto from '../../components/PostFourPhoto';
import ModalCategory from './components/ModalCategory';
// times-circle
const {width, height} = Dimensions.get('window');
// times-circle times-circle
export default function CreatePostScreen({
  navigation,
  route,
}: {
  navigation: any;
  route?: any;
}) {
  const [value, onChangeText] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);

  // console.log(JSON.stringify(navigation, null, 4));
  // console.log(route);
  // console.log(route.params.params);
  // console.log(route.name.CreatePostPhoto);

  const categoryLists = {
    category: [
      {label: 'Music', value: 'Music'},
      {label: 'Sport', value: 'Sport'},
      {label: 'Movie', value: 'Movie'},
      {label: 'TV Show', value: 'TVShow'},
      {label: 'Gamer', value: 'Gamer'},
      {label: 'Education', value: 'Education'},
      {label: 'News', value: 'News'},
      {label: 'Series', value: 'Series'},
      {label: 'Mystery', value: 'Mystery'},
      {label: 'Travel', value: 'Travel'},
      {label: 'Shopping', value: 'Shopping'},
      {label: 'Technology', value: 'Technology'},
      {label: 'Food', value: 'Food'},
    ],
  };

  const Text = () => {
    const [textAreaValue, setTextAreaValue] = useState('');
    const demoValueControlledTextArea = (e: any) => {
      setTextAreaValue(e.currentTarget.value);
    };

    return (
      <Stack space={4} w="100%" h="100%">
        <TextArea
          value={textAreaValue}
          onChange={demoValueControlledTextArea}
        />
      </Stack>
    );
  };
  const OnPressModal = () => {
    setModalVisible(true);
    console.log('Open');
  };

  return (
    <Background>
      <ProfileHeader />

      <ScrollView>
        <TextInputTitle
          onChangeText={text => onChangeText(text)}
          placeholderTextColor="#b3b3b3"
          blurOnSubmit={false}
          multiline={true}
          value={value}
          placeholder="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis gravida augue at est accumsan adipiscing"
          style={{
            padding: '5%',
            backgroundColor: '#24305e',
            flex: 1,
            height: height * 0.2,
          }}
        />

        {route?.params?.route === 'Choose' ? (
          // <PostOnePhoto navigation={navigation} />
          // <PostsThreePhoto navigation={navigation} />
          <PostTwoPhoto navigation={navigation} />
        ) : // <PostFourPhoto navigation={navigation} />
        null}
      </ScrollView>
      <ViewAllButtons>
        <TabBottom>
          <Button
            onPress={() => navigation.navigate('Posts', {screen: 'TagFriend'})}>
            <ImageIcon
              source={require('../../assets/images/icons/twousers.png')}
              resizeMode="contain"
              style={{
                width: width * 0.1,
                height: width * 0.1,
              }}
            />
          </Button>
          <Button onPress={OnPressModal}>
            <ImageIcon
              source={require('../../assets/images/icons/shop1.png')}
              resizeMode="contain"
              style={{width: width * 0.1, height: width * 0.1}}
            />
          </Button>
          <Button
            onPress={() =>
              navigation.navigate('Posts', {screen: 'ChoosePicture'})
            }>
            <ImageIcon
              source={require('../../assets/images/icons/image.png')}
              resizeMode="contain"
              style={{width: width * 0.072, height: width * 0.056}}
            />
          </Button>
          <Button
            onPress={() =>
              navigation.navigate('Posts', {screen: 'ChooseVideo'})
            }>
            <ImageIcon
              source={require('../../assets/images/icons/camara.png')}
              resizeMode="contain"
              style={{
                width: width * 0.072,
                height: width * 0.056,
                paddingHorizontal: '20%',
              }}
            />
          </Button>
        </TabBottom>
        <Button onPress={() => navigation.navigate('PostReadyOnFeed')}>
          <DBHText color={'#99ff00'} size={28}>
            Post
          </DBHText>
        </Button>
      </ViewAllButtons>
      <ModalCategory
        visible={modalVisible}
        categoryLists={categoryLists}
        close={() => setModalVisible(false)}
      />
    </Background>
  );
}
// 24305e

const Container = styled(View)`
  flex: 1;
`;
// height: 50%;

const Background = styled(View)`
  flex: 1;
  background-color: #060c30;
  padding-top: 10%;
`;
const ViewInput = styled(View)`
  width: 100%;
  height: 100%;
`;
const TextInputTitle = styled(TextInput)`
  color: white;
  font-family: DBHelvethaicaX-55Regular;
  font-size: 20px;
  width: 100%;
  height: 100%;
  text-align-vertical: top;
`;
const TabBottom = styled(View)`
  width: 85%;
  align-items: center;
  flex-direction: row;
`;
// background-color: #060c30;
const Button = styled(TouchableOpacity)`
  padding-horizontal: 1%;
`;
const ViewAllButtons = styled(View)`
  flex: 1;
  background-color: #060c30;
  flex-direction: row;
  width: 100%;
  height: 15%;
  padding-horizontal: 5%;
  align-items: center;
  bottom: 0px;
`;
const ImageIcon = styled(Image)``;
// background-color: #060c30;
const ViewPost = styled(View)`
  flex: 1;
`;
const CloseIcon = styled(TouchableOpacity)`
  position: absolute;
  end: 3%;
  top: 3%;
`;
