import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  SafeAreaView,
  FlatList,
  Dimensions,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {DBHText} from '../../components/StyledText';
import styled from 'styled-components';
import {ButtonGreen1} from '../../components/ButtonComponent';
import {data} from '../hamberger/data';
import SearchBar from '../search/components/SearchBar';
import {Col, Row, Grid} from 'react-native-easy-grid';
import LinearGradient from 'react-native-linear-gradient';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCircle} from '@fortawesome/pro-solid-svg-icons';
import Search from '../../screens/hamberger/components/Search';

export default function TagFriendScreen({navigation}: {navigation: any}) {
  const [isChoose, setIsChoose] = useState([]);
  const ChoosePhoto = (id: any) => {
    if (isChoose.indexOf(id) > -1) {
      setIsChoose(isChoose.filter(x => x != id));
    } else {
      setIsChoose(oldArray => [...oldArray, id]);
    }
  };
  return (
    <Background
      source={require('../../assets/images/BG_Hamberger.jpg')}
      resizeMode="cover">
      <Search />

      <Grid>
        {/* <ViewAll> */}
        <FlatList
          data={data}
          keyExtractor={(item: any) => item.id}
          renderItem={data => {
            return (
              <Row style={{paddingVertical: '3%'}}>
                <Col style={{}} size={0.25}>
                  <ViewImage>
                    <Linear
                      start={{x: 0.2, y: 1.5}}
                      end={{x: 1.3, y: 0}}
                      colors={['#257CE2', '#33D52B', '#99FF00']}>
                      <ImageProfile
                        source={{uri: data.item.user_profile}}
                        resizeMode="cover"
                      />
                    </Linear>
                  </ViewImage>
                </Col>
                <Col style={{}}>
                  <ViewTexts>
                    <DBHText numberOfLines={1}>{data.item.user_name}</DBHText>
                    <DBHText numberOfLines={1}>{data.item.bio}</DBHText>
                  </ViewTexts>
                </Col>
                <Col size={0.15} style={{alignItems: 'center'}}>
                  <TouchableOpacity onPress={() => ChoosePhoto(data.item.id)}>
                    <ViewSelection>
                      {isChoose?.indexOf(data.item.id) > -1 ? (
                        <Image
                          source={require('../../assets/images/select_interest&creator.png')}
                          resizeMode={'cover'}
                          style={{width: 20, height: 20}}
                        />
                      ) : (
                        <FontAwesomeIcon
                          icon={faCircle}
                          color="#fff"
                          size={20}
                        />
                      )}
                    </ViewSelection>
                  </TouchableOpacity>
                </Col>
              </Row>
            );
          }}
        />
        {/* </ViewAll> */}
      </Grid>
      <ButtonGreen1
        title="Select"
        onPress={() => navigation.navigate('CreatePostPhoto')}
        padding={5}
        marginTop={5}
      />
    </Background>
  );
}
const Background = styled(ImageBackground)`
  flex: 1;
  padding: 25px;
  padding-top: 40px;
`;
const ViewList = styled(View)`
  flex-direction: row;
  align-items: center;
  padding-vertical: 1.5%;
`;
const ViewAll = styled(View)`
  margin-top: 5%;
`;

const Linear = styled(LinearGradient)`
  justify-content: center;
  align-items: center;
  align-content: center;
  border-radius: 17px;
  width: 55px;
  height: 54px;
  padding-vertical: 28px;
`;
const ImageProfile = styled(Image)`
  width: 51.51px;
  height: 51.51px;
  border-radius: 17px;
  border-width: 2.5px;
  border-color: #fff;
`;
const ViewImage = styled(View)`
  flex: 0.2;
`;
const ViewTexts = styled(View)`
  flex: 0.45;
  padding-end: 10px;
`;
const Touchable = styled(TouchableOpacity)`
  flex: 1;
  width: 30;
  height: 30px;
  background-color: #fff6;
  margin: 1%;
  border-radius: 30px;
`;
const ViewSelection = styled(View)`
  align-items: center;
  align-content: center;
  justify-content: center;
  flex: 1;
`;
