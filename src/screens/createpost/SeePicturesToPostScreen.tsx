import React, {useState} from 'react';
import {
  View,
  Image,
  FlatList,
  PermissionsAndroid,
  Text,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  TextInput,
  ScrollView,
} from 'react-native';
import styled from 'styled-components';
import {DBHText} from '../../components/StyledText';
import {data} from '../hamberger/data';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faHeart} from '@fortawesome/pro-light-svg-icons';
import {ButtonGreen1} from '../../components/ButtonComponent';

const {width, height} = Dimensions.get('window');

export default function SeePicturesToPostScreen({
  navigation,
}: {
  navigation: any;
}) {
  const RenderItem = ({data}: {data: any}) => {
    // console.log(data.item);

    return (
      <Container>
        <VieWImage>
          <CloseIcon style={{zIndex: 1}}>
            <Image
              source={require('../../assets/images/icons/close.png')}
              style={{
                width: width * 0.08,
                height: width * 0.08,
              }}
              resizeMode={'cover'}
            />
          </CloseIcon>
          <Image
            source={require('../../assets/images/mockimage/Rectangle3752.png')}
            style={{width: '100%', height: height * 0.47}}
            // resizeMode="contain"
            resizeMode="cover"
          />
        </VieWImage>
        <ViewBottomAll>
          <Touchable>
            {/* <ImagegalaxyEngagement
              source={require('../../assets/images/galaxyicon/likepostingalaxy.png')}
              resizeMode="contain"
              color="white"
              // tintColor={'#99ff00'}
              tintColor={'#ffff'}
            /> */}
            <FontAwesomeIcon icon={faHeart} color="white" size={24} />
            <TextEngagement>
              99k
              {/* {data.item.star} */}
            </TextEngagement>
          </Touchable>
          <Touchable>
            <ImageGalaxyComment
              source={require('../../assets/images/galaxyicon/commenticon.png')}
              resizeMode="contain"
              color="white"
              // tintColor={'#99ff00'}
              tintColor={'#ffff'}
            />
            <TextEngagement>
              99k
              {/* {data.item.comment} */}
            </TextEngagement>
          </Touchable>
          <Touchable>
            <ImageGalaxyShare
              source={require('../../assets/images/galaxyicon/shareicongalaxy.png')}
              resizeMode="contain"
              color="white"
              // tintColor={'#99ff00'}
              tintColor={'#ffff'}
            />
            <TextEngagement>
              99k
              {/* {data.item.share} */}
            </TextEngagement>
          </Touchable>
        </ViewBottomAll>
        <DBHText
          numberOfLines={2}
          style={{
            fontSize: 20,
            color: '#b3b3b3',
            paddingHorizontal: '5%',
            paddingTop: '2%',
          }}>
          {/* {data.item.post} */}
          Lorem ipsum dolor sit amet, consectetur adipis elit. Duis gravida
          augue at est accumsan . . .
        </DBHText>
      </Container>
    );
  };
  return (
    <Background>
      <RenderItem data={data} />
      <ButtonGreen1
        // image={() => <FontAwesomeIcon icon={faHeart} color="white" size={24} />}
        onPress={() => navigation.navigate('ChoosePicture')}
        title="Add Photo"
        padding={5}
        marginTop={20}
      />
      {/* <FlatList
        data={data.video}
        listKey="post"
        keyExtractor={item => item.id}
        renderItem={(data) =>}
      /> */}
      {/* <RenderItem data={} /> */}
    </Background>
  );
}
const Background = styled(View)`
  flex: 1;
  background-color: #101531;
`;
const Container = styled(View)`
  flex: 1;
  margin-top: 50px;
`;
const VieWImage = styled(View)``;
const CloseIcon = styled(TouchableOpacity)`
  position: absolute;
  end: 3%;
  top: 3%;
`;
const ViewBottomAll = styled(View)`
  flex-direction: row;
  justify-content: space-between;
  padding: 3% 0px 3% 0px;
  margin-horizontal:5%
  border-bottom-width: 1px;
  border-color: #fff;

`;
const Touchable = styled(TouchableOpacity)`
  flex-direction: row;
`;
const ImageGalaxyShare = styled(Image)`
  width: 30px;
  height: 22.26px;
`;
const ImagegalaxyEngagement = styled(Image)`
  width: 28.5px;
  height: 28px;
  margin-top: -5px;
`;
const TextEngagement = styled(DBHText)`
  color: #b3b3b3;
  font-size: 20px;
  padding-horizontal: 2%;
`;
const ImageGalaxyComment = styled(Image)`
  width: 25px;
  height: 23.33px;
`;
