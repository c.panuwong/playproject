import { atom, selector, useRecoilState, useRecoilValue } from 'recoil';
export const searchInput = atom({
  key: 'searchinput',
  default: '',
});

