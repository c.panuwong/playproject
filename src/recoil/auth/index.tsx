import { atom, selector, useRecoilState, useRecoilValue } from 'recoil';
export const username = atom({
  key: 'username',
  default: 'gotnaja',
});

export const channel = atom({
  key: 'channel',
  default: [],
});

