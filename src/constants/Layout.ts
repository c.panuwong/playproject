import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default {
  window: {
    width, //414
    height, //896
  },
  isSmallDevice: width < 375,
};

// ROOM CHAT
export const chat_modalize_height = height / 3.5;

