import * as React from 'react';
// @ts-ignore
import {Text as DefaultText} from 'react-native';

type TextProps = InputProps & DefaultText['props'];
type InputProps = {
  size?: number;
  color?: string;
};
export function DBHText(props: TextProps) {
  const {style, size, color, ...otherProps} = props;
  return (
    <DefaultText
      style={[
        {
          fontSize: size || 16,
          color: color || 'white',
          fontFamily: 'DBHelvethaicaX-55Regular',
        },
        style,
      ]}
      {...otherProps}
    />
  );
}
