import React from 'react';
import {View, Text, Image, TouchableOpacity, Dimensions} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faChevronCircleRight} from '@fortawesome/pro-light-svg-icons';

const {width, height} = Dimensions.get('window');
// chevron-circle-right
export default function PostFourPhoto({navigation}: {navigation: any}) {
  return (
    <Grid>
      <Col size={2}>
        <Image
          source={require('../assets/images/mockimage/Rectangle3751.png')}
          style={{width: '100%', height: height * 0.5}}
          resizeMode={'cover'}
        />
      </Col>
      <Col size={1}>
        <Row size={1}>
          <Image
            source={require('../assets/images/mockimage/Rectangle3751.png')}
            style={{width: '100%', height: height * 0.167}}
            resizeMode={'cover'}
          />
        </Row>
        <Row size={1}>
          <Image
            source={require('../assets/images/mockimage/Rectangle3751.png')}
            style={{width: '100%', height: height * 0.167}}
            resizeMode={'cover'}
          />
        </Row>
        <Row size={1} style={{alignItems: 'center', justifyContent: 'center'}}>
          <TouchableOpacity
            onPress={() => navigation.navigate('SeePicturesToPost')}
            style={{position: 'absolute', zIndex: 2}}>
            <FontAwesomeIcon
              icon={faChevronCircleRight}
              color="#fff"
              size={width * 0.08}
            />
          </TouchableOpacity>

          <Image
            source={require('../../../assets/images/mockimage/Rectangle3751.png')}
            style={{width: '100%', height: height * 0.167}}
            resizeMode={'cover'}
          />
        </Row>
      </Col>
    </Grid>
  );
}
