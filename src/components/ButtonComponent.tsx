import React from 'react';
import {Text, View, Button, TouchableOpacity, Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
// @ts-ignore
import styled from 'styled-components';
interface IHeaderButtonProps {
  onPress: any;
  title: string;
  padding: number;
  marginTop: number;
}
export const ButtonGreen1 = ({
  onPress,
  title,
  padding,
  marginTop,
}: IHeaderButtonProps) => {
  return (
    <LinearGradient
      start={{x: 0.2, y: 1.5}}
      end={{x: 1.3, y: 0}}
      colors={['#257CE2', '#33D52B', '#99FF00']}
      style={{
        justifyContent: 'center',
        borderRadius: 25,
        marginTop: marginTop,
      }}>
      <TouchableOpacity
        onPress={onPress}
        style={{
          justifyContent: 'center',
          paddingHorizontal: padding,
          height: 52,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Title>{title}</Title>
      </TouchableOpacity>
    </LinearGradient>
  );
};

const Title = styled(Text)`
  color: white;
  font-size: 20px;
  line-height: 21px;
  text-align: center;
  font-family: DBHelvethaicaX-55Regular;
`;
interface ButtonDisbleProps {
  color: Array<string>;
  onPress: any;
  title: string;
  padding: number;
  marginTop: number;
}
export const ButtonDisble = ({
  color,
  onPress,
  title,
  padding,
  marginTop,
}: ButtonDisbleProps) => {
  return (
    <LinearGradient
      start={{x: 0.2, y: 1.5}}
      end={{x: 1.3, y: 0}}
      colors={color}
      style={{
        justifyContent: 'center',
        borderRadius: 25,
        marginTop: marginTop,
      }}>
      <TouchableOpacity
        onPress={onPress}
        style={{
          justifyContent: 'center',
          paddingHorizontal: padding,
          height: 52,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Title>{title}</Title>
      </TouchableOpacity>
    </LinearGradient>
  );
};

interface ButtonFollowProps {
  onPress: any;
  title: string;
  // icon?: StyleProp<ViewStyle>;
  // styles?: StyleProp<ViewStyle>;
}
export const ButtonFollow = ({onPress, title}: ButtonFollowProps) => {
  return (
    <LinearGradient
      start={{x: 0.2, y: 1.5}}
      end={{x: 1.3, y: 0}}
      colors={['#257CE2', '#33D52B', '#99FF00']}
      style={{
        justifyContent: 'center',
        borderRadius: 25,
        alignContent: 'center',
        alignItems: 'center',
        width: 120,
        height: 36,
        padding: 5,
        borderWidth: 1,
        borderColor: ['#257CE2', '#33D52B', '#99FF00'],
      }}>
      <TouchableOpacity onPres={onPress} style={{flexDirection: 'row'}}>
        <Image
          source={require('../assets/images/bottom_4.png')}
          style={{width: 20, height: 20}}
          resizeMode="cover"
        />
        <Text style={{color: '#fff', paddingHorizontal: 5}}>{title}</Text>
      </TouchableOpacity>
    </LinearGradient>
  );
};

interface IButtonBorderProps {
  onPress: any;
  title: string;
  padding?: number;
  // marginTop: number;
  color?: string;
  height?: number;
  textColor?: string;
}

export const ButtonBorder = ({
  onPress,
  title,
  padding,
  color,
  height,
  textColor,
}: IButtonBorderProps) => {
  return (
    <LinearGradient
      start={{x: 0.2, y: 1.5}}
      end={{x: 1.3, y: 0}}
      colors={['#257CE2', '#33D52B', '#99FF00']}
      style={{
        justifyContent: 'center',
        borderRadius: 25,
        // marginTop: marginTop,
        width: '100%',
      }}>
      <TouchableOpacity
        onPress={onPress}
        style={{
          justifyContent: 'center',
          padding: padding || '5%',
          height: height || 45,
          // flexDirection: 'row',
          // alignItems: 'center',
          backgroundColor: color || '#020b3c',
          marginHorizontal: '2%',
          marginVertical: '2%',
          borderRadius: 25,
        }}>
        <Title style={{color: textColor || '#fff'}}>{title}</Title>
      </TouchableOpacity>
    </LinearGradient>
  );
};
