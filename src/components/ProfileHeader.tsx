import React from 'react';
import {View, TouchableOpacity, Image, Dimensions} from 'react-native';
import styled from 'styled-components';
import {DBHText} from './StyledText';
import LinearGradient from 'react-native-linear-gradient';
import {faEllipsisV} from '@fortawesome/pro-regular-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';

const {width, height} = Dimensions.get('window');

export default function ProfileHeader({
  subtitle,
  button,
}: {
  subtitled?: string;
  button?: any;
}) {
  return (
    <ViewProfile>
      <ViewHeader>
        <Linear
          start={{x: 0.2, y: 1.5}}
          end={{x: 1.3, y: 0}}
          colors={['#257CE2', '#33D52B', '#99FF00']}>
          <ImageView
            source={require('../assets/images/mockimage/Rectangle3750.png')}
            resizeMode="cover"
            style={{
              width: width * 0.1,
              height: width * 0.1,
              borderRadius: width * 0.035,
            }}
          />
        </Linear>
        <ViewTextHeader>
          <DBHText size={22}>jdijdijiwdjiiwimdk</DBHText>
          {subtitle ? (
            <DBHText size={16} color="#b3b3b3">
              {subtitle}
            </DBHText>
          ) : null}
        </ViewTextHeader>
      </ViewHeader>
      <TouchableOpacity>
        {button ? (
          <FontAwesomeIcon icon={faEllipsisV} size={25} color="#ffff" />
        ) : null}
      </TouchableOpacity>
    </ViewProfile>
  );
}
const ViewHeader = styled(View)`
  flex-direction: row;
  align-items: center;
  align-content: center;
  justify-content: center;
`;
const ViewProfile = styled(View)`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 2%;
`;
const Linear = styled(LinearGradient)`
  justify-content: center;
  align-items: center;
  align-content: center;
  border-radius: 17px;
  width: 23px;
  height: 23px;
  padding: 23px;
`;
const ViewTextHeader = styled(View)`
  padding-horizontal: 4%;
`;
const ImageView = styled(Image)`
  border-width: 2px;
  border-color: #ffffff;
`;
