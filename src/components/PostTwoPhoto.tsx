import React from 'react';
import {Image, Dimensions} from 'react-native';
import styled from 'styled-components';
import {Col, Row, Grid} from 'react-native-easy-grid';

const {width, height} = Dimensions.get('window');

export default function PostTwoPhoto() {
  return (
    <Grid>
      <Col>
        <Image
          source={require('../assets/images/mockimage/Rectangle3751.png')}
          style={{width: '100%', height: height * 0.5}}
          resizeMode={'cover'}
        />
      </Col>
      <Col>
        <Image
          source={require('../assets/images/mockimage/Rectangle3751.png')}
          style={{width: '100%', height: height * 0.5}}
          resizeMode={'cover'}
        />
      </Col>
    </Grid>
  );
}
