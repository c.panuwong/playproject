import React from 'react';
import {View, Text} from 'react-native';
import Arrow from 'react-native-vector-icons/AntDesign';
export default function HeaderBackButton({navigation,onPress}: {navigation: any,onPress:any}) {
  return (
    <Arrow
      name="left"
      size={27}
      color="white"
      style={{marginLeft: 10}}
      onPress={onPress}
    />
  );
}
