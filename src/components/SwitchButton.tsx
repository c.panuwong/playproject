import React, {useState} from 'react';
import {View, Text, Switch} from 'react-native';
import styled from 'styled-components';
import {DBHText} from './StyledText';

export default function SwitchButton({
  title,
  onValueChange,
  value,
  texTitle,
}: {
  title: string;
  onValueChange: any;
  value: any;
  texTitle?: string;
}) {
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);

  return (
    <ViewContent>
      <ViewText>
        <DBHText size={26}>{title}</DBHText>
        {texTitle ? (
          <DBHText
            size={18}
            style={{color: '#b3b3b3', marginRight: '10%', marginTop: '1%'}}>
            {texTitle}
          </DBHText>
        ) : null}
      </ViewText>
      <ViewSwitch>
        <Switch
          trackColor={{false: '#fff', true: '#fff'}}
          thumbColor={value ? '#1adf45' : '#e1e1e1'}
          ios_backgroundColor="#3e3e3e"
          onValueChange={onValueChange}
          value={value}
        />
      </ViewSwitch>
    </ViewContent>
  );
}
const ViewContent = styled(View)`
  flex: 1;
  flex-direction: row;
`;
const ViewSwitch = styled(View)`
  flex: 0.1;
`;
const ViewText = styled(View)`
  flex: 0.9;
`;
