import React from 'react';
import {View, Text, Image, TouchableOpacity, Dimensions} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';

const {width, height} = Dimensions.get('window');

export default function PostsThreePhoto({navigation}: {navigation: any}) {
  return (
    <Grid>
      <Col size={2}>
        <Image
          source={require('../assets/images/mockimage/Rectangle3751.png')}
          style={{width: '100%', height: height * 0.5}}
          resizeMode={'cover'}
        />
      </Col>
      <Col size={1}>
        <Row>
          <Image
            source={require('../assets/images/mockimage/Rectangle3751.png')}
            style={{width: '100%', height: height * 0.25}}
            resizeMode={'cover'}
          />
        </Row>
        <Row>
          <Image
            source={require('../assets/images/mockimage/Rectangle3751.png')}
            style={{width: '100%', height: height * 0.25}}
            resizeMode={'cover'}
          />
        </Row>
      </Col>
    </Grid>
  );
}
