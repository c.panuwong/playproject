import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faChevronLeft} from '@fortawesome/pro-regular-svg-icons';

// chevron-left

export default function BackButton({
  navigation,
  onPress,
}: {
  navigation: any;
  onPress: any;
}) {
  return (
    <View style={{paddingTop: '5%', paddingLeft: '5%'}}>
      <TouchableOpacity
        onPress={onPress}
        style={{
          backgroundColor: '#777777',
          width: 40,
          height: 40,
          borderRadius: 20,
          alignContent: 'center',
          justifyContent: 'center',
          opacity: 0.5,
        }}>
        <FontAwesomeIcon
          icon={faChevronLeft}
          color="#ffffff"
          size={30}
          style={{
            marginLeft: 1,
          }}
        />
      </TouchableOpacity>
    </View>
  );
}
