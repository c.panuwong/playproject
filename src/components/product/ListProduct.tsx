import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
import styled from 'styled-components';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {data} from '../../screens/profiles/data';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faAngleDown} from '@fortawesome/pro-regular-svg-icons';

// import SeeMoreButton from './SeeMoreButton';

// const {width, height} = Dimensions.get('window');
const {width, height} = Dimensions.get('window');

interface ISlider {
  image: string[];
}
interface IPropsData {
  product: Array<object>;
  title: string;
}

export default function ListProduct({product, title}: IPropsData) {
  const [isBookMark, setIsBookMark] = useState(false);

  const [isSeeMore, setIsSeeMore] = useState(product.image);

  const [isBookMarks, setIsBookMarks] = useState([]);

  const [isUpdate, setIsUpdate] = useState(false);

  const handleAddSeeMore = () => {
    // setIsSeeMore(oldArray => [...oldArray, {id}, {id}, {id}, {id}]);
    setIsSeeMore(oldArray => [
      ...oldArray,
      {
        name: 'Lorem ipsum dolor sit amet, consect opppppppppppppp',
        keys: '9',
        price: '100',
        url: 'https://images.pexels.com/photos/6776613/pexels-photo-6776613.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      },
      {
        name: ' Lorem ipsum dolor sit amet, consect  tijigjvisjge',
        keys: '10',
        price: '100',

        url: 'https://images.pexels.com/photos/6081495/pexels-photo-6081495.png?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      },
      {
        name: ' Lorem ipsum dolor sit amet, consect  rrrrrrr',
        keys: '11',
        price: '100',

        url: 'https://images.pexels.com/photos/3987613/pexels-photo-3987613.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
      },
      {
        name: ' Lorem ipsum dolor sit amet, consect  rrrrrrr',
        keys: '12',
        price: '3100',
        url: 'https://images.pexels.com/photos/3987613/pexels-photo-3987613.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
      },
    ]);
    console.log(isSeeMore);
  };

  const AddAndRemoveBookmark = (id: any) => {
    if (isBookMarks.indexOf(id) > -1) {
      setIsBookMarks(isBookMarks.filter(x => x != id));
    } else {
      setIsBookMarks(oldArray => [...oldArray, id]);
    }
    // console.log(id);
  };

  const SeeMoreButton = () => {
    return (
      <TouchableOpacityStyle
        onPress={handleAddSeeMore}
        style={{width: width * 0.25, height: height * 0.05}}>
        <ViewButton>
          <TextStyled>See More</TextStyled>
          <FontAwesomeIcon icon={faAngleDown} color="#fff" size={20} />
        </ViewButton>
      </TouchableOpacityStyle>
    );
  };

  return (
    <FlatList
      data={isSeeMore}
      listKey="Shops"
      numColumns={2}
      // keyExtractor={item}
      extraData={isSeeMore}
      scrollEnabled={false}
      showsVerticalScrollIndicator={false}
      ListHeaderComponent={() => <TextTitle> {title} </TextTitle>}
      keyExtractor={(item, index) => item.keys + index}
      ListFooterComponent={SeeMoreButton}
      renderItem={data => {
        return (
          <Row
            key={data.item.keys}
            style={{
              marginBottom: '1%',
            }}>
            <ViewContent
              style={{
                // height: height * 0.35,
                width: '100%',
                padding: '1.5%',
              }}>
              <ImageProduct
                source={{uri: data.item.url}}
                resizeMode={'cover'}
                style={{width: '100%', height: height * 0.23}}
              />
              <ViewHeader>
                <ViewTexts>
                  <TextNameProduct numberOfLines={2}>
                    {data.item.name}
                  </TextNameProduct>
                  <TextPrice>฿ {data.item.price}</TextPrice>
                </ViewTexts>
                <ViewIcon>
                  <TouchableOpacity
                    onPress={() => AddAndRemoveBookmark(data.item.keys)}>
                    <Icon
                      name={
                        isBookMarks?.indexOf(data.item.keys) > -1
                          ? 'bookmark'
                          : 'bookmark-o'
                      }
                      color={
                        isBookMarks?.indexOf(data.item.keys) > -1
                          ? '#99ff00'
                          : '#ffffff'
                      }
                      size={30}
                    />
                  </TouchableOpacity>
                </ViewIcon>
              </ViewHeader>
            </ViewContent>
          </Row>
        );
      }}
    />
  );
}
const ButtonSeeMore = styled(View)`
  margin-vertical: 2%;
`;
const TextTitle = styled(Text)`
  font-family: DBHelvethaicaX-55Regular;
  font-size: 26px;
  color: #ffffff;
`;

const ViewContent = styled(View)`
  align-items: center;
  align-content: center;
  justify-content: center;
`;
// width: width * 0.47;
// height: height * 0.23;

const ImageProduct = styled(Image)`
  border-radius: 5px;
`;

const ViewTexts = styled(View)`
  width: 85%;
`;

const TextNameProduct = styled(Text)`
  padding: 3% 5% 0px 3%;
  font-family: DBHelvethaicaX-55Regular;
  font-size: 22px;
  color: #b3b3b3;
`;

const TextPrice = styled(Text)`
  padding: 0px 3%;
  width: 100%;
  font-family: DBHelvethaicaX-55Regular;
  font-size: 22px;
  color: #fff;
`;
// font-weight: bold;

const ViewHeader = styled(View)`
  width: 100%;
  flex-direction: row;
`;
const ViewIcon = styled(View)`
  padding-top: 3%;
  flex-basis: 15%;
  width: 10%;
`;
const TextStyled = styled(Text)`
  font-size: 18px;
  color: #fff;
  font-family: DBHelvethaicaX-55Regular;
  justify-content: center;
  align-self: center;
`;
const ViewButton = styled(View)`
  flex-direction: row;
  justify-content: center;
  align-self: center;
`;

const TouchableOpacityStyle = styled(TouchableOpacity)`
  background-color: #2c3970;
  border-radius: 15px;
  justify-content: center;
  align-self: center;
  border-width: 1px;
  border-color: #fff;
`;
