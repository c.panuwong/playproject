import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  Checkbox,
} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCircle} from '@fortawesome/pro-duotone-svg-icons';

interface IParam {
  value: string;
  label: string;
}
export default function ListCategory({categoryLists}: {categoryLists: any}) {
  const [isSelected, setSelection] = useState([]);

  const handleSelected = (id: any) => {
    if (isSelected.indexOf(id) > -1) {
      setSelection(isSelected.filter(x => x != id));
    } else {
      setSelection(oldArray => [...oldArray, id]);
    }
  };

  const ListCategorys = ({categoryLists}) => {
    return (
      <FlatList
        data={categoryLists}
        listKey="category"
        numColumns={2}
        key={2}
        scrollEnabled={false}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item: IParam) => item.value}
        renderItem={data => {
          return (
            <Row key={data.item.value} style={{marginVertical: '1%'}}>
              <Col>
                <TouchableOpacity
                  onPress={() => handleSelected(data.item.value)}>
                  <Row>
                    <ViewSelection>
                      {isSelected?.indexOf(data.item.value) > -1 ? (
                        <Image
                          source={require('../../assets/images/select_interest&creator.png')}
                          resizeMode={'cover'}
                          style={{width: 20, height: 20}}
                        />
                      ) : (
                        <FontAwesomeIcon
                          icon={faCircle}
                          color="#999999"
                          size={20}
                        />
                      )}
                    </ViewSelection>

                    <Text style={{color: '#fff', fontSize: 20}}>
                      {data.item.label}
                    </Text>
                  </Row>
                </TouchableOpacity>
              </Col>
            </Row>
          );
        }}
      />
    );
  };

  return <ListCategorys categoryLists={categoryLists.category} />;
}

const ViewSelection = styled(View)`
  margin-horizontal: 20px;
  align-self: center;
`;
