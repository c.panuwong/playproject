import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
} from 'react-native';
import styled from 'styled-components';
import {Col, Row, Grid} from 'react-native-easy-grid';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faShare, faBookmark} from '@fortawesome/pro-regular-svg-icons';
// import {faBookmark} from '@fortawesome/pro-solid-svg-icons';
import Icon from 'react-native-vector-icons/Ionicons';

import ReadMore from '@fawazahmed/react-native-read-more';
// pro-solid-svg-icons
const {width, height} = Dimensions.get('window');

export default function HeaderProduct() {
  const [isBookMark, setIsBookMark] = useState(false);

  const handleBookMark = () => {
    setIsBookMark(!isBookMark);
    // console.log(isBookMark);
  };

  return (
    <>
      <ViewHeaders1>
        <ViewHeaders2>
          <ViewProfile>
            <ImageProfile
              source={{
                uri: 'https://images.pexels.com/photos/8747027/pexels-photo-8747027.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
              }}
            />
          </ViewProfile>
          <ViewName>
            <TextHeader numberOfLines={1}>Ralph Edwards</TextHeader>
          </ViewName>
          <ViewIcons>
            <StyleTouchableOpacity onPress={handleBookMark}>
              {isBookMark === true ? (
                <Icon name="bookmark-sharp" color="#99ff00" size={18} />
              ) : (
                <FontAwesomeIcon icon={faBookmark} color="#fff" size={20} />
              )}
            </StyleTouchableOpacity>
            <StyleTouchableOpacity>
              <FontAwesomeIcon icon={faShare} color="#fff" size={20} />
            </StyleTouchableOpacity>
          </ViewIcons>
        </ViewHeaders2>
        <View>
          <TextDescription>
            Lorem ipsum dolor sit amet, consectetur
          </TextDescription>
          <TextNameChannel>Movie - Gamer -TV Show</TextNameChannel>
          <TextDescription>฿ 999</TextDescription>
        </View>
      </ViewHeaders1>
      <ViewSubHeader>
        <TextSubDescription
          numberOfLines={3}
          seeMoreStyle={styles.seeMoreStyle}
          seeLessStyle={styles.seeLessStyle}
          seeMoreText="Read More"
          seeLessText="See Less">
          {
            ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Craspellentesque posuere augue at laoreet. Quisque ut justo aliquam, vestibulum nibh a, mattis dolor. Vestibulum at molestie lectus.Suspendisse blandit accumsan cursus. Quisque ullamcorper quis libero vel blandit. Sed et mauris sed magna viverra aliquet quis eget est. Pellentesque vitae ornare augue. Maecenas vel nisl arcu. '
          }
        </TextSubDescription>
      </ViewSubHeader>
    </>
  );
}

const styles = StyleSheet.create({
  seeMoreStyle: {
    color: '#e7e7e7e7',
    fontSize: 14,
    alignSelf: 'center',
    fontFamily: 'Entypo',
  },
  seeLessStyle: {
    color: '#e7e7e7e7',
    fontSize: 14,
    alignSelf: 'center',
    fontFamily: 'Entypo',
  },
});

const ViewHeaders2 = styled(View)`
  flex-direction: row;
  align-items: center;
  flex: 1;
  flex-basis: auto;
`;

const ViewProfile = styled(View)`
  background-color: #fff;
  width: 36px;
  height: 36px;
  border-radius: 12px;
  align-items: center;
  align-content: center;
  justify-content: center;
  flex-basis: auto;
`;
const ViewName = styled(View)`
  flex-basis: 70%;
  padding-horizontal: 5%;
`;

const ViewIcons = styled(View)`
  flex-direction: row;
  align-items: center;
  align-items: flex-end;
  flex-basis: auto;
`;

const StyleTouchableOpacity = styled(TouchableOpacity)`
  flex-basis: 10%;
`;
const TextHeader = styled(Text)`
  font-family: DBHelvethaicaX-55Regular;
  font-size: 22px;
  color: #b3b3b3;
`;
const ImageProfile = styled(Image)`
  width: 28px;
  height: 28px;
  border-radius: 9px;
`;
const TextDescription = styled(Text)`
  font-family: DBHelvethaicaX-55Regular;
  font-size: 26px;
  color: #ffffff;
`;
const TextNameChannel = styled(Text)`
  font-family: DBHelvethaicaX-55Regular;
  font-size: 18px;
  color: #b3b3b3;
`;
const ViewSubHeader = styled(View)`
  background-color: #171c39;
  padding: 3% 3% 3% 5%;
`;
const TextSubDescription = styled(ReadMore)`
  font-family: DBHelvethaicaX-55Regular;
  font-size: 20px;
  margin-horizontal: 1%;
  color: #b3b3b3;
`;
const ViewHeaders1 = styled(View)`
  padding: 3% 5%;
  align-items: center;
  flex: 1;
  flex-basis: auto;
`;
