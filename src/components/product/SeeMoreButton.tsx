import React, {useState} from 'react';
import styled from 'styled-components';

import {View, Text, TouchableOpacity, Dimensions} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faAngleDown} from '@fortawesome/pro-regular-svg-icons';

const {width, height} = Dimensions.get('window');

export default function SeeMoreButton() {
  return (
    <TouchableOpacityStyle style={{width: width * 0.25, height: height * 0.05}}>
      <ViewButton>
        <TextStyled>See More</TextStyled>
        <FontAwesomeIcon icon={faAngleDown} color="#fff" size={20} />
      </ViewButton>
    </TouchableOpacityStyle>
  );
}
const TextStyled = styled(Text)`
  font-size: 18px;
  color: #fff;
  font-family: DBHelvethaicaX-55Regular;
  justify-content: center;
  align-self: center;
`;
const ViewButton = styled(View)`
  flex-direction: row;
  justify-content: center;
  align-self: center;
`;

const TouchableOpacityStyle = styled(TouchableOpacity)`
  background-color: #2c3970;
  border-radius: 15px;
  justify-content: center;
  align-self: center;
  border-width: 1px;
  border-color: #fff;
`;
