import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import { DBHText } from './StyledText';
export default function RadioButton({
  data,
  value,
  setvalue,
}: {
  data: any;
  value: any;
  setvalue: any;
}) {
  return (
    <View style={{flexDirection: 'row'}}>
      {data.map((res: any) => {
        return (
          <View key={res.key} style={styles.container}>
            <TouchableOpacity
              style={ value === res.key ? styles.radioCircle : styles.radioCircleNotSelect}
              onPress={() => {
                setvalue(res.key);
              }}>
              {value === res.key && <View style={styles.selectedRb} />}
            </TouchableOpacity>
            <DBHText style={styles.radioText} size={20}>{res.text}</DBHText>
          </View>
        );
      })}
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  radioText: {
    marginRight: 35,
    paddingLeft:5
  },
  radioCircle: {
    height: 26,
    width: 26,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: '#1ADF45',
    alignItems: 'center',
    justifyContent: 'center',
  },
  radioCircleNotSelect: {
    height: 26,
    width: 26,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: '#1ADF45',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'#fff'
  },
  selectedRb: {
    width: 15,
    height: 15,
    borderRadius: 50,
    backgroundColor: '#1ADF45',
  },
  result: {
    marginTop: 20,
    color: 'white',
    fontWeight: '600',
    backgroundColor: '#F3FBFE',
  },
});
