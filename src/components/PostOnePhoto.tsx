import React from 'react';
import {View, Text, TouchableOpacity, Image, Dimensions} from 'react-native';
import styled from 'styled-components';

const {width, height} = Dimensions.get('window');

export default function PostOnePhoto({navigation}: {navigation: any}) {
  return (
    <ViewPost>
      <CloseIcon style={{zIndex: 1}}>
        <Image
          source={require('../assets/images/icons/close.png')}
          style={{
            width: width * 0.08,
            height: width * 0.08,
          }}
          resizeMode={'cover'}
        />
      </CloseIcon>

      <Image
        source={require('../assets/images/mockimage/Rectangle3751.png')}
        style={{width: width, height: height * 0.5}}
        resizeMode={'cover'}
      />
    </ViewPost>
  );
}
const ViewPost = styled(View)`
  flex: 1;
`;
const CloseIcon = styled(TouchableOpacity)`
  position: absolute;
  end: 3%;
  top: 3%;
`;
