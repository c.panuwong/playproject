import Ionicons from 'react-native-vector-icons/Ionicons';
import React from 'react';
import {Menu} from 'react-native-paper';
import {DBHText} from './StyledText';
import { Alert } from 'react-native';

type EllipsisMenuChatProps = {
  visible: boolean;
  onPress: () => void;
  close: () => void;
};

const EllipsisMenuChat: React.FC<EllipsisMenuChatProps> = ({
  visible,
  onPress,
  close,
}) => {
  const blockOrUnblock = () => {
    // to block user
  };

  const muteOrUnmute = () => {
    // to mute user
  };

  return (
    <Menu
      visible={visible}
      onDismiss={close}
      anchor={
        <Ionicons
          name="ellipsis-horizontal"
          size={26}
          color={'#fff'}
          style={{marginRight: 15}}
          onPress={onPress}
        />
      }
      contentStyle={{
        backgroundColor: '#000',
        minHeight: 120,
        minWidth: 170,
        padding: 15,
        borderRadius: 10,
      }}>
      <MenuList
        name="remove-circle-outline"
        title={'บล๊อคบุคคลนี้'}
        onPress={() => Alert.alert('บล็อค')}
      />
      <MenuList
        name="notifications-off-outline"
        title={'ปิดแจ้งเตือน'}
        onPress={() => Alert.alert('ปิดแจ้งเตือน')}
      />
    </Menu>
  );
};

interface IMenuType {
  name: React.ComponentProps<typeof Ionicons>['name'];
  title: string;
  onPress: () => void;
}

const MenuList = ({name, title, onPress}: IMenuType) => {
  return (
    <Menu.Item
      icon={() => <Ionicons name={name} size={24} color={'#fff'} />}
      onPress={onPress}
      title={<DBHText size={14}>{title}</DBHText>}
    />
  );
};

export default EllipsisMenuChat;
