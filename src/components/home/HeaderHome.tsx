import React, {useState} from 'react';
import {Animated, TouchableOpacity} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faSlidersH, faBell} from '@fortawesome/pro-regular-svg-icons';
const HeaderHome = ({
  fadeAnimation,
  fadeAnimationswap,
  navigation
}: {
  fadeAnimation: any;
  fadeAnimationswap: any;
  navigation: any;
}) => {
  return (
    <>
      <RowHeader>
        <Colheader>
          <Row>
            <ImageLogo
              style={{opacity: fadeAnimation}}
              source={require('../../assets/images/logo_login.png')}
              resizeMode="cover"
            />
            <TextBesideLogo style={{opacity: fadeAnimation}}>
              DOTPLAY
            </TextBesideLogo>
          </Row>
        </Colheader>
        <Colheaderswap>
          <Row>
            <ImageLogoswap
              style={{opacity: fadeAnimationswap}}
              source={require('../../assets/images/home2_icon.png')}
              resizeMode="cover"
            />
          </Row>
        </Colheaderswap>
        <Col>
          <FontAwesomeIcon
            icon={faBell}
            size={28}
            color="#fff"
            // style={{marginTop: 10, right: 3}}
          />
        </Col>
        <Col>
          <TouchableOpacity onPress={()=> navigation.navigate('Hamberger')}>
            <FontAwesomeIcon icon={faSlidersH} size={28} color="#fff" />
          </TouchableOpacity>
        </Col>
      </RowHeader>
    </>
  );
};

const RowHeader = styled(Row)`
  background-color: #060c30;
  height: 48px;
  padding: 10px 0 0 10px;
  width:100%;
`;
const TextBesideLogo = styled(Animated.Text)`
  color: #fff;
  font-size: 16px;
  margin-left: 10px;
  padding-top: 3px;
`;
const ImageLogo = styled(Animated.Image)`
  width: 31px;
  height: 31px;
`;
const ImageLogoswap = styled(Animated.Image)`
  width: 37px;
  height: 30px;
`;
const ImageCategory = styled(Animated.Image)`
  width: 39px;
  height: 39px;
  top: 0px;
`;

const Colheader = styled(Col)`
  width: 45%;
`;
const Colheaderswap = styled(Col)`
  width: 30%;
`;

export default HeaderHome;
