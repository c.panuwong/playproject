import React, {useState} from 'react';
import {
  Animated,
  View,
  Text,
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  FlatList,
} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
import styled from 'styled-components';

const Galaxy = ({popcreater}: {popcreater: any}) => {
  const DATA = [
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      title: '#First Item',
      url: require('../../assets/images/mockimage/Rectangle4100.png'),
    },
    {
      id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
      title: '#Second Item',
      url: require('../../assets/images/mockimage/Rectangle4100-1.png'),
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d72',
      title: '#Third Item',
      url: require('../../assets/images/mockimage/Rectangle4100-2.png'),
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d74',
      title: '#Third Item',
      url: require('../../assets/images/mockimage/Rectangle4100-3.png'),
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d78',
      title: '#Third Item',
      url: require('../../assets/images/mockimage/Rectangle4100-4.png'),
    },
  ];

  const Item = ({url, title}: {url: any; title: any}) => (
    <View>
      <Imageslide source={{uri: url[0].oss_file_url}} />
      <TextImageSlide>{title}</TextImageSlide>
    </View>
  );

  const renderItem = ({item}: {item: any}) => (
    <Item url={item.display_image} title={item.user_display_name} />
  );

  return (
    <View style={{height: 120, width: '100%', backgroundColor: '#060C30'}}>
      <FlatList
        data={popcreater}
        renderItem={renderItem}
        horizontal
        keyExtractor={item => item.id}
      />
    </View>
  );
};

const Imageslide = styled(Image)`
  width: 80px;
  height: 80px;
  border-radius: 20px;
  border-width: 2px;
  border-color: #c4c4c4;
  margin: 10px 10px 0 10px;
`;
const TextImageSlide = styled(Text)`
  font-size: 12px;
  color: #fff;
  text-align: center;
`;
export default Galaxy;
