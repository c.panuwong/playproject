import React, {useState} from 'react';
import {
  Animated,
  View,
  Text,
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
// @ts-ignore
import styled from 'styled-components';
// @ts-ignore
import IconStar from 'react-native-vector-icons/EvilIcons';
// @ts-ignore
import IcoLive from 'react-native-vector-icons/EvilIcons';
// @ts-ignore
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faImage,
  faPlayCircle,
  faVideo,
} from '@fortawesome/pro-regular-svg-icons';

const FeedNoFlat = ({navigation, data}: {navigation: any; data: any}) => {
  const GoToContentAll = () => {
    navigation.navigate('ContentAll', {screen: 'ContentAllScreen'});
  };
  const checkType = (type: string) => {
    return (
      <>
        {type == 'image' ? (
          <FontAwesomeIcon
            icon={faImage}
            size={25}
            color="#FFF"
            style={{position: 'absolute', left: 10, top: 10}}
          />
        ) : type == 'video' ? (
          <FontAwesomeIcon
            icon={faPlayCircle}
            size={25}
            color="#FFF"
            style={{position: 'absolute', left: 10, top: 10}}
          />
        ) : type == 'live' ? (
          <FontAwesomeIcon
            icon={faVideo}
            size={25}
            color="#FFF"
            style={{position: 'absolute', left: 10, top: 10}}
          />
        ) : null}
      </>
    );
  };
  return (
    <SafeAreaView>
      <Row>
        <ColWid66>
          <TouchableOpacity onPress={GoToContentAll}>
            <ImageFeed1
              source={{uri: data[0]?.upload_files[0]?.oss_file_url}}
              resizeMode="cover"
            />
          </TouchableOpacity>
          {checkType(data[0]?.content_type)}
          <IconStar
            name="star"
            color="white"
            size={30}
            style={{right: 10, bottom: 10, position: 'absolute'}}
          />
        </ColWid66>
        <Col>
          <Row>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('ConfigLive', {
                  screen: 'LiveMainScreen',
                  params: {
                    type: 'join',
                    channel: 'dotplay2test',
                  },
                })
              }
              style={{width: '100%', height: '100%'}}>
              <ImageFeed2
                source={{uri: data[1]?.upload_files[0]?.oss_file_url}}
                resizeMode="cover"
              />
            </TouchableOpacity>

            {checkType(data[1]?.content_type)}

            <IconStar
              name="star"
              color="white"
              size={30}
              style={{right: 10, bottom: 10, position: 'absolute'}}
            />
          </Row>
          <Row>
            <ImageFeed2
              source={{uri: data[2]?.upload_files[0]?.oss_file_url}}
              resizeMode="cover"
            />
            {checkType(data[2]?.content_type)}

            <IconStar
              name="star"
              color="white"
              size={30}
              style={{right: 10, bottom: 10, position: 'absolute'}}
            />
          </Row>
        </Col>
      </Row>

      <Row>
        <ColWid33>
          <ImageFeed2
            source={{uri: data[3]?.upload_files[0]?.oss_file_url}}
            resizeMode="cover"
          />
          {checkType(data[3]?.content_type)}
          <IconStar
            name="star"
            color="white"
            size={30}
            style={{right: 10, bottom: 10, position: 'absolute'}}
          />
        </ColWid33>
        <Col>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('ContentAll', {screen: 'VideoMore'})
            }>
            <ImageFeed2
              source={{uri: data[4]?.upload_files[0]?.oss_file_url}}
              resizeMode="cover"
            />
          </TouchableOpacity>
          {checkType(data[4]?.content_type)}
          <IconStar
            name="star"
            color="white"
            size={30}
            style={{right: 10, bottom: 10, position: 'absolute'}}
          />
        </Col>
      </Row>

      <Row>
        <Col>
          <ImageFeed2
            source={{uri: data[5]?.upload_files[0]?.oss_file_url}}
            resizeMode="cover"
          />
          {checkType(data[5]?.content_type)}
          <IconStar
            name="star"
            color="white"
            size={30}
            style={{right: 10, bottom: 10, position: 'absolute'}}
          />
        </Col>
        <ColWid33>
          <ImageFeed2
            source={{uri: data[6]?.upload_files[0]?.oss_file_url}}
            resizeMode="cover"
          />
          {checkType(data[6]?.content_type)}

          <IconStar
            name="star"
            color="white"
            size={30}
            style={{right: 10, bottom: 10, position: 'absolute'}}
          />
        </ColWid33>
      </Row>

     

      <Row>
        <ColWid33>
          <ImageFeed2
            source={{uri: data[7]?.upload_files[0]?.oss_file_url}}
            resizeMode="cover"
          />
          {checkType(data[7]?.content_type)}

          <IconStar
            name="star"
            color="white"
            size={30}
            style={{right: 10, bottom: 10, position: 'absolute'}}
          />
        </ColWid33>
        <ColWid33>
        <ImageFeed2
            source={{uri: data[8]?.upload_files[0]?.oss_file_url}}
            resizeMode="cover"
          />
          {checkType(data[8]?.content_type)}

          <IconStar
            name="star"
            color="white"
            size={30}
            style={{right: 10, bottom: 10, position: 'absolute'}}
          />
        </ColWid33>
        <ColWid33>
        <ImageFeed2
            source={{uri: data[9]?.upload_files[0]?.oss_file_url}}
            resizeMode="cover"
          />
          {checkType(data[9]?.content_type)}

          <IconStar
            name="star"
            color="white"
            size={30}
            style={{right: 10, bottom: 10, position: 'absolute'}}
          />
        </ColWid33>
      </Row>

      <Row>
        <Col>
          <Row>
          <ImageFeed2
            source={{uri: data[10]?.upload_files[0]?.oss_file_url}}
            resizeMode="cover"
          />
          {checkType(data[10]?.content_type)}

          <IconStar
            name="star"
            color="white"
            size={30}
            style={{right: 10, bottom: 10, position: 'absolute'}}
          />
          </Row>
          <Row>
          <ImageFeed2
            source={{uri: data[11]?.upload_files[0]?.oss_file_url}}
            resizeMode="cover"
          />
          {checkType(data[11]?.content_type)}

          <IconStar
            name="star"
            color="white"
            size={30}
            style={{right: 10, bottom: 10, position: 'absolute'}}
          />
          </Row>
        </Col>
        <ColWid66>
        <ImageFeed2
            source={{uri: data[12]?.upload_files[0]?.oss_file_url}}
            resizeMode="cover"
          />
          {checkType(data[12]?.content_type)}

          <IconStar
            name="star"
            color="white"
            size={30}
            style={{right: 10, bottom: 10, position: 'absolute'}}
          />
        </ColWid66>
      </Row>

      <Row>
        <Col>
        <ImageFeed2
            source={{uri: data[13]?.upload_files[0]?.oss_file_url}}
            resizeMode="cover"
          />
        </Col>
      </Row>
    </SafeAreaView>
  );
};

const ImageFeed1 = styled(Image)`
  width: 100%;
  height: 350px;
  border-width: 2px;
  border-color: #000;
`;
const ImageFeed2 = styled(Image)`
  width: 100%;
  height: 175px;
  border-width: 2px;
  border-color: #000;
`;
const ImageFeed3 = styled(Image)`
  width: 100%;
  height: 100px;
  border-width: 2px;
  border-color: #000;
`;
const ColWid33 = styled(Col)`
  width: 33.33%;
`;
const ColWid66 = styled(Col)`
  width: 66.66%;
`;

export default FeedNoFlat;
