import React, {useState} from 'react';
import {
  Animated,
  View,
  Text,
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
// @ts-ignore
import styled from 'styled-components';

// @ts-ignore
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faChevronSquareRight} from '@fortawesome/pro-regular-svg-icons';
const Hashtag = ({
  ColTextBelowSlide,
  ImageMore,
  navigation,
  hashtag,
}: {
  ColTextBelowSlide: any;
  ImageMore: any;
  navigation: any;
  hashtag: any;
}) => {
  const DATA = [
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      title: '#First Item',
      url: 'https://www.pikpng.com/pngl/m/514-5147505_green-square-png-illustration-clipart.png',
    },
    {
      id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
      title: '#Second Item',
      url: 'https://www.pikpng.com/pngl/m/514-5147505_green-square-png-illustration-clipart.png',
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d72',
      title: '#Third Item',
      url: 'https://www.pikpng.com/pngl/m/514-5147505_green-square-png-illustration-clipart.png',
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d74',
      title: '#Third Item',
      url: 'https://www.pikpng.com/pngl/m/514-5147505_green-square-png-illustration-clipart.png',
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d78',
      title: '#Third Item',
      url: 'https://www.pikpng.com/pngl/m/514-5147505_green-square-png-illustration-clipart.png',
    },
  ];

  const Item = ({title}: {title: any}) => (
    <View>
      <TextHashtag>{title}</TextHashtag>
    </View>
  );

  const renderItem = ({item}: {item: any}) => <Item title={item.name} />;

  const GoToHashAll = () => {
    navigation.navigate('HashTagAll', {screen: 'HashTagAllScreen'});
  };

  return (
    <View style={{width: '100%', backgroundColor: '#060C30'}}>
      <Row>
        <ColTextBelowSlide>
          <TextTitleHashtag>Galaxy Trends</TextTitleHashtag>
        </ColTextBelowSlide>
        <Col>
          <TouchableOpacity onPress={GoToHashAll}>
            <FontAwesomeIcon
              icon={faChevronSquareRight}
              size={31}
              color="#fff"
              style={{marginTop: 10, right: 3}}
            />
          </TouchableOpacity>
        </Col>
      </Row>
      <FlatList
        data={hashtag}
        renderItem={renderItem}
        horizontal
        keyExtractor={(item:any,index:any) => `${index}`}
      />
    </View>
  );
};

const TextHashtag = styled(Text)`
  font-size: 16px;
  color: #99ff00;
  text-align: center;
  margin: 10px 10px 10px 10px;
  padding: 5px;
  border-radius: 20px;
  border-width: 2px;
  border-color: gray;
`;
const TextTitleHashtag = styled(Text)`
  font-size: 18px;
  color: #fff;
  padding: 10px;
`;
export default Hashtag;
