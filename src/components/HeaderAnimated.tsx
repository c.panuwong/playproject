import React, {useState, forwardRef, useImperativeHandle} from 'react';
import {Animated, View} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faSlidersH, faBell} from '@fortawesome/pro-regular-svg-icons';
import HeaderBackButton from './HeaderBackButton';
import {DBHText} from './StyledText';
const HeaderAnimated = forwardRef((props: any, ref: any) => {
  let [fadeAnimationPosition] = useState<any>(new Animated.Value(-200));
  let [fadeAnimationswap] = useState<any>(new Animated.Value(1));
  const fadeOut = () => {
    Animated.timing(fadeAnimationPosition, {
      toValue: -200,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  };

  const fadeInswap = () => {
    Animated.timing(fadeAnimationswap, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  };
  const fadeOutswap = () => {
    Animated.timing(fadeAnimationswap, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };
  const fadeIn = () => {
    Animated.timing(fadeAnimationPosition, {
      toValue: -3,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };
  useImperativeHandle(ref, () => ({
    handleLogo: (ref: any) => {
      handleLogo(ref);
    },
  }));
  const handleLogo = (event: any) => {
    if (event.nativeEvent.contentOffset.y == 0) {
      fadeOut();
      fadeInswap();
    } else if (
      event.nativeEvent.contentOffset.y > 40 &&
      event.nativeEvent.contentOffset.y <= 300
    ) {
      fadeIn();
      fadeOutswap();
    } else {
    }
  };

  return (
    <>
      <RowHeader>
        <Colheader>
          <Row>
            <HeaderBackButton
              navigation={props.navigation}
              onPress={() => props.navigation.goBack()}
            />
          </Row>
        </Colheader>
        <Colheaderswap>
          <Row>
            <ImageLogoswap
              style={{opacity: fadeAnimationswap}}
              source={require('../assets/images/home2_icon.png')}
              resizeMode="cover"
            />
            <Animated.View
              style={{
                position: 'absolute',
                left: -18,
                transform: [{translateY: fadeAnimationPosition}],
              }}>
              <DBHText size={34} color="white">
                {props.title}
              </DBHText>
            </Animated.View>
          </Row>
        </Colheaderswap>
        {/* <Col>
          <FontAwesomeIcon
            icon={faBell}
            size={28}
            color="#fff"
            // style={{marginTop: 10, right: 3}}
          />
        </Col>
        <Col>
          <TouchableOpacity onPress={() => props.navigation.navigate('Hamberger')}>
            <FontAwesomeIcon icon={faSlidersH} size={28} color="#fff" />
          </TouchableOpacity>
        </Col> */}
      </RowHeader>
    </>
  );
});

const RowHeader = styled(Row)`
  background-color: #060c30;
  height: 48px;
  padding: 10px 0 0 10px;
  width: 100%;
`;
const TextBesideLogo = styled(Animated.Text)`
  color: #fff;
  font-size: 16px;
  margin-left: 10px;
  padding-top: 3px;
`;
const ImageLogo = styled(Animated.Image)`
  width: 31px;
  height: 31px;
`;
const ImageLogoswap = styled(Animated.Image)`
  width: 37px;
  height: 30px;
`;
const ImageCategory = styled(Animated.Image)`
  width: 39px;
  height: 39px;
  top: 0px;
`;

const Colheader = styled(Col)`
  width: 45%;
`;
const Colheaderswap = styled(Col)`
  width: 30%;
`;

export default HeaderAnimated;
