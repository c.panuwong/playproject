import React from 'react';
import {View, TouchableOpacity, Image, Alert} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

// @ts-ignore
import {Col, Row} from 'react-native-easy-grid';
// @ts-ignore
import styled from 'styled-components';
import {DBHText} from '../StyledText';
interface IHeaderResultProps {
  galaxy_description: any;
  navigation: any;
  galaxy_name: string;
  follow: number;
}
export default function HeaderResultPost({
  galaxy_description,
  navigation,
  galaxy_name,
  follow,
}: IHeaderResultProps) {
  const ClickHandle = (navigation: any) => {
    Alert.alert('Handle')
  };

  return (
    <RowResult>
      <Col style={{width: '15%'}}>
        <LinearGradientImageGalaxy
          colors={[
            'rgba(15, 103, 206, 1)',
            'rgba(51, 213, 43, 1)',
            'rgba(153, 255, 0, 1)',
          ]}>
          <TouchableOpacity onPress={() => ClickHandle(navigation)}>
            <ImageGalaxy
              source={require('../../assets/images/mockimage/girlgalaxy.png')}
              resizeMode="cover"
            />
          </TouchableOpacity>
        </LinearGradientImageGalaxy>
      </Col>
      <Col style={{width: '70%'}}>
          <RowNameWithFollowLeft>
            <TouchableOpacity onPress={() => ClickHandle(navigation)}>
              <DBHText size={22}>{galaxy_name}</DBHText>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => ClickHandle(navigation)} style={{marginLeft:10}}>
              {FollowCheck(follow)}
            </TouchableOpacity>
          </RowNameWithFollowLeft>
        <Row>
          <TouchableOpacity onPress={() => ClickHandle(navigation)}>
            <DBHText size={16} color="#B3B3B3">{galaxy_description}</DBHText>
          </TouchableOpacity>
        </Row>
      </Col>
      <Col>
        <TouchableOpacity onPress={() => ClickHandle(navigation)}>
          <Imagegalaxymorebtn
            source={require('../../assets/images/galaxymorebtn.png')}
            resizeMode="cover"
          />
        </TouchableOpacity>
      </Col>
    </RowResult>
  );
}
const FollowCheck = (follow: any) => {
  if (follow == 1) {
    return (
      <TextFollow>
        <ImageFollowing
          source={require('../../assets/images/followingicon.png')}
          resizeMode="cover"
        />
        &nbsp;Following
      </TextFollow>
    );
  } else {
    return <TextFollow>+ Follow</TextFollow>;
  }
};
const RowResult = styled(Row)`
  width: 100%;
  margin: 20px;
  margin-bottom:10px;
  margin-top:10px;
`;
const LinearGradientImageGalaxy = styled(LinearGradient)`
  padding: 2px;
  border-radius: 20px;
  width: 54px;
`;
const ImageGalaxy = styled(Image)`
  width: 50px;
  height: 50px;
  border-radius: 15px;
`;
const RowNameWithFollowLeft = styled(Row)`
  width: 100%;
`;
const Imagegalaxymorebtn = styled(Image)`
  width: 3px;
  height: 15px;
  margin-top: 10px;
  align-self: center;
`;
const TextFollow = styled(DBHText)`
  color: #99ff00;
  font-size: 22px;
`;
const ImageFollowing = styled(Image)`
  width: 10px;
  height: 10px;
`;

