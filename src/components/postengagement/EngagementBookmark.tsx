import React from 'react';
import {View, Text, Image} from 'react-native';
import styled from 'styled-components';
import {DBHText} from '../StyledText';

export default function EngagementBookmark() {
  return (
    <>
      <EngagementBoxView>
        <ImageBookmark
          source={require('../../assets/images/galaxyicon/bookmarkgalaxy.png')}
          resizeMode="cover"
        />
      </EngagementBoxView>
    </>
  );
}
const ImageBookmark = styled(Image)`
  width: 16px;
  height: 19px;
  margin-top: 5px;
  align-self: flex-end;
`;
const EngagementBoxView = styled(View)`
  flex: 0.3px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
