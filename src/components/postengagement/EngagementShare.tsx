import React from 'react';
import {View, Text, Image} from 'react-native';
import styled from 'styled-components';
import {DBHText} from '../StyledText';

export default function EngagementShare({amount}: {amount: string}) {
  return (
    <>
      <EngagementBoxView>
      <ImageGalaxyShare
                source={require('../../assets/images/galaxyicon/shareicongalaxy.png')}
                resizeMode="cover"
              />
        <DBHText size={20}>{amount}</DBHText>
      </EngagementBoxView>
    </>
  );
}
const ImageGalaxyShare = styled(Image)`
  width: 30px;
  height: 22.26px;
  margin-right: 5px;
  margin-left: 7px;
`;
const EngagementBoxView = styled(View)`
  flex: 0.3px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
