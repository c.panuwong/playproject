import React from 'react';
import {View, Text, Image} from 'react-native';
import styled from 'styled-components';
import {DBHText} from '../StyledText';

export default function EngagementComment({amount}: {amount: string}) {
  return (
    <>
      <EngagementBoxView>
        <ImageGalaxyComment
          source={require('../../assets/images/galaxyicon/commenticon.png')}
          resizeMode="cover"
        />
        <DBHText size={20}>{amount}</DBHText>
      </EngagementBoxView>
    </>
  );
}
const ImageGalaxyComment = styled(Image)`
  width: 25px;
  height: 23.33px;
  margin-right: 5px;
  margin-left: 7px;
`;
const EngagementBoxView = styled(View)`
  flex: 0.3px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
