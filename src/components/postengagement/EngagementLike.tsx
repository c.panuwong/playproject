import React from 'react';
import {View, Text, Image} from 'react-native';
import styled from 'styled-components';
import { DBHText } from '../StyledText';

export default function EngagementLike({amount}:{amount:string}) {
  return (
    <>
      <EngagementBoxView>
        <ImagegalaxyEngagement
          source={require('../../assets/images/galaxyicon/likepostingalaxy.png')}
          resizeMode="cover"
        />
        <DBHText size={20}>{amount}</DBHText>
      </EngagementBoxView>
    </>
  );
}
const ImagegalaxyEngagement = styled(Image)`
  width: 26.5px;
  height: 26px;
  margin-top: -5px;
  margin-right: 5px;
  margin-left: 7px;
`;
const EngagementBoxView = styled(View)`
  flex: 0.3px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;